<?php

namespace App\Http;

use App\Http\Routers\AdminRouter;
use App\Http\Routers\CashRouter;
use App\Http\Routers\ClientRouter;
use App\Http\Routers\CordovaRouter;
//use Illuminate\Foundation\Http\Kernel as HttpKernel;
use SebastiaanLuca\Router\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        //\App\Http\Middleware\VerifyCsrfToken::class,
        \GeneaLabs\LaravelCaffeine\Http\Middleware\LaravelCaffeineDripMiddleware::class,
        //\App\Http\Middleware\FixLocale::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.cordova' => \App\Http\Middleware\AuthenticateCordova::class,
        'auth.cash' => \App\Http\Middleware\AuthenticateCash::class,
        'auth.admin' => \App\Http\Middleware\AuthenticateAdmin::class,
        'admin' => \App\Http\Middleware\Admin::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'agent' => \App\Http\Middleware\AgentRedirect::class,
    ];

    /**
     * The routers to automatically map.
     *
     * @var array
     */
    protected $routers = [
        AdminRouter::class,
        ClientRouter::class,
        CordovaRouter::class,
        CashRouter::class,
    ];
}
