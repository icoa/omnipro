<?php

namespace App\Http\Middleware;
use Closure;

class FixLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        setlocale(LC_TIME,$this->getPhpLocale(\App::getLocale()));
        return $next($request);
    }

    function isWindows(){
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            return true;
        }
        return false;
    }

    function getPhpLocale($lang){
        $locale = 'en_US';
        $isWindows = $this->isWindows();
        switch($lang){
            case 'it':
                $locale = $isWindows ? 'ita' : 'it_IT.utf8';
                break;
            case 'en':
                $locale = $isWindows ? 'eng' : 'en_US.utf8';
                break;
        }
        return $locale;
    }
}
