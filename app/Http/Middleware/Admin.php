<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Cache;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $role = Cache::remember('cache.role',60,function(){
           return Auth::user()->role->code;
        });

        $routeName = $request->route()->getName();
        if($role == 'viewer' and (Str::contains($routeName,['edit','create','delete']))){
            if($request->ajax()){
                return response('Unauthorized.', 401);
            }
            \Flash::warning("Privilegi non sufficienti per l'azione scelta");
            return redirect()->route( str_replace(['edit','delete','create'],'index',$routeName) );
        }
        return $next($request);
    }
}
