<?php

namespace App\Http\Middleware;

use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class Demand
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $demand_id = Session::get('demand');
        $deliver_id = Session::get('deliver');
        $method = $request->getMethod();

        //\Utils::log(compact('routeName','demand_id','deliver_id','method'),__METHOD__);

        if($demand_id > 0 AND $routeName != 'cordova.demands.unblock'){
            if($method == 'GET' AND $routeName != 'cordova.demands.edit'){
                return redirect()->route('cordova.demands.edit',['id' => $demand_id]);
            }
        }

        if($deliver_id > 0 AND $routeName != 'cordova.delivers.unblock'){
            if($method == 'GET' AND $routeName != 'cordova.delivers.edit'){
                return redirect()->route('cordova.delivers.edit',['id' => $deliver_id]);
            }
        }

        return $next($request);
    }
}
