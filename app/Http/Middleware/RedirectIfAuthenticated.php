<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class RedirectIfAuthenticated
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->check()) {
            $user = $this->auth->user();
            $where = '/';

            if(get_class($user) == 'App\User'){
                $role = $user->role;
                if($role->theme == 'admin'){
                    $where = '/admin';
                }
                if($role->theme == 'cordova'){
                    $where = '/cordova';
                }
                if($role->theme == 'cash'){
                    $where = '/cash';
                }
                if($role->theme == 'default'){
                    $where = '/';
                }
            }

            return redirect($where);
        }

        return $next($request);
    }
}
