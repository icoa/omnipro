<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Session;
use Jenssegers\Agent\Agent;


class AgentRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!Session::has('emulator')){
            if($request->get('emulator') == 1){
                Session::put('emulator',1);
                return $next($request);
            }
        }else{
            return $next($request);
        }
        if(!Session::has('agent_check')){

            $agent = new Agent();
            $isDesktop = $agent->isDesktop();

            if($isDesktop){
                Session::put('agent_check','desktop');
                return redirect('/admin');
            }
        }else{
            if(Session::get('agent_check') == 'desktop'){
                return redirect('/admin');
            }
        }
        return $next($request);
    }
}
