<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class AuthenticateCash
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('cash/auth/login');
            }
        }else{
            $rolecode = $this->auth->user()->role->code;
            switch ($rolecode){
                case 'waiter':
                    return redirect()->to('/');
                    break;
                case 'cash':
                    //return redirect()->to('/cash');
                    break;
                case 'admin':
                    return redirect()->to('/admin');
                    break;
                case 'kitchen':
                    return redirect()->to('/cordova');
                    break;
            }
        }

        return $next($request);
    }
}
