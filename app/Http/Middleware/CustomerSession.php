<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Cache;

class CustomerSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Cache::remember('cache.user',5,function(){
           return Auth::user()->toArray();
        });

        //\Utils::log($user,__METHOD__);

        if(is_array($user)){
            $user = (object)$user;
            $redirect = false;
            $errorCode = null;
            if($user->active == 0){
                Auth::logout();
                $redirect = true;
                $errorCode = 'user_inactive';
            }

            if(strtotime($user->expire_at) <= time()){
                Auth::logout();
                $redirect = true;
                $errorCode = 'user_expired';
            }
            //\Utils::log(compact('errorCode','redirect'),__METHOD__);
            if($redirect){
                if($request->ajax()){
                    return response('Unauthorized.', 401);
                }else{
                    return redirect('/auth/login')->withErrors(['error' => trans('messages.'.$errorCode)]);
                }
            }
        }

        return $next($request);

    }
}
