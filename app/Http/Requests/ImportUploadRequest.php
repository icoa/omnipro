<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ImportUploadRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $default = [
            'excel' => 'required',

        ];

        return $default;
    }

    public function attributes(){
        return [
            'excel' => 'File excel',
        ];
    }
}
