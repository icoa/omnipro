<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserCreationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:255',
            'email' => 'email|unique:users',
            'username' => 'required|min:3|max:32|unique:users,username',
            'password' => 'required|min:3|confirmed',
            'password_confirmation' => 'required|min:3',
            'role_id' => 'required',
            'department_options' => 'required_if:role_id,4'
        ];
    }

    public function attributes(){
        return [
            'name' => 'Nome',
            'email' => 'Email',
            'username' => 'Nome utente',
            'password_confirmation' => 'Conferma password',
            'role_id' => 'Ruolo',
            'department_options' => 'Dipartimenti',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'department_options.required_if' => "Se il Ruolo è User è obbligatorio specificare almeno un dipartimento",
        ];
    }
}
