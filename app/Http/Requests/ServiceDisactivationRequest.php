<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServiceDisactivationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'service_id' => 'required',
            'discharge_date' => 'required|after_activation:'.$this->get('service_id').'|before_or_equal:'.date('d-m-Y'),
            'disable_date' => 'required|after_custom_date:'.$this->get('discharge_date').'|before_or_equal:'.date('d-m-Y'),
            'disactivation_notes' => 'required_with_dates:'.$this->get('discharge_date').','.$this->get('disable_date').'|required_if_not_date:discharge_date,'.date('d-m-Y').'|required_if_not_date:disable_date,'.date('d-m-Y'),
        ];
        return $rules;
    }

    public function attributes(){
        return [
            'service_id' => trans('service.service_id'),
            'discharge_date' => trans('service.discharge_date'),
            'disable_date' => trans('service.disable_date'),
            'disactivation_notes' => trans('service.disactivation_notes'),
        ];
    }

    public function messages(){
        return [

        ];
    }
}
