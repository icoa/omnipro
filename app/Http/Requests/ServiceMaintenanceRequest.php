<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServiceMaintenanceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'service_id' => 'required',
            'maintenance_date' => 'required|before_or_equal:'.date('d-m-Y'),
        ];
        return $rules;
    }

    public function attributes(){
        return [
            'service_id' => trans('service.service_id'),
            'maintenance_date' => trans('service.maintenance_date'),
        ];
    }

    public function messages(){
        return [

        ];
    }
}
