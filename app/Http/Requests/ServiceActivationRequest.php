<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServiceActivationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'patient_name' => 'required',
            'patient_lastname' => 'required',
            'active_date' => 'required_active_date:active_date,'.date('d-m-Y'),
            'product_id' => 'required',
            'sdo' => 'required',
            //'engine_serial' => 'required',
            'engine_serial' => 'required_if:stand_by,1',
            'department_id' => 'required',
            'braden_point1' => 'required',
            'braden_point2' => 'required',
            'braden_point3' => 'required',
            'braden_point4' => 'required',
            'braden_point5' => 'required',
            'braden_point6' => 'required',
            'injuries' => 'required',
            'stand_by' => 'required',
            //'activation_notes' => 'required_if_not_date:active_date,'.date('d-m-Y'),
        ];
        return $rules;
    }

    public function attributes(){
        return [
            'patient_name' => trans('service.patient_name'),
            'patient_lastname' => trans('service.patient_lastname'),
            'active_date' => trans('service.active_date'),
            'product_id' => trans('service.product_id'),
            'sdo' => trans('service.sdo'),
            'department_id' => trans('service.department_id'),
            'engine_serial' => trans('service.engine_serial'),
            'injuries' => trans('service.injuries'),
            'stand_by' => trans('service.stand_by'),
            'activation_notes' => trans('service.activation_notes'),
        ];
    }

    public function messages(){
        return [
            'braden_point1.required' => 'Devi selezionare almeno un valore',
            'braden_point2.required' => 'Devi selezionare almeno un valore',
            'braden_point3.required' => 'Devi selezionare almeno un valore',
            'braden_point4.required' => 'Devi selezionare almeno un valore',
            'braden_point5.required' => 'Devi selezionare almeno un valore',
            'braden_point6.required' => 'Devi selezionare almeno un valore',
            'engine_serial.required_if' => "Matrica motore è obbligatorio se è attivo Posizionamento stand-by",
        ];
    }
}
