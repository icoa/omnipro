<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServiceCloseRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'service_id' => 'required',
        ];
        return $rules;
    }

    public function attributes(){
        return [
            'service_id' => 'Tavolo/i',
        ];
    }

    public function messages(){
        return [
            'service_id.required' => 'Deve selezionare almeno un Tavolo',
        ];
    }
}
