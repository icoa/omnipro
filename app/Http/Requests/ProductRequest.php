<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'code' => 'required',
            'day_cost' => 'required',
            'department_options' => 'required'
        ];
    }

    public function attributes(){
        return [
            'name' => 'Nome modello',
            'code' => 'Codice modello',
            'day_cost' => 'Costo giornaliero',
            'department_options' => 'Reparti'
        ];
    }
}
