<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FormatRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|unique:formats',
        ];
        if($this->get('id') > 0){
            $rules['name'] = 'required|unique:formats,name,'.$this->get('id');
        }
        return $rules;
    }

    public function attributes(){
        return [
            'name' => 'Denominazione',
        ];
    }
}
