<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:255',
            'email' => 'email|unique:users,email,'.$this->get('id'),
            'username' => 'required|min:3|max:32|unique:users,username,'.$this->get('id'),
            'password' => 'min:3|confirmed',
            'password_confirmation' => 'required_with:password|min:3',
        ];
    }

    public function attributes(){
        return [
            'name' => 'Nome',
            'email' => 'Email',
            'username' => 'Nome utente',
            'password_confirmation' => 'Conferma password',
            'role_id' => 'Ruolo',
            'category_options' => 'Categorie di riferimento',
        ];
    }
}
