<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 14/05/2016
 * Time: 19:46
 */

namespace App\Http\Routers;

use SebastiaanLuca\Router\Routers\BaseRouter;
use SebastiaanLuca\Router\Routers\RouterInterface;
use Route;

class AdminRouter extends BaseRouter implements RouterInterface
{

    /**
     * The default controller namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers\Admin';
    protected $prefix = 'admin';

    /**
     * Map the routes.
     */
    public function map()
    {
        $this->router->group(['prefix' => 'admin', 'namespace' => $this->getNamespace()], function () {

            Route::group(['prefix' => 'auth'], function () {
                Route::get('login', 'AuthController@getLogin');
                Route::post('login', 'AuthController@postLogin');
                Route::get('logout', 'AuthController@getLogout');
            });

            $this->router->get('/', ['as' => $this->prefixed('home'), 'uses' => 'HomeController@getIndex']);
            $this->router->get('/search', ['as' => $this->prefixed('search'), 'uses' => 'HomeController@search']);

            //DEPARTMENTS
            $this->router->any('/departments/data', ['as' => $this->prefixed('departments.datatable'), 'uses' => 'DepartmentsController@datatable']);
            $this->router->resource('departments', 'DepartmentsController', [
                'except' => ['show']
            ]);

            //PRODUCTS
            $this->router->any('/products/data', ['as' => $this->prefixed('products.datatable'), 'uses' => 'ProductsController@datatable']);
            $this->router->resource('products', 'ProductsController', [
                'except' => ['show']
            ]);

            //SERVICES ACTIVATION
            $this->router->any('/services_activation/fetch/{services}', ['as' => $this->prefixed('services_activation.fetch'), 'uses' => 'ServicesActivationController@fetch']);
            $this->router->any('/services_activation/data', ['as' => $this->prefixed('services_activation.datatable'), 'uses' => 'ServicesActivationController@datatable']);
            $this->router->get('/services_activation/confirm/{services}', ['as' => $this->prefixed('services_activation.confirm'), 'uses' => 'ServicesActivationController@confirm']);
            $this->router->get('/services_activation/braden/{services}', ['as' => $this->prefixed('services_activation.braden'), 'uses' => 'ServicesActivationController@braden']);
            $this->router->patch('/services_activation/send/{services}', ['as' => $this->prefixed('services_activation.send'), 'uses' => 'ServicesActivationController@send']);
            $this->router->resource('services_activation', 'ServicesActivationController');

            //SERVICES DISACTIVATION
            $this->router->any('/services_disactivation/data', ['as' => $this->prefixed('services_disactivation.datatable'), 'uses' => 'ServicesDisactivationController@datatable']);
            $this->router->get('/services_disactivation/confirm/{services}', ['as' => $this->prefixed('services_disactivation.confirm'), 'uses' => 'ServicesDisactivationController@confirm']);
            $this->router->patch('/services_disactivation/send/{services}', ['as' => $this->prefixed('services_disactivation.send'), 'uses' => 'ServicesDisactivationController@send']);
            $this->router->resource('services_disactivation', 'ServicesDisactivationController');

            //SERVICES MAINTENANCE
            $this->router->any('/services_maintenance/data', ['as' => $this->prefixed('services_maintenance.datatable'), 'uses' => 'ServicesMaintenanceController@datatable']);
            $this->router->get('/services_maintenance/confirm/{services}', ['as' => $this->prefixed('services_maintenance.confirm'), 'uses' => 'ServicesMaintenanceController@confirm']);
            $this->router->patch('/services_maintenance/send/{services}', ['as' => $this->prefixed('services_maintenance.send'), 'uses' => 'ServicesMaintenanceController@send']);
            $this->router->resource('services_maintenance', 'ServicesMaintenanceController');

            //SERVICES TRANSFER
            $this->router->any('/services_transfer/data', ['as' => $this->prefixed('services_transfer.datatable'), 'uses' => 'ServicesTransferController@datatable']);
            $this->router->get('/services_transfer/confirm/{services}', ['as' => $this->prefixed('services_transfer.confirm'), 'uses' => 'ServicesTransferController@confirm']);
            $this->router->patch('/services_transfer/send/{services}', ['as' => $this->prefixed('services_transfer.send'), 'uses' => 'ServicesTransferController@send']);
            $this->router->resource('services_transfer', 'ServicesTransferController');

            //SERVICES AUTH
            $this->router->any('/services_authorization/data', ['as' => $this->prefixed('services_authorization.datatable'), 'uses' => 'ServicesAuthorizationController@datatable']);
            $this->router->resource('services_authorization', 'ServicesAuthorizationController',[
                'except' => ['create']
            ]);

            //SERVICES HISTORY
            $this->router->any('/services_history/excel', ['as' => $this->prefixed('services_history.excel'), 'uses' => 'ServicesHistoryController@excel']);
            $this->router->any('/services_history/data', ['as' => $this->prefixed('services_history.datatable'), 'uses' => 'ServicesHistoryController@datatable']);
            $this->router->resource('services_history', 'ServicesHistoryController',[
                'except' => ['create']
            ]);

            //USERS
            $this->router->any('/users/data', ['as' => $this->prefixed('users.datatable'), 'uses' => 'UsersController@datatable']);
            $this->router->get('/users/profile', ['as' => $this->prefixed('users.profile'), 'uses' => 'UsersController@profile']);
            $this->router->patch('/users/updateme', ['as' => $this->prefixed('users.updateme'), 'uses' => 'UsersController@updateme']);
            $this->router->resource('users', 'UsersController', [
                'except' => ['show']
            ]);

            //PERMISSIONS
            $this->router->resource('permissions', 'PermissionsController', [
                'except' => ['show','edit','create']
            ]);

            //CITIES
            $this->router->any('/cities/data', ['as' => $this->prefixed('cities.datatable'), 'uses' => 'CitiesController@datatable']);
            $this->router->resource('cities', 'CitiesController', [
                'except' => ['show']
            ]);

            //STAT
            $this->router->controller('stat', 'StatController',[
                'getCheck' => 'ajax.check',
                'getLoad' => 'ajax.load',
                'getState' => 'ajax.state',
            ]);

            //AJAX
            $this->router->controller('ajax', 'AjaxController',[
                'getCheck' => 'ajax.check',
                'getLoad' => 'ajax.load',
                'getState' => 'ajax.state',
            ]);

        });
    }

    public function prefixed($route){
        return $this->prefix.'.'.$route;
    }
}