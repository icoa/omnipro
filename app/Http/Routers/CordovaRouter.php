<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 14/05/2016
 * Time: 19:46
 */

namespace App\Http\Routers;

use SebastiaanLuca\Router\Routers\BaseRouter;
use SebastiaanLuca\Router\Routers\RouterInterface;
use Route;

class CordovaRouter extends BaseRouter implements RouterInterface
{

    /**
     * The default controller namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers\Cordova';
    protected $prefix = 'cordova';

    /**
     * Map the routes.
     */
    public function map()
    {
        $this->router->group(['prefix' => 'cordova', 'namespace' => $this->getNamespace()], function () {

            $this->router->get('/manifest.json', ['as' => $this->prefixed('manifest'), 'uses' => 'FileController@getManifest']);

            Route::group(['prefix' => 'auth'], function () {
                Route::get('login', 'AuthController@getLogin');
                Route::post('login', 'AuthController@postLogin');
                Route::get('logout', 'AuthController@getLogout');
            });

            $this->router->get('/', ['as' => $this->prefixed('home'), 'uses' => 'HomeController@getIndex']);
            $this->router->post('/done/{pivot_id}', ['as' => $this->prefixed('home.done'), 'uses' => 'HomeController@postDone']);
            $this->router->get('/order/{order_id}', ['as' => $this->prefixed('home.order'), 'uses' => 'HomeController@getOrder']);
            $this->router->get('/orders', ['as' => $this->prefixed('home.orders'), 'uses' => 'HomeController@orders']);

        });
    }

    public function prefixed($route)
    {
        return $this->prefix . '.' . $route;
    }
}