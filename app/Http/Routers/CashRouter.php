<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 14/05/2016
 * Time: 19:46
 */

namespace App\Http\Routers;

use SebastiaanLuca\Router\Routers\BaseRouter;
use SebastiaanLuca\Router\Routers\RouterInterface;
use Route;

class CashRouter extends BaseRouter implements RouterInterface
{

    /**
     * The default controller namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers\Cash';
    protected $prefix = 'cash';

    /**
     * Map the routes.
     */
    public function map()
    {
        $this->router->group(['prefix' => 'cash', 'namespace' => $this->getNamespace()], function () {

            $this->router->get('/manifest.json', ['as' => $this->prefixed('manifest'), 'uses' => 'FileController@getManifest']);

            Route::group(['prefix' => 'auth'], function () {
                Route::get('login', 'AuthController@getLogin');
                Route::post('login', 'AuthController@postLogin');
                Route::get('logout', 'AuthController@getLogout');
            });

            $this->router->post('services/decrement/{id}', ['as' => $this->prefixed('services.decrement'), 'uses' => 'ServicesController@decrement']);
            $this->router->resource('services', 'ServicesController');
            $this->router->get('/', ['as' => $this->prefixed('home'), 'uses' => 'HomeController@getIndex']);
            $this->router->post('/verified/{order_id}', ['as' => $this->prefixed('home.verified'), 'uses' => 'HomeController@postVerified']);
            $this->router->post('/accepted/{service_id}', ['as' => $this->prefixed('home.accepted'), 'uses' => 'HomeController@postAccepted']);
            $this->router->get('/checkout/{service_id}', ['as' => $this->prefixed('home.checkout'), 'uses' => 'HomeController@checkout']);
            //$this->router->get('/bottle/{bottle_id}', ['as' => $this->prefixed('home.bottle'), 'uses' => 'HomeController@bottle']);
            $this->router->get('/order/{order_id}', ['as' => $this->prefixed('home.order'), 'uses' => 'HomeController@order']);
            $this->router->get('/allservices', ['as' => $this->prefixed('home.allservices'), 'uses' => 'HomeController@services']);
            $this->router->get('/orders', ['as' => $this->prefixed('home.orders'), 'uses' => 'HomeController@orders']);
            $this->router->get('/checkouts', ['as' => $this->prefixed('home.checkouts'), 'uses' => 'HomeController@checkouts']);
            $this->router->get('/tables', ['as' => $this->prefixed('tables'), 'uses' => 'HomeController@tables']);


        });
    }

    public function prefixed($route)
    {
        return $this->prefix . '.' . $route;
    }
}