<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 14/05/2016
 * Time: 19:46
 */

namespace App\Http\Routers;

use SebastiaanLuca\Router\Routers\BaseRouter;
use SebastiaanLuca\Router\Routers\RouterInterface;
use Route;

class ClientRouter extends BaseRouter implements RouterInterface
{

    /**
     * The default controller namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers\Client';
    protected $prefix = '';

    /**
     * Map the routes.
     */
    public function map()
    {
        $this->router->group(['namespace' => $this->getNamespace(), 'middleware' => ['agent']], function () {

            $this->router->get('/manifest.json', ['as' => $this->prefixed('manifest'), 'uses' => 'FileController@getManifest']);

            Route::group(['prefix' => 'auth'], function () {
                Route::get('login', [
                    'as' => 'client.auth.login', 'uses' => 'AuthController@getLogin'
                ]);
                Route::post('login', 'AuthController@postLogin');
                Route::get('logout', 'AuthController@getLogout');
            });

            $this->router->get('/', ['as' => $this->prefixed('home'), 'uses' => 'HomeController@getIndex']);

            Route::get('services/barcode', ['as' => 'services.barcode', 'uses' => 'ServicesController@barcode']);
            Route::post('services/search', ['as' => 'services.search', 'uses' => 'ServicesController@search']);
            $this->router->resource('services', 'ServicesController');

            $this->router->controller('pages', 'PageController',[
                'getAbout' => $this->prefixed('pages.about'),
                'getHelp' => $this->prefixed('pages.help'),
            ]);

        });
    }

    public function prefixed($route)
    {
        return $route;
    }
}