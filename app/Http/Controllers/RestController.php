<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 15:45
 */

namespace App\Http\Controllers;

use Theme;

class RestController extends Controller
{

    protected $theme;

    function __construct(){
        $this->theme = Theme::uses('default')->layout('rest');
    }
}