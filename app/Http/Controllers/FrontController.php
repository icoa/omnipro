<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 15:45
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use ReflectionClass;
use Theme;
use Utils;
use Roomix;
use Illuminate\Support\Str;
use App\Http\Controllers\ReceiptController;

class FrontController extends Controller
{

    protected $themeName = 'default';
    protected $theme;
    protected $module;
    protected $entity;
    protected $entities;
    protected $model;
    protected $acl = true;

    function __construct()
    {
        $this->theme = Theme::uses('default')->layout('default');
        $this->theme->asset()->usePath()->add('fonts', 'css/fonts.css');

        $production = !config('app.debug');
        if ($production) {
            $this->theme->asset()->usePath()->add('main', Utils::elixir('css/all.css'));
            $this->theme->asset()->usePath()->add('app', Utils::elixir('js/all.js'));
        } else {
            $this->theme->asset()->usePath()->add('main', 'css/all.css');
            $this->theme->asset()->usePath()->add('app', 'js/all.js');
        }

        $this->middleware('auth', ['except' => ['getAjax', 'getWake']]);
    }

    protected function render($view, $data = [])
    {
        $data['module'] = $this->module;
        if(isset($this->entity)){
            $data['entity'] = $this->entity;
        }
        if(isset($this->entities)){
            $data['entities'] = $this->entities;
        }
        $data = array_merge($data,$this->defaults());
        return $this->theme->scope("{$this->module}.{$view}", $data)->render();
    }

    protected function defaults(){
        return [];
    }

    protected function redirect($view, $data = [])
    {
        return redirect()->route("{$this->themeName}.{$this->module}.{$view}", $data);
    }

    protected function asset($name, $path, $dependencies = [])
    {
        $this->theme->asset()->usePath()->add($name, $path, $dependencies);
    }

    public function index()
    {
        return $this->render('index');
    }

    public function show($model)
    {
        $present = null;
        if(method_exists($model,'lazyLoad')){
            $model->lazyLoad();
        }
        if(method_exists($model,'present')){
            $present = $model->present();
        }
        return $this->render('show',compact('model','present'));
    }

    public function edit($model)
    {
        $present = null;
        if(method_exists($model,'lazyLoad')){
            $model->lazyLoad();
        }
        if(method_exists($model,'present')){
            $present = $model->present();
        }
        return $this->render('edit', compact('model','present'));
    }

    public function create()
    {
        $model = null;
        return $this->render('create', compact('model'));
    }

    public function getDelete(Request $request, $module, $model)
    {
        $orderBy = $request->get("orderby", "name");
        $reflector = "App\\$model";
        $rows = $reflector::orderBy($orderBy)->get();
        $data = ['module' => $module, 'model' => $model, 'rows' => $rows];
        return $this->theme->scope("delete.index", $data)->render();
    }

    public function postDelete(Request $request, $module, $model)
    {
        $orderBy = $request->get("orderby", "name");
        $data = [
            "module" => $module,
            "model" => $model,
            "orderby" => $orderBy,
        ];

        if (!$request->has('items')) {
            Flash::error('Devi selezionare almeno un elemento');
            return redirect()->route("model.delete", $data);
        }

        $errors = [];
        $reflector = "App\\$model";
        $items = $request->get('items');

        foreach ($items as $item) {
            $element = $reflector::find($item);

            if ($element->canBeDeleted()) {
                $element->delete();
            } else {
                $errors[] = $element->name;
            }
        }

        if (count($errors) == 0) {
            Flash::success('Rimozione effettuata con successo!');
            return redirect()->route("$module.index");

        } else {
            if (count($errors) == 1) {
                $message = "L'elemento %s non pu&ograve; essere cancellato perchè è riferito da altre entità.";
            } else {
                $message = "Gli elementi %s non possono essere cancellati perchè sono riferiti da altre entità.";
            }
            Flash::error(sprintf($message, implode(', ', $errors)));

            return redirect()->route("model.delete", $data);

        }

    }


    public function getWake($module)
    {
        $response = ['success' => false];
        try {
            if(Str::startsWith($module,'receipt')){
                $tokens = explode('.',$module);
                $module = $tokens[0];
                $rows = ReceiptController::getRooms($tokens[1]);
            }else{
                $context = Roomix::getUserAccessibleServices($module);
                $rows = $context['services'];
            }
            $html = Theme::partial("rooms.$module",compact('rows','module'));
            $response['html'] = $html;
            $response['success'] = true;
        } catch (\Exception $e) {
            $response['error'] = $e->getMessage();
        }
        return response()->json($response);
    }

}