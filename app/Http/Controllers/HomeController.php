<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 16:09
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Theme;

class HomeController extends FrontController
{

    protected $acl = false;

    public function getIndex()
    {
        $role = \Roomix::userRole();

        $route = 'admin.home';

        \Utils::log($route,__METHOD__);


        return redirect()->route($route);


    }

    function redirectHome()
    {
        return redirect("/");
    }
}