<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 16:09
 */

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AreaRequest;
use App\Zone;
use App\Area;
use Utils;
use Roomix;
use Flash;
use Yajra\Datatables\Datatables;
use Input;
use App\Customer;

class PrintController extends AdminController
{
    protected $module = 'prints';
    protected $layout = 'print';

    function getAreas(){
        $rows = [];
        $zones = Zone::active()->orderByTranslation('name')->get();
        foreach($zones as $zone){
            $areas = Area::active()->where('zone_id',$zone->id)->orderByTranslation('name')->get();
            foreach($areas as $area){
                $area->zoneName = $zone->name;
                $rows[] = $area;
            }
        }
        return $this->render('areas',compact('rows'));
    }


    function getCustomer($id){
        $model = Customer::find($id);
        return $this->render('customer',compact('model'));
    }
}