<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use Laracasts\Flash\Flash;
use Yajra\Datatables\Datatables;
use Input;
use App\User;
use App\Role;
use App\Http\Requests\UserCreationRequest;
use App\Http\Requests\UserUpdationRequest;
use App\Http\Requests\ProfileRequest;
use Auth;


class UsersController extends AdminController
{
    protected $module = 'users';
    protected $entity = 'Utente';
    protected $entities = 'Utenti';

    function __construct()
    {
        parent::__construct();
        $versioning = config('roomix.asset_versioning');
        $this->asset('users', "js/components/users.$versioning.js");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::lists('name', 'id');

        return $this->render('create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserCreationRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreationRequest $request)
    {
        $user = User::create($request->all());

        Flash::success("Record <strong>{$this->entity}</strong> creato con successo");

        return $this->redirectByTask($request,$user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($user)
    {
        $roles = Role::lists('name', 'id');
        return $this->render('edit',['model' => $user, 'roles' => $roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdationRequest|Request $request
     * @param  int $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdationRequest $request, $user)
    {
        $data = ($request->has('password')) ? $request->all() : $request->except('password');

        $user->update($data);

        Flash::success("Record <strong>{$this->entity}</strong> aggiornato con successo");

        return $this->redirectByTask($request,$user);
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function datatable()
    {

        \Utils::watch();
        $rows = User::with(['role'])
            ->select(['users.*']);

        return Datatables::of($rows)
            ->filterColumn('users.departments', 'whereRaw', "users.id in (select user_id from department_user where department_id REGEXP ?) ", ["$1"])
            ->editColumn('users.name', function ($row) {
                $route = route('admin.users.edit',$row->id);
                return "<a href='$route'>$row->name</a>";
            })
            ->editColumn('created_at', function ($row) {
                return $row->created_at->format('d/m/Y H:i');
            })
            ->editColumn('updated_at', function ($row) {
                return $row->updated_at->format('d/m/Y H:i');
            })
            ->editColumn('last_login_at', function ($row) {
                return ($row->last_login_at) ? $row->last_login_at->format('d/m/Y H:i') : null;
            })
            ->editColumn('role.name', function ($row) {
                return "<strong>{$row->role->name}</strong>";
            })
            ->editColumn('departments', function ($row) {
                $label = $row->present()->departmentName;
                return "<strong>{$label}</strong>";
            })
            ->addColumn('actions', function ($row) {
                return $this->column_actions('users',$row);
            })
            ->make(true);
    }

    /**
     * Update the current user profile
     *
     * @param ProfileRequest|Request $request
     * @param  int $user
     * @return \Illuminate\Http\Response
     */
    public function updateme(ProfileRequest $request)
    {
        $user = Auth::user();
        $data = ($request->has('password')) ? $request->all() : $request->except('password');

        if($request->has('password')){
            $data['pasw_changed'] = 1;
        }

        $user->update($data);

        Flash::success("Il tuo Profilo è stato aggiornato con successo");

        return redirect()->route('admin.users.profile');
    }


    /**
     *
     */
    public function profile(){
        $model = Auth::user();
        return $this->render('profile',compact('model'));
    }

}
