<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 14/05/2016
 * Time: 19:53
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\FrontController as FrontController;

use Illuminate\Http\Request;
use Theme;
use Utils;
use Roomix;
use Illuminate\Support\Str;

class AdminController extends FrontController
{
    protected $themeName = 'admin';
    protected $layout = 'default';

    public function __construct()
    {
        $this->theme = Theme::uses($this->themeName)->layout($this->layout);
        Roomix::setTheme($this->theme);
        $versioning = config('roomix.asset_versioning');

        $production = !config('app.debug');
        if ($production) {
            $this->theme->asset()->usePath()->add('main', Utils::elixir('css/all.'.$versioning.'.css', $this->themeName));
            $this->theme->asset()->usePath()->add('app', Utils::elixir('js/all.'.$versioning.'.js', $this->themeName));
        } else {
            $this->theme->asset()->usePath()->add('main', 'css/all.'.$versioning.'.css');
            $this->theme->asset()->usePath()->add('app', 'js/all.'.$versioning.'.js');
        }
        $this->middleware('auth.admin');
        $this->middleware('admin');
        \App::setLocale(config('app.locale'));
    }

    protected function defaults()
    {
        $columnSizes = [
            'xs' => [12, 12],
            'sm' => [6, 6],
            'md' => [4, 8],
            'lg' => [3, 9]
        ];
        return compact('columnSizes');
    }

    protected function redirectByTask(Request $request, $model)
    {
        $task = $request->get('task', 'index');

        switch ($task) {
            case 'edit':
                $where = $this->redirect('edit', $model->id);
                break;

            default:
                $where = $this->redirect('index');
                break;
        }
        return $where;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $model
     * @return \Illuminate\Http\Response
     */
    public function destroy($model)
    {
        \Utils::watch();
        $canBeDeleted = true;
        if (method_exists($model, 'canBeDeleted')) {
            $canBeDeleted = $model->canBeDeleted();
        }
        if ($canBeDeleted) {
            $model->delete();
        }
        $data = ['success' => $canBeDeleted];
        return response()->json($data);
    }


    protected function column_actions($module, $model, $prepend = null)
    {
        $edit = route("admin.$module.edit", $model->id);
        $delete = route("admin.$module.destroy", $model->id);
        return "<div class='btn-group'>$prepend
                <a class='btn btn-sm btn-secondary' href='$edit' title='Modifica'><i class='font-icon font-icon-pencil'></i></a>
                <a class='btn btn-sm btn-danger app-action' data-action='onDeleteRecord' href='$delete' title='Rimuovi'><i class='font-icon font-icon-trash'></i></a>
                </div>";
    }


    protected function column_boolean($value){
        return $value == 1 ? '<span class="label label-success">SI</span>' : '<span class="label label-danger">NO</span>';
    }
}