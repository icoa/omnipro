<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\ServiceRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Service;
use Utils;
use Roomix;
use Flash;
use Yajra\Datatables\Datatables;
use Input;
use DB, Cache;
use Illuminate\Support\Str;
use Excel;

class ServicesHistoryController extends AdminController
{
    protected $module = 'services_history';
    protected $entity = 'Storico';
    protected $entities = 'Storico';
    protected $repository;


    function __construct(ServiceRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $versioning = config('roomix.asset_versioning');
        $this->asset('services_history', "js/components/services_history.$versioning.js");
        return $this->render('index');
    }


    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function datatable()
    {
        \Utils::watch();
        $columns = \DB::connection()->getSchemaBuilder()->getColumnListing("services");
        //\Utils::log($columns, __METHOD__);
        $except = ['signature'];
        $selects = array_diff($columns, $except);
        foreach ($selects as $key => $value) {
            $selects[$key] = 'services.'.$value;
        }
        $selects = array_merge($selects, [
            'products.name as product_name',
            'departments.code as department_code',
            'departments.name as department_name',
            'transfer.name as transfer_name',
            //'events.created_at as delivered_at',
        ]);
        //\Utils::log($selects, __METHOD__);
        $rows = Service::withoutStatus(Service::STATUS_CREATED)
            ->leftJoin('products', 'services.product_id', '=', 'products.id')
            ->leftJoin('departments', 'services.department_id', '=', 'departments.id')
            ->leftJoin('departments as transfer', 'services.transfer_id', '=', 'transfer.id')
            /*->leftJoin('service_events as events', function($join){
              $join->on('services.id', '=', 'events.service_id')->where('events.status', '=', \DB::raw(Service::STATUS_DELIVERED));
            })*/
            ->select($selects);

        $dtBuilder = Datatables::of($rows)
            ->editColumn('event_at', function ($row) {
                return $row->event_at->format('d/m/Y H:i');
            })
            ->editColumn('updated_at', function ($row) {
                return $row->updated_at->format('d/m/Y H:i');
            })
            ->editColumn('active_date', function ($row) {
                return $row->active_date->format('d/m/Y');
            })
            ->editColumn('type', function ($row) {
                return $row->present()->typeName;
            })
            ->editColumn('days', function ($row) {
                return $row->present()->daysText;
            })
            ->editColumn('cost', function ($row) {
                return $row->present()->costEuro;
            })
            ->editColumn('injuries', function ($row) {
                return $this->column_boolean($row->injuries);
            })
            ->editColumn('stand_by', function ($row) {
                return $this->column_boolean($row->stand_by);
            })
            ->editColumn('status', function ($row) {
                return $row->present()->statusHtml;
            })
            ->editColumn('confirmed_at', function ($row) {
                return $row->present()->whenConfirmed;
            })
            ->editColumn('sent_at', function ($row) {
                return $row->present()->whenSent;
            })
            ->editColumn('sent_activation_at', function ($row) {
                return $row->present()->whenSentActivationAt;
            })
            ->editColumn('mt_requested_at', function ($row) {
                return $row->present()->maintenanceRequestedAtDate;
            })
            ->editColumn('delivered_at', function ($row) {
                return $row->present()->whenDelivered;
            })
            ->editColumn('disable_date', function ($row) {
                return $row->present()->disableDate;
            })
            ->editColumn('transfer_date', function ($row) {
                return $row->present()->transferDate;
            })
            ->editColumn('cost_starts_at', function ($row) {
                return $row->present()->whenCostStartsAt;
            })
            ->editColumn('cost_ends_at', function ($row) {
                return $row->present()->whenCostEndsAt;
            })
            ->editColumn('discount', function ($row) {
                return $row->present()->discount;
            })
            ->addColumn('actions', function ($row) {
                return $this->column_actions('services_history', $row);
            });

        $results = $dtBuilder->make(true);
        $sql = $dtBuilder->getQueryBuilder()->toSql();
        $bindings = $dtBuilder->getQueryBuilder()->getBindings();

        foreach ($bindings as $binding) {
            $binding = \DB::connection()->getPdo()->quote($binding);
            $sql = preg_replace('/\?/', $binding, $sql, 1);
        }
        $limitPost = strpos($sql, 'limit');
        $sql = substr($sql, 0, $limitPost);

        //Utils::log($sql,__METHOD__);
        Cache::forever('excel_query', $sql);

        return $results;
    }


    function excel()
    {
        $query = Cache::get('excel_query');
        $filename = Str::slug('OmniPro - Storico richieste - ' . date('dmY_Hi'));
        $table = $this->createTableFromQuery($query);
        $table = utf8_decode($table);
        return \Response::make($table)
            ->header('Content-Disposition', "attachment; filename=$filename.xls")
            ->header('Pragma', "no-cache")
            ->header('Expires', "0")
            ->header('Content-Type', 'application/vnd.ms-excel');
        return $table;
        Excel::create($filename, function ($excel) use ($query) {
            $table = $this->createTableFromQuery($query);
            $excel->sheet('Richieste', function ($sheet) use ($table) {
                $sheet->loadView('excel.table', compact('table'));
            });
        })->download('xlsx');
        return $query;
    }


    protected function createTableFromQuery($query)
    {
        \Utils::log($query, __METHOD__);
        $rows = \DB::select($query);

        foreach ($rows as $row) {
            $row->active_date = $this->revertDate($row->active_date);
            $row->discharge_date = $this->revertDate($row->discharge_date);
            $row->disable_date = $this->revertDate($row->disable_date);
            $row->maintenance_date = $this->revertDate($row->maintenance_date);
            $row->transfer_date = $this->revertDate($row->transfer_date);

            $row->eloquent = new Service((array)$row);
        }

        return \Theme::partial('services.excel.history', compact('rows'));
    }


    protected function revertDate($date)
    {
        if (is_null($date))
            return $date;

        return Carbon::createFromFormat('Y-m-d', $date);
    }


    protected function column_actions($module, $model, $prepend = null)
    {
        $show = route("admin.$module.show", $model->id);

        $html = "<div class='btn-group'>";
        $html .= "<a class='btn btn-sm btn-secondary' href='$show' title='Dettagli'><i class='glyphicon glyphicon-pencil'></i></a>";
        $html .= '</div>';
        return $html;
    }


    protected function redirectByTask(Request $request, $model)
    {
        $task = $request->get('task', 'index');

        switch ($task) {
            case 'edit':
                $where = $this->redirect('edit', $model->id);
                break;

            case 'confirm':
                $where = $this->redirect('confirm', $model->id);
                break;

            default:
                $where = $this->redirect('index');
                break;
        }
        return $where;
    }
}