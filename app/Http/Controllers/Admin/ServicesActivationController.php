<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ServiceActivationRequest as ServiceRequest;
use App\Repositories\ServiceRepository;
use Illuminate\Http\Request;
use App\Service;
use Utils;
use Roomix;
use Flash;
use Yajra\Datatables\Datatables;
use Input;
use DB;

class ServicesActivationController extends AdminController
{
    protected $module = 'services_activation';
    protected $entity = 'Richiesta attivazione';
    protected $entities = 'Richieste attivazione';
    protected $repository;


    function __construct(ServiceRepository $repository)
    {
        $this->repository = $repository;
        $versioning = config('roomix.asset_versioning');
        parent::__construct();
        $this->asset('services_activation', "js/components/services_activation.$versioning.js");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->render('index');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param ServiceRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceRequest $request)
    {
        $model = $this->repository->createActivation($request);

        //Flash::success("Record <strong>{$this->entity}</strong> creato con successo");

        return $this->redirectByTask($request, $model);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param ServiceRequest|Request $request
     * @param  int $model
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceRequest $request, $model)
    {
        $this->repository->updateActivation($request,$model);

        //Flash::success("Record <strong>{$this->entity}</strong> aggiornato con successo");

        return $this->redirectByTask($request, $model);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request|Request $request
     * @param  int $model
     * @return \Illuminate\Http\Response
     */
    public function send(Request $request, $model)
    {
        $this->repository->sendActivation($request,$model);

        Flash::success("La richiesta è stata inviata con successo");

        return $this->redirect('index');
    }


    /**
     * Show the confirm view.
     *
     * @param  int $model
     * @return \Illuminate\Http\Response
     */
    public function confirm($model)
    {
        $present = null;
        if(method_exists($model,'lazyLoad')){
            $model->lazyLoad();
        }
        if(method_exists($model,'present')){
            $present = $model->present();
        }
        return $this->render('confirm', compact('model','present'));
    }


    /**
     * Show the braden PDF.
     *
     * @param  int $model
     * @return \Illuminate\Http\Response
     */
    public function braden($model)
    {
        return $this->repository->bradenPdf($model);
    }



    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function datatable()
    {

        $rows = Service::with('product','department')
            ->withoutStatus(Service::STATUS_CREATED)
            ->user()
            ->withType(Service::TYPE_ACTIVATION)
            ->leftJoin('products','services.product_id', '=','products.id')
            ->leftJoin('departments','services.department_id', '=','departments.id')
            ->select(
                'services.*',
                'products.name as product_name',
                'departments.code as department_code',
                'departments.name as department_name'
            );

        return Datatables::of($rows)
            ->editColumn('created_at', function ($row) {
                return $row->created_at->format('d/m/Y H:i');
            })
            ->editColumn('sent_at', function ($row) {
                return $row->sent_at->format('d/m/Y H:i');
            })
            ->editColumn('updated_at', function ($row) {
                return $row->updated_at->format('d/m/Y H:i');
            })
            ->editColumn('active_date', function ($row) {
                return $row->active_date->format('d/m/Y');
            })
            ->editColumn('injuries', function ($row) {
                return $this->column_boolean($row->injuries);
            })
            ->editColumn('stand_by', function ($row) {
                return $this->column_boolean($row->stand_by);
            })
            ->editColumn('status', function ($row) {
                return $row->present()->statusHtml;
            })
            ->addColumn('actions', function ($row) {
                return $this->column_actions('services_activation',$row);
            })
            ->make(true);
    }


    protected function column_actions($module, $model, $prepend = null)
    {
        $edit = route("admin.$module.show", $model->id);
        return "<div class='btn-group'>$prepend
                <a class='btn btn-sm btn-secondary' href='$edit' title='Dettagli'><i class='font-icon font-icon-pencil'></i></a>                
                </div>";
    }


    protected function redirectByTask(Request $request, $model)
    {
        $task = $request->get('task', 'index');

        switch ($task) {
            case 'edit':
                $where = $this->redirect('edit', $model->id);
                break;

            case 'confirm':
                $where = $this->redirect('confirm', $model->id);
                break;

            default:
                $where = $this->redirect('index');
                break;
        }
        return $where;
    }


    public function fetch(Service $service){
        $success = true;
        $service->date = $service->active_date->format('d-m-Y');
        $service->date_sql = $service->active_date->format('Y-m-d');
        $obj = $service->toArray();
        return response()->json(compact('success','obj'));
    }
}