<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DepartmentRequest;
use App\Department;
use Utils;
use Roomix;
use Flash;
use Yajra\Datatables\Datatables;
use Input;
use Illuminate\Http\Request;

class DepartmentsController extends AdminController
{
    protected $module = 'departments';
    protected $entity = 'Reparto';
    protected $entities = 'Reparti';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $versioning = config('roomix.asset_versioning');
        $this->asset('departments', "js/components/departments.$versioning.js");
        return $this->render('index');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param DepartmentRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentRequest $request)
    {
        $model = Department::create($request->all());

        $this->updateRelations($request,$model);

        Flash::success("Record <strong>{$this->entity}</strong> creato con successo");

        return $this->redirectByTask($request, $model);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param DepartmentRequest|Request $request
     * @param  int $model
     * @return \Illuminate\Http\Response
     */
    public function update(DepartmentRequest $request, $model)
    {
        $data = $request->all();

        $model->update($data);

        $this->updateRelations($request,$model);

        Flash::success("Record <strong>{$this->entity}</strong> aggiornato con successo");

        return $this->redirectByTask($request, $model);
    }


    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function datatable()
    {
        \Utils::watch();
        $rows = Department::with('users');

        return Datatables::of($rows)
            ->editColumn('name', function ($row) {
                $route = route('admin.departments.edit', $row->id);
                $html = $row->present()->productsList();
                return "<a href='$route'>$row->name</a><p>$html</p>";
            })
            ->editColumn('users', function ($row) {
                return $row->users->count();
            })
            ->editColumn('active', function ($row) {
                return $row->active == 1 ? "<label class='label label-success'>Attivo</label>" : "<label class='label label-danger'>Disattivo</label>";
            })
            ->editColumn('created_at', function ($row) {
                return $row->created_at->format('d/m/Y H:i');
            })
            ->editColumn('updated_at', function ($row) {
                return $row->updated_at->format('d/m/Y H:i');
            })
            ->addColumn('actions', function ($row) {
                return $this->column_actions('departments', $row);
            })
            ->make(true);
    }

    /**
     * Update the main relations.
     *
     * @param Request $request
     * @param  Department $model
     * @return void
     */
    public function updateRelations(Request $request, $model)
    {
        $options = $request->get('product_options');
        $model->products()->sync($options);
    }
}