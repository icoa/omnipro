<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 16:09
 */

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CityRequest;
use App\City;
use Utils;
use Roomix;
use Flash;
use Yajra\Datatables\Datatables;
use Input;

class CitiesController extends AdminController
{
    protected $module = 'cities';
    protected $entity = 'Comune';
    protected $entities = 'Comuni';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $versioning = config('roomix.asset_versioning');
        $this->asset('cities', "js/components/cities.$versioning.js");
        return $this->render('index');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param CityRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CityRequest $request)
    {
        $user = City::create($request->all());

        Flash::success("Record <strong>{$this->entity}</strong> creato con successo");

        return $this->redirectByTask($request, $user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param CityRequest|Request $request
     * @param  int $model
     * @return \Illuminate\Http\Response
     */
    public function update(CityRequest $request, $model)
    {
        $data = $request->all();

        $model->update($data);

        Flash::success("Record <strong>{$this->entity}</strong> aggiornato con successo");

        return $this->redirectByTask($request, $model);
    }


    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function datatable()
    {

        $rows = City::with(['state','state.district']);

        return Datatables::of($rows)
            ->editColumn('name', function ($row) {
                $route = route('admin.cities.edit', $row->id);
                return "<a href='$route'>$row->name</a>";
            })
            ->editColumn('state_id', function ($row) {
                return isset($row->state) ? $row->state->name : null;
            })
            ->editColumn('district', function ($row) {
                return (isset($row->state) AND isset($row->state->district)) ? $row->state->district->name : null;
            })
            ->editColumn('created_at', function ($row) {
                return $row->created_at->format('d/m/Y H:i');
            })
            ->editColumn('updated_at', function ($row) {
                return $row->updated_at->format('d/m/Y H:i');
            })
            ->addColumn('actions', function ($row) {
                return $this->column_actions('cities',$row);
            })
            ->make(true);
    }
}