<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 16:09
 */

namespace App\Http\Controllers\Admin;

use App\Lib\Report;

class StatController extends AdminController
{
    protected $module = 'prints';
    protected $layout = 'print';

    function getAll(){

        $periods = [
            ['start' => '2016-10-27', 'end' => '2016-10-31'],
            ['start' => '2016-10-31', 'end' => '2016-11-30'],
            ['start' => '2016-11-30', 'end' => '2016-12-31'],
        ];

        $overall = ['start' => $periods[0]['start'], 'end' => $periods[2]['end']];
        $from = $overall['start'];
        $to = $overall['end'];

        $report = new Report($from,$to);
        $html = '<h1>Report generale</h1>';
        $html .= $report->overall();
        $html .= '<p><br></p><h1>Report mensile</h1>';

        foreach($periods as $period){
            $from = $period['start'];
            $to = $period['end'];

            $html .= $report->monthly($from,$to);
            $html .= '<hr>';
        }

        return $this->render('default',['content' => $html]);
    }
}