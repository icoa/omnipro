<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 16:09
 */

namespace App\Http\Controllers\Admin;


use Utils;
use Roomix;
use Flash;
use Session;
use Auth;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\Component;
use App\Role;
use Illuminate\Http\Request;

class PermissionsController extends AdminController
{

    protected $module = 'permissions';
    protected $entity = 'Permesso';
    protected $entities = 'Permessi';



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = Component::where('active',1)->orderBy('name')->get();
        $roles = Role::with('components')->where('code','<>','waiter')->orderBy('name')->get();
        return $this->render('index',compact('rows','roles'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Utils::log($request->all());
        $component_role = $request->get('component_role',[]);
        foreach($component_role as $role_id => $components){
            $role = Role::find($role_id);
            $role->components()->sync($components);
        }

        Flash::success("Permessi aggiornati con successo");

        return $this->redirect('index');
    }

}