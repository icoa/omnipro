<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 16:09
 */

namespace App\Http\Controllers\Admin;

use Helper;
use Form;
use Auth;
use Input;
use App\State;
use App\Lib\NotificationManager;


class AjaxController extends AdminController
{

    protected $module = 'ajax';

    function getCheck(){

        $passedCounter = (int)\Input::get('count');

        $data = NotificationManager::check($passedCounter);

        return response()->json(compact('data'));
    }

    function getLoad(){
        $user = Auth::user();
        $type = Input::get('type');
        $module = Input::get('name');
        $html = null;
        $success = false;

        switch($module){
            case 'dashboard':
                $html = $this->theme->partial('lists.dashboard',HomeController::data());
                break;
        }
        $success = !is_null($html);

        return response()->json(compact('success','html'));
    }


    function getState($state_id){
        \Utils::watch();
        $model = State::with('district','cities')->where('code',$state_id)->first();
        $response = null;
        if($model){
            $response = $model->toArray();
            $response['cities'] = '';
            foreach($model->cities as $city){
                $response['cities'] .= "<option value='$city->id'>$city->name</option>";
            }
        }
        return response()->json($response);
    }


}