<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 16:09
 */

namespace App\Http\Controllers\Admin;


use Auth;
use App\User;

class HomeController extends AdminController{

    protected $acl = false;

    public function getIndex()
    {
        return $this->render('home.index', HomeController::data());
    }

    function redirectHome(){
        return redirect("/");
    }

    static function data(){
        $user = Auth::user();

        return [];
    }

    function search(){

    }
}