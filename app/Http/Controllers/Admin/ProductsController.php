<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductRequest;
use App\Product;
use Utils;
use Roomix;
use Flash;
use Yajra\Datatables\Datatables;
use Input;
use Illuminate\Http\Request;

class ProductsController extends AdminController
{
    protected $module = 'products';
    protected $entity = 'Modello';
    protected $entities = 'Modelli';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $versioning = config('roomix.asset_versioning');
        $this->asset('products', "js/components/products.$versioning.js");
        return $this->render('index');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param ProductRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $model = Product::create($request->all());

        $this->updateRelations($request, $model);

        Flash::success("Record <strong>{$this->entity}</strong> creato con successo");

        return $this->redirectByTask($request, $model);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param ProductRequest|Request $request
     * @param  int $model
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $model)
    {
        $data = $request->all();

        $model->update($data);

        $this->updateRelations($request, $model);

        Flash::success("Record <strong>{$this->entity}</strong> aggiornato con successo");

        return $this->redirectByTask($request, $model);
    }


    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function datatable()
    {

        $rows = Product::where('id', '>', 0);

        return Datatables::of($rows)
            ->editColumn('name', function ($row) {
                $route = route('admin.products.edit', $row->id);
                return "<a href='$route'>$row->name</a>";
            })
            ->editColumn('active', function ($row) {
                return $row->active == 1 ? "<label class='label label-success'>Attivo</label>" : "<label class='label label-danger'>Disattivo</label>";
            })
            ->editColumn('day_cost', function ($row) {
                return $row->present()->cost();
            })
            ->editColumn('created_at', function ($row) {
                return $row->created_at->format('d/m/Y H:i');
            })
            ->editColumn('updated_at', function ($row) {
                return $row->updated_at->format('d/m/Y H:i');
            })
            ->addColumn('actions', function ($row) {
                return $this->column_actions('products', $row);
            })
            ->make(true);
    }


    /**
     * Update the main relations.
     *
     * @param Request $request
     * @param  Product $model
     * @return void
     */
    public function updateRelations(Request $request, $model)
    {
        $options = $request->get('department_options');
        $model->departments()->sync($options);
    }
}