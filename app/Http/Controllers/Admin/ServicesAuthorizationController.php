<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ServiceAuthorizationRequest as ServiceRequest;
use App\Repositories\ServiceRepository;
use Illuminate\Http\Request;
use App\Service;
use Utils;
use Roomix;
use Flash;
use Yajra\Datatables\Datatables;
use Input;
use DB;

class ServicesAuthorizationController extends AdminController
{
    protected $module = 'services_authorization';
    protected $entity = 'Autorizzazione servizio';
    protected $entities = 'Autorizzazioni servizi';
    protected $repository;


    function __construct(ServiceRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $versioning = config('roomix.asset_versioning');
        $this->asset('services_authorization', "js/components/services_authorization.$versioning.js");
        return $this->render('index');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param ServiceRequest|Request $request
     * @param  int $model
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceRequest $request, $model)
    {
        $this->repository->authorize($request, $model);

        Flash::success("Record <strong>{$this->entity}</strong> aggiornato con successo");

        return $this->redirectByTask($request, $model);
    }





    public function edit($model)
    {
        $present = null;
        if(method_exists($model,'lazyLoad')){
            $model->lazyLoad();
        }
        if(method_exists($model,'present')){
            $present = $model->present();
        }
        $action = request()->get('action','approve');
        return $this->render('edit', compact('model','present','action'));
    }


    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function datatable()
    {

        $rows = Service::with('product','department','transfer_department')
            ->withoutStatus(Service::STATUS_CREATED)
            ->leftJoin('products','services.product_id', '=','products.id')
            ->leftJoin('departments','services.department_id', '=','departments.id')
            ->leftJoin('departments as transfer','services.transfer_id', '=','transfer.id')
            ->select('services.*', 'products.name as product_name', 'departments.name as department_name', 'departments.code as department_code', 'transfer.name as transfer_name');

        return Datatables::of($rows)
            ->editColumn('event_at', function ($row) {
                return $row->event_at->format('d/m/Y H:i');
            })
            ->editColumn('updated_at', function ($row) {
                return $row->updated_at->format('d/m/Y H:i');
            })
            ->editColumn('sent_at', function ($row) {
                return $row->sent_at->format('d/m/Y');
            })
            ->editColumn('active_date', function ($row) {
                return $row->active_date->format('d/m/Y');
            })
            ->editColumn('type', function ($row) {
                return $row->present()->typeName;
            })
            ->editColumn('injuries', function ($row) {
                return $this->column_boolean($row->injuries);
            })
            ->editColumn('stand_by', function ($row) {
                return $this->column_boolean($row->stand_by);
            })
            ->editColumn('status', function ($row) {
                return $row->present()->statusHtml;
            })
            ->addColumn('actions', function ($row) {
                return $this->column_actions('services_authorization',$row);
            })
            ->make(true);
    }


    protected function column_actions($module, $model, $prepend = null)
    {
        $show = route("admin.$module.show", $model->id);
        $confirm = route("admin.$module.edit", $model->id).'?action=approve';
        $reject = route("admin.$module.edit", $model->id).'?action=reject';
        $html = "<div class='btn-group'>";
        $html .= "<a class='btn btn-sm btn-secondary' href='$show' title='Dettagli'><i class='glyphicon glyphicon-pencil'></i></a>";
        if($model->canReject())
            $html .= "<a class='btn btn-sm btn-danger' href='$reject' title='Rifiuta'><i class='glyphicon glyphicon-thumbs-down'></i></a>";
        if($model->canApprove())
            $html .= "<a class='btn btn-sm btn-success' href='$confirm' title='Approva'><i class='glyphicon glyphicon-thumbs-up'></i></a>";

        $html .= '</div>';
        return $html;
    }


    protected function redirectByTask(Request $request, $model)
    {
        $task = $request->get('task', 'index');

        switch ($task) {
            case 'edit':
                $where = $this->redirect('edit', $model->id);
                break;

            case 'confirm':
                $where = $this->redirect('confirm', $model->id);
                break;

            default:
                $where = $this->redirect('index');
                break;
        }
        return $where;
    }
}