<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 15:45
 */

namespace App\Http\Controllers;

use DB;


class ImportController extends Controller
{

    function __construct(){
        $this->_style();
    }

    public function getIndex()
    {
        $this->_print( "Running import..." );
        /*$rows = DB::table('import')->get();
        $this->_print($rows);*/
        //$this->_update();
        //$this->_related();
        $this->_reset();
    }

    private function _update(){
        $rows = DB::table('herbs')->get();
        foreach($rows as $row){
            $tokens = explode('_',$row->probability);
            $min = null;
            $max = null;
            if(count($tokens) == 2){
                $min = $tokens[0];
                $max = $tokens[1];
            }else{
                $min = $row->probability;
                $max = $row->probability;
            }
            $min = $this->_hundred($min);
            $max = $this->_hundred($max);
            DB::table('herbs')->where('id',$row->id)->update(['min_prob' => $min, 'max_prob' => $max]);
        }
    }

    private function _related(){
        $rows = DB::table('herbs')->get();
        $zones = [];
        $application_types = [];
        $application_uses = [];

        foreach($rows as $row){
            $zones[] = $row->zone;
            $application_types[] = $row->application_type;
            $application_uses[] = $row->application_use;
        }

        $zones = array_unique($zones);
        $application_types = array_unique($application_types);
        $application_uses = array_unique($application_uses);

        foreach($zones as $zone){
            $zone_id = DB::table('zones')->insertGetId(['name' => ucfirst(trim($zone))]);
            DB::table('herbs')->where('zone',$zone)->update(['zone_id' => $zone_id]);
        }

        foreach($application_types as $type){
            $type_id = DB::table('application_types')->insertGetId(['name' => ucfirst(trim($type))]);
            DB::table('herbs')->where('application_type',$type)->update(['application_type_id' => $type_id]);
        }

        foreach($application_uses as $use){
            $use_id = DB::table('application_uses')->insertGetId(['name' => ucfirst(trim($use))]);
            DB::table('herbs')->where('application_use',$use)->update(['application_use_id' => $use_id]);
        }
    }

    private function _hundred($txt){
        if($txt == '00')return '100';
        return $txt;
    }

    private function _reset(){
        $str = 'test';
        $password = \Hash::make($str);
        \DB::table('users')->update(compact('password'));
        $this->_print("Password resetted");
    }

    private function _print($txt){
        if(is_object($txt) OR is_array($txt)){
            $txt = print_r($txt,1);
        }
        echo '<pre>'.$txt.'</pre>'.PHP_EOL;
    }

    private function _style(){
        echo '<style>
html, body{
background: #000; font-family: "Courier New", Courier, monospace; color:#fff; padding: 10px; margin:0;
}
</style>';
    }
}