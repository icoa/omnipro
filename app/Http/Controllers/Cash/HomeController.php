<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 16:09
 */

namespace App\Http\Controllers\Cash;

use App\Events\BottleWasDone;
use App\Http\Controllers\Cordova\CordovaController;
use App\Repositories\OrderRepository;
use Carbon\Carbon;
use DB;
use Theme;
use App\Repositories\CashRepository;
use App\Order;

class HomeController extends CashController{

    protected $acl = true;
    protected $repository;
    protected $orderRepository;

    public function __construct(CashRepository $repository, OrderRepository $orderRepository)
    {
        $this->repository = $repository;
        $this->orderRepository = $orderRepository;
        parent::__construct();
    }

    public function getIndex()
    {
        \Utils::watch();
        $view = [
            'rows' => $this->repository->all(),
            'checkouts' => $this->repository->checkouts(),
            //'services' => $this->repository->services(),
            //'tables' => $this->repository->freeTables(),
        ];
        return $this->render('home.index',$view);
    }
/*
    function redirectHome(){
        return redirect("/");
    }*/

    public function postVerified($order_id){
        $order = Order::find($order_id);
        if($order)
            $this->orderRepository->setVerified($order);

        $response = ['success' => true, 'id' => $order_id, 'message' => 'Prodotto chiuso con successo'];
        return response()->json($response);
    }

    public function postBottleVerified($pivot_id){
        $this->repository->verifyBottle($pivot_id);
        $response = ['success' => true, 'id' => $pivot_id, 'message' => 'Prodotto chiuso con successo'];
        return response()->json($response);
    }

    public function postAccepted($service_id){
        return $this->postClosed($service_id);
        /*
        $this->repository->acceptService($service_id);
        $response = ['success' => true, 'id' => $service_id, 'message' => 'Tavolo chiuso con successo'];
        return response()->json($response);*/
    }

    public function postClosed($service_id){
        $this->repository->closeService($service_id);
        $response = ['success' => true, 'id' => $service_id, 'message' => 'Tavolo chiuso con successo'];
        return response()->json($response);
    }

    public function services(){
        $view = [
            'services' => $this->repository->services(),
            'tables' => $this->repository->freeTables(),
        ];
        $success = true;
        $html =  Theme::partial('lists.services',$view);

        return response()->json(compact('success','html'));
    }

    public function checkout($service_id){
        $checkout = $this->repository->checkout($service_id);
        $success = true;
        $html =  Theme::partial('lists.checkouts',['checkouts' => [$checkout]]);
        $message = "Nuova richiesta di conto per ".$checkout->present()->fullname;

        return response()->json(compact('success','html','message'));
    }

    public function bottle($pivot_id){
        $pivot = $this->repository->pivot($pivot_id);
        $success = true;
        $html =  Theme::partial('lists.bottles',['rows' => [$pivot]]);
        $message = "Prodotto {$pivot->bottle->name} ordinato per {$pivot->order->service->name}";

        return response()->json(compact('success','html','message'));
    }

    public function order($order_id){
        $order = $this->repository->order($order_id);
        $success = true;
        $html =  Theme::partial('lists.orders',['rows' => [$order]]);
        $message = "&Egrave; stato richiesto un Nuovo ordine";

        return response()->json(compact('success','html','message'));
    }
/*
    public function negativeOrder($order_id){
        $bottles = $this->repository->negativeBottlesForOrder($order_id);
        $success = true;
        $html =  Theme::partial('lists.bottles',['rows' => $bottles]);
        $message = "Nuova richiesta rimozione prodotti";

        return response()->json(compact('success','html','message'));
    }
*/

    public function orders(){
        $rows = $this->repository->all();
        $success = true;
        $html =  Theme::partial('lists.orders',['rows' => $rows]);

        return response()->json(compact('success','html'));
    }

    public function bottles(){
        $rows = $this->repository->all();
        $success = true;
        $html =  Theme::partial('lists.bottles',['rows' => $rows]);

        return response()->json(compact('success','html'));
    }

    public function checkouts(){
        $rows = $this->repository->checkouts();
        $success = true;
        $html =  Theme::partial('lists.checkouts',['checkouts' => $rows]);

        return response()->json(compact('success','html'));
    }

    public function tables()
    {

        $view = [
            'services' => $this->repository->services(),
            'tables' => $this->repository->freeTables(),
        ];
        return $this->render('home.tables',$view);
    }

}