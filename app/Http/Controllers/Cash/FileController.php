<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 16:09
 */

namespace App\Http\Controllers\Cash;

use Illuminate\Support\Facades\Auth;
use Theme;

class FileController extends CashController{

    protected $acl = false;

    public function getManifest()
    {
        $data = [
            'url' => config('app.url').'/cash'
        ];
        $content = view('manifest',$data)->render();
        return response($content,200,['Content-type' => 'application/json']);
    }
}