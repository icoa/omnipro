<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 14/05/2016
 * Time: 19:53
 */

namespace App\Http\Controllers\Cash;

use App\Http\Controllers\FrontController as FrontController;

use Illuminate\Http\Request;
use Theme;
use Utils;
use Roomix;
use Illuminate\Support\Str;
use App\Lib\Helpers\Cash;

class CashController extends FrontController
{
    protected $themeName = 'cash';

    private $helper;
    private $user;

    public function __construct()
    {

        $this->helper = new Cash();
        $this->user = Roomix::user();

        $asset_version = config('roomix.asset_versioning', '0001');

        view()->share('helper', $this->helper);
        view()->share('user', $this->user);

        $layout = request()->ajax() ? 'rest' : 'default';

        $this->theme = Theme::uses($this->themeName)->layout($layout);
        Roomix::setTheme($this->theme);

        $this->theme->asset()->usePath()->add('fonts', "css/fonts.$asset_version.css");

        $production = !config('app.debug');
        if ($production) {
            $this->theme->asset()->usePath()->add('main', Utils::elixir('css/all.css', $this->themeName));
            $this->theme->asset()->usePath()->add('app', Utils::elixir('js/all.js', $this->themeName));
        } else {
            $this->theme->asset()->usePath()->add('main', 'css/all.' . $asset_version . '.css');
            $this->theme->asset()->usePath()->add('app', 'js/all.' . $asset_version . '.js');
        }

        if($this->acl)
            $this->middleware('auth.cash');
    }

}