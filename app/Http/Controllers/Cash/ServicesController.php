<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 16:09
 */

namespace App\Http\Controllers\Cash;

use App\Events\BottleWasDone;
use App\Http\Controllers\Cordova\CordovaController;
use App\Repositories\OrderRepository;
use Carbon\Carbon;
use DB;
use Theme;
use App\Repositories\CashRepository;
use App\Order;

class ServicesController extends CashController{

    protected $acl = true;
    protected $repository;
    protected $orderRepository;

    public function __construct(CashRepository $repository, OrderRepository $orderRepository)
    {
        $this->repository = $repository;
        $this->orderRepository = $orderRepository;
        parent::__construct();
    }

    public function index()
    {
        \Utils::watch();
        $view = [
            'services' => $this->repository->workingServices(),
        ];
        return $this->render('services.index',$view);
    }

    public function show($model)
    {
        \Utils::watch();
        $view = [
            'model' => $model,
            'rows' => $this->repository->service($model->id),
        ];
        return $this->render('services.show',$view);
    }

    public function decrement($id){
        \Utils::log($id,__METHOD__);
        $this->repository->decrement(($id));
        $response = ['success' => true, 'id' => $id];
        return response()->json($response);
    }

}