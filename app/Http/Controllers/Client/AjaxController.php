<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 16:09
 */

namespace App\Http\Controllers\Client;

use Helper;
use Form;
use Session;
use App\Lib\NotificationManager;
use Auth;
use Input;

class AjaxController extends ClientController
{

    protected $module = 'ajax';

    function getAreas($zone_id){
        $list = Helper::areasOptionsByZone($zone_id);
        $html = [];
        foreach ($list as $value => $display)
        {
            $html[] = \Form::getSelectOption($display, $value, null);
        }
        $html = implode('',$html);
        return response()->json(compact('html'));
    }

    function getLang($lang){
        Session::set('locale',$lang);
        return redirect()->route('home');
    }

    function getCheck(){

        $passedCounter = (int)\Input::get('count');

        $data = NotificationManager::check($passedCounter);

        return response()->json(compact('data'));

    }

    function getLoad(){
        $user = Auth::user();
        $type = Input::get('type');
        $module = Input::get('name');
        $html = null;
        $success = false;

        switch($module){
            case 'demands':
                $rows = $user->demands()->get();
                $html = $this->theme->partial('lists.demands',compact('rows','module','user'));
                break;
            case 'notifications':
                $rows = $user->notifications()->orderBy('created_at','desc')->get();
                $html = $this->theme->partial('lists.notifications',compact('rows','module','user'));
                break;
        }
        $success = !is_null($html);

        return response()->json(compact('success','html'));
    }

}