<?php

namespace App\Http\Controllers\Client;

use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Theme;
use Utils;
use App\Activity;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     */
    protected $theme;

    protected $username = 'username';

    //protected $loginPath = '/login';

    protected $redirectPath = '/';

    protected $redirectAfterLogout = '/auth/login';

    public function __construct()
    {
        //\Config::set('auth.model',\App\Customer::class);

        $this->theme = Theme::uses('default')->layout('login');

        $asset_version = config('roomix.asset_versioning', '0001');

        $this->theme->asset()->usePath()->add('fonts', 'css/fonts.css');

        $production = !config('app.debug');
        if($production){
            $this->theme->asset()->usePath()->add('main', Utils::elixir('css/all.css','default') );
            $this->theme->asset()->usePath()->add('app', Utils::elixir('js/all.js','default') );
        }else{
            $this->theme->asset()->usePath()->add('main', "css/all.$asset_version.css");
            $this->theme->asset()->usePath()->add('app', "js/all.$asset_version.js");
        }

        $this->middleware('guest', ['except' => 'getLogout']);
    }


    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        return $this->theme->scope('auth.login')->render();
    }


    public function getToken($token)
    {
        $token = trim(urlencode($token));
        $customer = Customer::where(\DB::raw('MD5(password)'),$token)->first();
        if($customer){
            $expire_at = strtotime($customer->expire_at);
            if($expire_at <= time()){
                \Flash::error('Questo account è scaduto');
                return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
            }
            \Auth::loginUsingId($customer->id,true);
            return redirect(property_exists($this, 'redirectPath') ? $this->redirectPath : '/');
        }
        \Flash::error('Non è stato trovato un account valido');
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }

    public function getLogout(){
        $user = Auth::user();
        try{
            Activity::log([
                'event' => 'logout',
                'user_id' => $user->id,
                'target' => 'Events\Auth',
                'target_id' => $user->id,
                'text' => $user->username." was logged out",
            ]);
        }catch(\Exception $e){

        }

        Auth::logout();

        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }

}
