<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 16:09
 */

namespace App\Http\Controllers\Client;

use Illuminate\Support\Facades\Auth;
use Theme;

class HomeController extends ClientController{

    public function getIndex()
    {
        return $this->render('home.index');
    }

    function redirectHome(){
        return redirect("/");
    }
}