<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 16:09
 */

namespace App\Http\Controllers\Client;


use App\Repositories\OrderRepository;
use App\Service;
use App\Repositories\DriverRepository;
use Illuminate\Http\Request;
use Utils;
use Roomix;
use Flash;
use Auth;

class ServicesController extends ClientController
{

    protected $module = 'services';
    protected $repository;

    function __construct(DriverRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
        Utils::log('construct');
    }


    public function index()
    {
        $mode = request('mode');
        $rows = $this->repository->paginate();
        return $this->render('index', compact('rows', 'mode'));
    }

    public function edit($model)
    {
        $action = '';
        $actionLabel = '';
        $button_color = '';
        switch ($model->status){
            case Service::STATUS_CONFIRMED:
            case Service::STATUS_REVERTED:
                $action = 'take';
                $actionLabel = 'Prendi in carico questa consegna';
                $button_color = 'bg-blue';
                break;
            case Service::STATUS_SHIPPING:
                $action = 'deliver';
                $actionLabel = 'Conferma consegna servizio';
                $button_color = 'bg-green';
                break;
            case Service::STATUS_DELIVERED:
                $action = 'revert';
                $actionLabel = 'Rimuovi lo status CONSEGNATO';
                $button_color = 'bg-red';
                break;
        }
        //if the current service is owned by an another "driver"
        if($model->shipping_by > 0 and $model->shipping_by != Auth::user()->id){
            $action = 'reown';
            $actionLabel = 'Rimuovi incarico ad autista corrente';
            $button_color = 'bg-brown';
        }

        $present = null;
        if (method_exists($model, 'lazyLoad')) {
            $model->lazyLoad();
        }
        if (method_exists($model, 'present')) {
            $present = $model->present();
        }
        return $this->render('edit', compact('model', 'present', 'action', 'actionLabel', 'button_color'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  Service $model
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $model)
    {
        Utils::log($request->all(),__METHOD__);
        $this->repository->update($request, $model);

        if (request()->ajax()) {
            //$message = trans('app.components.notifications.tableEdited');
            $message = 'Azione eseguita con successo';
            return response()->json(compact('message'));
        }

        Flash::success(trans('app.components.notifications.tableEdited'));

        return $this->redirect('index');
    }


    public function search(Request $request)
    {
        $success = false;
        $html = null;
        $code = $request->get('code');
        $rows = $this->repository->search($code);
        $count = count($rows);
        if($count > 0){
            $success = true;
            $html = $this->theme->partial('lists.services',compact('rows'));
        }
        return response()->json(compact('success','html','count'));
    }


    public function checkout(ServiceCloseRequest $request)
    {

        $model = Service::find($request->get('service_id'));

        $success = $this->repository->checkout($model);
        $error = ! $success;

        if (request()->ajax()) {
            $message = ($error) ? "Impossibile chiedere il conto per questo tavolo: le comande sono ancora in lavorazione" : trans('app.components.notifications.tableCheckedout');
            return response()->json(compact('message','error'));
        }

        Flash::success(trans('app.components.notifications.tableCheckedout'));

        return $this->redirect('index');
    }


    public function barcode()
    {
        return $this->render('barcode');
    }


    public function showCheckout()
    {

        $repository = new OrderRepository();

        $service_id = request()->get('service_id');

        return $this->render('checkout', compact('service_id', 'repository'));
    }

    public function tables()
    {

        $repository = new OrderRepository();

        $services = $repository->ajaxServices();

        return response()->json(compact('services'));
    }


}