<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 16:09
 */

namespace App\Http\Controllers\Client;

use App\Repositories\ServiceRepository;
use App\Service;
use Illuminate\Http\Request;
use App\Order;
use App\Repositories\OrderRepository;
use App\Http\Requests\OrderRequest;
use Utils;
use Roomix;
use Flash;
use Auth;

class OrdersController extends ClientController
{

    protected $module = 'orders';
    protected $repository;
    protected $serviceRepository;

    function __construct(OrderRepository $repository, ServiceRepository $serviceRepository)
    {
        $this->repository = $repository;
        $this->serviceRepository = $serviceRepository;
        parent::__construct();
    }


    public function index()
    {
        $rows = $this->repository->all();
        return $this->render('index',compact('rows'));
    }

    public function create(){
        $service_id = request()->get('service_id',null);
        $service = Service::with('tables')->find($service_id);
        $route = route( request()->get('return', $this->module.'.index') );
        $model = [
            'service_id' => $service_id,
            'service' => $service,
            'repository' => $this->repository,
            'route' => $route,
        ];
        return $this->render('create', $model);
    }

    public function choose()
    {
        \Utils::watch();
        $rows = $this->serviceRepository->selectable();
        return $this->render('choose',compact('rows'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param OrderRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        $this->repository->create($request);

        if(request()->ajax()){
            $message = trans('app.components.notifications.orderCreated');
            return response()->json(compact('message'));
        }

        Flash::success(trans('app.components.notifications.orderCreated'));

        return $this->redirect('index');
    }


}