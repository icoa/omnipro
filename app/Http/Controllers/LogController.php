<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 16:09
 */

namespace App\Http\Controllers;

use Theme;
use App\Service;
use App\User;

class LogController extends FrontController{

    protected $acl = false;



    public function getUser($id,$date)
    {
        $model = User::with('role')->find($id);
        $model->activities = $model->activities()->with('user')->where(\DB::raw('DATE(created_at)'),$date)->get();

        $view = array(
            'model' => $model,
            'date' => $date
        );

        return $this->theme->scope('logs.user', $view)->render();
    }

    public function getUsers()
    {
        $users = User::with('role')->whereNotIn('role_id',[1,6])->where('id','<>',\Roomix::userId())->orderBy('name')->get();

        $view = array(
            'users' => $users
        );

        return $this->theme->scope('logs.users', $view)->render();
    }

    public function getDates($id)
    {
        $model = User::with('role')->find($id);
        $query = "select distinct(date(created_at)) as date,count(id) as cnt from log_activities where user_id={$model->id} group by date order by date desc";
        $rows = \DB::select($query);

        $view = array(
            'model' => $model,
            'rows' => $rows
        );

        return $this->theme->scope('logs.dates', $view)->render();
    }
}