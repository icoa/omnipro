<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\FrontController;
use Illuminate\Support\Facades\Auth;

class PushController extends FrontController
{
    public function getAjax($method)
    {
        switch ($method) {
            case "new-messages":
                $qty = Auth::check() ? Auth::user()->countNotificationsNotRead() : 0;
                return [
                    "success" => true,
                    "data" => $qty
                ];
                break;
        }
        return \Response::json(compact('success','html'));

    }
}
