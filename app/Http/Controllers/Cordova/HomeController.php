<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 16:09
 */

namespace App\Http\Controllers\Cordova;

use App\Events\BottleWasDone;
use App\Http\Controllers\Cordova\CordovaController;
use App\Repositories\OrderRepository;
use Carbon\Carbon;
use DB;
use Theme;
use App\Repositories\GalleyRepository;
use App\BottleOrder;
use App\Order;

class HomeController extends CordovaController{

    protected $acl = true;
    protected $repository;
    protected $orderRepository;

    public function __construct(GalleyRepository $repository, OrderRepository $orderRepository)
    {
        $this->repository = $repository;
        $this->orderRepository = $orderRepository;
        parent::__construct();
    }

    public function getIndex()
    {
        $view = [
            'rows' => $this->repository->all()
        ];
        return $this->render('home.index',$view);
    }

    function redirectHome(){
        return redirect("/");
    }

    public function postBottleDone($pivot_id){
        $model = BottleOrder::find($pivot_id);

        if($model){
            $model->done = 1;
            $model->done_at = Carbon::now();
            $model->save();
        }

        event(new BottleWasDone($model));

        $response = ['success' => true, 'id' => $pivot_id];
        return response()->json($response);
    }


    public function postDone($order_id){
        $model = Order::find($order_id);
        $this->orderRepository->setCompleted($model);
        $response = ['success' => true, 'id' => $order_id];
        return response()->json($response);
    }

    public function getOrder($order_id){
        $order = $this->repository->order($order_id);
        $success = true;
        $html =  Theme::partial('lists.home',['rows' => [$order]]);
        $message = "Nuovo ordine per ".$order->service->present()->fullname;

        return response()->json(compact('success','html','message'));
    }

    public function orders(){
        $rows = $order = $this->repository->all();
        $success = true;
        $html =  Theme::partial('lists.home',['rows' => $rows]);
        $message = "Comande e tavoli aggiornati";

        return response()->json(compact('success','html','message'));
    }

}