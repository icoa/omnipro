<?php

namespace App\Http\Controllers\Cordova;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Theme;
use Utils;
use App\Activity;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     */
    protected $theme;

    protected $username = 'username';

    protected $loginPath = '/cordova/auth/login';

    protected $redirectPath = '/cordova';
    protected $redirectTo = '/cordova';

    protected $redirectAfterLogout = '/cordova/auth/login';

    public function __construct()
    {
        $this->theme = Theme::uses('cordova')->layout('login');

        $asset_version = config('roomix.asset_versioning', '0001');

        $this->theme->asset()->usePath()->add('fonts', 'css/fonts.css');

        $production = !config('app.debug');
        if($production){
            $this->theme->asset()->usePath()->add('main', Utils::elixir('css/all.css','cordova') );
            $this->theme->asset()->usePath()->add('app', Utils::elixir('js/all.js','cordova') );
        }else{
            $this->theme->asset()->usePath()->add('main', "css/all.$asset_version.css");
            $this->theme->asset()->usePath()->add('app', "js/all.$asset_version.js");
        }

        $this->middleware('guest', ['except' => 'getLogout']);
    }


    protected function handleUserWasAuthenticated(Request $request, $throttles)
    {
        if ($throttles) {
            $this->clearLoginAttempts($request);
        }

        if (method_exists($this, 'authenticated')) {
            return $this->authenticated($request, Auth::user());
        }

        return redirect()->to($this->redirectPath());
    }


    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        return $this->theme->scope('auth.login')->render();
    }

    public function getLogout(){
        $user = Auth::user();
        try{
            Activity::log([
                'event' => 'logout',
                'user_id' => $user->id,
                'target' => 'Events\Auth',
                'target_id' => $user->id,
                'text' => $user->username." was logged out",
            ]);
        }catch(\Exception $e){

        }

        Auth::logout();

        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }

}
