<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 16:09
 */

namespace App\Http\Controllers\Cordova;

use Theme;
use App\Feedback;

class PageController extends CordovaController{

    protected $acl = false;
    protected $module = 'pages';

    public function getAbout()
    {
        return $this->render('about');
    }

    public function getHelp()
    {
        return $this->render('help');
    }

}