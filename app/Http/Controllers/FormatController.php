<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 16:09
 */

namespace App\Http\Controllers\Admin;

use App\Http\Requests\FormatRequest;
use App\Format;
use Utils;
use Roomix;
use Flash;
use Yajra\Datatables\Datatables;
use Input;

class FormatController extends AdminController
{
    protected $module = 'formats';
    protected $entity = 'Formato';
    protected $entities = 'Formati';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $versioning = config('roomix.asset_versioning');
        $this->asset('formats', "js/components/formats.$versioning.js");
        return $this->render('index');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param CategoryRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormatRequest $request)
    {
        $user = Format::create($request->all());

        Flash::success("Record <strong>{$this->entity}</strong> creato con successo");

        return $this->redirectByTask($request, $user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param CategoryRequest|Request $request
     * @param  int $model
     * @return \Illuminate\Http\Response
     */
    public function update(FormatRequest $request, $model)
    {
        $data = $request->all();

        $model->update($data);

        Flash::success("Record <strong>{$this->entity}</strong> aggiornato con successo");

        return $this->redirectByTask($request, $model);
    }


    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function datatable()
    {

        $rows = Format::with(['bottles']);

        return Datatables::of($rows)
            ->editColumn('name', function ($row) {
                $route = route('admin.formats.edit', $row->id);
                return "<a href='$route'>$row->name</a>";
            })
            ->editColumn('total', function ($row) {
                return $row->bottles->count();
            })
            ->editColumn('created_at', function ($row) {
                return $row->created_at->format('d/m/Y H:i');
            })
            ->editColumn('updated_at', function ($row) {
                return $row->updated_at->format('d/m/Y H:i');
            })
            ->addColumn('actions', function ($row) {
                return $this->column_actions('formats',$row);
            })
            ->make(true);
    }
}