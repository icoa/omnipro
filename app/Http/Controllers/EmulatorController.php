<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 28/08/2015
 * Time: 15:45
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use ReflectionClass;
use Theme;
use Utils;
use Roomix;
use Illuminate\Support\Str;


class EmulatorController extends Controller
{

    protected $theme;
    protected $module;
    protected $model;
    protected $acl = true;

    function __construct()
    {
        $this->theme = Theme::uses('default')->layout('emulator');
        $this->theme->asset()->usePath()->add('fonts', 'css/fonts.css');

        $this->theme->asset()->usePath()->add('main', 'css/emulator.css');

    }

    function getIndex($mode = 'default'){
        \Auth::logout();
        $uri = [
            'default' => '/',
            'cash' => '/cash',
            'cordova' => '/cordova',
        ];
        $valid = ['default','cash','cordova'];
        if(!in_array($mode,$valid))
            $mode = 'default';
        $data = [
            'mode' => $mode,
            'size' => $mode == 'default' ? 'handset' : 'tablet',
            'url' => $uri[$mode],
        ];
        return $this->render('default', $data);
    }

    function redirect(){
        return response()->redirectTo('/emulator/default');
    }

    protected function render($view, $data = [])
    {
        return $this->theme->scope("emulator.{$view}", $data)->render();
    }
    protected function asset($name, $path, $dependencies = [])
    {
        $this->theme->asset()->usePath()->add($name, $path, $dependencies);
    }
}