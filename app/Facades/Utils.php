<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 17/09/2015
 * Time: 10:22
 */

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Utils extends Facade {

    protected static function getFacadeAccessor() { return 'utils'; }

}