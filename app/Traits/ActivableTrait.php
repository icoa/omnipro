<?php
namespace App\Traits;

trait ActivableTrait {

    public function scopeActive($query){
        return $query->where('active',1);
    }
} 