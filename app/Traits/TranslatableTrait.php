<?php
namespace App\Traits;


trait TranslatableTrait {

    public function scopeOrderByTranslation($query, $orderField, $orderType = 'ASC', $locale = null)
    {
        $translationTable = $this->getTranslationsTable();
        $table = $this->getTable();
        $locale = $locale ? $locale : \App::getLocale();

        return $query->join("{$translationTable} as t", "t.{$this->getRelationKey()}", "=", "{$table}.id")
            ->where('locale', $locale)
            ->groupBy("{$table}.id")
            ->select("{$table}.*")
            ->orderBy("t.{$orderField}", $orderType)
            ->with('translations');
    }
}
