<?php
namespace App\Traits;

use Auth;
use App\Activity;

/**
 * Add event-triggered references to the authorised user that triggered them
 */
trait LoggableTrait
{

    /** @var array $fields Mapping of events to fields */
    protected static $loggable = ['created','updated','deleted'];

    public function getActivityParamsForEvent($event){
        $text = null;
        $name = isset($this->name) ? $this->name : static::class;
        $target = isset($this) ? get_class($this) : null;
        $target_id = isset($this->id) ? $this->id : null;
        $username = Auth::user()->username;
        switch($event){
            case 'created':
                $text = "$name was created by ".$username;
                break;
            case 'updated':
                $text = "$name was updated by ".$username;
                break;
            case 'deleted':
                $text = "$name was deleted by ".$username;
                break;
            case 'deleting':
                $text = "$name was deleting by ".$username;
                break;
        }
        if($text){
            return compact('event','text','target','target_id');
        }
        return null;
    }


    /**
     * Set the default events to be recorded if the $recordEvents
     * property does not exist on the model.
     *
     * @return array
     */
    protected static function getRecordActivityEvents()
    {
        if (isset(static::$loggable)) {
            return static::$loggable;
        }
        return [
            'created', 'updated', 'deleting', 'deleted',
        ];
    }

    /**
     * Overrides the model's booter to register the event hooks
     */
    public static function boot()
    {
        $events = static::getRecordActivityEvents();
        if(!is_array($events))return;
        foreach($events as $eventName){
            static::$eventName(function ($model) use ($eventName) {
                $params = $model->getActivityParamsForEvent($eventName);
                if (is_array($params) AND count($params)) {
                    Activity::log($params);
                }
            });
        }
    }
}

?>