<?php

namespace App;

use App\Presenters\Contracts\PresentableInterface;
use App\Traits\PresentableTrait;

class BradenChoice extends BlameableModel implements PresentableInterface
{
    protected $presenter = 'App\Presenters\BradenChoicePresenter';

    protected static $loggable = [];

    use PresentableTrait;

    protected $fillable = ['name', 'description', 'points', 'braden_var_id'];

    public function braden_var()
    {
        return $this->belongsTo('App\BradenVar');
    }

    public function setNameAttribute($value){
        $this->attributes['name'] = ucfirst(trim($value));
    }

    public function canBeDeleted()
    {
        return false;
    }
}
