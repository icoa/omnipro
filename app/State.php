<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{

    protected $fillable = ['name','code', 'district_id'];

    /**
     * Return district associated to the state
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToOne
     */
    public function district()
    {
        return $this->belongsTo('App\District');
    }

    public function cities(){
        return $this->hasMany('App\City')->orderBy('priority','desc')->orderBy('name');
    }

}
