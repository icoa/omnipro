<?php

namespace App;

use App\Presenters\Contracts\PresentableInterface;
use App\Traits\PresentableTrait;
use App\Repositories\ServiceRepository;
use App\Http\Requests\Request;
use Carbon\Carbon;
use Utils;

class ServiceEvent extends BlameableModel implements PresentableInterface
{


    protected $presenter = 'App\Presenters\ServiceEventPresenter';

    use PresentableTrait;

    protected $fillable = [
        'service_id',
        'type',
        'status',
    ];

    public function service(){
        return $this->belongsTo('App\Service');
    }

    public function user(){
        return $this->belongsTo('App\User','created_by')->with('role');
    }

}