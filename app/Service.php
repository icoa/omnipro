<?php

namespace App;

use App\Presenters\Contracts\PresentableInterface;
use App\Traits\PresentableTrait;
use App\Repositories\ServiceRepository;
use App\Http\Requests\Request;
use Carbon\Carbon;
use Utils;

class Service extends BlameableModel implements PresentableInterface
{
    protected $presenter = 'App\Presenters\ServicePresenter';

    const STATUS_CREATED = 'created';
    const STATUS_REQUESTED = 'requested';
    const STATUS_CONFIRMED = 'confirmed';
    const STATUS_REJECTED = 'rejected';
    const STATUS_SHIPPING = 'shipping';
    const STATUS_DELIVERED = 'delivered';
    const STATUS_REVERTED = 'reverted';

    const TYPE_ACTIVATION = 'activation';
    const TYPE_MAINTENANCE = 'maintenance';
    const TYPE_DISACTIVATION = 'disactivation';
    const TYPE_TRANSFER = 'transfer';

    use PresentableTrait;

    protected $fillable = [
        'parent_id',
        'name',
        'status',
        'type',
        'sdo',
        'patient_name',
        'patient_lastname',
        'patient_initials',
        'injuries',
        'stand_by',
        'engine_serial',
        'product_id',
        'department_id',
        'braden_point1',
        'braden_point2',
        'braden_point3',
        'braden_point4',
        'braden_point5',
        'braden_point6',
        'braden_total',
        'transfer_id',
        'event_by',
        'confirmed_by',
        'shipping_by',
        'delivered_by',
        'notes',
        'event_at',
        'sent_at',
        'sent_activation_at',
        'confirmed_at',
        'delivered_at',
        'active_date',
        'discharge_date',
        'disable_date',
        'maintenance_date',
        'transfer_date',
        'start_date',
        'mt_requested_at',
        'days',
        'cost',
        'pre_cost',
        'signature',
        'activation_notes',
        'disactivation_notes',
        'transfer_notes',
        'mt_actions_performed',
        'discount',
        'cost_starts_at',
        'cost_ends_at',
        'transfer_date',
        'disable_date',
        'maintenance_date',
        'discharge_date',
        'active_date',
        'mt_requested_at',
    ];

    protected $dates = [
        'event_at',
        'sent_at',
        'sent_activation_at',
        'confirmed_at',
        'shipping_at',
        'delivered_at',
        'active_date',
        'discharge_date',
        'disable_date',
        'maintenance_date',
        'transfer_date',
        'start_date',
        'cost_starts_at',
        'cost_ends_at',
        'mt_requested_at',
    ];

    public function confirmedBy()
    {
        return $this->belongsTo('App\User', 'confirmed_by');
    }

    public function deliveredBy()
    {
        return $this->belongsTo('App\User', 'delivered_by');
    }

    public function eventBy()
    {
        return $this->belongsTo('App\User', 'event_by');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    public function transfer_department()
    {
        return $this->belongsTo('App\Department', 'transfer_id');
    }

    public function events()
    {
        return $this->hasMany('App\ServiceEvent');
    }

    public function parent()
    {
        return $this->belongsTo('App\Service', 'parent_id');
    }

    public function driver()
    {
        return $this->belongsTo('App\User', 'shipping_by');
    }

    public function canBeDeleted()
    {
        $canBeDeleted = true;

        return $canBeDeleted;
    }

    public function setSdoAttribute($value)
    {
        $this->attributes['sdo'] = strtoupper(trim($value));
    }

    public function setEngineSerialAttribute($value)
    {
        $this->attributes['engine_serial'] = strtoupper(trim($value));
    }

    public function setActiveDateAttribute($value)
    {
        $this->attributes['active_date'] = $this->date($value);
    }

    public function setDischargeDateAttribute($value)
    {
        $this->attributes['discharge_date'] = $this->date($value);
    }

    public function setDisableDateAttribute($value)
    {
        $this->attributes['disable_date'] = $this->date($value);
    }

    public function setMaintenanceDateAttribute($value)
    {
        $this->attributes['maintenance_date'] = $this->date($value);
    }

    public function setTransferDateAttribute($value)
    {
        $this->attributes['transfer_date'] = $this->date($value);
    }

    public function formActiveDateAttribute($value)
    {
        return $this->local_date($value);
    }

    public function formDischargeDateAttribute($value)
    {
        return $this->local_date($value);
    }

    public function formDisableDateAttribute($value)
    {
        return $this->local_date($value);
    }

    public function formMaintenanceDateAttribute($value)
    {
        return $this->local_date($value);
    }

    public function formTransferDateAttribute($value)
    {
        return $this->local_date($value);
    }

    private function date($value)
    {
        if (is_null($value))
            return null;
        if ($value instanceof Carbon)
            return $value->format('Y-m-d');

        try {
            return Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');
        } catch (\Exception $e) {
            return Carbon::createFromFormat('Y-m-d', $value)->format('Y-m-d');
        }

    }

    private function local_date($value)
    {
        if (is_null($value))
            return null;
        if ($value instanceof Carbon)
            return $value->format('d-m-Y');
        if (is_string($value)) {
            return Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function scopeWithStatus($query, $status)
    {
        return is_array($status) ? $query->whereIn('status', $status) : $query->where('status', $status);
    }

    public function scopeWithoutStatus($query, $status)
    {
        return is_array($status) ? $query->whereNotIn('services.status', $status) : $query->where('services.status', '<>', $status);
    }

    public function scopeWithType($query, $type)
    {
        return is_array($type) ? $query->whereIn('type', $type) : $query->where('type', $type);
    }

    public function scopeWithoutType($query, $type)
    {
        return is_array($type) ? $query->whereNotIn('type', $type) : $query->where('type', '<>', $type);
    }

    public function scopeDriver($query)
    {
        //customer wants to disable the fact that a 'driver' can only see unassigned services or services assigned to him
        /*$user = \Auth::user()->id;
        return $query->where(function($subQuery) use($user){
            $subQuery->whereNull('shipping_by')->orWhere('shipping_by',$user);
        });*/
        $user = \Auth::user()->id;
        return $query->where(function ($subQuery) use ($user) {
            $subQuery->whereNull('shipping_by')->orWhere(function($innerQuery) use($user){
                $innerQuery->where('shipping_by', $user)->orWhereIn('type', [Service::TYPE_MAINTENANCE, Service::TYPE_DISACTIVATION]);
            });
        });
    }

    public function scopeUser($query)
    {
        $user = \Auth::user();
        $role = $user->role->code;
        if ($role == 'user') {
            return $query->whereIn('department_id', $user->department_options);
        }
        return $query;
    }

    public function canApprove()
    {
        switch ($this->status) {
            case Service::STATUS_CONFIRMED:
            case Service::STATUS_DELIVERED:
            case Service::STATUS_SHIPPING:
            case Service::STATUS_REVERTED:
                return false;
                break;
        }
        return true;
    }

    public function canReject()
    {
        switch ($this->status) {
            case Service::STATUS_REJECTED:
            case Service::STATUS_DELIVERED:
            case Service::STATUS_SHIPPING:
            case Service::STATUS_REVERTED:
                return false;
                break;
        }
        return true;
    }

    /**
     * @return bool
     */
    public function hasParent()
    {
        return $this->parent_id > 0;
    }

    /**
     * Get the array of columns
     * @return mixed
     */
    private function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    /**
     * Exclude an array of elements from the result.
     * @param $query
     * @param $columns
     * @return mixed
     */
    public function scopeExclude($query, $columns)
    {
        return $query->select(array_diff($this->getTableColumns(), (array)$columns));
    }
}