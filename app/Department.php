<?php

namespace App;

use App\Presenters\Contracts\PresentableInterface;
use App\Traits\PresentableTrait;

class Department extends BlameableModel implements PresentableInterface
{
    protected $presenter = 'App\Presenters\DepartmentPresenter';

    protected static $loggable = [];

    use PresentableTrait;

    protected $fillable = ['name', 'description', 'code', 'active'];

    public function services()
    {
        return $this->hasMany('App\Service');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    /**
     * Return products associated to the user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    public function setNameAttribute($value){
        $this->attributes['name'] = ucfirst(trim($value));
    }

    public function setCodeAttribute($value){
        $this->attributes['code'] = strtoupper(trim($value));
    }

    public function getProductOptionsAttribute()
    {
        return $this->products->lists('id')->toArray();
    }

    public function canBeDeleted()
    {
        $canBeDeleted = true;
        if ($this->services()->count() > 0)
            $canBeDeleted = false;

        if ($this->users()->count() > 0)
            $canBeDeleted = false;

        return $canBeDeleted;
    }
}
