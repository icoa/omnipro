<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    // Constants
    const ROLE_ADMIN = "admin";
    const ROLE_WAITER = "waiter";
    const ROLE_PRODUCTION = "production";
    const ROLE_RECEPTION = "reception";
    const ROLE_VIEWER = "viewer";

    /**
     * Returns users with this role
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }

    /**
     * Returns components associated with this role
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function components()
    {
        return $this->belongsToMany('App\Component');
    }
}
