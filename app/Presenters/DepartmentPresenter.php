<?php
namespace App\Presenters;

class DepartmentPresenter extends Presenter {

    function productsList(){
        $names = $this->entity->products->lists('name')->toArray();
        if(empty($names)){
            return null;
        }
        $html = '';
        foreach ($names as $name){
            $html .= "<span class='label' style='margin:5px 5px 0 0'>$name</span>";
        }
        return $html;
    }

}