<?php
namespace App\Presenters;
use Utils;

class ProductPresenter extends Presenter {

    function cost(){
        return Utils::euro($this->entity->day_cost);
    }

}