<?php
namespace App\Presenters;
use App\ServiceEvent;
use Utils;

class ServiceEventPresenter extends Presenter {

    function created(){
        return $this->entity->created_at->format('d/m/Y H:i');
    }

    function updated(){
        return $this->entity->updated_at->format('d/m/Y H:i');
    }

    function createdHuman(){
        return $this->entity->created_at->diffForHumans();
    }

    function createdFull(){
        return $this->entity->created_at->formatLocalized("%d %b, %H:%M");
    }

    function time(){
        return $this->entity->created_at->formatLocalized("%H:%M");
    }

    function when(){
        return $this->createdHuman();
    }

    function description(){
        $user = $this->entity->user->name;
        $typeName = trans('service.types.' . $this->entity->type);
        $event = "<strong>$user</strong> ha " . trans('service.statuses_actions.' . $this->entity->status) . " il servizio di <span class='label'>$typeName</span>";
        return $event;
    }

}