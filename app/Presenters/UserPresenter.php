<?php
namespace App\Presenters;

use App\User;
use App\Room;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UserPresenter extends Presenter {

    private $currentStatus;

    /**
     * @return mixed
     */
    public function rolename()
    {
        return ucfirst($this->entity->role->name);
    }

    public function rolecode()
    {
        return ucfirst($this->entity->role->code);
    }

    public function theme()
    {
        return 'theme-'.$this->entity->role->theme;
    }

    public function departmentName()
    {
        $names = $this->entity->departments->lists('name')->toArray();
        return empty($names) ? null : implode(' / ',$names);
    }

    public function currentStatus()
    {
        $lastActivity = $this->entity->lastActivity();

        $html = '<span class="color-gray">Inattivo</span>';
        if ($lastActivity and substr($lastActivity->event, 0, 7) == 'entered') {
            $html = '<strong class="color-green">In lavorazione</strong>';
        }

        return $html;
    }

    public function currentRoom()
    {
        $lastActivity = $this->entity->lastActivity();

        $html = '<i class="room-round" style="border-color: #ddd;"></i>';
        if ($lastActivity and $lastActivity->extra) {
            $data = unserialize($lastActivity->extra);
            $room = Room::find($data["room_id"]);
            $html = $room->present()->nameHtml();
        }

        return $html;
    }

    public function lastActivityDate()
    {
        $lastActivity = $this->entity->lastActivity();
        return $lastActivity ? $lastActivity->present()->when() : 'N/D';
    }

    public function lastActivityDescription()
    {
        $lastActivity = $this->entity->lastActivity();
        return $lastActivity ? $lastActivity->present()->explainService() : '';
    }

    function requestsCount(){
        return $this->entity->requests()->count();
    }

    function deliversCount(){
        return $this->entity->delivers()->count();
    }

    function requestsBadge(){
        $count = $this->requestsCount();
        return ($count == 0) ? null : "<span class='badge bg-red'>$count</span>";
    }

    function deliversBadge(){
        $count = $this->deliversCount();
        return ($count == 0) ? null : "<span class='badge bg-red'>$count</span>";
    }

    function adminStatus(){
        switch($this->entity->status){
            case User::STATUS_ACCEPTED:
                $label = 'In servizio';
                $class = 'label-primary';
                break;
            case User::STATUS_DELIVERED:
                $label = 'In attesa';
                $class = 'label-warning';
                break;
            case User::STATUS_ROUTED:
                $label = 'In consegna';
                $class = 'label-info';
                break;
            case User::STATUS_DEFAULT:
                $label = 'Libero';
                $class = 'label-success';
                break;
        }
        return "<span class='label $class'>$label</span>";
    }

}