<?php namespace App\Presenters;

use Carbon\Carbon;

abstract class Presenter {

    /**
     * @var mixed
     */
    protected $entity;
    protected $locale;

    /**
     * @param $entity
     */
    function __construct($entity)
    {
        $this->locale = \App::getLocale();
        Carbon::setLocale($this->locale);
        $this->entity = $entity;
    }

    /**
     * Allow for property-style retrieval
     *
     * @param $property
     * @return mixed
     */
    public function __get($property)
    {
        if (method_exists($this, $property))
        {
            return $this->{$property}();
        }
        return $this->entity->{$property};
    }

    public function forceLocale(){
        \App::setLocale('it');
    }

    public function revertLocale(){
        \App::setLocale($this->locale);
    }
} 