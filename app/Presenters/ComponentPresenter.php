<?php
namespace App\Presenters;

class ComponentPresenter extends Presenter {

    /**
     * @return mixed
     */
    public function link()
    {
        return ($this->entity->route) ? route($this->entity->route) : $this->entity->url;
    }

}