<?php
namespace App\Presenters;

use App\Service;
use Utils;

class ServicePresenter extends Presenter
{

    protected $user;

    function __construct($entity)
    {
        parent::__construct($entity);
        $this->user = \Auth::user();
    }

    function created()
    {
        return $this->entity->created_at->format('d/m/Y H:i');
    }

    function creator()
    {
        return $this->entity->createdBy->name;
    }

    function eventBy()
    {
        return $this->entity->eventBy->name;
    }

    function updated()
    {
        return $this->entity->updated_at->format('d/m/Y H:i');
    }

    function createdHuman()
    {
        return $this->entity->created_at->diffForHumans();
    }

    function createdFull()
    {
        return $this->entity->created_at->formatLocalized("%d %b, %H:%M");
    }

    function time()
    {
        return $this->entity->created_at->formatLocalized("%H:%M");
    }

    function when()
    {
        return $this->entity->event_at->format('d/m/Y H:i');
    }

    function whenConfirmed()
    {
        return ($this->entity->confirmed_at) ? $this->entity->confirmed_at->format('d/m/Y H:i') : null;
    }

    function whenSent()
    {
        return ($this->entity->sent_at) ? $this->entity->sent_at->format('d/m/Y H:i') : null;
    }

    function whenSentActivationAt()
    {
        return ($this->entity->sent_at) ? $this->entity->sent_at->format('d/m/Y H:i') : null;
    }

    function whenDelivered()
    {
        return ($this->entity->delivered_at) ? $this->entity->delivered_at->format('d/m/Y H:i') : null;
    }

    function whenCostStartsAt()
    {
        return ($this->entity->cost_starts_at) ? $this->entity->cost_starts_at->format('d/m/Y') : null;
    }

    function discount()
    {
        return ($this->entity->discount > 0) ? $this->entity->discount . ' %' : null;
    }

    function whenCostEndsAt()
    {
        return ($this->entity->cost_ends_at) ? $this->entity->cost_ends_at->format('d/m/Y') : null;
    }

    function activeDate()
    {
        return ($this->entity->active_date) ? $this->entity->active_date->format('d/m/Y') : null;
    }

    function dischargeDate()
    {
        return ($this->entity->discharge_date) ? $this->entity->discharge_date->format('d/m/Y') : null;
    }

    function disableDate()
    {
        return ($this->entity->disable_date) ? $this->entity->disable_date->format('d/m/Y') : null;
    }

    function maintenanceDate()
    {
        return ($this->entity->maintenance_date) ? $this->entity->maintenance_date->format('d/m/Y') : null;
    }

    function maintenanceRequestedAtDate()
    {
        return ($this->entity->mt_requested_at) ? $this->entity->mt_requested_at->format('d/m/Y H:i') : null;
    }

    function transferDate()
    {
        return ($this->entity->transfer_date) ? $this->entity->transfer_date->format('d/m/Y') : null;
    }

    function productName()
    {
        return $this->entity->product->name;
    }

    function departmentName()
    {
        return $this->entity->department->name;
    }

    function transferDepartment()
    {
        return ($this->entity->transfer_department) ? $this->entity->transfer_department->name : null;
    }

    function injuriesText()
    {
        return $this->entity->injuries ? 'SI' : 'NO';
    }

    function standByText()
    {
        return $this->entity->stand_by ? 'SI' : 'NO';
    }

    function typeName()
    {
        $label = '';
        $class = '';
        switch ($this->entity->type) {
            case Service::TYPE_ACTIVATION:
                $label = trans('service.types.' . Service::TYPE_ACTIVATION);
                $class = 'label-success';
                break;
            case Service::TYPE_DISACTIVATION:
                $label = trans('service.types.' . Service::TYPE_DISACTIVATION);
                $class = 'label-danger';
                break;
            case Service::TYPE_MAINTENANCE:
                $label = trans('service.types.' . Service::TYPE_MAINTENANCE);
                $class = 'label-warning';
                break;
            case Service::TYPE_TRANSFER:
                $label = trans('service.types.' . Service::TYPE_TRANSFER);
                $class = 'bg-brown';
                break;
            default:
                return null;
                break;
        }
        return "<span class='label $class'>$label</span>";
    }

    function typeText()
    {
        return trans('service.types.' . $this->entity->type);
    }

    function typeBadge()
    {
        $label = '';
        $class = '';
        switch ($this->entity->type) {
            case Service::TYPE_ACTIVATION:
                $label = 'A';
                $class = 'bg-green';
                break;
            case Service::TYPE_DISACTIVATION:
                $label = 'D';
                $class = 'bg-red';
                break;
            case Service::TYPE_MAINTENANCE:
                $label = 'M';
                $class = 'bg-blue';
                break;
            case Service::TYPE_TRANSFER:
                $label = 'T';
                $class = 'bg-brown';
                break;
            default:
                return null;
                break;
        }
        return "<span class='badge badge-type $class'>$label</span>";
    }

    function bradenPoints()
    {
        $values = [];
        for ($i = 1; $i <= 6; $i++) {
            $values[] = $this->entity->{"braden_point" . $i};
        }
        $html = implode('&nbsp;|&nbsp;', $values);
        $route = route('admin.services_activation.braden', $this->entity->id);
        $html .= " <a style='font-size: 13px; margin-left: 10px' href='$route' target='_blank'>(Stampa Scala Braden)</a>";
        return $html;
    }


    function statusHtml()
    {
        $label = '';
        $class = '';
        switch ($this->entity->status) {
            case Service::STATUS_CREATED:
                $label = trans('service.statuses.' . Service::STATUS_CREATED);
                $class = '';
                break;
            case Service::STATUS_REQUESTED:
                $label = trans('service.statuses.' . Service::STATUS_REQUESTED);
                $class = 'label-info';
                break;
            case Service::STATUS_CONFIRMED:
                $label = trans('service.statuses.' . Service::STATUS_CONFIRMED);
                $class = 'label-success';
                break;
            case Service::STATUS_REJECTED:
                $label = trans('service.statuses.' . Service::STATUS_REJECTED);
                $class = 'label-danger';
                break;
            case Service::STATUS_SHIPPING:
                $label = trans('service.statuses.' . Service::STATUS_SHIPPING);
                $class = 'label-warning';
                break;
            case Service::STATUS_DELIVERED:
                $label = trans('service.statuses.' . Service::STATUS_DELIVERED);
                $class = 'label-success2';
                break;
            case Service::STATUS_REVERTED:
                $label = trans('service.statuses.' . Service::STATUS_REVERTED);
                $class = 'label-danger';
                break;
            default:
                return null;
                break;
        }
        return "<span class='label $class'>$label</span>";
    }


    function statusBadge()
    {
        $label = '';
        $class = '';
        switch ($this->entity->status) {
            case Service::STATUS_CONFIRMED:
                $label = trans('service.statuses.' . Service::STATUS_CONFIRMED);
                $class = 'bg-blue';
                break;
            case Service::STATUS_SHIPPING:
                $label = trans('service.statuses.' . Service::STATUS_SHIPPING);
                $class = 'bg-orange';
                break;
            case Service::STATUS_DELIVERED:
                $label = trans('service.statuses.' . Service::STATUS_DELIVERED);
                $class = 'bg-green';
                break;
            case Service::STATUS_REVERTED:
                $label = trans('service.statuses.' . Service::STATUS_REVERTED);
                $class = 'bg-purple';
                break;
            default:
                return null;
                break;
        }
        return "<span class='badge $class'>$label</span>";
    }


    function daysText()
    {
        return $this->entity->days > 0 ? $this->entity->days . ' GG' : 'N/D';
    }

    function costEuro()
    {
        return Utils::euro($this->entity->cost);
    }

    function notesHtml()
    {
        return strlen($this->entity->notes) > 0 ? nl2br($this->entity->notes) : 'N/D';
    }

    function activationNotesHtml()
    {
        return strlen($this->entity->activation_notes) > 0 ? nl2br($this->entity->activation_notes) : 'N/D';
    }

    function disactivationNotesHtml()
    {
        return strlen($this->entity->disactivation_notes) > 0 ? nl2br($this->entity->disactivation_notes) : 'N/D';
    }

    function transferNotesHtml()
    {
        return strlen($this->entity->transfer_notes) > 0 ? nl2br($this->entity->transfer_notes) : 'N/D';
    }

    function name()
    {
        $sdo = $this->entity->sdo;
        $patient_lastname = $this->entity->patient_lastname;
        $patient_name = $this->entity->patient_name;

        return "SDO: $sdo, Paziente: " . "$patient_lastname" . "$patient_name";
    }

    function uo()
    {
        return $this->entity->department ? $this->entity->department->code : 'N/D';
    }

    function signatureHtml()
    {
        return $this->entity->signature != null ? "<img src='{$this->entity->signature}' style='width:100%; border:1px solid #e6e6e6'>" : null;
    }

    function actionsPerformedHtml()
    {
        return strlen($this->entity->mt_actions_performed) > 0 ? nl2br($this->entity->mt_actions_performed) : 'N/D';
    }

    function parentName()
    {
        return $this->entity->hasParent() ? "Servizio N° {$this->entity->parent_id}" : null;
    }

    function parentNameLinked()
    {
        if ($this->entity->hasParent()) {
            $name = $this->parentName();
            $show = route("admin.services_history.show", $this->entity->parent_id);
            return "<a href='$show'>$name</a>";
        }
        return null;
    }

    function shippedDriverName(){
        if($this->entity->shipping_by == $this->user->id){
            return '<strong style="font-size: 10px; text-transform: uppercase; color: green">te</strong>';
        }
        if(isset($this->entity->driver)){
            return '<strong style="font-size: 10px; text-transform: uppercase; color:blue;">' . $this->driver->name . '</strong>';
        }
        return '<strong style="font-size: 10px; text-transform: uppercase; color:darkgrey">Non Assegnato</strong>';
    }
}