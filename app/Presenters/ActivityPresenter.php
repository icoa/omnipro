<?php
namespace App\Presenters;
use Illuminate\Support\Str;
use App\Service;
use Theme;

class ActivityPresenter extends Presenter {

    private $service;
    /**
     * @return mixed
     */
    public function when()
    {
        return date('d/m/Y H:i', strtotime($this->entity->created_at));
    }

    /**
     * @return mixed
     */
    public function explainService()
    {
        $event = null;
        $implicitRole = null;
        $message = $this->entity->text;
        if(Str::contains($this->entity->event,'.')){
            $tokens = explode('.',$this->entity->event);
            $event = $tokens[0];
            $implicitRole = $tokens[1];
        }else{
            $event = $this->entity->event;
        }

        switch($event){
            case 'entered':
                $message = sprintf("%s (%s) è entrato/a nella camera", $this->entity->user->name, $this->entity->user->role->name);
                break;
            case 'exited':
                $message = sprintf("%s (%s) è uscito/a dalla camera", $this->entity->user->name, $this->entity->user->role->name);
                break;
            case 'created':
                $message = sprintf("%s (%s) ha creato il servizio", $this->entity->user->name, $this->entity->user->role->name);
                break;
            case 'assign':
                $message = sprintf("%s (%s) ha assegnato una cameriera al servizio", $this->entity->user->name, $this->entity->user->role->name);
                break;
            case 'login':
                $message = sprintf("%s (%s) ha eseguito il Login", $this->entity->user->name, $this->entity->user->role->name);
                break;
            case 'logout':
                $message = sprintf("%s (%s) si è disconnesso/a dal sistema", $this->entity->user->name, $this->entity->user->role->name);
                break;
        }


        return $message;
    }

    /**
     * @return mixed
     */
    public function explainUser()
    {
        $message = $this->explainService();
        return $message;
    }

    /**
     * @return mixed
     */
    public function explainServiceExtra()
    {
        $text = null;
        if(isset($this->entity->extra) AND $this->entity->extra != ''){
            $data = unserialize($this->entity->extra);
            if(isset($data['id'])){
                $service = Service::with('room','receipt')->find($data['id']);
                if($service){
                    foreach($data as $key => $val){
                        if($key != 'room' AND $key != 'receipt')
                            $service->setAttribute($key,$val);
                        $this->service = $service;
                        $text = Theme::partial('service.details',['row' => $service]);
                    }
                }
            }
            //$text = '<pre>'.print_r($data,1).'<pre>';
        }
        return $text;
    }


    /**
     * @return mixed
     */
    public function explainUserExtra()
    {
        $text = null;
        if($this->entity->target == 'App\Service'){
            $text = $this->explainServiceExtra();
            if($this->service){
                \Utils::log($this->service->id,__METHOD__);
                $text .= Theme::partial('service.basic',['row' => $this->service]);
            }
        }
        return $text;
    }

}