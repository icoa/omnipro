<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\EmailReport::class,
        \App\Console\Commands\GcmTest::class,
        \App\Console\Commands\WebsocketServer::class,
        \App\Console\Commands\WebsocketClient::class,
        \App\Console\Commands\CssRewrite::class,
        \App\Console\Commands\CustomMigration::class,
        \App\Console\Commands\Ping::class,
        \App\Console\Commands\Factory::class,
        \App\Console\Commands\UserList::class,
        \App\Console\Commands\UserPasw::class,
        \App\Console\Commands\ExecTask::class,
        \App\Console\Commands\Tools::class,
        \App\Console\Commands\CalculateCosts::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();
    }
}
