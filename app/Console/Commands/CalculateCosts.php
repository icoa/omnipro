<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Product;
use App\Department;
use App\Repositories\ServiceRepository;

class CalculateCosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'omnipro:calculate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate all costs for services';

    protected $repository;

    function __construct(ServiceRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment("Executing calculate and costs...");
        $this->repository->calculateDaysAndCosts($this);
        $this->info('DONE');
    }

}
