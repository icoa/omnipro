<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use ZMQContext;
use ZMQ;
use Utils;

class WebsocketClient extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ws:client';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Websocket client test';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $env = \App::environment();
        $host = config("roomix.ws.host");
        $port = config("roomix.ws.port");
        $this->info("Connecting WebSocket server... [$host:$port]");
        $this->info("Current environment: [$env]");

        \Ratchet\Client\connect("ws://$host:$port")->then(function($conn) {
            $conn->on('message', function($msg) use ($conn) {
                echo "Received: {$msg}\n";
                $conn->close();
            });

            $entryData = ['message' => 'Hello there...'];
            $stream = [
                'type' => 'echo',
                'data' => $entryData,
            ];
            Utils::log($stream,'STREAM');

            $conn->send(json_encode($stream));

            //test message to kitchen
            $entryData = ['message' => 'Kitchen, are You there?'];
            $stream = [
                'type' => 'single',
                'data' => $entryData,
            ];
            $conn->send(json_encode($stream));

            $conn->close();
        }, function ($e) {
            echo "Could not connect: {$e->getMessage()}\n";
        });

    }

}
