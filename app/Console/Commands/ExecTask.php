<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\BradenChoice;
use App\BradenVar;
use App\Department;


class ExecTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exec:task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute a task';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment("Firing ExecTask");

        $this->import_departments();

    }


    private function import_braden()
    {
        $data = [
            [
                'name' => 'Percezione sensoriale',
                'description' => 'Abilità a rispondere in modo corretto alla sensazione di disagio correlata alla pressione.',
                'position' => 1,
                'choices' => [
                    [
                        'name' => 'Non limitata',
                        'description' => 'Risponde agli ordini verbali.<br>Non ha deficit sensoriale che limiti la capacità di sentire  ed esprimere il dolore o il disagio.',
                        'points' => 4,
                    ],
                    [
                        'name' => 'Leggermente limitata',
                        'description' => 'Risponde agli ordini verbali ma non può comunicare sempre il suo disagio o il bisogno di cambiare posizione.<br>O Ha impedimenti al sensorio che limita la capacità di avvertire il dolore o il disagio in 1o 2 estremità.',
                        'points' => 3,
                    ],
                    [
                        'name' => 'Molto limitata',
                        'description' => 'Risponde solo a stimoli dolorosi. Non può comunicare il proprio disagio se non gemendo o agitandosi.<br>O Ha impedimento al sensorio che limita la percezione del dolore almeno per la metà del corpo.',
                        'points' => 2,
                    ],
                    [
                        'name' => 'Completamente limitata',
                        'description' => 'Non vi è risposta (non geme, non si contrae o afferra)allo stimolo doloroso, a causa del diminuito livello di coscienza od alla sedazione.<br>O Limitata capacità di percepire dolore in molte zone del corpo.',
                        'points' => 1,
                    ]
                ]
            ],

            [
                'name' => 'Umidità',
                'description' => 'Grado di esposizione della pelle all’umidità.',
                'position' => 2,
                'choices' => [
                    [
                        'name' => 'Raramente bagnato',
                        'description' => 'La pelle è abitualmente asciutta. Le lenzuola sono cambiate ad intervalli di routine.',
                        'points' => 4,
                    ],
                    [
                        'name' => 'Occasionalmente bagnato',
                        'description' => 'La pelle è occasionalmente umida, richiede un cambio di lenzuola extra 1 volta al giorno.',
                        'points' => 3,
                    ],
                    [
                        'name' => 'Spesso bagnato',
                        'description' => 'Pelle sovente ma Non sempre umida. Le lenzuola devono essere cambiate almeno 1 volta per turno.',
                        'points' => 2,
                    ],
                    [
                        'name' => 'Costantemente bagnato',
                        'description' => 'La pelle è mantenuta costantemente umida dalla traspirazione, dall’urina, ecc. Ogni volta che il paziente si muove o si gira lo si trova sempre bagnato.',
                        'points' => 1,
                    ]
                ]
            ],

            [
                'name' => 'Attività',
                'description' => 'Grado di attività fisica.',
                'position' => 3,
                'choices' => [
                    [
                        'name' => 'Cammina Frequentemente',
                        'description' => 'Cammina al di fuori della camera almeno 2 volte al giorno e dentro la camera 1 volta ogni due ore (al di fuori delle ore di riposo).',
                        'points' => 4,
                    ],
                    [
                        'name' => 'Cammina occasionalmente',
                        'description' => 'Cammina occasionalmente durante il giorno ma per brevi distanze cono senza aiuto, trascorre la maggior parte di ogni turno aletto o sulla sedia.',
                        'points' => 3,
                    ],
                    [
                        'name' => 'In poltrona',
                        'description' => 'Capacità di camminare severamente limitata o inesistente. Non mantiene la posizione eretta e/o deve essere assistito nello spostamento sulla sedia a rotelle.',
                        'points' => 2,
                    ],
                    [
                        'name' => 'Allettato',
                        'description' => 'Costretto a letto.',
                        'points' => 1,
                    ]
                ]
            ],

            [
                'name' => 'Mobilità',
                'description' => 'Capacità di cambiare e di controllare le posizioni del corpo.',
                'position' => 4,
                'choices' => [
                    [
                        'name' => 'Limitazioni assenti',
                        'description' => 'Si sposta frequentemente e senza assistenza.',
                        'points' => 4,
                    ],
                    [
                        'name' => 'Parzialmente limitata',
                        'description' => 'Cambia frequentemente la posizione con minimi spostamenti del corpo.',
                        'points' => 3,
                    ],
                    [
                        'name' => 'Molto limitata',
                        'description' => 'Cambia occasionalmente posizione del corpo o delle estremità, ma è incapace di fare frequenti o significativi cambiamenti di posizione senza aiuto.',
                        'points' => 2,
                    ],
                    [
                        'name' => 'Completamente immobile',
                        'description' => 'Non può fare alcun cambiamento di posizione senza assistenza.',
                        'points' => 1,
                    ]
                ]
            ],

            [
                'name' => 'Nutrizione',
                'description' => 'Assunzione usuale di cibo.',
                'position' => 5,
                'choices' => [
                    [
                        'name' => 'Eccellente',
                        'description' => 'Mangia la maggior parte del cibo, Non rifiuta mai il pasto, talvolta mangia tra i pasti.<br>Non necessita di integratori.',
                        'points' => 4,
                    ],
                    [
                        'name' => 'Adeguata',
                        'description' => 'Mangia più della metà dei pasti, 4 porzioni o più di proteine al giorno. Usualmente assume integratori.<br>O Si alimenta artificialmente con TPN, assumendo il Quantitativo nutrizionale necessario.',
                        'points' => 3,
                    ],
                    [
                        'name' => 'Probabilmente inadeguata',
                        'description' => 'Raramente mangia un pasto completo, generalmente mangiala metà dei cibi offerti. Le proteine assunte includono 3 porzioni di carne o latticini al giorno, occasionalmente integratori alimentari.<br>O Riceve meno quantità ottimale di dieta liquida o enterale (con SNG).',
                        'points' => 2,
                    ],
                    [
                        'name' => 'Molto povera',
                        'description' => 'Non mangia mai un pasto completo.<br>Raramente mangia più di 1/3 di qualsiasi cibo offerto. 2 o meno porzioni di proteine al giorno. Assume pochi liquidi e nessun integratore.<br>O E’ a digiuno o mantenuto con fleboclisi o beve bevande per più di 5 giorni.',
                        'points' => 1,
                    ]
                ]
            ],

            [
                'name' => 'Frizionamento e scivolamento',
                'description' => '',
                'position' => 6,
                'choices' => [
                    [
                        'name' => 'Senza problemi apparenti',
                        'description' => 'Si sposta nel letto e sulla sedia in modo autonomo ed ha sufficiente forza muscolare per sollevarsi completamente e durante i movimenti.',
                        'points' => 3,
                    ],
                    [
                        'name' => 'Problema potenziale',
                        'description' => 'Si muove poco e necessita di assistenza minima.<br>Durante lo spostamento la cute fa attrito con le lenzuola o con il piano della poltrona, occasionalmente può slittare.',
                        'points' => 2,
                    ],
                    [
                        'name' => 'Problema',
                        'description' => 'Richiede da una moderata a una massima assistenza nei movimenti.<br>Frequentemente scivola nel letto o nella poltrona.<br>Frequentemente richiede riposizionamenti con la massima assistenza. Sono presenti spasticità, contratture, agitazione, che causano costantemente attrito contro il piano del letto o della poltrona.',
                        'points' => 1,
                    ]
                ]
            ],
        ];


        foreach ($data as $import) {
            $bradenVar = new BradenVar($import);
            $bradenVar->save();
            foreach ($import['choices'] as $choice) {
                $bradenChoice = new BradenChoice($choice);
                $bradenChoice->braden_var_id = $bradenVar->id;
                $bradenChoice->save();
            }
        }
    }


    private function import_departments()
    {
        $text = file_get_contents(base_path('sources/departments.txt'));
        $lines = explode(PHP_EOL, $text);
        foreach ($lines as $line) {
            $tokens = explode('|', $line);

            $name = $tokens[0];

            $code = 'PES8549' . strtoupper( substr($name,0,4) );
            $description = 'Medici: ' . $tokens[1];
            if(isset($tokens[2])){
                $description .= ' - Infierm.: ' . $tokens[2];
            }

            $this->comment("Importing $name, $code, $description");
            Department::create(compact('code','name','description'));
        }

    }


}
