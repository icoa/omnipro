<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use App\Lib\Websocket;
use React\EventLoop\Factory;
use React\ZMQ\Context;
use ZMQ;

class WebsocketServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ws:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Websocket server daemon service';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $env = \App::environment();
        $host = config("roomix.ws.host");
        $port = config("roomix.ws.port");
        $this->info("Starting WebSocket server... [$host:$port]");
        $this->info("Current environment: [$env]");

        $shell = &$this;
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    new Websocket($shell)
                )
            ),
            $port,
            $host
        );
        $server->run();
    }

}
