<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use File;

class CssRewrite extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'css:rewrite';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rewrite path in CSS';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = public_path('themes/admin/assets/build/css');
        $this->comment("Accessing folder [$path]");
        $files = File::files($path);
        foreach($files as $file){
            $content = File::get($file);
            $content = str_replace(['url(../fonts','url(../img'],['url(../../fonts','url(../../img'],$content);
            File::put($file,$content);
            $this->info("[$file] rewrited succesfully");

        }
    }


}
