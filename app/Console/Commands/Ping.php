<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Utils;

class Ping extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ws:ping';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Websocket client ping';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $env = \App::environment();
        $host = config("roomix.ws.client");
        $port = config("roomix.ws.port");
        $this->comment("Connecting WebSocket server... [$host:$port]");
        //$this->comment("Current environment is [$env]");

        \Ratchet\Client\connect("ws://$host:$port")->then(function($conn) {

            $entryData = ['message' => 'pong'];
            $stream = [
                'type' => 'echo',
                'data' => $entryData,
            ];

            $conn->send(json_encode($stream));

            $conn->close();

            $this->info('PONG');

        }, function ($e) {
            $this->error( "Could not connect: {$e->getMessage()}" );
        });

    }

}
