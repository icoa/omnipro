<?php

namespace App\Console\Commands;

use App\Lib\Report;
use Illuminate\Console\Command;
use Mail;

class EmailReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate an HTML report and sends it to an email address';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if(!config('roomix.report.enabled')){
            $this->comment('Sending report is disabled... skipping');
            return;
        }else{
            $this->comment('Sending email report: processing...');
        }

        $recipients = explode(';',config('roomix.report.recipient'));
        $ccn = explode(';',config('roomix.report.ccn'));

        //$params = ['html' => 'Hello World'];

        $from = date('Y-m-01', strtotime( date('Y-m-d') . ' -1 month'));
        $to = date('Y-m-d',strtotime($from.' + 1 month'));
        $this->comment('from: '.$from);
        $this->comment('to: '.$to);

        $report = new Report($from,$to);
        $monthly = $report->monthly();
        $html = '<h1>Report generale</h1>';
        $html .= $report->overall();
        $html .= '<p><br></p><h1>Report mensile</h1>';
        $html .= $monthly;
        //$this->info($html);

        $params = compact('html');

        $bool = Mail::send('emails.report', $params, function ($m) use ($recipients, $params, $ccn) {
            $m->subject('Ge.Co.: Invio Report');
            foreach($recipients as $recipient){
                $recipient = trim($recipient);
                if(filter_var($recipient,FILTER_VALIDATE_EMAIL))
                    $m->to($recipient);
            }
            foreach($ccn as $recipient){
                $recipient = trim($recipient);
                if(filter_var($recipient,FILTER_VALIDATE_EMAIL))
                    $m->bcc($recipient);
            }
            $m->from('geco@icoa.it', 'Ge.Co.');
        });

        if($bool){
            $this->info("Email succesfully sended");
        }else{
            $this->error("Process cannot send email!");
        }

    }



    
}
