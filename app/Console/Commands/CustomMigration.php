<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;
use App\BottleMovement;
use App\Activity;

class CustomMigration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:custom {method}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Perform a custom migration';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $method = $this->argument('method');
        if(method_exists($this,$method)){
            $this->comment("Executing [$method]");
            $this->$method();
        }else{
            $this->error("No [$method] method founded");
        }
    }

    private function users_zones(){
        $rows = User::whereNotNull('zone_id')->get();
        foreach($rows as $user){
            $user->zones()->sync([$user->zone_id]);
            $this->info("User [$user->id] succesfully mapped");
        }
    }

    private function bottle_movements(){
        $builder = Activity::where('id','>=',0);
        $builder->where('target','=','App\BottleOrder');
        $rows = $builder->get();
        BottleMovement::truncate();
        foreach($rows as $row){
            $extra = (object)unserialize($row->extra);

            $bottle_order_id = $extra->id;
            $bottle_id = $extra->bottle_id;
            $order_id = $extra->order_id;
            $quantity = $extra->quantity;
            $original_quantity = $extra->original_quantity;
            $when = $row->created_at;
            $data = compact('bottle_order_id','bottle_id','order_id','quantity','when');

            if($original_quantity > 0){
                $movement_quantity = $original_quantity;
                BottleMovement::register($bottle_order_id,$movement_quantity);
            }
            $diff = abs( abs($quantity) - abs($original_quantity) );
            if($diff > 0){
                $movement_quantity = -$diff;
                BottleMovement::register($bottle_order_id,$movement_quantity);
            }

            $this->info("Registered BottleOrder [$bottle_order_id]");
        }
    }

    private function test(){
        $this->info('test');
    }

    
}
