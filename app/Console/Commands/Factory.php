<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class Factory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:factory';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Return the database to factory standard';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $proceed = $this->confirm("Are you sure you want to return the database to factory mode?",true);
        if($proceed){
            $this->comment("Truncating all tables...");
            DB::statement("SET FOREIGN_KEY_CHECKS=0;");
            DB::table('log_activities')->truncate();
            DB::table('service_events')->truncate();
            DB::table('services')->truncate();
            DB::statement("SET FOREIGN_KEY_CHECKS=1;");
            $this->info('DONE');
        }else{
            $this->comment("Exiting without doing nothing...");
        }
    }

}
