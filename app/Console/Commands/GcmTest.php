<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GcmTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gcm:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test GCM';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $to = "APA91bFIj2WLkD3W4kbZcGO7dyI-TKKX0QpYCwtzqE2cNC0GbnUfQ7_gvQKOUloSb9T-6OZMxKdHXj8biiMYVgRJJP-C6b3PfpC7Kzu4G77PqMeGekHU9W6qTwnu0YTtWGNd6tGMBQka";
        $to = "dV0rjMY09DY:APA91bHc63QP5AZo_s24itaRtJuYIZ5m0ibl7U1N-tG5cTx0FW3RECKjMv_QYOhrc2wOMqPJIK5UJ_6jobnyZRx6QW-vNRBBXkFUfKqBQDe5empeKsfPSBRbrNB9kLq6jEZ6QAPxoGUT";
        $to = "eYTULaSMkl0:APA91bEF4MydbqF7bpxqexHlLkQKCdkQo07rEMvI_fNmxWbzm6rublwopsW2KV2Pdsnb4SOfy0ffUaFI8LvVZgN3iCvN_Vtfs6hEDq-cCIFsgXwDe3OHSkKy0v4CliD_POIQ7zhUPutx";
        $title = "Push Notification Title";
        $message = "Push Notification Message sended on " . date("d-m-Y H:i:s");
        $this->sendPush($to, $title, $message, ['action' => 'test']);
    }

    function sendPush($to, $title, $message, $data = [])
    {
// API access key from Google API's Console
// replace API
        define('API_ACCESS_KEY', 'AIzaSyDz7zWL6A6Vg1yAXU4ku0MRqXQGxGT9lLA');
        $registrationIds = array($to);
        $icon = config('app.url') . '/static/android-icon-96x96.png';
        $msg = array
        (
            'message' => $message,
            'title' => $title,
            'data' => $data,
            'notId' => time(),
            "ledColor" => [0, 0, 0, 255],
            'image' => $icon,
            'iconColor' => 'silver',
            //'vibrate' => 1,
            "vibrationPattern" => [2000, 1000, 500, 500],
            'sound' => 'default'
            // you can also add images, additionalData
        );
        $fields = array
        (
            'registration_ids' => $registrationIds,
            'data' => $msg
        );
        $headers = array
        (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        echo $result;
    }
}
