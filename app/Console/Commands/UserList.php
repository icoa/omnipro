<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;


class UserList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show a list of all users';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $rows = User::with('role')->orderBy('id')->get();
        $headers = ['ID','USERNAME','NAME','ROLE'];
        $data = [];
        foreach($rows as $row){
            $data[] = [$row->id, $row->username, $row->name, $row->role->name];
        }
        $this->table($headers,$data);
        $this->comment("If You want to update the password of a User type the command:");
        $this->comment("php artisan user:pasw [ID]");
        $this->comment("where [ID] is the numeric ID of the User");
    }


}
