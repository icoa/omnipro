<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Product;
use App\Department;

class Tools extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tools:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generic tools runner';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment("Executing tools...");
        $departments = Department::all();
        foreach ($departments as $department){
            $products = $department->id <= 8 ? [1,2,3] : [1];
            $department->products()->sync($products);
        }
        $this->info('-> DONE');
    }

}
