<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use ZMQContext;
use ZMQ;

class ZMQClient extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ws:client';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Websocket client test';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $env = \App::environment();
        $host = config("roomix.ws.host");
        $port = config("roomix.ws.port");
        $this->info("Connecting WebSocket server... [$host:$port]");
        $this->info("Current environment: [$env]");

        // This is our new stuff
        $context = new ZMQContext();
        $socket = $context->getSocket(ZMQ::SOCKET_PUSH);
        $socket->connect("tcp://$host:$port");
        $this->info("Connected to server!");

        $entryData = ['message' => 'Hello there...'];

        $socket->send(json_encode($entryData));
    }

}
