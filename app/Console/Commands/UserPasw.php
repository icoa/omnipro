<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use DB;

class UserPasw extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:pasw {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the password od a User with a given id';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->argument('id');
        if($id == '' OR $id <= 0 OR $id == null){
            $this->error("Invalid ID given. Exiting immediately.");
            return;
        }
        $user = User::find($id);
        if(is_null($user)){
            $this->error("No user founded with id: $id. Cannot proceed.");
            return;
        }
        $this->info("User with id [$id] found | Username is [$user->username]");
        $password = $this->secret("Please enter the password for the given User");

        if($password == '' OR Str::length($password) < 4 OR $password == null){
            $this->error("Invalid PASSWORD given. A password must be at least 4 characters long.");
            return;
        }

        $password2 = $this->secret("Please re-enter the password for confirmation");
        if($password != $password2){
            $this->error("The given password do not match! Cannot proceed.");
            return;
        }

        $hash = Hash::make($password);
        DB::table('users')->where('id',$id)->update(['password' => $hash]);
        $this->info("Password for User [$id] succesfully changed");


    }


}
