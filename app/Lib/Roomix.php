<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 17/09/2015
 * Time: 10:02
 */

namespace App\Lib;

use App\Offer;
use Auth;
use Illuminate\Routing\Router;
use App\Receipt;
use App\Service;
use Request;
use Session;
use Event;
use Theme;
use Cache;

class Roomix
{
    protected $user;
    protected $cache = [];
    protected $theme;

    function __construct()
    {
        $this->user = Auth::check() ? Auth::user()->present() : null;
        Event::listen('locale.changed', function () {
            setlocale(LC_TIME, $this->getPhpLocale(\App::getLocale()));
        });
    }

    function setTheme($theme)
    {
        $this->theme = $theme;
    }

    function getTheme()
    {
        return $this->theme;
    }

    function view($name, $params = [])
    {
        return view('theme.default::views.' . $name, $params);
    }

    function user()
    {
        return $this->user;
    }

    function userId()
    {
        return ($this->user) ? $this->user->id : null;
    }

    function userRole()
    {
        return ($this->user AND $this->user->role_id > 0) ? $this->user->role->code : null;
    }

    function username()
    {
        return ($this->user) ? $this->user->username : 'unknown';
    }

    function theme()
    {
        return ($this->user) ? $this->user->theme : '';
    }

    function configJS()
    {
        $ristomix = new \stdClass();
        if ($this->user) {
            $ristomix->user = [
                'id' => $this->user->id,
                'username' => $this->user->username,
                'role' => $this->userRole(),
            ];
        }

        $ristomix->config = [
            'name' => config('roomix.name'),
            'url' => config('app.url'),
            'lang' => \App::getLocale(),
            'env' => \App::environment(),
            'ws' => "ws://" . config('roomix.ws.client') . ":" . config('roomix.ws.port'),
        ];

        $ristomix->lexicon = trans('javascript');

        $data = json_encode($ristomix, JSON_FORCE_OBJECT | JSON_UNESCAPED_SLASHES);
        return "<script>var Ristomix = $data;</script>";
    }

    function isWindows()
    {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            return true;
        }
        return false;
    }

    function getPhpLocale($lang)
    {
        $locale = 'en_US';
        $isWindows = $this->isWindows();
        switch ($lang) {
            case 'it':
                $locale = $isWindows ? 'ita' : 'it_IT.utf8';
                break;
            case 'en':
                $locale = $isWindows ? 'eng' : 'en_US.utf8';
                break;
        }
        return $locale;
    }

    function forgetPartial($partial, $theme = null)
    {
        $namespace = is_null($theme) ? 'default' : "theme." . $theme;
        $key = "$namespace::partials.$partial";
        \Utils::log($key, __METHOD__);
        Cache::forget($key);
    }

    function ws($type, $data)
    {
        $host = config("roomix.ws.host");
        $port = config("roomix.ws.port");
        $user_id = $this->userId();
        \Ratchet\Client\connect("ws://$host:$port")->then(function ($conn) use($type,$data,$user_id) {
            \Utils::log(compact('type', 'data', 'user_id'),__METHOD__);
            if(is_array($data)){
                $data['emitter_id'] = $user_id;
            }
            if(is_object($data)){
                $data->emitter_id = $user_id;
            }
            $conn->send(json_encode(compact('type', 'data')));
            $conn->close();
        }, function ($e) {
            \Utils::log("Could not connect: {$e->getMessage()}\n", __METHOD__);
        });
    }


}