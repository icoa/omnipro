<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 17/09/2015
 * Time: 10:02
 */

namespace App\Lib;

use Carbon\Carbon;
use Event;
use Log;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Request;

class Utils
{
    protected $isWatching = false;
    protected $canWrite = false;
    protected $errorLog = false;

    function __construct()
    {
        $this->canWrite = config('roomix.writelogs');
        $this->errorLog = new Logger('error_log'); //The logger
        $this->errorLog->pushHandler(new RotatingFileHandler(storage_path('logs/error.log'),60, Logger::WARNING));
    }

    function quote($str)
    {
        return strtr($str, '"', '\"');
    }

    private function _getLog($value, $message = '')
    {
        if (!$this->canWrite) return false;
        $value = $this->_format($value,$message);
        return $value;
    }

    private function _format($value,$message){
        if (is_object($value) || is_array($value))
            $value = print_r($value, true);
        if ($message != '')
            $value = $message . ": " . $value;

        return $value;
    }

    public function log($value, $message = '')
    {
        $m = $this->_getLog($value, $message);
        if ($m != false) {
            Log::info($m);
        }
    }

    public function warning($value, $message = '')
    {
        $value = $this->_format($value,$message);
        $this->errorLog->addWarning($value);
    }

    public function error($value, $message = '')
    {
        $value = $this->_format($value,$message);
        $this->errorLog->addError($value);
    }

    public function logSql(&$queryObj, $message = '')
    {
        if (!$this->canWrite) return false;
        try {
            $queryObj->explain();
        } catch (\Exception $ex) {
            Log::info($ex->getMessage());
        }
    }


    public function watch()
    {
        if (!$this->canWrite OR $this->isWatching) return;
        Utils::log("Calling Utils::watch");
        $this->isWatching = true;
        Event::listen('illuminate.query', function ($sql, $bindings) {
            foreach ($bindings as $binding) {
                $binding = \DB::connection()->getPdo()->quote($binding);
                $sql = preg_replace('/\?/', $binding, $sql, 1);
                //$sql = htmlspecialchars($sql);
            }
            Log::info('SQL WATCH: ' . $sql);
        });
    }

    public function unwatch()
    {
        Event::forget('illuminate.query');
        $this->isWatching = false;
    }

    function elixir($file, $theme)
    {
        static $manifest = null;

        if (is_null($manifest))
        {
            $manifest = json_decode(file_get_contents(public_path()."/themes/$theme/assets/build/rev-manifest.json"), true);
        }

        if (isset($manifest[$file]))
        {
            return 'build/'.$manifest[$file];
        }

        throw new InvalidArgumentException("File {$file} not defined in asset manifest.");
    }

    function activeState($route, $active = 'active', $exact = false) {
        if($exact){
            return Request::url() == route($route) ? $active : null;
        }
        return strpos(Request::url(), route($route)) !== false ? $active : '';
    }

    function now(){
        return date('Y-m-d H:i:s');
    }

    function euro($v){
        return '&euro; ' . number_format($v,2,'.','');
    }

    function mysql_date($date, $return = false, $format = 'd-m-Y'){
        if($date == '' or is_null($date))
            return null;

        if($date instanceof Carbon){
            return ($return) ? $date : $date->format('Y-m-d');
        }

        $carbon = Carbon::createFromFormat($format,$date);
        return ($return) ? $carbon : $carbon->format('Y-m-d');
    }

}