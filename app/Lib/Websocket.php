<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 19/05/2016
 * Time: 16:45
 */

namespace App\Lib;
use Illuminate\Console\Command;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use SplObjectStorage;
use Exception;
use Utils;


class Websocket implements MessageComponentInterface
{
    protected $clients;
    protected $users = [];
    protected $userConnections = [];

    private $shell;



    public function __construct(Command $shell)
    {
        $this->clients = new SplObjectStorage;
        $this->shell = $shell;
        $shell->info('WebSocket server is listening...');
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->shell->comment("New connection! ({$conn->resourceId})");
        $this->clients->attach($conn);
    }

    public function onMessage(ConnectionInterface $from, $data)
    {
        $this->shell->comment("onMessage");
        $this->shell->comment($data);
        $id = $from->resourceId;
        $data = json_decode($data, true);
        if (isset($data['data']) && count($data['data']) != 0) {
            $type = $data['type'];
            $received = (object)$data['data'];
            $this->shell->comment("messageType: $type");
            Utils::log($data, __METHOD__);
            $method = "__$type";
            if(method_exists($this,$method)){
                $this->$method($from,$received);
            }
        }
    }



    public function onClose(ConnectionInterface $conn)
    {
        $this->shell->comment("onClose");
        if (isset($this->users[$conn->resourceId])) {
            unset($this->users[$conn->resourceId]);
        }
        if (isset($this->userConnections[$conn->resourceId])) {
            unset($this->userConnections[$conn->resourceId]);
        }
        $this->clients->detach($conn);
    }

    public function onError(ConnectionInterface $conn, Exception $e)
    {
        $this->shell->comment("onError");
        $this->shell->comment($e->getMessage());
        Utils::log($e->getTraceAsString());
        $conn->close();
    }


    public function send($client, $type, $data)
    {
        $this->shell->comment(__METHOD__ . " [$type]");
        $send = array(
            "type" => $type,
            "data" => $data
        );
        $send = json_encode($send, true);
        $client->send($send);
        return true;
    }

    public function getConnectionByUserId($user_id){
        foreach($this->users as $connectionId => $user){
            if($user_id == $user->id)
                return $this->userConnections[$connectionId];
        }
        return null;
    }

    public function getConnectionsByUserRole($role){
        $connections = [];
        foreach($this->users as $connectionId => $user){
            if($role == $user->role)
                $connections[] = $this->userConnections[$connectionId];
        }
        return $connections;
    }


    public function sendToUser($user_id,$type,$data){
        $connection = $this->getConnectionByUserId($user_id);
        if($connection){
            return $this->send($connection,$type,$data);
        }
        Utils::log("No connection found for user [$user_id]",__METHOD__);
    }

    public function sendToRole($role,$type,$data){
        $connections = $this->getConnectionsByUserRole($role);
        $many = count($connections);
        Utils::log("Found $many connections",__METHOD__);
        foreach($connections as $connection){
            $this->send($connection,$type,$data);
        }
    }



    private function __register(ConnectionInterface $from, $data){
        $id = $from->resourceId;
        Utils::log($data,__METHOD__);
        $this->userConnections[$id] = $from;
        $this->users[$id] = $data;
    }


    private function __echo(ConnectionInterface $from, $data){
        $id = $from->resourceId;
        $this->send($from, "echo", $data);
    }


    private function __single(ConnectionInterface $from, $data){
        $id = $from->resourceId;
        $this->sendToRole('kitchen','single',$data);
    }


    private function __OrderWasCreated(ConnectionInterface $from, $data){
        $this->sendToRole('kitchen','OrderWasCreated',$data);
        $this->sendToRole('waiter','OrderWasCreated',$data);
    }

    private function __NegativeOrderWasCreated(ConnectionInterface $from, $data){
        $this->sendToRole('cash','NegativeOrderWasCreated',$data);
        //$this->sendToRole('waiter','NegativeOrderWasCreated',$data);
    }

    private function __ServiceWasCheckedOut(ConnectionInterface $from, $data){
        $this->sendToRole('cash','ServiceWasCheckedOut',$data);
        $this->sendToRole('waiter','ServiceWasCheckedOut',$data);
    }

    private function __ServiceWasClosed(ConnectionInterface $from, $data){
        $this->sendToRole('cash','ServiceWasClosed',$data);
        $this->sendToRole('waiter','ServiceWasClosed',$data);
    }

    private function __ServiceWasCreated(ConnectionInterface $from, $data){
        $this->sendToRole('cash','ServiceWasCreated',$data);
        $this->sendToRole('waiter','ServiceWasCreated',$data);
    }

    private function __ServiceWasCompleted(ConnectionInterface $from, $data){
        $this->sendToRole('waiter','ServiceWasCompleted',$data);
    }

    private function __ServiceWasAccepted(ConnectionInterface $from, $data){
        $this->sendToRole('waiter','ServiceWasAccepted',$data);
    }

    private function __ServiceWasUpdated(ConnectionInterface $from, $data){
        $this->sendToRole('cash','ServiceWasUpdated',$data);
        $this->sendToRole('waiter','ServiceWasUpdated',$data);
        $this->sendToRole('kitchen','ServiceWasUpdated',$data);
    }

    private function __BottleWasDone(ConnectionInterface $from, $data){
        $this->sendToRole('cash','BottleWasDone',$data);
    }

    private function __OrderWasCompleted(ConnectionInterface $from, $data){
        $this->sendToRole('cash','OrderWasCompleted',$data);
        //$this->sendToRole('waiter','OrderWasCompleted',$data);
    }

}