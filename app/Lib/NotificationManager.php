<?php
/**
 * Created by PhpStorm.
 * User: e.purificato
 * Date: 22/09/2015
 * Time: 15:02
 */

namespace App\Lib;

use App\Facades\Roomix;
use App\Jobs\SendPushNotification;
use Exception;
use App\Notification;
use App\Demand;
use App\Role;
use App\PushNotification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class NotificationManager
{

    protected $demand;

    protected $user;

    /**
     * NotificationManager constructor.
     * @param Service|null $service
     */
    function __construct(Demand $demand = null)
    {
        $this->demand = $demand;
        $this->user = Auth::user();
    }


    static function check($passedCounter){
        $user = Auth::user();
        $data = ['notify' => false];
        $actualCounter = $user->countNotificationsNotRead();
        $notifications = [];
        if($actualCounter > $passedCounter){
            $rows = $user->notifications()->orderBy('created_at','desc')->take( $actualCounter - $passedCounter)->get();
            foreach($rows as $row){
                $notifications[] = ['title' => $row->title, 'message' => $row->message, 'id' => $row->id];
            }
            if(count($notifications) > 0){
                $data['notify'] = true;
                $data['count'] = $actualCounter;
            }
        }
        $data['notifications'] = $notifications;
        return $data;
    }


    function newDemand()
    {

        try {
            $this->demandHasBeenCreated();

        } catch (Exception $e) {
            \Utils::log($e->getMessage());
        }

    }

    function updateDemand()
    {
        $demand = $this->demand;
        $user = $this->user;
        $role = $user->role->code;
        try {
            $present = $demand->present();
            //status has changed
            if ($demand->isDirty('status')) {

                //and the current user is a waiter
                if ($role == Role::ROLE_WAITER) {
                    switch ($demand->status) {

                        case Demand::STATUS_ACCEPTED:
                            $this->demandHasBeenAccepted();
                            break;

                        case Demand::STATUS_ORDERED:
                            $this->demandHasBeenOrdered();
                            break;

                        case Demand::STATUS_ROUTED:
                            $this->demandHasBeenRouted();
                            break;

                        case Demand::STATUS_CLOSED:
                            $this->demandHasBeenClosed();
                            break;
                    }
                }

                //and the current user is a production center
                if ($role == Role::ROLE_PRODUCTION) {
                    switch ($demand->status) {

                        case Demand::STATUS_DELIVERED:
                            $this->demandHasBeenDelivered();
                            break;

                    }
                }

            }


            //assigner has changed
            if ($demand->isDirty('assigned_by')) {
                //and the current user is a production center
                if ($role == Role::ROLE_PRODUCTION) {
                    switch ($demand->status) {

                        case Demand::STATUS_ASSIGNED:
                            $this->demandHasBeenAssigned();
                            break;

                    }
                }
            }

        } catch (Exception $e) {
            \Utils::log($e->getMessage());
        }

    }





    private function demandHasBeenCreated()
    {
        $present = null;
        $demand = $this->demand;
        $present = $this->demand->present();
        $present->forceLocale();

        //to all waiters in the selected zone
        Notification::init()
            ->title('Richiesta assistenza ' . $present->name)
            ->message("Il Cliente {$present->customerName} (Camera {$present->roomName}) ha richiesto un'assistenza")
            ->params($this->demand->toArray())
            ->url(route('cordova.demands.show', $demand->id))
            ->fromMe()
            ->sendToZone($demand->zone_id, 'waiter');

        //to all production centre in the selected zone
        Notification::init()
            ->title('Richiesta assistenza ' . $present->name)
            ->message("Il Cliente {$present->customerName} (Camera {$present->roomName}) ha richiesto un'assistenza")
            ->params($this->demand->toArray())
            ->url(route('cordova.demands.show', $demand->id))
            ->fromMe()
            ->sendToZone($demand->zone_id, 'production');

        //..and to maitre
        Notification::init()
            ->title("Nuova Richiesta di assistenza {$present->code} da {$present->name}")
            ->message("Il Cliente {$present->customerName} (Camera {$present->roomName}) ha inviato una richiesta di assistenza dalla postazione {$present->name}")
            ->params($this->demand->toArray())
            ->url(route('admin.demands.show', $demand->id))
            ->fromMe()
            ->sendToRole('maitre');

        //send a push notification here
        $push = PushNotification::init()
            ->title('Chiamata ' . $present->name)
            ->message("$present->name per {$present->customerName} (Camera {$present->roomName})")
            ->params(['action' => 'demands.show'])
            ->url(route('cordova.demands.show', $demand->id))
            ->toZone($demand->zone_id, 'waiter')
            ->send();

        app('Illuminate\Contracts\Bus\Dispatcher')->dispatch( new SendPushNotification( $push ));


        if ($present) {
            $present->revertLocale();
        }
    }


    private function demandHasBeenAccepted()
    {
        $demand = $this->demand;
        $present = $demand->present();
        //the waiter has accepted the demand, send notification to customer...

        $locale = $demand->locale;

        Notification::init()
            ->title( trans('notifications.demand.accepted.title', [], null, $locale) )
            ->message( trans('notifications.demand.accepted.msg', ['name' => $present->assignerName], null, $locale) )
            ->params($this->demand->toArray())
            ->url(route('demands.show', $demand->id))
            ->fromMe()
            ->toCustomer($demand->customer_id)
            ->send();

        //...and all the other waiters of the same zone...
        Notification::init()
            ->title("Richiesta {$present->name} accettata")
            ->message("La richiesta del Cliente {$present->customerName} (Camera {$present->roomName}) è stata presa in carico da {$present->assignerName}")
            ->params($this->demand->toArray())
            ->url(route('cordova.demands.show', $demand->id))
            ->fromMe()
            ->sendToZone($demand->zone_id, 'waiter');

        //..and to maitre
        Notification::init()
            ->title("Richiesta {$present->code} accettata")
            ->message("La richiesta del Cliente {$present->customerName} (Camera {$present->roomName}) è stata presa in carico da {$present->assignerName}")
            ->params($this->demand->toArray())
            ->url(route('admin.demands.show', $demand->id))
            ->fromMe()
            ->sendToRole('maitre');

        //send a push notification here
        $push = PushNotification::init()
            ->title('Accettata ' . $present->name)
            ->message("$present->name richiesta accettata da {$present->assignerName}")
            ->params(['action' => 'demands.show'])
            ->url(route('cordova.demands.show', $demand->id))
            ->fromMe()
            ->toZone($demand->zone_id, 'waiter')
            ->send();

        app('Illuminate\Contracts\Bus\Dispatcher')->dispatch( new SendPushNotification( $push ));
    }



    private function demandHasBeenAssigned()
    {
        $demand = $this->demand;
        $present = $demand->present();
        //the production centre has assigned the demand to a waiter, send notification to customer...

        $locale = $demand->locale;
        $username = \Roomix::username();

        Notification::init()
            ->title( trans('notifications.demand.accepted.title', [], null, $locale) )
            ->message( trans('notifications.demand.accepted.msg', ['name' => $present->assignerName], null, $locale) )
            ->params($this->demand->toArray())
            ->url(route('demands.show', $demand->id))
            ->fromMe()
            ->toCustomer($demand->customer_id)
            ->send();

        //...and all the other waiters of the same zone...
        Notification::init()
            ->title("Richiesta {$present->name} assegnata")
            ->message("La richiesta del Cliente {$present->customerName} (Camera {$present->roomName}) è stata assegnata a {$present->assignerName} dal centro di produzione $username")
            ->params($this->demand->toArray())
            ->url(route('cordova.demands.show', $demand->id))
            ->fromMe()
            ->sendToZone($demand->zone_id, 'waiter');

        //..and to maitre
        Notification::init()
            ->title("Richiesta {$present->code} assegnata")
            ->message("La richiesta del Cliente {$present->customerName} (Camera {$present->roomName}) è stata assegnata a {$present->assignerName} dal centro di produzione $username")
            ->params($this->demand->toArray())
            ->url(route('admin.demands.show', $demand->id))
            ->fromMe()
            ->sendToRole('maitre');

        //send a push notification here
        $push = PushNotification::init()
            ->title('Assegnata ' . $present->name)
            ->message("$present->name richiesta assegnata a {$present->assignerName}")
            ->params(['action' => 'demands.show'])
            ->url(route('cordova.demands.show', $demand->id))
            ->fromMe()
            ->toZone($demand->zone_id, 'waiter')
            ->send();

        app('Illuminate\Contracts\Bus\Dispatcher')->dispatch( new SendPushNotification( $push ));
    }


    private function demandHasBeenOrdered()
    {
        $demand = $this->demand;
        $present = $demand->present();
        //the waiter has sended the demand to the production center, send notification to production center...
        Notification::init()
            ->title("Nuova comanda da {$present->name}")
            ->message("{$present->assignerName} ha inviato una comanda dalla postazione {$present->name}, per il Cliente {$present->customerName} (Camera {$present->roomName})")
            ->params($this->demand->toArray())
            ->url(route('admin.commands.show', $demand->id))
            ->fromMe()
            ->sendToZone($demand->zone_id, 'production');

        //..and to maitre
        Notification::init()
            ->title("Richiesta {$present->code} inoltrata al centro di produzione")
            ->message("La richiesta del Cliente {$present->customerName} (Camera {$present->roomName}) è stata inviata da {$present->assignerName} dalla postazione {$present->name} al centro di produzione della zona")
            ->params($this->demand->toArray())
            ->url(route('admin.demands.show', $demand->id))
            ->fromMe()
            ->sendToRole('maitre');
    }


    private function demandHasBeenRouted()
    {
        $demand = $this->demand;
        $present = $demand->present();
        //the waiter has sended the confirmation for the delivery, send notification to production center...
        Notification::init()
            ->title("Comanda {$present->name} in consegna")
            ->message("{$present->delivererName} ha confermato la richiesta di consegna della comanda alla postazione {$present->name}, per il Cliente {$present->customerName} (Camera {$present->roomName})")
            ->params($this->demand->toArray())
            ->url(route('admin.commands.show', $demand->id))
            ->fromMe()
            ->sendToZone($demand->zone_id, 'production');

        //..and to maitre
        Notification::init()
            ->title("Comanda {$present->code} in consegna")
            ->message("La richiesta del Cliente {$present->customerName} (Camera {$present->roomName}) è in consegna da parte di {$present->delivererName} per la postazione {$present->name}")
            ->params($this->demand->toArray())
            ->url(route('admin.demands.show', $demand->id))
            ->fromMe()
            ->sendToRole('maitre');

        //..and to the customer
        $locale = $demand->locale;

        Notification::init()
            ->title( trans('notifications.demand.routed.title', [], null, $locale) )
            ->message( trans('notifications.demand.routed.msg', ['name' => $present->delivererName], null, $locale) )
            ->params($this->demand->toArray())
            ->url(route('demands.show', $demand->id))
            ->fromMe()
            ->toCustomer($demand->customer_id)
            ->send();
    }


    private function demandHasBeenClosed()
    {
        $demand = $this->demand;
        $present = $demand->present();
        //the waiter has sended the confirmation for the delivery, send notification to production center...
        Notification::init()
            ->title("Ordine {$present->name} consegnato")
            ->message("{$present->delivererName} ha consegnato l'ordine alla postazione {$present->name}, per il Cliente {$present->customerName} (Camera {$present->roomName})")
            ->params($this->demand->toArray())
            ->url(route('admin.commands.show', $demand->id))
            ->fromMe()
            ->sendToZone($demand->zone_id, 'production');

        //..and to maitre
        Notification::init()
            ->title("Ordine {$present->code} consegnato")
            ->message("L'ordine del Cliente {$present->customerName} (Camera {$present->roomName}) è stato consegnato da {$present->delivererName} alla postazione {$present->name}")
            ->params($this->demand->toArray())
            ->url(route('admin.demands.show', $demand->id))
            ->fromMe()
            ->sendToRole('maitre');
    }


    private function demandHasBeenDelivered()
    {
        $demand = $this->demand;
        $present = $demand->present();
        //the production has selected a waiter to send a delivery request, send notification to single waiter
        Notification::init()
            ->title("Richiesta consegna {$present->name}")
            ->message("Il centro di produzione della tua zona ti ha inviato una richiesta di consegna per la postazione {$present->name}, del Cliente {$present->customerName} (Camera {$present->roomName})")
            ->params($this->demand->toArray())
            ->url(route('cordova.delivers.show', $demand->id))
            ->fromMe()
            ->toUser($demand->delivered_by)
            ->send();

        //..and to maitre
        Notification::init()
            ->title("La comanda {$present->code} è pronta ed è stata inviata una richiesta di consegna")
            ->message("La comanda del Cliente {$present->customerName} (Camera {$present->roomName}) è pronta, ed è stata inviata una richiesta di consegna dal centro di produzione di {$present->zoneName} a {$present->delivererName}")
            ->params($this->demand->toArray())
            ->url(route('admin.demands.show', $demand->id))
            ->fromMe()
            ->sendToRole('maitre');

        //send a push notification here
        $push = PushNotification::init()
            ->title('Consegna ' . $present->name)
            ->message("$present->name richiesta consegna per {$present->customerName} (Camera {$present->roomName})")
            ->params(['action' => 'delivers.show'])
            ->url(route('cordova.delivers.show', $demand->id))
            ->fromMe()
            ->toUser($demand->delivered_by)
            ->send();

        app('Illuminate\Contracts\Bus\Dispatcher')->dispatch( new SendPushNotification( $push ));
    }

}