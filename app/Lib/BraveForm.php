<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 25/07/2016
 * Time: 15:01
 */

namespace App\Lib;
use AdamWathan\BootForms\BootForm;
use AdamWathan\BootForms\BasicFormBuilder;

class BraveForm extends BootForm
{
    public function __construct(BasicFormBuilder $basicFormBuilder, HorizontalBraveFormBuilder $horizontalFormBuilder)
    {
        $this->basicFormBuilder = $basicFormBuilder;
        $this->horizontalFormBuilder = $horizontalFormBuilder;
    }
}