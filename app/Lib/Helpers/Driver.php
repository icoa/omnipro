<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 03/10/2016
 * Time: 13:27
 */

namespace App\Lib\Helpers;


class Driver
{
    public function routes()
    {
        return [
            'main' => [
                [
                    'title' => 'Servizi - Ultimi 30 giorni',
                    'icon' => 'info',
                    'url' => route('services.index') . '?mode=latest',
                    'class' => 'ajax',
                    'data' => ['data-ignore-cache="true"'],
                ],
                [
                    'title' => 'Tutti i servizi',
                    'icon' => 'assignment',
                    'url' => route('services.index'),
                    'class' => 'ajax',
                    'data' => ['data-ignore-cache="true"'],
                ],
                [
                    'title' => 'Ricerca servizio',
                    'icon' => 'search',
                    'url' => route('services.barcode'),
                    'class' => 'ajax',
                    'data' => ['data-ignore-cache="true"'],
                ]
            ],
            'toolbar' => [
                [
                    'title' => 'Aiuto',
                    'icon' => 'help',
                    'url' => route('pages.help'),
                    'class' => 'ajax',
                    'data' => ['data-ignore-cache="true"'],
                ],
                [
                    'title' => 'About',
                    'icon' => 'info',
                    'url' => route('pages.about'),
                    'class' => 'ajax',
                    'data' => ['data-ignore-cache="true"'],
                ]
            ],
        ];
    }
}