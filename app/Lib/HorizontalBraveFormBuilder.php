<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 25/07/2016
 * Time: 15:25
 */

namespace App\Lib;
use AdamWathan\BootForms\HorizontalFormBuilder;

class HorizontalBraveFormBuilder extends HorizontalFormBuilder
{
    protected function getControlSizes()
    {
        $controlSizes = [];
        foreach ($this->columnSizes as $breakpoint => $sizes) {
            $controlSizes[$breakpoint] = $sizes[1];
        }
        return $controlSizes;
    }
}