<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 30/09/2016
 * Time: 17:29
 */

namespace App\Lib;

use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampServerInterface;
use Illuminate\Console\Command;
use Utils;

class Pusher implements WampServerInterface
{
    private $command;

    function __construct(Command $command)
    {
        $this->command = $command;
    }

    public function onSubscribe(ConnectionInterface $conn, $topic)
    {
        $this->command->comment(__METHOD__);
        Utils::log(__METHOD__);
    }

    public function onUnSubscribe(ConnectionInterface $conn, $topic)
    {
        $this->command->comment(__METHOD__);
        Utils::log(__METHOD__);
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->command->comment(__METHOD__);
        Utils::log(__METHOD__);
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->command->comment(__METHOD__);
        Utils::log(__METHOD__);
    }

    public function onCall(ConnectionInterface $conn, $id, $topic, array $params)
    {
        $this->command->comment(__METHOD__);
        Utils::log(__METHOD__);
        return;
        // In this application if clients send data it's because the user hacked around in console
        $conn->callError($id, $topic, 'You are not allowed to make calls')->close();
    }

    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible)
    {
        $this->command->comment(__METHOD__);
        Utils::log(__METHOD__);
        return;
        // In this application if clients send data it's because the user hacked around in console
        $conn->close();
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $this->command->comment(__METHOD__);
        Utils::log(__METHOD__);
        return;
    }

    /**
     * @param string JSON'ified string we'll receive from ZeroMQ
     */
    public function onEntry($message) {
        $this->command->comment(__METHOD__);
        Utils::log(__METHOD__);
        return;
    }
}