<?php

namespace App\Lib\FormBuilder\Form\Facades;

use Illuminate\Support\Facades\Facade;

class Form extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'App\Lib\FormBuilder.form';
    }
}
