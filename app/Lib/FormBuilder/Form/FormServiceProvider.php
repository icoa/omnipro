<?php

namespace App\Lib\FormBuilder\Form;

use App\Lib\FormBuilder\Form\ErrorStore\IlluminateErrorStore;
use App\Lib\FormBuilder\Form\OldInput\IlluminateOldInputProvider;
use Illuminate\Support\ServiceProvider;

class FormServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function register()
    {
        $this->registerErrorStore();
        $this->registerOldInput();
        $this->registerFormBuilder();
    }

    protected function registerErrorStore()
    {
        $this->app->singleton('App\Lib\FormBuilder.form.errorstore', function ($app) {
            return new IlluminateErrorStore($app['session.store']);
        });
    }

    protected function registerOldInput()
    {
        $this->app->singleton('App\Lib\FormBuilder.form.oldinput', function ($app) {
            return new IlluminateOldInputProvider($app['session.store']);
        });
    }

    protected function registerFormBuilder()
    {
        $this->app->singleton('App\Lib\FormBuilder.form', function ($app) {
            $formBuilder = new FormBuilder;
            $formBuilder->setErrorStore($app['App\Lib\FormBuilder.form.errorstore']);
            $formBuilder->setOldInputProvider($app['App\Lib\FormBuilder.form.oldinput']);
            $formBuilder->setToken($app['session.store']->token());

            return $formBuilder;
        });
    }

    public function provides()
    {
        return ['App\Lib\FormBuilder.form'];
    }
}
