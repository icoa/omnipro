<?php

namespace App\Lib\FormBuilder\Form\Elements;

class Email extends Text
{
    protected $attributes = [
        'type' => 'email',
    ];
}
