<?php

namespace App\Lib\FormBuilder\Form\Elements;

class Hidden extends Input
{
    protected $attributes = [
        'type' => 'hidden',
    ];
}
