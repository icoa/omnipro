<?php

namespace App\Lib\FormBuilder\Form\Elements;

class File extends Input
{
    protected $attributes = [
        'type' => 'file',
    ];
}
