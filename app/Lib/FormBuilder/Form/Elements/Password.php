<?php

namespace App\Lib\FormBuilder\Form\Elements;

class Password extends Text
{
    protected $attributes = [
        'type' => 'password',
    ];
}
