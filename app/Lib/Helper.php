<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 17/09/2015
 * Time: 10:02
 */

namespace App\Lib;

use App\Department;
use App\Service;
use App\User;
use App\Role;
use App\State;
use App\City;
use App\District;
use App\Format;
use App\Beverage;
use App\Bottle;
use App\Product;

class Helper
{

    function activeOptions()
    {
        return [1 => 'Attivo', 0 => 'Disattivo'];
    }

    function booleanOptions()
    {
        return ['' => 'Scegli...', 1 => 'SI', 0 => 'NO'];
    }

    function localesOptions()
    {
        return ['it' => 'Italiano', 'en' => 'Inglese'];
    }

    function usernames()
    {
        return User::orderBy('username')->lists('username', 'username');
    }

    function admins()
    {
        return User::orderBy('username')->whereNotIn('role_id', [2])->lists('username', 'username');
    }

    function usernamesByRole($role_id)
    {
        return is_array($role_id) ? User::whereIn('role_id', $role_id)->orderBy('username')->lists('username', 'username') : User::where('role_id', $role_id)->orderBy('username')->lists('username', 'username');
    }

    function rolesOptions($withEmpty = false)
    {
        $data = [];
        if ($withEmpty) {
            $data[''] = 'Seleziona un ruolo...';
        }
        $rows = Role::lists('name', 'id')->toArray();
        $data = $data + $rows;
        return $data;
    }

    function yesNo()
    {
        return [0 => 'NO', 1 => 'SI'];
    }

    function usersByRole($role)
    {
        $data = [];
        $role = Role::where('code', $role)->first();
        $users = $role->users()->orderBy('name')->get();
        foreach ($users as $user) {
            $data[$user->id] = "$user->name ($user->username)";
        }
        return $data;
    }


    function departmentsOptions($withEmpty = false, $label = 'Seleziona un dipartimento...')
    {
        $user = \Roomix::user();
        $data = [];
        $builder = Department::orderBy('name');
        if ($user->role->code == 'user') {
            $builder->whereIn('departments.id', $user->department_options);
        }
        $rows = $builder->get();

        if ($withEmpty) {
            $data[''] = $label;
        }
        foreach ($rows as $row) {

            $data[$row->id] = $row->name;
        }
        return $data;
    }

    function allDepartmentsOptions($withEmpty = false, $label = 'Seleziona un dipartimento...')
    {
        $user = \Roomix::user();
        $data = [];
        $builder = Department::orderBy('name');
        if ($user->role->code == 'user') {
            $builder->whereNotIn('departments.id', $user->department_options);
        }
        $rows = $builder->get();

        if ($withEmpty) {
            $data[''] = $label;
        }
        foreach ($rows as $row) {
            $data[$row->id] = $row->name;
        }
        return $data;
    }


    function serviceDisactivableOptions($withEmpty = true, $label = 'Seleziona una posizione...')
    {

        $data = [];
        $builder = Service::user()->withType(Service::TYPE_ACTIVATION)->orderBy('created_at', 'desc');

        $rows = $builder->get();

        if ($withEmpty) {
            $data[''] = $label;
        }
        foreach ($rows as $row) {
            $status = $row->status;
            $label = "SDO: $row->sdo, Paziente: $row->patient_lastname" . "." . "$row->patient_name" . ".";
            //Un servizio può essere disattivato solo se è stato confermato (dunque anche se è in consegna o già arrivato, non prima)

            if ($status == Service::STATUS_DELIVERED) {

                $data[$row->id] = $label;

            }
        }
        return $data;
    }

    function serviceMaintenanceableOptions($withEmpty = true, $label = 'Seleziona una posizione...')
    {


        $data = [];
        $builder = Service::user()->withType(Service::TYPE_ACTIVATION)->orderBy('event_at', 'desc');

        $rows = $builder->get();

        if ($withEmpty) {
            $data[''] = $label;
        }

        foreach ($rows as $row) {
            $status = $row->status;
            $type = $row->type;
            $label = "SDO: $row->sdo, Paziente: $row->patient_lastname" . "." . "$row->patient_name" . ".";

            switch ($type) {
                case Service::TYPE_ACTIVATION : {//TODO

                    //un'attivazione può essere manutenuta solo se è arrivata
                    if ($status == Service::STATUS_DELIVERED) {

                        $data[$row->id] = $label;

                    }

                };
                    break;

                case Service::TYPE_DISACTIVATION : {
                    //Una disattivazione può essere mandata in manutenzione solo se la data di disattivazione è posteriore alla data di manutenzione
                    $data[$row->id] = $label;

                };
                    break;

                default : {
                } //TODO altri casi

            }

            //Non si può richiedere la manutenzione di un servizio che non è ancora arrivato


        }
        return $data;
    }

    function serviceTransferableOptions($withEmpty = true, $label = 'Seleziona una posizione...')
    {

        $data = [];

        $builder = Service::user()->withType(Service::TYPE_ACTIVATION)->orderBy('event_at', 'desc');

        $rows = $builder->get();

        if ($withEmpty) {
            $data[''] = $label;
        }
        foreach ($rows as $row) {
            //Non si può richiedere il trasferimento di un servizio che non è ancora arrivato
            $status = $row->status;
            $label = "SDO: $row->sdo, Paziente: $row->patient_lastname" . "." . "$row->patient_name" . ".";
            if ($status == Service::STATUS_DELIVERED) {

                $data[$row->id] = $label;

            }
        }
        return $data;
    }


    function serviceStatusOptions()
    {

        $data = [
            Service::STATUS_REQUESTED => trans('service.statuses.' . Service::STATUS_REQUESTED),
            Service::STATUS_CONFIRMED => trans('service.statuses.' . Service::STATUS_CONFIRMED),
            Service::STATUS_REJECTED => trans('service.statuses.' . Service::STATUS_REJECTED),
            Service::STATUS_SHIPPING => trans('service.statuses.' . Service::STATUS_SHIPPING),
            Service::STATUS_DELIVERED => trans('service.statuses.' . Service::STATUS_DELIVERED),
            Service::STATUS_REVERTED => trans('service.statuses.' . Service::STATUS_REVERTED),
        ];

        return $data;
    }

    function serviceTypesOptions()
    {

        $data = [
            Service::TYPE_ACTIVATION => trans('service.types.' . Service::TYPE_ACTIVATION),
            Service::TYPE_DISACTIVATION => trans('service.types.' . Service::TYPE_DISACTIVATION),
            Service::TYPE_MAINTENANCE => trans('service.types.' . Service::TYPE_MAINTENANCE),
            Service::TYPE_TRANSFER => trans('service.types.' . Service::TYPE_TRANSFER),

        ];

        return $data;
    }



    function productOptions($withEmpty = false, $label = 'Seleziona un modello...')
    {
        $user = \Roomix::user();
        $data = [];
        $builder = Product::orderBy('name');

        if ($user->role_id != 2 and $user->role_id != 1) { //Admin == 1; Dec == 2

            $ids = [];
            foreach ($user->departments as $department) {
                $ids = array_merge($ids, $department->products->lists('id')->toArray());
            }
            $builder->whereIn('id', $ids);

        }

        $rows = $builder->get();

        if ($withEmpty) {
            $data[''] = $label;
        }
        foreach ($rows as $row) {
            $data[$row->id] = $row->name;
        }
        return $data;
    }

    function statesOptionsIds($withEmpty = false, $label = 'Seleziona una provincia...')
    {
        \Utils::watch();
        $data = [];
        $builder = State::orderBy('name');
        $rows = $builder->get();

        if ($withEmpty) {
            $data[''] = $label;
        }
        foreach ($rows as $row) {

            $data[$row->id] = $row->name;
        }
        return $data;
    }


    function statesOptions($withEmpty = false, $label = 'Seleziona una provincia...')
    {
        $data = [];
        $builder = State::orderBy('priority', 'desc')->orderBy('name');
        $rows = $builder->get();

        if ($withEmpty) {
            $data[''] = $label;
        }
        foreach ($rows as $row) {
            $data[$row->code] = $row->name;
        }
        return $data;
    }

    function citiesOptions($state = null, $withEmpty = false, $label = 'Seleziona un comune...')
    {
        $data = [];
        \Utils::watch();
        if ($state) {
            $builder = City::orderBy('priority', 'desc')->orderBy('name');
            $builder->where('state_id', function ($query) use ($state) {
                $query->select('id')->from('states')->where('code', $state);
            });
            $rows = $builder->get();
        } else {
            $rows = [];
        }
        \Utils::unwatch();

        if ($withEmpty) {
            $data[''] = $label;
        }
        foreach ($rows as $row) {
            $data[$row->id] = $row->name;
        }
        return $data;
    }

    function districtsOptions($state = null, $withEmpty = false, $label = 'Seleziona una regione...')
    {
        $data = [];
        \Utils::watch();
        if ($state) {
            $builder = District::orderBy('name');
            $builder->where('id', function ($query) use ($state) {
                $query->select('district_id')->from('states')->where('code', $state);
            });
            $rows = $builder->get();
        } else {
            $rows = [];
        }
        \Utils::unwatch();

        if ($withEmpty) {
            $data[''] = $label;
        }
        foreach ($rows as $row) {
            $data[$row->id] = $row->name;
        }
        return $data;
    }


}