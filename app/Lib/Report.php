<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 07/02/2017
 * Time: 17:26
 */

namespace App\Lib;

use App\Activity;
use App\Bottle;
use DB;


class Report
{
    protected $from;
    protected $to;

    function __construct($from,$to)
    {
        $this->from = $from;
        $this->to = $to;
    }


    function overall(){
        $this->from = Activity::min('created_at');
        return $this->getServices();
    }


    function monthly($from = null,$to = null){
        if(!is_null($from))
            $this->from = $from;

        if(!is_null($to))
            $this->to = $to;

        return $this->getServices();
    }


    private function getServices(){
        $stats = [];
        //Utils::watch();
        //$query = "select * from `log_activities` where `created_at` <= '2016-12-01' and `created_at` >= '2016-11-01' and `target` like '%Service%'";
        //$rows = DB::select($query);
        $builder = Activity::where('created_at','<=',$this->to)->where('created_at','>',$this->from);
        $builder->where('target','=','App\Service');
        $builder->where('event','created');
        $created = $builder->count('id');
        //echo "[services] created:= $created";
        //echo "<br>";
        $stats['services_created'] = $created;

        $builder = Activity::where('created_at','<=',$this->to)->where('created_at','>',$this->from);
        $builder->where('target','=','App\Service');
        $builder->where('event','completed');
        $created = $builder->count('id');
        //echo "[services] completed:= $created";
        //echo "<br>";
        $stats['services_completed'] = $created;

        $builder = Activity::where('created_at','<=',$this->to)->where('created_at','>',$this->from);
        $builder->where('target','=','App\Service');
        $builder->where('event','closed');
        $created = $builder->count('id');
        //echo "[services] closed:= $created";
        //echo "<br>";
        $stats['services_closed'] = $created;

        $builder = Activity::where('created_at','<=',$this->to)->where('created_at','>',$this->from);
        $builder->where('target','=','App\Service');
        $builder->where('event','checkout');
        $created = $builder->count('id');
        //echo "[services] checkout:= $created";
        //echo "<br>";
        $stats['services_checkout'] = $created;



        $builder = Activity::where('created_at','<=',$this->to)->where('created_at','>',$this->from);
        $builder->where('target','=','App\Order');
        $builder->where('event','created');
        $created = $builder->count('id');
        //echo "[orders] created:= $created";
        //echo "<br>";
        $stats['orders_created'] = $created;

        $builder = Activity::where('created_at','<=',$this->to)->where('created_at','>',$this->from);
        $builder->where('target','=','App\Order');
        $builder->where('event','completed');
        $created = $builder->count('id');
        //echo "[orders] completed:= $created";
        //echo "<br>";
        $stats['orders_completed'] = $created;

        $builder = Activity::where('created_at','<=',$this->to)->where('created_at','>',$this->from);
        $builder->where('target','=','App\Order');
        $builder->where('event','verified');
        $created = $builder->count('id');
        //echo "[orders] verified:= $created";
        //echo "<br>";
        $stats['orders_verified'] = $created;

        $stats_bottles = [];

        $builder = Activity::where('created_at','<=',$this->to)->where('created_at','>',$this->from);
        $builder->where('target','=','App\Order');
        $builder->where('event','verified');
        $order_ids = $builder->lists('target_id')->toArray();




        $bottles_ids = DB::table('bottle_order')->whereIn('order_id',$order_ids)->lists('bottle_id');
        $bottles = Bottle::with('format')->whereIn('id',$bottles_ids)->get();

        //echo "Vendite virtuali:<br>";
        foreach($bottles as $bottle){
            $stat_bottle = new \stdClass();
            $stat_bottle->id = $bottle->id;
            $sum1 = DB::table('bottle_movements')->where('sign','increment')->whereIn('order_id',$order_ids)->where('bottle_id',$bottle->id)->sum('quantity');
            $sum2 = DB::table('bottle_order')->whereIn('order_id',$order_ids)->where('bottle_id',$bottle->id)->sum('quantity');
            $many = $sum1 + $sum2;
            $stat_bottle->total = $many;
            $stat_bottle->real = ($sum2 > 0) ? $sum2 : 0;
            $stat_bottle->negative = $stat_bottle->total - $stat_bottle->real;
            $stat_bottle->name = $bottle->name .' ('. $bottle->present()->formatName.')';
            $stats_bottles[] = $stat_bottle;
            //echo "$bottle->name ({$bottle->present()->formatName}): $many<br>";
        }

        $from = date('d/m/Y', strtotime($this->from));
        $to = date('d/m/Y', strtotime($this->to));

        return view('stats',compact('stats','stats_bottles','from','to'));
    }
}