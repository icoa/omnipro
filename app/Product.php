<?php

namespace App;

use App\Presenters\Contracts\PresentableInterface;
use App\Traits\PresentableTrait;

class Product extends BlameableModel implements PresentableInterface
{
    protected $presenter = 'App\Presenters\ProductPresenter';

    protected static $loggable = [];

    use PresentableTrait;

    protected $fillable = ['name', 'day_cost', 'code', 'active'];

    public function services()
    {
        return $this->hasMany('App\Service');
    }

    /**
     * Return departments associated to the user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function departments()
    {
        return $this->belongsToMany('App\Department');
    }

    public function setNameAttribute($value){
        $this->attributes['name'] = ucfirst(trim($value));
    }

    public function setCodeAttribute($value){
        $this->attributes['code'] = strtoupper(trim($value));
    }

    public function setDayCostAttribute($value){
        $this->attributes['day_cost'] = (float)$value;
    }

    public function getDepartmentOptionsAttribute()
    {
        return $this->departments->lists('id')->toArray();
    }

    public function canBeDeleted()
    {
        $canBeDeleted = true;
        if ($this->services()->count() > 0)
            $canBeDeleted = false;

        return $canBeDeleted;
    }
}
