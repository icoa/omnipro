<?php

namespace App\Handlers\Events;

use App\Events\ServiceWasClosed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Roomix;

class SendClosedServiceToCash
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ServiceWasClosed  $event
     * @return void
     */
    public function handle(ServiceWasClosed $event)
    {
        $service = $event->service;
        //send notification to all cash attached in the WebSocket
        $data = $service->toArray();
        Roomix::ws('ServiceWasClosed', $data);
    }
}
