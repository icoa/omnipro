<?php

namespace App\Handlers\Events;

use App\Events\ServiceWasAccepted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Roomix;

class SendAcceptedServiceToWaiters
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ServiceWasCompleted  $event
     * @return void
     */
    public function handle(ServiceWasAccepted $event)
    {
        \Utils::log(__METHOD__);
        $service = $event->service;

        $data = $service->toArray();
        Roomix::ws('ServiceWasAccepted', $data);
    }
}
