<?php

namespace App\Handlers\Events;

use App\Events\ServiceWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Roomix;

class SendCreatedServiceToCash
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ServiceWasCreated  $event
     * @return void
     */
    public function handle(ServiceWasCreated $event)
    {
        \Utils::log(__METHOD__);
        $service = $event->service;

        $data = $service->toArray();
        Roomix::ws('ServiceWasCreated', $data);
    }
}
