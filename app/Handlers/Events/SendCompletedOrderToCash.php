<?php

namespace App\Handlers\Events;

use App\Events\OrderWasCompleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Roomix;

class SendCompletedOrderToCash
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderWasCreated $event
     * @return void
     */
    public function handle(OrderWasCompleted $event)
    {
        \Utils::log(__METHOD__);
        $order = $event->order;
        //send notification to all cash attached in the WebSocket
        $data = $order->toArray();
        Roomix::ws('OrderWasCompleted', $data);

    }
}
