<?php

namespace App\Handlers\Events;

use App\Events\OrderWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Roomix;
Use App\BottleMovement;

class RegisterBottleMovements
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderWasCreated  $event
     * @return void
     */
    public function handle(OrderWasCreated $event)
    {
        \Utils::log(__METHOD__);
        $order = $event->order;

        foreach($order->bottleOrders as $bottleOrder){
            if($bottleOrder->quantity != 0){
                BottleMovement::register($bottleOrder,$bottleOrder->quantity);
            }
        }
    }
}
