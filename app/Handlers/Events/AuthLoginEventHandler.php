<?php

namespace App\Handlers\Events;

use App\Events;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Activity;
use Auth;
use DB;
use Cookie;
use Carbon\Carbon;

class AuthLoginEventHandler
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Events  $event
     * @return void
     */
    public function handle()
    {
        $user = Auth::user();
        $table = $user->getTable();
        DB::table($table)->where('id',$user->id)->update(['last_login_at' => Carbon::now()]);
        Activity::log([
           'event' => 'login',
           'user_id' => $user->id,
           'target' => 'Events\Auth',
           'target_id' => $user->id,
           'text' => $user->username." was logged in",
        ]);
        Cookie::queue(Cookie::make('last_login', $user->username, 60 * 24 * 7));
    }
}
