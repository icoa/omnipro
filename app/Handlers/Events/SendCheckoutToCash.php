<?php

namespace App\Handlers\Events;

use App\Events\ServiceWasCheckedOut;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Roomix;

class SendCheckoutToCash
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ServiceWasCheckedOut  $event
     * @return void
     */
    public function handle(ServiceWasCheckedOut $event)
    {
        $service = $event->service;
        //send notification to all cash attached in the WebSocket
        $data = $service->toArray();
        Roomix::ws('ServiceWasCheckedOut', $data);
    }
}
