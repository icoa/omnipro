<?php

namespace App\Handlers\Events;

use App\Events\OrderWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Roomix;

class SendOrderToCash
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderWasCreated  $event
     * @return void
     */
    public function handle(OrderWasCreated $event)
    {
        \Utils::log(__METHOD__);
        $order = $event->order;
        if($order->negative > 0){
            //send notification to all cash attached in the WebSocket
            $data = $order->toArray();
            Roomix::ws('NegativeOrderWasCreated', $data);
        }
    }
}
