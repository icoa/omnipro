<?php

namespace App\Handlers\Events;

use App\Events\BottleWasDecremented;
use App\Repositories\OrderRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Utils;

class CheckOrderStatus
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     * Check if the order has been totally satisfied because all bottle in that order has been verified
     *
     * @param  BottleWasDecremented  $event
     * @param  OrderRepository  $orderRepository
     * @return void
     */
    public function handle(BottleWasDecremented $event )
    {
        \Utils::log(__METHOD__);
        $orderRepository = new OrderRepository();
        try{
            $order = $event->bottleOrder->order()->first();
            $bottles = $order->bottleOrders()->get();
            Utils::log($bottles->toArray(),__METHOD__);
            $setVerified = true;
            foreach($bottles as $bottle){
                if($bottle->verified == 0)
                    $setVerified = false;
            }
            Utils::log( ($setVerified ? 'proceed' : 'do not proceed'),__METHOD__);
            if($setVerified)
                $orderRepository->setVerified($order);
        }catch(\Exception $e){
            Utils::log($e->getMessage(),__METHOD__);
        }
    }
}
