<?php

namespace App\Handlers\Events;

use App\Events\ServiceWasUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Roomix;

class SendUpdatedServiceToCash
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ServiceWasUpdated  $event
     * @return void
     */
    public function handle(ServiceWasUpdated $event)
    {
        \Utils::log(__METHOD__);
        $service = $event->service;

        $data = $service->toArray();
        Roomix::ws('ServiceWasUpdated', $data);
    }
}
