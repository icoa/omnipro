<?php

namespace App\Handlers\Events;

use App\Events\BottleWasDone;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Roomix;

class SendBottleToCash
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BottleWasDone  $event
     * @return void
     */
    public function handle(BottleWasDone $event)
    {
        \Utils::log(__METHOD__);
        $pivot_id = $event->pivot_id;

        //send notification to all cash attached in the WebSocket
        $data = ['id' => $pivot_id];
        Roomix::ws('BottleWasDone', $data);
    }
}
