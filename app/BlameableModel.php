<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 19/09/2015
 * Time: 17:42
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;


class BlameableModel extends Model
{
    use SoftDeletes;
    use Traits\BlameableTrait, Traits\LoggableTrait{
        Traits\BlameableTrait::boot as bootBlameable;
        Traits\LoggableTrait::boot as bootLoggable;
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $blameable = array("created", "updated", "deleted");
    protected $isBlameable = true;

    /**
     * Overrides the model's booter to register the event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::bootBlameable();
        self::bootLoggable();
    }

    public function canBeDeleted()
    {
        return true;
    }

    protected function date_accessor($value){
        $d = null;
        if(trim($value) != ''){
            $date = new \DateTime($value);
            $d = $date->format('d-m-Y');
        }
        return $d;
    }

    protected function date_mutator($value){
        return trim($value) != '' ? Carbon::createFromFormat('d-m-Y', $value) : null;
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function flagMe($target){
        try{
            \DB::table('flags')->insert(['target' => $target, 'target_id' => $this->id, 'user_id' => \Auth::user()->id]);
        }catch(\Exception $e){

        }
    }
}