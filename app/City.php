<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class City extends Model {

    protected $table = 'cities';

    protected $fillable = ['name', 'slug', 'priority', 'state_id'];

    /**
     * Return state associated to the city
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToOne
     */
    public function state()
    {
        return $this->belongsTo('App\State');
    }

    function setNameAttribute($value){
        $this->attributes['name'] = ucfirst(trim($value));
        $this->attributes['slug'] = Str::slug(trim($value));
    }

}
