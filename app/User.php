<?php

namespace App;

use DB;
use Fenos\Notifynder\Notifable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Hash;
use App\Presenters\Contracts\PresentableInterface;
use App\Traits\PresentableTrait;


class User extends BlameableModel implements AuthenticatableContract, CanResetPasswordContract, PresentableInterface
{
    use Authenticatable, CanResetPassword, PresentableTrait;

    // Constants
    const STATUS_DEFAULT = "default";
    const STATUS_ACCEPTED = "accepted";
    const STATUS_DELIVERED = "delivered";
    const STATUS_ROUTED = "routed";


    protected $presenter = 'App\Presenters\UserPresenter';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'username', 'password', 'role_id', 'pasw_changed'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $dates = ['deleted_at', 'last_login_at'];

    /**
     * Return roles associated to the user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    /**
     * Return departments associated to the user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function departments()
    {
        return $this->belongsToMany('App\Department');
    }

    /**
     * Return a list of roles ids associated with the current user
     *
     * @return array
     */
    /*public function getRoleListAttribute()
    {
        return $this->roles->lists('id')->all();
    }*/

    /**
     * Hash password before saving it
     *
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords($value);
    }

    public function getDepartmentOptionsAttribute()
    {
        return $this->departments->lists('id')->toArray();
    }

    /**
     * Return true if the user has the role
     *
     * @param $role
     * @return bool
     */
    public function hasRole($role)
    {
        if (is_array($role)) {
            return in_array($this->role->code, $role);
        } else {
            return $this->role->code == strtolower($role);
        }
    }

    /**
     * Return true if the user has permission
     *
     * @param $permission
     * @return bool
     */
    public function hasPermission($permission = null)
    {
        // TODO: This is temporary to check
        return $this->role->code != 'production';
    }

    /**
     * Return all components that the user can access to
     *
     * @return array
     */
    public function components()
    {
        return $this->role->components()->active();
    }

    /**
     * Return all activities for the user
     *
     * @return array
     */
    public function activities()
    {
        return $this->hasMany('App\Activity')->where('event', '!=', 'updated')->orderBy('created_at', 'desc');
    }

    /**
     * Return all users with the role
     *
     * @return array
     */
    public function scopeAllWithRole($query, $code)
    {
        return $query->whereHas('role', function ($query) use ($code) {
            $query->where('code', '=', $code);
        });
    }

    /**
     * Return all users with the department
     *
     * @return array
     */
    public function scopeAllWithDepartment($query, $department_id)
    {
        return $query->whereHas('departments', function ($query) use ($department_id) {
            is_array($department_id) ? $query->whereIn('department_id', $department_id) : $query->where('department_id', $department_id);
        });
    }

    public function canBeDeleted()
    {
        \Utils::watch();
        $blame_models = [
            'App\Department',
            'App\Service',
            'App\Product',
        ];

        $canBeDeleted = true;
        foreach ($blame_models as $model) {
            $count = $model::where('created_by', $this->id)->orWhere('updated_by', $this->id)->count();
            if ($count > 0) {
                $canBeDeleted = false;
            }
        }

        $count = Service::where('event_by', $this->id)
            ->orWhere('confirmed_by', $this->id)
            ->orWhere('confirmed_by', $this->id)
            ->orWhere('shipping_by', $this->id)
            ->orWhere('delivered_by', $this->id)->count();

        if ($count > 0) {
            $canBeDeleted = false;
        }

        return $canBeDeleted;
    }

    public function lastActivity()
    {
        $query = $this->activities();
        if (!$this->hasRole(['admin', 'reception', 'viewer'])) {
            $query->where('event', 'like', '%.%');
        }
        return $query->latest()->first();
    }

    public function notifications()
    {
        return $this->hasMany('App\Notification', 'to_id')->where('to_type', 'user');
    }

    public function scopeReadNotifications()
    {
        return $this->notifications()->where('read', 1);
    }

    public function scopeUnreadNotifications()
    {
        return $this->notifications()->where('read', 0);
    }

    public function countNotificationsNotRead()
    {
        return $this->notifications()->where('read', 0)->count('id');
    }

    public function services()
    {
        return $this->hasMany('App\Service', 'created_by')->with('tables');
    }

}


User::saved(function($model){

    $category_options = \Input::get('department_options');
    $role_id = \Input::get('role_id');

    if (is_array($category_options)) {
        if (!in_array($role_id, [4])) {
            $category_options = [];
        }
        $model->departments()->sync($category_options);
    }
});