<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Lib\Helper;
use App;
use Blade;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::extend(function($value, $compiler)
        {
            $value = preg_replace("/@set\('(.*?)'\,(.*)\)/", '<?php $$1 = $2; ?>', $value);
            return $value;
        });

        Blade::extend(function($value, $compiler)
        {
            $value = preg_replace("/@role\('(.*?)'\)/", '<?php if(Auth::user()->hasRole("$1")): ?>', $value);
            return $value;
        });

        Blade::extend(function($value, $compiler)
        {
            $value = preg_replace("/@notrole\('(.*?)'\)/", '<?php if(!Auth::user()->hasRole("$1")): ?>', $value);
            return $value;
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('helper', function()
        {
            return new Helper();
        });
    }
}
