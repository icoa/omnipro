<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Lib\Roomix;
use App;

class RoomixServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('roomix', function()
        {
            return new Roomix();
        });
    }
}
