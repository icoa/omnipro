<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use Blade;
use Illuminate\Support\Str;
use Theme;
use Utils;
use Roomix;
use Validator;
use App\Repositories\ServiceRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Blade::directive('themeView', function ($expression) {
            if (Str::startsWith($expression, '(')) {
                $expression = substr($expression, 1, -1);
            }
            $themeNS = Theme::getThemeNamespace();
            $expression = str_replace(["'", '"'], null, "$themeNS::views.$expression");
            return "<?php echo \$__env->make('$expression', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>";
        });

        Blade::directive('themePartial', function ($expression) {
            if (Str::startsWith($expression, '(')) {
                $expression = substr($expression, 1, -1);
            }
            $themeNS = Theme::getThemeNamespace();
            $expression = str_replace(["'", '"'], null, "$themeNS::partials.$expression");
            return "<?php echo \$__env->make('$expression', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>";
        });

        Blade::extend(function($value) {
            return preg_replace('/\@set(.+)/', '<?php ${1}; ?>', $value);
        });


        Validator::extendImplicit('required_with_dates', function($attribute, $value, $parameters, $validator) {
            $role = \Roomix::userRole();
            if($role != 'user')
                return true;

            if($parameters[0] != '' and $parameters[1] != ''){
                $discharge_date = Utils::mysql_date($parameters[0], true);
                $disable_date = Utils::mysql_date($parameters[1], true);
                if(!$disable_date->eq($discharge_date)){
                    return false;
                }
            }
            return true;
        });




        Validator::extend('after_activation', function($attribute, $value, $parameters, $validator) {
            $role = \Roomix::userRole();
            if($role != 'user')
                return true;

            $date = Utils::mysql_date($value, true);
            if($date){
                //getting activation date
                $repository = new ServiceRepository();
                $service = $repository->find($parameters[0]);
                if($service){
                    if($service->active_date->gt($date))
                        return false;
                }
            }

            return true;
        });


        Validator::extend('after_custom_date', function($attribute, $value, $parameters, $validator) {
            $role = \Roomix::userRole();
            if($role != 'user')
                return true;

            $date = Utils::mysql_date($value, true);
            if($date){
                //getting compare date
                $cmp_date = Utils::mysql_date($parameters[0], true);
                if($cmp_date and $cmp_date->gt($date)){
                    return false;
                }
            }
            return true;
        });
        Validator::extend('before_or_equal', function($attribute, $value, $parameters, $validator) {
            $role = \Roomix::userRole();
            if($role != 'user')
                return true;

            $date = Utils::mysql_date($value, true);
            if($date){
                //getting compare date
                $cmp_date = Utils::mysql_date($parameters[0], true);
                if($cmp_date and $cmp_date->lt($date)){
                    return false;
                }
            }
            return true;
        });


        Validator::extendImplicit('required_if_not_date', function($attribute, $value, $parameters, $validator) {
            $role = \Roomix::userRole();
            if($role != 'user')
                return true;

            $field = $parameters[0];
            $length = strlen(trim($value));
            if($length == 0 and $parameters[1] != ''){
                $request_field = request($field);

                $cmp_date = $date = Utils::mysql_date($request_field, true);
                $date = Utils::mysql_date($parameters[1], true);

                if($date and $cmp_date){
                    if($date->eq($cmp_date))
                        return true;

                    return false;
                }
            }
            return true;
        });



        Validator::extendImplicit('required_active_date', function($attribute, $value, $parameters, $validator) {
            $role = \Roomix::userRole();
            $length = strlen(trim($value));
            if($role != 'user'){
                return $length == 0 ? false : true;
            }
            if($length == 0)
                return false;

            $field = $parameters[0];
            if($parameters[1] != ''){
                $request_field = request($field);

                $cmp_date = $date = Utils::mysql_date($request_field, true);
                $date = Utils::mysql_date($parameters[1], true);

                if($date and $cmp_date){
                    if($date->eq($cmp_date))
                        return true;

                    return false;
                }
            }
            return false;
        });




    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
