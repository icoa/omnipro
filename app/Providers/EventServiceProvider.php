<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

//events
use App\Events\OrderWasCreated;
use App\Events\OrderWasCompleted;
use App\Events\BottleWasDone;
use App\Events\ServiceWasCheckedOut;
use App\Events\ServiceWasClosed;
use App\Events\ServiceWasCreated;
use App\Events\ServiceWasUpdated;
use App\Events\ServiceWasCompleted;
use App\Events\ServiceWasAccepted;
use App\Events\BottleWasDecremented;

//handlers
use App\Handlers\Events\SendOrderToGalley;
use App\Handlers\Events\SendOrderToCash;
use App\Handlers\Events\SendBottleToCash;
use App\Handlers\Events\SendCheckoutToCash;
use App\Handlers\Events\SendClosedServiceToCash;
use App\Handlers\Events\SendCreatedServiceToCash;
use App\Handlers\Events\SendUpdatedServiceToCash;
use App\Handlers\Events\SendCompletedServiceToWaiters;
use App\Handlers\Events\SendAcceptedServiceToWaiters;
use App\Handlers\Events\SendCompletedOrderToCash;
use App\Handlers\Events\CheckOrderStatus;
use App\Handlers\Events\RegisterBottleMovements;


class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'auth.login' => [
            'App\Handlers\Events\AuthLoginEventHandler',
        ],
        'auth.logout' => [
            'App\Handlers\Events\AuthLogoutEventHandler',
        ],
        OrderWasCreated::class => [
            SendOrderToGalley::class,
            SendOrderToCash::class,
            RegisterBottleMovements::class,
        ],
        BottleWasDone::class => [
            SendBottleToCash::class,
        ],
        ServiceWasCreated::class => [
            SendCreatedServiceToCash::class,
        ],
        ServiceWasUpdated::class => [
            SendUpdatedServiceToCash::class,
        ],
        ServiceWasCheckedOut::class => [
            SendCheckoutToCash::class,
        ],
        ServiceWasClosed::class => [
            SendClosedServiceToCash::class,
        ],
        ServiceWasCompleted::class => [
            SendCompletedServiceToWaiters::class,
        ],
        ServiceWasAccepted::class => [
            SendAcceptedServiceToWaiters::class,
        ],
        OrderWasCompleted::class => [
            SendCompletedOrderToCash::class,
        ],
        BottleWasDecremented::class => [
            CheckOrderStatus::class,
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
