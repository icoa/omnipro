<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 25/07/2016
 * Time: 15:05
 */

namespace App\Providers;
use App;
use App\Lib\BraveForm;
use App\Lib\HorizontalBraveFormBuilder;

use AdamWathan\BootForms\BootFormsServiceProvider;
use Illuminate\Support\ServiceProvider;

class BraveFormsServiceProvider extends BootFormsServiceProvider
{

    protected function registerHorizontalFormBuilder()
    {
        $this->app['bootform.horizontal'] = $this->app->share(function ($app) {
            return new HorizontalBraveFormBuilder($app['adamwathan.form']);
        });
    }

    protected function registerBootForm()
    {
        $this->app['braveform'] = $this->app->share(function ($app) {
            return new BraveForm($app['bootform.basic'], $app['bootform.horizontal']);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['braveform'];
    }
}