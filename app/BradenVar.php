<?php

namespace App;

use App\Presenters\Contracts\PresentableInterface;
use App\Traits\PresentableTrait;

class BradenVar extends BlameableModel implements PresentableInterface
{
    protected $presenter = 'App\Presenters\BradenVarPresenter';

    protected static $loggable = [];

    use PresentableTrait;

    protected $fillable = ['name', 'description', 'position'];

    public function choices()
    {
        return $this->hasMany('App\BradenChoice')->orderBy('points','desc');
    }

    public function setNameAttribute($value){
        $this->attributes['name'] = ucfirst(trim($value));
    }

    public function canBeDeleted()
    {
        $canBeDeleted = true;
        if ($this->choices()->count() > 0)
            $canBeDeleted = false;

        return $canBeDeleted;
    }
}
