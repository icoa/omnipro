<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Presenters\Contracts\PresentableInterface;
use App\Traits\PresentableTrait;

class Activity extends Model implements PresentableInterface
{
    use PresentableTrait;

    protected $presenter = 'App\Presenters\ActivityPresenter';

    protected $fillable = ['event','text','extra','target','target_id','user_id'];
    protected $table = 'log_activities';

    /**
     * Get the user that the activity belongs to.
     *
     * @return object
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id')->with('role');
    }



    static function log(array $params){
        if(isset($params['extra']) AND (is_object($params) OR is_array($params))){
            if(method_exists($params['extra'],'toArray')){
                $params['extra'] = $params['extra']->toArray();
            }
            $params['extra'] = serialize($params['extra']);
        }
        if(!isset($params['user_id'])){
            $params['user_id'] = Auth::check() ? Auth::user()->id : null;
        }
        //\Utils::log($params,__METHOD__);
        Activity::create($params);
    }

    static function text($text){
        return static::log(compact('text'));
    }

    static function logEvent($event,$text = null){
        return static::log(compact('text','event'));
    }
}
