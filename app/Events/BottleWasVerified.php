<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Activity;
use App\BottleOrder;

class BottleWasVerified extends Event
{
    use SerializesModels;

    public $bottleOrder;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($bottleOrder)
    {
        $this->bottleOrder = $bottleOrder;

        Activity::log([
            'event' => 'bottle.verified',
            'text' => "BottleOrder {$bottleOrder->id} has been verified",
            'target' => get_class($bottleOrder),
            'target_id' => $bottleOrder->id,
            'extra' => $bottleOrder,
        ]);
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
