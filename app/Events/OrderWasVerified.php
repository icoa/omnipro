<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Order;
use App\Activity;

class OrderWasVerified extends Event
{
    use SerializesModels;

    public $order;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        \Utils::log(__METHOD__);
        $this->order = $order;
        Activity::log([
            'event' => 'verified',
            'text' => "Order {$order->id} has been verified",
            'target' => get_class($order),
            'target_id' => $order->id,
            'extra' => $order,
        ]);
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
