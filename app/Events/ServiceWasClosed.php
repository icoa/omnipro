<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Service;
use App\Activity;

class ServiceWasClosed extends Event
{
    use SerializesModels;

    public $service;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
        Activity::log([
            'event' => 'closed',
            'text' => "Service {$service->name} has been closed",
            'target' => get_class($service),
            'target_id' => $service->id,
            'extra' => $service,
        ]);
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
