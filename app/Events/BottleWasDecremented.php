<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Activity;
use App\BottleMovement;

class BottleWasDecremented extends Event
{
    use SerializesModels;

    public $bottleOrder;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($bottleOrder)
    {
        $this->bottleOrder = $bottleOrder;

        Activity::log([
            'event' => 'bottle.decremented',
            'text' => "BottleOrder {$bottleOrder->id} has been decremented",
            'target' => get_class($bottleOrder),
            'target_id' => $bottleOrder->id,
            'extra' => $bottleOrder,
        ]);
        //register the 'bottle' movement
        BottleMovement::register($bottleOrder, -1);
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
