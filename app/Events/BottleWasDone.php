<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Activity;
use App\BottleOrder;

class BottleWasDone extends Event
{
    use SerializesModels;

    public $pivot_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($pivot_id)
    {
        $this->pivot_id = $pivot_id;
        $model = BottleOrder::find($pivot_id);
        Activity::log([
            'event' => 'bottle.done',
            'text' => "BottleOrder {$model->id} has been done",
            'target' => get_class($model),
            'target_id' => $model->id,
            'extra' => $model,
        ]);
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
