<?php

namespace App;

use App\Traits\PresentableTrait;
use Illuminate\Database\Eloquent\Model;

class Component extends Model
{
    use PresentableTrait;

    protected $presenter = 'App\Presenters\ComponentPresenter';

    /**
     * Return roles associated to the component
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    /**
     * Return only active roles
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1)->orderBy('order');
    }
}
