<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 03/10/2016
 * Time: 15:58
 */

namespace App\Repositories;


use App\Service;
use App\ServiceEvent;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Utils;
use Cache;
use DB;
use PDF;
use Illuminate\Console\Command;

class ServiceRepository extends Repository
{

    protected $user;
    protected $eventRepository;

    function __construct()
    {
        $this->user = Auth::user();
        $this->eventRepository = new ServiceEventRepository();
    }


    private function getBradenTotal($data)
    {
        $total = 0;
        for ($i = 1; $i <= 6; $i++) {
            $total += (int)array_get($data, "braden_point" . $i, 0);
        }
        return $total;
    }


    function createActivation(Request $request)
    {
        $data = $request->all();
        $data['status'] = Service::STATUS_CREATED;
        $data['type'] = Service::TYPE_ACTIVATION;
        $data['patient_name'] = strtoupper(trim(rtrim($data['patient_name'], '.')));
        $data['patient_lastname'] = strtoupper(trim(rtrim($data['patient_lastname'], '.')));
        $data['patient_initials'] = $data['patient_lastname'] . '.' . $data['patient_name'] . '.';
        $data['braden_total'] = $this->getBradenTotal($data);

        if ($this->user->id == 9) {
            $data['status'] = Service::STATUS_DELIVERED;
        }

        $service = Service::create($data);
        return $service;

        //event(new ServiceWasCreated($service));
    }


    function updateActivation(Request $request, Service $model)
    {
        $data = $request->all();
        $data['type'] = Service::TYPE_ACTIVATION;
        $data['patient_name'] = strtoupper(trim(rtrim($data['patient_name'], '.')));
        $data['patient_lastname'] = strtoupper(trim(rtrim($data['patient_lastname'], '.')));
        $data['patient_initials'] = $data['patient_lastname'] . '.' . $data['patient_name'] . '.';
        $data['braden_total'] = $this->getBradenTotal($data);

        $model->update($data);

        return $model;
    }


    function sendActivation(Request $request, Service $model)
    {
        $data = [];
        $data['status'] = Service::STATUS_REQUESTED;

        //aggiunta
        if ($model->getAttributes()['type'] == Service::TYPE_TRANSFER) {

            $data['type'] = Service::TYPE_ACTIVATION;

            $data['status'] = Service::STATUS_DELIVERED; //I trasferimenti non devono essere consegnati dall'autista e devono essere automaticamente approvati e "consegnati"
            $data['confirmed_at'] = Carbon::now();
            $data['confirmed_by'] = $this->user->id;

        }
        //fine aggiunta
        if ($this->user->id == 9) {
            $data['status'] = Service::STATUS_DELIVERED;
        }

        $data['sent_activation_at'] = Carbon::now();
        $data['sent_at'] = Carbon::now();
        $data['event_at'] = Carbon::now();
        //$data['start_date'] = Carbon::now();
        $data['event_by'] = $this->user->id;

        $model->update($data);

        $this->eventRepository->create($model);

        return $model;
    }


    function disactivate(Request $request, Service $model)
    {
        $data = $request->all();
        $model->update($data);
        return $model;
    }


    function sendDisactivation(Request $request, Service $model)
    {
        $data['type'] = Service::TYPE_DISACTIVATION;
        //$data['status'] = Service::STATUS_REQUESTED;


        $data['sent_at'] = Carbon::now();
        $data['event_at'] = Carbon::now();
        $data['event_by'] = $this->user->id;

        $data['status'] = Service::STATUS_CONFIRMED;
        $data['confirmed_at'] = Carbon::now();
        $data['confirmed_by'] = $this->user->id;

        $model->update($data);

        $this->eventRepository->create($model);

        return $model;
    }


    function maintenance(Request $request, Service $model)
    {
        $data = $request->all();
        $model->update($data);
        return $model;
    }


    function sendMaintenance(Request $request, Service $model)
    {
        $data['type'] = Service::TYPE_MAINTENANCE;
        //$data['status'] = Service::STATUS_REQUESTED;
        $data['mt_requested_at'] = Carbon::now();
        $data['sent_at'] = Carbon::now();
        $data['event_at'] = Carbon::now();
        $data['event_by'] = $this->user->id;

        $data['status'] = Service::STATUS_CONFIRMED;
        $data['confirmed_at'] = Carbon::now();
        $data['confirmed_by'] = $this->user->id;

        $model->update($data);

        $this->eventRepository->create($model);

        return $model;
    }


    function transfer(Request $request, Service $model)
    {
        $data = $request->all();
        $model->update($data);
        return $model;
    }


    function sendTransfer(Request $request, Service $model)
    {
        $data['type'] = Service::TYPE_TRANSFER;
        //$data['status'] = Service::STATUS_REQUESTED;
        $data['sent_at'] = Carbon::now();
        $data['event_at'] = Carbon::now();
        $data['event_by'] = $this->user->id;

        $data['status'] = Service::STATUS_DELIVERED; //I trasferimenti non devono essere consegnati dall'autista e devono essere automaticamente approvati e "consegnati"
        $data['confirmed_at'] = Carbon::now();
        $data['confirmed_by'] = $this->user->id;

        $model->update($data);

        $this->eventRepository->create($model);

        //model2 aggiunta

        $modelAttributes = $model->getAttributes();

        foreach ($modelAttributes as $key => $value) {
            $data[$key] = $value;
        }

        //$data['type'] = Service::TYPE_ACTIVATION;
        //$data['status'] = Service::STATUS_CONFIRMED;
        $data['parent_id'] = $model->id; //add relation
        $data['department_id'] = $modelAttributes['transfer_id'];
        $data['transfer_id'] = null;
        $data['active_date'] = Carbon::instance(new \DateTime($modelAttributes['transfer_date']));
        $data['transfer_date'] = null;
        $data['discharge_date'] = null;
        $data['disable_date'] = null;
        $data['start_date'] = null;
        $data['days'] = 0;
        $data['cost'] = 0;
        $data['disactivation_notes'] = null;
        $data['transfer_notes'] = null;
        $data['signature'] = null;
        $data['created_by'] = $modelAttributes['confirmed_by'];
        $data['deleted_by'] = null;
        $data['shipping_by'] = null;
        $data['delivered_by'] = null;
        //store the previous cost
        $data['pre_cost'] = $model->cost;
        $data['discount'] = $model->discount;
        $data['parent_id'] = $model->id;

        $model2 = Service::create($data);

        $this->sendActivation($request, $model2);

        //fine aggiunta

        return $model;
    }


    function authorize(Request $request, Service $model)
    {
        $action = $request->get('action');
        $data = [];
        if ($action == 'approve') {
            $data['status'] = Service::STATUS_CONFIRMED;
            $data['confirmed_at'] = Carbon::now();
            $data['confirmed_by'] = $this->user->id;
        }

        if ($action == 'reject') {
            $data['status'] = Service::STATUS_REJECTED;
            $data['confirmed_at'] = Carbon::now();
            $data['confirmed_by'] = $this->user->id;
        }

        $model->update($data);

        $this->eventRepository->create($model);

        return $model;
    }

    function calculateCost($days, $dayCost)
    {
        $cost = 0.0;
        $discount = 0.0;
        if ($days > 0 && $days < 8) {
            $cost = $dayCost * $days;
        } elseif ($days > 7 && $days < 15) {
            $discount = 5.0;
            $cost = $dayCost * $days * 0.95;
        } elseif ($days > 14 && $days < 31) {
            $discount = 10.0;
            $cost = $dayCost * $days * 0.90;
        } elseif ($days > 30) {
            $discount = 30.0;
            $cost = $dayCost * $days * 0.70;
        }
        return [$cost, $discount];
    }

    /**
     * @param Service $service
     * @param $days
     * @param $cost_starts_at
     */
    function sumDaysFromAncestors(Service $service, &$days, &$cost_starts_at)
    {
        if ($service and $service->parent_id > 0) {
            $ancestor = Service::where('id', $service->parent_id)->select('id', 'parent_id', 'days', 'cost_starts_at', 'created_at')->first();
            if ($ancestor) {
                $days += $ancestor->days;
                if($ancestor->cost_starts_at){
                    $cost_starts_at = $ancestor->cost_starts_at;
                }else{
                    $cost_starts_at = $ancestor->created_at;
                }
            }
           $this->sumDaysFromAncestors($ancestor, $days, $cost_starts_at);
        }
    }

    function calculateDaysAndCosts(Command $console = null)
    {
        $write = true;

        $rows = Service::with('product')
            //->whereIn('id', [3142, 3020, 2961])
            ->whereNotNull('active_date')->select(
                'id',
                'active_date',
                'delivered_at',
                'transfer_date',
                'maintenance_date',
                'disable_date',
                'product_id',
                'confirmed_at',
                'type',
                'status',
                'pre_cost',
                'parent_id'
            )->get();
        $now = Carbon::now();

        //probably smaller and congruent result with: where (ISNULL(cost_ends_at) or cost_ends_at >= 'YESTERDAY')

        DB::beginTransaction();

        foreach ($rows as $row) {

            $type = $row->type; //type of service
            $dayCost = $row->product->day_cost; //daily cost
            $status = $row->status;
            $cost_starts_at = $row->active_date;
            $cost_ends_at = $now;

            //check if there is any activation in the past
            $serviceEvent = ServiceEvent::where('service_id', $row->id)
                ->where('type', 'activation')
                ->where('status', Service::STATUS_CONFIRMED)
                ->first();
            if ($serviceEvent) {
                $cost_starts_at = $serviceEvent->created_at;
                $row->active_date = $cost_starts_at;
            }
            //Utils::log("Calculating costs for [$row->id]");
            if ($console) $console->comment("Calculating costs for [$row->id]");

            $days = 0;
            $cost = 0;
            $discount = 0;
            $ancestor_days = 0;

            $this->sumDaysFromAncestors($row, $ancestor_days, $cost_starts_at);
            if ($console) $console->line("Ancestors days: $ancestor_days");

            switch ($type) {

                case Service::TYPE_ACTIVATION : {

                    if ($status == Service::STATUS_DELIVERED) {

                        $days = $row->active_date->diffInDays($now, false) + $ancestor_days;
                        $costInfo = $this->calculateCost($days, $dayCost);
                        $cost = $costInfo[0];
                        $discount = $costInfo[1];

                    }

                };
                    break;
                case Service::TYPE_DISACTIVATION : {

                    $disable_date = $row->disable_date;

                    if ($now->lessThan(Carbon::instance(new \DateTime($disable_date)))) {


                        $days = $row->active_date->diffInDays($now, false) + $ancestor_days;
                        $costInfo = $this->calculateCost($days, $dayCost);
                        $cost = $costInfo[0];
                        $discount = $costInfo[1];

                    } else {

                        $days = $row->active_date->diffInDays($disable_date, false) + $ancestor_days;
                        $costInfo = $this->calculateCost($days, $dayCost);
                        $cost = $costInfo[0];
                        $discount = $costInfo[1];
                        $cost_ends_at = $disable_date;

                    }

                };
                    break;

                case Service::TYPE_TRANSFER : {

                    $transfer_date = $row->transfer_date;

                    if ($now->lessThan(Carbon::instance(new \DateTime($transfer_date)))) {

                        $days = $row->active_date->diffInDays($now, false) + $ancestor_days;
                        $costInfo = $this->calculateCost($days, $dayCost);
                        $cost = $costInfo[0];
                        $discount = $costInfo[1];

                    } else {

                        $days = $row->active_date->diffInDays($transfer_date, false) + $ancestor_days;
                        $costInfo = $this->calculateCost($days, $dayCost);
                        $cost = $costInfo[0];
                        $discount = $costInfo[1];
                        $cost_ends_at = $transfer_date;
                    }


                };
                    break;

                case Service::TYPE_MAINTENANCE : {

                    $maintenance_date = $row->maintenance_date;

                    if ($now->lessThan(Carbon::instance(new \DateTime($maintenance_date)))) {

                        $days = $row->active_date->diffInDays($now, false) + $ancestor_days;
                        $costInfo = $this->calculateCost($days, $dayCost);
                        $cost = $costInfo[0];
                        $discount = $costInfo[1];

                    } else {

                        $days = $row->active_date->diffInDays($maintenance_date, false) + $ancestor_days;
                        $costInfo = $this->calculateCost($days, $dayCost);
                        $cost = $costInfo[0];
                        $discount = $costInfo[1];
                        $cost_ends_at = $maintenance_date;
                    }

                };
                    break;

                default: {

                    $days = 0;
                    $cost = 0;
                    $discount = 0;

                };
                    break;
            }

            $service_cost = $cost;

            /*if ($row->pre_cost > 0) {
                $cost += $row->pre_cost;
            }*/

            $cost_starts_at = ($cost_starts_at) ? $cost_starts_at->format('Y-m-d H:i:s') : null;
            $cost_ends_at = ($cost_ends_at) ? $cost_ends_at->format('Y-m-d H:i:s') : null;

            $data = compact('service_cost', 'cost', 'days', 'discount', 'cost_starts_at', 'cost_ends_at');
            if ($console) print_r($data);

            if ($console) $console->line('== DONE ==');

            if($write)
                DB::table('services')->where('id', $row->id)->update(compact('days', 'cost', 'discount', 'cost_starts_at', 'cost_ends_at'));
        }
        DB::commit();
    }


    function bradenPdf(Service $model)
    {
        $pdf = PDF::loadView('pdf.braden', compact('model'));
        return $pdf->stream('scala_braden.pdf');
    }


    function find($id)
    {
        return Service::find($id);
    }

}