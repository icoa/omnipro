<?php

namespace App\Repositories;

use App\Service;
use App\ServiceEvent;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Utils;
use Cache;

class ServiceEventRepository extends Repository
{

    protected $user;

    function __construct()
    {
        $this->user = Auth::user();
    }


    function create(Service $service){
        $type = $service->type;
        $status = $service->status;
        $service_id = $service->id;
        ServiceEvent::create(compact('type','status','service_id'));
    }


}