<?php

namespace App\Repositories;

use App\Service;
use App\ServiceEvent;
use Carbon\Carbon;
use Utils;
use Cache;
use Illuminate\Http\Request;
use Auth;

class DriverRepository extends Repository
{

    protected $user;
    protected $eventRepository;

    function __construct()
    {
        $this->user = Auth::user();
        $this->eventRepository = new ServiceEventRepository();
    }

    private function builder()
    {
        $mode = request('mode');
        $builder = Service::with(
            [
                'driver',
            ]
        )->withStatus([
            Service::STATUS_CONFIRMED,
            Service::STATUS_SHIPPING,
            Service::STATUS_DELIVERED,
            Service::STATUS_REVERTED,
        ])->driver()
            ->select('id', 'name', 'status', 'type', 'sdo', 'patient_name', 'patient_initials', 'shipping_by', 'confirmed_at')
            ->orderBy('confirmed_at', 'desc');

        if ($mode == 'latest') {
            $pivot = Carbon::now()->addDays(-30)->format('Y-m-d');
            $builder->where('created_at', '>=', $pivot);
        }

        return $builder;
    }

    function all()
    {
        Utils::watch();
        return $this->builder()->get();
    }

    function paginate()
    {
        $mode = request('mode');
        $pageSize = config('theme.driver.pagesize');
        Utils::watch();
        return $mode == 'latest' ? $this->builder()->get() : $this->builder()->paginate($pageSize);
    }


    function update(Request $request, Service $model)
    {
        $action = $request->get('action');
        $data = [];
        if ($action == 'take') {
            $data['status'] = Service::STATUS_SHIPPING;
            $data['shipping_by'] = $this->user->id;
            $data['shipping_at'] = Carbon::now();
        }
        if ($action == 'deliver') {
            if ($model->getAttributes()['type'] == Service::TYPE_MAINTENANCE) {
                //$data['type'] = Service::TYPE_ACTIVATION;
                $data['status'] = Service::STATUS_DELIVERED;
                $data['delivered_by'] = $this->user->id;
                $data['delivered_at'] = Carbon::now();
                $data['signature'] = $request->get('signature');
                $data['mt_actions_performed'] = $request->get('mt_actions_performed');
                $model->update($data);
                $this->eventRepository->create($model);
                $data['type'] = Service::TYPE_ACTIVATION;
                $model->update($data);
                return $model;

            }
            $data['status'] = Service::STATUS_DELIVERED;
            $data['delivered_by'] = $this->user->id;
            $data['delivered_at'] = Carbon::now();
            $data['signature'] = $request->get('signature');
        }
        if ($action == 'revert') {/*
            $data['status'] = Service::STATUS_REVERTED;
            $data['delivered_by'] = null;
            $data['delivered_at'] = null;
            $data['shipping_by'] = null;
            $data['shipping_at'] = null;*/
            $data['status'] = Service::STATUS_SHIPPING;
            $data['shipping_by'] = $this->user->id;
            $data['shipping_at'] = Carbon::now();
        }
        if ($action == 'reown') {
            $data['status'] = Service::STATUS_SHIPPING;
            $data['shipping_by'] = $this->user->id;
            $data['shipping_at'] = Carbon::now();
        }

        $model->update($data);
        $this->eventRepository->create($model);

        return $model;
    }


    function search($code)
    {
        $code = strtoupper(trim($code));
        $rows = $this->builder()->where(function ($query) use ($code) {
            $query->where('sdo', $code)
                ->orWhere('id', $code)
                ->orWhere('patient_initials', $code)
                ->orWhere('engine_serial', $code);
        })->get();
        return $rows;
    }


}