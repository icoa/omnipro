<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // Users
        factory(User::class, 10)->create();

        // Roles
        $roles = ['Amministratore', 'Cameriere', 'Barista', 'Fiorista', 'Governante'];
        foreach ($roles as $role)
        {
            Role::create(['name' => $role]);
        }

        Model::reguard();
    }
}
