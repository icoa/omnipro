<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEstates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('estates', function (Blueprint $table) {
            /*
             *
             * bc	n°
bc_at	Data
district	Testo
town	Testo
headstate	Testo
location	Testo
address	Testo
registrazione_contratto	n°
stipula_contratto	Testo
scadenza_contratto	Testo
canone_annuo_contratto	Data
canone_mq	n°
stato_trattativa_bc	n°
riferimento_interno	Testo
backup	Testo

             * */
            $table->string('code');
            $table->string('name');
            $table->integer('bc')->unsigned()->nullable()->default(null);
            $table->date('bc_at')->nullable()->default(null);
            $table->integer('district_id')->unsigned()->nullable()->default(null);
            $table->integer('headstate_id')->unsigned()->nullable()->default(null);
            $table->integer('town_id')->unsigned()->nullable()->default(null);
            $table->string('location')->nullable()->default(null);
            $table->string('address')->nullable()->default(null);
            $table->string('cap', 6)->nullable()->default(null);
            $table->integer('registrazione_contratto')->unsigned()->nullable()->default(null);
            $table->string('stipula_contratto')->nullable()->default(null);
            $table->string('scadenza_contratto')->nullable()->default(null);
            $table->date('canone_annuo_contratto')->nullable()->default(null);
            $table->decimal('canone_mq', 20)->unsigned()->nullable()->default(null);
            $table->integer('stato_trattativa_bc')->unsigned()->nullable()->default(null);
            $table->string('riferimento_interno')->nullable()->default(null);
            $table->string('backup')->nullable()->default(null);

            /*dt_foglio	n°
dt_particella	n°
dt_subalterno	n°
dt_categoria_catastale	alfanumerico
dt_classe	Testo
dt_rendita_catastale	n°
dt_class_energetica	Testo
dt_mq	n°
dt_eventuali_manutenzioni	Testo
*/

            $table->integer('dt_foglio')->unsigned()->nullable()->default(null);
            $table->integer('dt_particella')->unsigned()->nullable()->default(null);
            $table->integer('dt_subalterno')->unsigned()->nullable()->default(null);
            $table->string('dt_categoria_catastale')->nullable()->default(null);
            $table->string('dt_classe')->nullable()->default(null);
            $table->decimal('dt_rendita_catastale', 20)->unsigned()->nullable()->default(null);
            $table->string('dt_class_energetica')->nullable()->default(null);
            $table->integer('dt_mq')->unsigned()->nullable()->default(null);
            $table->text('dt_eventuali_manutenzioni');

            /*am_canone_omi_mq	n°
am_data_rilevazioni_omi	data
am_scostamento_attuale_omi	n°
am_canone_di_mercato_x	n°
am_canone_di_mercato_y	n°
am_canone_di_mercato_identificato	n°
am_scostamento_attuale_mercato	n°
am_saving	%
am_scadenza_analisi_mercato	Data
am_stato_avanzamento	Testo
*/
            $table->decimal('am_canone_omi_mq', 20)->unsigned()->nullable()->default(null);
            $table->date('am_data_rilevazioni_omi')->nullable()->default(null);
            $table->decimal('am_scostamento_attuale_omi', 20)->unsigned()->nullable()->default(null);
            $table->decimal('am_canone_di_mercato_x', 20)->unsigned()->nullable()->default(null);
            $table->decimal('am_canone_di_mercato_y', 20)->unsigned()->nullable()->default(null);
            $table->decimal('am_canone_di_mercato_identificato', 20)->unsigned()->nullable()->default(null);
            $table->decimal('am_scostamento_attuale_mercato', 20)->unsigned()->nullable()->default(null);
            $table->decimal('am_saving', 20)->unsigned()->nullable()->default(null);
            $table->date('am_scadenza_analisi_mercato')->nullable()->default(null);
            $table->text('am_stato_avanzamento');

            /*at_ricerca_mercato
at_contatto_proprieta
at_presentazione_generale
at_eventuale_sopralluogo
at_definizione_accettazione
at_redazione_verbale
at_sottoscrizione_atto
at_elaborazione_contatto
*/
            $table->string('at_ricerca_mercato')->nullable()->default(null);
            $table->string('at_contatto_proprieta')->nullable()->default(null);
            $table->string('at_presentazione_generale')->nullable()->default(null);
            $table->string('at_eventuale_sopralluogo')->nullable()->default(null);
            $table->string('at_definizione_accettazione')->nullable()->default(null);
            $table->string('at_redazione_verbale')->nullable()->default(null);
            $table->string('at_sottoscrizione_atto')->nullable()->default(null);
            $table->string('at_elaborazione_contatto')->nullable()->default(null);

            /*ef_nuovo_canone_pattuito	n°
ef_decorrenza_nuove_condizioni	data
ef_residui_oggetti	n°
ef_saving_ottenuto	%
ef_saving_ottenuto_assoluto	n°
ef_fee	n°
*/
            $table->decimal('ef_nuovo_canone_pattuito', 20)->unsigned()->nullable()->default(null);
            $table->date('ef_decorrenza_nuove_condizioni')->nullable()->default(null);
            $table->integer('ef_residui_oggetti')->unsigned()->nullable()->default(null);
            $table->decimal('ef_saving_ottenuto', 20)->unsigned()->nullable()->default(null);
            $table->decimal('ef_saving_ottenuto_assoluto', 20)->unsigned()->nullable()->default(null);
            $table->decimal('ef_fee', 20)->unsigned()->nullable()->default(null);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('estates', function (Blueprint $table) {
            //
        });
    }
}
