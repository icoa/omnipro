<div class="card border-red">
    <div class="card-header with-header">
        Tavoli occupati
    </div>
    <div class="card-content">
        <div class="list-block media-list">
            <ul>
                @foreach($services as $service)
                    <li class="item-content">
                        <div class="item-inner">
                            <div class="item-title-row">
                                <div class="item-title">
                                    <span class="ellipsis">
                                        <span class="item-number">{!! $service->present()->tableListColors !!}</span> <strong class="item-name rc">{!! $service->name !!}</strong>
                                    </span>
                                </div>
                                <div class="item-after">{!! $service->createdBy->name !!}</div>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>

<div class="card border-green">
    <div class="card-header with-header">
        Tavoli liberi
    </div>
    <div class="card-content">
        <div class="content-block" style="padding:16px">
            @foreach($tables as $table)
                <span class="item-number table-widget" style="margin-bottom: 5px">{!! $table->name !!}</span>
            @endforeach
        </div>
    </div>
</div>