@foreach($rows as $pivot)
    <li class="swipeout filterable service_{!! $pivot->order->service->id !!}" data-id="{!! $pivot->id !!}" data-order="{!! $pivot->order_id !!}" data-service="{!! $pivot->order->service->id !!}" data-post="{!! route('cash.home.verified',$pivot->id) !!}">
        <div class="swipeout-content">
            <div class="item-content">
                <div class="item-inner">
                    <div class="item-title-row">
                        <div class="item-title"><strong>{!! $pivot->bottle->name !!}</strong> / {!! $pivot->bottle->present()->formatName !!} /
                            <small>{!! $pivot->bottle->present()->beverageName !!}</small>
                        </div>
                        <div class="item-after"><span class="quantity {!! $pivot->quantity < 0 ? 'negative' : '' !!}">{!! $pivot->quantity !!}</span></div>
                    </div>
                    <div class="item-subtitle">
                        {!! $pivot->order->service->present()->tableList !!} ({!! $pivot->order->service->name !!})
                    </div>
                    <small class="doneTime">{!! $pivot->doneTime !!}</small>
                </div>
            </div>
        </div>
        <div class="swipeout-actions-right">
            <a href="#" data-confirm="Confermi per <strong>{!! $pivot->quantity !!} X {!! $pivot->bottle->name !!} ({!! $pivot->bottle->present()->formatName !!})</strong>?"
               class="swipeout-delete swipeout-overswipe">Verificato</a>
        </div>
    </li>
@endforeach