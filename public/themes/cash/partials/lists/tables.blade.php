<div class="card">
    <div class="card-header with-header">
        Scegli il tavolo su cui effettuare lo storno
    </div>
    <div class="card-content">
        <div class="list-block media-list search-here searchbar-found">
            <ul>
                @foreach($services as $service)
                    <li class="item-content">
                        <a class="item-inner ajax" href="{!! route('cash.services.show',$service->id) !!}" data-ignore-cache="true">
                            <div class="item-title-row">
                                <div class="item-title">
                                    <span class="ellipsis">
                                        <span class="item-number">{!! $service->present()->tableListColors !!}</span> <strong class="item-name rc">{!! $service->name !!}</strong>
                                    </span>
                                </div>
                                <div class="item-after">{!! $service->createdBy->name !!}</div>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>