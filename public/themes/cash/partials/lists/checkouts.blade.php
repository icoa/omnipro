@foreach($checkouts as $service)
    <li style="border-left:10px solid rgba({!! $service->present()->rgb !!},1)" class="swipeout" data-id="{!! $service->id !!}"  data-post="{!! route('cash.home.accepted',$service->id) !!}">
        <div class="swipeout-content">
            <div class="item-content">
                <div class="item-inner">
                    <div class="item-title-row">
                        <div class="item-title">
                            <span class="item-number">{!! $service->present()->tableListColors !!}</span> <strong class="item-name rc">{!! $service->name !!}</strong>
                        </div>
                        <div class="item-after no-flex">
                            {!! $service->requestedBy->name !!}<span style="margin-top:4px; display: block">{!! $service->present()->requestedTime !!}</span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="swipeout-actions-right">
            <a class="swipeout-overswipe app-action bg-red" data-action="doCheckout">CHIUDI TAVOLO</a>
        </div>
    </li>
@endforeach