<?php
$last_login = Cookie::get('last_login');
?>
<div class="page" id="login" data-page="login">
    <!-- page-content has additional login-screen content -->
    <div class="page-content login-screen-content">
        <div class="sign-avatar">
            @if(App::environment() == 'demo')
                <img src="/static/android-icon-72x72@2x.png" width="72" height="72" style="margin:30px auto 15px; display: block">
            @else
                <img src="/assets/images/logo@2x.png" width="144" style="margin:30px auto 15px; display: block">
            @endif
        </div>
        <div class="login-screen-title">Accesso Cassa</div>
        <!-- Login form -->
        <form method="POST" action="/cash/auth/login" id="frm-login">
            {!! csrf_field() !!}

            <div class="list-block">
                <ul>
                    @if(App::environment() == 'demo')
                        <li class="item-content">
                            <div class="item-inner">
                                <div class="alert alert-warning" style="font-size: 70%">Attenzione: la modalità <strong>DEMO</strong> è attiva; puoi utilizzare <strong>"test"</strong> come password di accesso per tutte le Utenze; ti ricordiamo
                                    inoltre che tutti i dati e
                                    le attività verranno resattati ogni giorno alle 00:00.
                                </div>
                            </div>
                        </li>
                    @endif
                    <li class="item-content">
                        <div class="item-inner">
                            <div class="item-title label">Utente</div>
                            <div class="item-input">
                                {!! Form::select('username', Helper::usernamesByRole(2) , $last_login, ['placeholder' => 'Inserisci il nome utente', 'required' => 'required']) !!}
                            </div>
                            <div class="item-error">{{ $errors->first('username') }}</div>
                        </div>
                    </li>
                    <li class="item-content">
                        <div class="item-inner">
                            <div class="item-title label">Password</div>
                            <div class="item-input">
                                {!! Form::password('password', ['placeholder' => 'Inserisci la password', 'required' => 'required']) !!}
                            </div>
                            <div class="item-error">{{ $errors->first('password') }}</div>
                        </div>
                    </li>
                    <li>
                        <label class="label-checkbox item-content">
                            <input type="checkbox" name="remember" checked>
                            <div class="item-media"><i class="icon icon-form-checkbox"></i></div>
                            <div class="item-inner">
                                <div class="item-title">Ricordami</div>
                            </div>
                        </label>
                    </li>
                </ul>
            </div>
            <div class="content-block"><button type="submit" class="button button-big button-fill">ENTRA</button></div>
            <div class="content-block">
                <div class="list-block-label">{!! config('roomix.login.message') !!}</div>
                <div class="list-block-label"><small>{!! config('roomix.login.copyright') !!}</small></div>
            </div>
        </form>
    </div>
</div>