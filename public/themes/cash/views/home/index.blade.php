<div data-page="index" class="page" data-module="cash">

    <div class="navbar">
        <div class="navbar-inner">
            <div class="left">
                <a href="#" class="link icon-only app-action logout" data-action="logout"><i class="icon m-icon">exit_to_app</i></a>
            </div>
            <div class="center sliding">
                Cassa
            </div>
            <div class="right">
                <a id="showServicesAction" class="link icon ajax" href="{{ route("cash.services.index") }}?reload=true" data-ignore-cache="true">
                    <i class="icon m-icon">indeterminate_check_box</i><span>Storno</span>
                </a>
                <a id="showTablesAction" class="link icon ajax" href="{{ route("cash.tables") }}?reload=true" data-ignore-cache="true">
                    <i class="icon m-icon">apps</i><span>Tavoli</span>
                </a>
            </div>
        </div>
    </div>


    <div class="page-content">

        <div class="card" id="shouldUpdateCheckout" data-route="{!! route('cash.home.checkout', null) !!}" data-reload="{!! route('cash.home.checkouts') !!}">
            <div class="card-header with-header">
                Conti richiesti
            </div>
            <div class="card-content">
                <div class="list-block media-list">
                    <ul>
                        @themePartial('lists.checkouts')
                    </ul>
                </div>
            </div>
        </div>

        <div class="card with-filter">
            <div class="card-header with-header">
                Ordini
            </div>
            <div class="card-content">
                <div class="list-block media-list">
                    <div class="item-input">
                        <select id="filter"></select>
                    </div>
                    <a href="" class="cancel"><i class="icon m-icon">clear</i></a>
                </div>
            </div>
        </div>

        <div id="shouldUpdateBottles" data-order="{!! route('cash.home.order', null) !!}" data-reload="{!! route('cash.home.orders') !!}">
            @themePartial('lists.orders')
        </div>

    </div>

</div>
