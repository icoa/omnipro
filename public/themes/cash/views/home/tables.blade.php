<div data-page="tables" class="page" data-module="tables">

    <div class="navbar">
        <div class="navbar-inner">
            <div class="left"><a id="showOrdersAction" href="{{ route('home') }}" class="back link icon-only ajax"><i class="icon icon-back"></i></a></div>
            <div class="center sliding">
                Tavoli
            </div>
        </div>
    </div>

    <div class="page-content tab material">
        <div id="shouldUpdateServices" data-route="{!! route('cash.home.allservices') !!}">
            @themePartial('lists.services')
        </div>
    </div>
</div>
