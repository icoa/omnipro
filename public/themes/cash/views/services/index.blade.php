<div data-page="services.index" class="page" data-module="services">

    <div class="navbar">
        <div class="navbar-inner">
            <div class="left"><a id="showOrdersAction" href="{{ route('home') }}" class="back link icon-only ajax"><i class="icon icon-back"></i></a></div>
            <div class="center sliding">
                Scegli un Tavolo
            </div>
        </div>
    </div>
    <form data-search-list=".search-here" data-search-in=".item-title" class="searchbar searchbar-init">
        <div class="searchbar-input">
            <input type="search" placeholder="{!! trans('app.search') !!}"/><a class="searchbar-clear"></a>
        </div>
    </form>

    <div class="searchbar-overlay"></div>

    <div class="page-content material">
        <div>
            @themePartial('lists.tables')
        </div>
    </div>
</div>
