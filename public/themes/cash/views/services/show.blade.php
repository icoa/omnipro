<div data-page="services.show" class="page" data-module="services">

    <div class="navbar">
        <div class="navbar-inner">
            <div class="left"><a href="{{ route('cash.services.index') }}" class="back link icon-only ajax"><i class="icon icon-back"></i></a></div>
            <div class="center sliding">
                Esegui lo storno
            </div>
        </div>
    </div>

    <form data-search-list=".search-here" data-search-in=".item-title" class="searchbar searchbar-init">
        <div class="searchbar-input">
            <input type="search" placeholder="{!! trans('app.search') !!}"/><a class="searchbar-clear"></a>
        </div>
    </form>

    <div class="searchbar-overlay"></div>

    <div class="page-content material">
        <div class="content-block-title">
            Hai scelto il tavolo
        </div>
        <div class="content-block">
            <span class="ellipsis">
                <span class="item-number">{!! $model->present()->tableListColors !!}</span> <strong class="item-name rc">{!! $model->name !!}</strong>
            </span>
        </div>
        <div class="card">
            <div class="card-content">
                <div class="list-block media-list bottles-list search-here searchbar-found">
                    <ul id="bottleOrderHolder">
                        @foreach($rows as $row)
                            @if($row->quantity > 0)
                            <li
                                    id="bottleOrder_{!! $row->id !!}"
                                    class="swipeout"
                                    data-id="{!! $row->id !!}"
                                    data-route="{!! route('cash.services.decrement',$row->id) !!}"
                                    data-fullname="{!! $row->bottle->name !!} ({!! $row->bottle->present()->formatName !!})"
                            >
                                <div class="swipeout-content">
                                    <div class="item-content">
                                        <div class="item-inner">
                                            <div class="item-title-row">
                                                <div class="item-title rc">
                                                    <strong>{!! $row->bottle->name !!}</strong>
                                                    <span style="display: none">{!! $row->bottle->present()->formatName !!}</span>
                                                </div>
                                                <div class="item-after">
                                                    <span class="format rc">{!! $row->bottle->present()->formatName !!}</span>
                                                    <span class="button button-raised button-quantity {!! $row->quantity < 0 ? 'negative' : '' !!}">{!! $row->quantity !!}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="swipeout-actions-right">
                                    <a class="swipeout-overswipe bg-red app-action" data-id="{!! $row->id !!}" data-action="removeBottle"><i class="icon m-icon" style="margin-right: 5px">remove</i>ELIMINA 1 BOTTIGLIA</a>
                                </div>
                            </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
