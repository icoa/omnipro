<div data-page="feedback" class="page">
    <div class="navbar">
        <div class="navbar-inner">
            <div class="left"><a href="/" class="back link icon-only"><i class="icon icon-back"></i></a></div>
            <div class="center">Feedback</div>
            <div class="right">{!! Theme::partial('nabvar-notifications') !!}</div>
        </div>
    </div>
    <div class="page-content">
        <div class="content-block-title">Segnala un problema</div>
        <div class="content-block">
            <p>Questa interfaccia ti permette di inviare una segnalazione di malfunzionamento dell'applicazione.</p>

            <p>Per favore includi il maggior numero di dettagli possibili per aiutare la direzione a trovare una soluzione al tuo problema - Grazie!</p>
        </div>
        <form id="feedbackForm">
            {!! csrf_field() !!}
        <div class="list-block inputs-list">
            <ul>
                <li>
                    <div class="item-content">
                        <div class="item-inner">
                            <div class="item-title label">Scrivi qui il problema</div>
                            <div class="item-input item-input-field">
                                {!! Form::textarea('message',null,['rows' => 5, 'placeholder' => "Descrivi a parole tue il problema che hai incontrato...",'class' => 'full']) !!}
                            </div>
                            <div class="item-error">{{ $errors->first('message') }}</div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="content-block">
            {!! Form::submit('INVIA SEGNALAZIONE',['class' => 'button button-fill button-raised button-big']) !!}
        </div>
        </form>
    </div>
</div>