<!DOCTYPE html>
<html>
<head>
    <title>{!! Theme::get('title') !!}</title>
    {!! Theme::partial('head') !!}
    {!! Theme::asset()->styles() !!}
    {!! Roomix::configJS() !!}
</head>
<body>
{!! Theme::partial('header') !!}

{!! Theme::content() !!}

{!! Theme::partial('footer') !!}

{!! Theme::asset()->scripts() !!}

<script>
    (function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{!! csrf_token() !!}'
            }
        });
    })();
</script>
</body>
</html>