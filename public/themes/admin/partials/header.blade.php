<?php $user = Auth::user(); ?>
<header class="site-header">
    <div class="container-fluid">
        <a href="{!! route('admin.home') !!}" class="site-logo">
            <img class="hidden-md-down" src="/static/android-icon-96x96.png" alt="">
            <img class="hidden-lg-up" src="/static/android-icon-96x96.png" alt="">
        </a>
        <button class="hamburger hamburger--htla">
            <span>toggle menu</span>
        </button>
        <div class="site-header-content">
            <div class="site-header-content-in">
                <div class="site-header-shown">
                    <div class="dropdown" style="float: left; top:-5px;">
                        <small><strong>{!! $user->name !!}</strong><br>{!! $user->role->name !!}</small>
                    </div>

                    <div class="dropdown user-menu">
                        <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="/assets/images/avatar-2-64.png" alt="">
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
                            <a class="dropdown-item" href="/admin/users/profile"><span class="font-icon glyphicon glyphicon-user"></span>Profilo</a>
                            <a class="dropdown-item" href="/admin/auth/logout"><span class="font-icon glyphicon glyphicon-log-out"></span>Logout</a>
                        </div>
                    </div>

                    <button type="button" class="burger-right">
                        <i class="font-icon-menu-addl"></i>
                    </button>

                </div><!--.site-header-shown-->

                <div class="mobile-menu-right-overlay"></div>
                <div class="site-header-collapsed">
                    <div class="site-header-collapsed-in">
                        @if(Roomix::userRole() == 'user')
                        <button class="btn btn-nav btn-rounded btn-inline btn-danger-outline" type="button">
                            {!! Auth::user()->present()->departmentName !!}
                        </button>
                        @endif
                        <div class="dropdown dropdown-typical">
                            <a class="dropdown-toggle" id="dd-header-sales" data-target="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="font-icon font-icon-cogwheel"></span>
                                <span class="lbl">Componenti</span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dd-header-sales">
                                <?php
                                $components = $user->components;
                                ?>
                                @foreach($components as $component)
                                    <a class="dropdown-item" href="{!! $component->present()->link !!}">
                                        <span class="font-icon {!! $component->present()->icon !!}"></span>{!! $component->present()->name !!}</a>
                                @endforeach
                            </div>
                        </div>
                    </div><!--.site-header-collapsed-in-->
                </div><!--.site-header-collapsed-->
            </div><!--site-header-content-in-->
        </div><!--.site-header-content-->
    </div><!--.container-fluid-->
</header><!--.site-header-->