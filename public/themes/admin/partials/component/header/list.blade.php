<div class="tbl-cell">
    <h3>Gestione {!! $entities !!}</h3>
    <ol class="breadcrumb breadcrumb-simple">
        <li><a href="{!! route("admin.$module.index") !!}">{!! $entities !!}</a></li>
        <li class="active">Elenco {!! $entities !!}</li>
    </ol>
</div>