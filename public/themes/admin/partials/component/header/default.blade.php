<component-toolbar inline-template back="{!! route("admin.$module.create") !!}">
    <header class="section-header">
        <div class="tbl">
            <div class="tbl-row">
                @themePartial('component.header.list')
                <div class="tbl-cell tbl-toolbar pull-right">
                    @if(Auth::user()->hasPermission())
                        <button @click="submit('back')" type="button" class="btn-square-icon btn btn-primary">
                        <i class="fa fa-plus"></i>
                        Aggiungi {!! $entity !!}
                        </button>
                    @endif
                </div>
            </div>
        </div>
    </header>
</component-toolbar>