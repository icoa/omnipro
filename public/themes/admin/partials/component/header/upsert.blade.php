<component-toolbar inline-template back="{!! route("admin.$module.index") !!}">
    <header class="section-header">
        <div class="tbl">
            <div class="tbl-row">
                <div class="tbl-cell">
                    <h3>Gestione {!! $entities !!}</h3>
                    <ol class="breadcrumb breadcrumb-simple">
                        <li><a href="{!! route("admin.$module.index") !!}">{!! $entities !!}</a></li>
                        <li class="active">@if(isset($model))Modifica @else Aggiungi @endif {!! $entity !!}</li>
                    </ol>
                </div>
                <div class="tbl-cell tbl-toolbar pull-right">
                    <button @click="submit('back')" type="button" class="btn-square-icon btn btn-secondary">
                        <i class="fa fa-angle-left"></i>
                        Torna indietro
                    </button>
                    <button @click="submit('edit')" type="button" class="btn-square-icon btn btn-success">
                        <i class="fa fa-repeat"></i>
                        Salva e rimani
                    </button>
                    <button @click="submit('index')" type="button" class="btn-square-icon btn btn-success">
                        <i class="fa fa-save"></i>
                        Salva e chiudi
                    </button>
                </div>
            </div>
        </div>
    </header>
</component-toolbar>