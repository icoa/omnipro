<component-toolbar inline-template>
    <header class="section-header">
        <div class="tbl">
            <div class="tbl-row">
                @themePartial('component.header.list')
            </div>
        </div>
    </header>
</component-toolbar>