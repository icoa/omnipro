<component-toolbar inline-template>
    <header class="section-header">
        <div class="tbl">
            <div class="tbl-row">
                <div class="tbl-cell">
                    <h3>Modifica Profilo</h3>
                    <ol class="breadcrumb breadcrumb-simple">
                        <li class="active">Modifica dati del Profilo personale</li>
                    </ol>
                </div>
                <div class="tbl-cell tbl-toolbar pull-right">
                    <button @click="submit('edit')" type="button" class="btn-square-icon btn btn-success">
                        <i class="fa fa-repeat"></i>
                        Salva
                    </button>
                </div>
            </div>
        </div>
    </header>
</component-toolbar>