<component-toolbar inline-template>
    <header class="section-header">
        <div class="tbl">
            <div class="tbl-row">
                @themePartial('component.header.list')
                <div class="tbl-cell tbl-toolbar pull-right">
                    @if(Auth::user()->hasPermission())
                        <a class="btn-square-icon btn btn-success" href="{!! route("admin.$module.excel") !!}">
                        <i class="fa fa-file-excel-o"></i>
                        Esporta in Excel
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </header>
</component-toolbar>