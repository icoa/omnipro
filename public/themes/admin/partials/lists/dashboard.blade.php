<?php
$components = Auth::user()->components;
/**
 * @param $route
 * @return bool
 */
function checkRoute($route)
{
    return Route::getRoutes()->hasNamedRoute($route);

    return false;
}

?>
<section class="widget">
    <header class="widget-header-dark">Benvenuto {!! Auth::user()->name !!}</header>
    <div class="tab-content widget-tabs-content">
        @set $counter = 0
        @foreach($components as $component)
            <div class="tab-pane @if($counter == 0) active @endif" id="tab-{!! $component->id !!}" role="tabpanel">
                <div class="proj-page-section">
                    <h3>{!! $component->name !!}</h3>
                    <p>Seleziona una delle azioni possibili per questo componente:</p>
                    <a href="{!! route("admin.$component->resource.index") !!}" class="btn btn-rounded btn-inline">Elenco {!! $component->name !!}</a>
                    @if(checkRoute("admin.$component->resource.create"))
                        <a href="{!! route("admin.$component->resource.create") !!}" class="btn btn-rounded btn-inline btn-secondary">
                            {!! $component->add !!}
                        </a>
                    @endif
                </div>
            </div>
            @set $counter++
        @endforeach
    </div>
    <div class="widget-tabs-nav bordered">
        <ul class="tbl-row" role="tablist">
            @set $counter = 0
            @foreach($components as $component)
                <li class="nav-item">
                    <a class="nav-link @if($counter == 0) active @endif" data-toggle="tab"
                       href="#tab-{!! $component->id !!}" role="tab">
                        <i class="font-icon {!! $component->icon !!}"></i>
                        {!! $component->name !!}
                    </a>
                </li>
                @set $counter++
            @endforeach
        </ul>
    </div>
</section>