<?php
$data = [
        'discharge_date' => $model->present()->dischargeDate,
        'disable_date' => $model->present()->disableDate,
        'disactivation_notes' => $model->present()->disactivationNotesHtml,
]
?>
<p><br></p>
<h5 class="with-border">Dettagli disattivazione</h5>
<div class="tbl tbl-top">
    @foreach($data as $key => $value)
        <div class="tbl-row">
            <div class="tbl-cell tbl-th">{!! trans("service.$key") !!}</div>
            <div class="tbl-cell">{!! $value !!}</div>
        </div>
    @endforeach
</div>