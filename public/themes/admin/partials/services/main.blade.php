<?php
$data = [
        'type' => $model->present()->typeName,
        'status' => $model->present()->statusHtml,
        'active_date' => $model->present()->activeDate,
        'sdo' => $model->present()->sdo,
        'product_id' => $model->present()->productName,
        'uo' => $model->present()->uo,
        'department_id' => $model->present()->departmentName,
        'engine_serial' => $model->present()->engine_serial,
        'injuries' => $model->present()->injuriesText,
        'stand_by' => $model->present()->standByText,
        'patient_lastname' => $model->present()->patient_lastname,
        'patient_name' => $model->present()->patient_name,
        //'patient_initials' => $model->present()->patient_initials,
        'braden' => $model->present()->bradenPoints,
        'braden_total' => $model->present()->braden_total,
        'created_by' => $model->present()->creator,
        'created_at' => $model->present()->created,
        'event_by' => $model->present()->eventBy,
        'event_at' => $model->present()->when,
        'activation_notes' => $model->present()->activationNotesHtml,
        'mt_actions_performed' => $model->present()->actionsPerformedHtml,
]
?>
<div class="tbl tbl-top">
    @foreach($data as $key => $value)
        <div class="tbl-row">
            <div class="tbl-cell tbl-th">{!! trans("service.$key") !!}</div>
            <div class="tbl-cell">{!! $value !!}</div>
        </div>
    @endforeach
    @if($model->signature != null)
        <div class="tbl-row">
            <div class="tbl-cell tbl-th">{!! trans("service.signature") !!}</div>
            <div class="tbl-cell">{!! $model->present()->signatureHtml !!}</div>
        </div>
    @endif
</div>