<div class="clearfix">
    <component-toolbar inline-template back="{!! route("admin.$module.index") !!}">
        <br>
        <button @click="submit('send')" type="button" class="btn btn-success pull-right">
        Conferma ed invia <i class="fa fa-save"></i>
        </button>
    </component-toolbar>
</div>