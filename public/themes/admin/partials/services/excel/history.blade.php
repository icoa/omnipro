<table border="1">
    <thead>
    <tr>
        <th align="center">ID</th>
        <th align="center">@lang('service.type')</th>
        <th align="center">@lang('service.parent_id')</th>
        <th align="center">@lang('service.sent_activation_at')</th>
        <th align="center">@lang('service.active_date')</th>
        <th align="center">@lang('service.uo')</th>
        <th align="center">@lang('service.department_id')</th>
        <th align="center">@lang('service.patient_lastname')</th>
        <th align="center">@lang('service.patient_name')</th>
        <th align="center">@lang('service.sdo')</th>
        <th align="center">@lang('service.product_id')</th>
        <th align="center">@lang('service.injuries')</th>
        <th align="center">@lang('service.stand_by')</th>
        <th align="center">@lang('service.activation_notes')</th>
        <th align="center">Status</th>
        <th align="center">@lang('service.alt.confirmed_at')</th>
        <th align="center">@lang('service.alt.sent_at')</th>
        <th align="center">@lang('service.alt.maintenance_date')</th>
        <th align="center">@lang('service.alt.delivered_at')</th>
        <th align="center">@lang('service.alt.disable_date')</th>
        <th align="center">@lang('service.alt.transfer_date')</th>
        <th align="center">@lang('service.days')</th>
        <th align="center">@lang('service.cost_starts_at')</th>
        <th align="center">@lang('service.cost_ends_at')</th>
        <th align="center">@lang('service.discount')</th>
        <th align="center">@lang('service.cost')</th>
        <th align="center">@lang('service.event_at')</th>
    </tr>
    </thead>
    <tbody>
    <?php $counter = 0; ?>
    @foreach($rows as $row)
        <tr class="row{!! $counter % 2 !!}">
            <td align="center">{!! $row->id !!}</td>
            <td align="center">{!! $row->eloquent->present()->typeName !!}</td>
            <td align="center">{!! $row->parent_id !!}</td>
            <td align="center">{!! $row->eloquent->present()->whenSentActivationAt !!}</td>
            <td align="center">{!! $row->active_date->format('d/m/Y') !!}</td>
            <td align="center">{!! $row->department_code !!}</td>
            <td align="center">{!! $row->department_name !!}</td>
            <td align="center">{!! $row->patient_lastname !!}</td>
            <td align="center">{!! $row->patient_name !!}</td>
            <td align="center">{!! $row->sdo !!}</td>
            <td align="center">{!! $row->product_name !!}</td>
            <td align="center">{!! $row->injuries == 1 ? 'Sì' : 'No' !!}</td>
            <td align="center">{!! $row->stand_by == 1 ? 'Sì' : 'No' !!}</td>
            <td align="center">{!! $row->activation_notes !!}</td>
            <td align="center">{!! $row->eloquent->present()->statusHtml !!}</td>
            <td align="center">{!! $row->eloquent->present()->whenConfirmed !!}</td>
            <td align="center">{!! $row->eloquent->present()->whenSent !!}</td>
            <td align="center">{!! $row->eloquent->present()->maintenance_date !!}</td>
            <td align="center">{!! $row->eloquent->present()->whenDelivered !!}</td>
            <td align="center">{!! $row->eloquent->present()->disableDate !!}</td>
            <td align="center">{!! $row->eloquent->present()->transferDate !!}</td>
            <td align="center">{!! $row->eloquent->present()->daysText !!}</td>
            <td align="center">{!! $row->eloquent->present()->whenCostStartsAt !!}</td>
            <td align="center">{!! $row->eloquent->present()->whenCostEndsAt !!}</td>
            <td align="center">{!! $row->eloquent->present()->discount !!}</td>
            <td align="center" class="euro">{!! $row->eloquent->present()->costEuro !!}</td>
            <td align="center">{!! $row->eloquent->event_at->format('d/m/Y H:i') !!}</td>
        </tr>
        <?php $counter++; ?>
    @endforeach
    </tbody>
</table>