<?php
$data = [
        'transfer_date' => $model->present()->transferDate,
        'transfer_id' => $model->present()->transferDepartment,
        'transfer_notes' => $model->present()->transferNotesHtml,
]
?>
<p><br></p>
<h5 class="with-border">Dettagli trasferimento</h5>
<div class="tbl tbl-top">
    @foreach($data as $key => $value)
        <div class="tbl-row">
            <div class="tbl-cell tbl-th">{!! trans("service.$key") !!}</div>
            <div class="tbl-cell">{!! $value !!}</div>
        </div>
    @endforeach
</div>