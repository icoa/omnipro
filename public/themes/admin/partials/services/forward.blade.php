<div class="clearfix">
    <component-toolbar inline-template back="{!! route("admin.$module.index") !!}">
        <br>
        <button @click="submit('confirm')" type="button" class="btn btn-success pull-right">
        Avanti <i class="fa fa-angle-right"></i>
        </button>
    </component-toolbar>
</div>