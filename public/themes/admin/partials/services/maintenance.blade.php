<?php
$data = [
        'maintenance_date' => $model->present()->maintenanceDate,
        'notes' => $model->present()->notesHtml,
]
?>
<p><br></p>
<h5 class="with-border">Dettagli manutenzione</h5>
<div class="tbl tbl-top">
    @foreach($data as $key => $value)
        <div class="tbl-row">
            <div class="tbl-cell tbl-th">{!! trans("service.$key") !!}</div>
            <div class="tbl-cell">{!! $value !!}</div>
        </div>
    @endforeach
</div>