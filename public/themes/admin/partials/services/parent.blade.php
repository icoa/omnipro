<?php
$data = [
        'parent_id' => $model->present()->parentNameLinked,
        'type' => $model->parent->present()->typeName,
]
?>
<p><br></p>
<h5 class="with-border">Servizio genitore</h5>
<div class="tbl tbl-top">
    @foreach($data as $key => $value)
        <div class="tbl-row">
            <div class="tbl-cell tbl-th">{!! trans("service.$key") !!}</div>
            <div class="tbl-cell">{!! $value !!}</div>
        </div>
    @endforeach
</div>