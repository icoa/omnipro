<?php
$data = [
        'cost_starts_at' => $model->present()->whenCostStartsAt,
        'days' => $model->present()->daysText,
        'cost' => $model->present()->costEuro,
]
?>
<p><br></p>
<h5 class="with-border">Dettagli amministrativi</h5>
<div class="tbl tbl-top">
    @foreach($data as $key => $value)
        <div class="tbl-row">
            <div class="tbl-cell tbl-th">{!! trans("service.$key") !!}</div>
            <div class="tbl-cell">{!! $value !!}</div>
        </div>
    @endforeach
</div>