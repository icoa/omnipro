@set $events = $model->events()->with('user')->orderBy('created_at','desc')->get();


<div class="comment-rows-container " tabindex="0">
    @foreach($events as $event)
        <div class="comment-row-item">
            <div class="avatar-preview avatar-preview-32">
                <a href="javascript:;">
                    <img src="/assets/images/avatar-2-64.png">
                </a>
            </div>
            <div class="tbl comment-row-item-header">
                <div class="tbl-row">
                    <div class="tbl-cell tbl-cell-name">{!! $event->user->name !!} ({!! $event->user->present()->roleName !!})
                    </div>
                    <div class="tbl-cell tbl-cell-date">{!! $event->present()->createdFull !!}</div>
                </div>
            </div>
            <div class="comment-row-item-content">
                <p>
                    {!! $event->present()->description !!}
                </p>
            </div>
        </div>
    @endforeach
</div>
