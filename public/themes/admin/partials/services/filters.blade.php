<div class="hidden">
    {!! Form::select('_products',Helper::productOptions(),null,['id' => 'productsSelect']) !!}
    {!! Form::select('_status',Helper::serviceStatusOptions(),null,['id' => 'statusSelect']) !!}
    {!! Form::select('_type',Helper::serviceTypesOptions(),null,['id' => 'typeSelect']) !!}
</div>