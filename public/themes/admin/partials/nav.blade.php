<?php
$components = Auth::user()->components;
?>
<div class="mobile-menu-left-overlay"></div>
<nav class="side-menu">
    <ul class="side-menu-list">
        <li class="grey {!! Utils::activeState( 'admin.home', 'opened', true ) !!}">
            <a href="{!! route('admin.home') !!}">
                <i class="font-icon font-icon-speed"></i>
                <span class="lbl">Dashboard</span>
            </a>
        </li>
        @foreach($components as $component)
            <li class="grey {!! Utils::activeState( $component->route, 'opened' ) !!}">
                <a href="{{ $component->present()->link }}">
                    <i class="font-icon {{ $component->present()->icon }}"></i>
                    <span class="lbl">{{ $component->present()->name }}</span>
                </a>
            </li>
        @endforeach
    </ul>
</nav><!--.side-menu-->