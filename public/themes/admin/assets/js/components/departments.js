$(document).ready(function () {
    var $table = $('#table');

    var columns = [
        {data: 'id', name: 'id', class:'center',yadcf:'exact'},
        {data: 'code', name: 'code',yadcf:'default'},
        {data: 'name', name: 'name',yadcf:'default'},
        {data: 'users', name: 'users', class:'center', searchable:false, yadcf:null},
        {data: 'active', name: 'active', class:'center', searchable:true, yadcf:'boolean'},
        {data: 'created_at', name: 'created_at', class:'center', searchable:true, yadcf:'date'},
        {data: 'updated_at', name: 'updated_at', class:'center', searchable:true, yadcf:'date'},
        {data: 'actions', name: 'actions', class: 'center', searchable: false, sorting: false, yadcf:null}
    ];

    YadcfHelper.init(columns);

    var $dataTable = $table.DataTable({
        //dom: 'lfrBtip',
        dom: '<"top clearfix"iBfr>t<"bottom clearfix"lp><"clear">',
        searching: true,
        processing: true,
        serverSide: true,
        ajax: $table.data('uri'),
        language:datatables_i18n['it'],
        buttons: [ 'colvis' ],
        stateSave: false,
        stateDuration: 60 * 60 * 24,
        order: [YadcfHelper.getIndex('name'),'asc'],
        columns: columns
    });

    window.$dataTable = $dataTable;

    yadcf.init($dataTable,YadcfHelper.getDefinitions());

});