$(document).ready(function () {
    var $confirm = $('#confirmImport');

    $confirm.click(function (e) {
        e.preventDefault();
        swal({
                title: "Sei sicuro di voler procedere?",
                text: "Attenzione: non sarai in grado di recuperare i dati sovrascritti",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Sì, procedi",
                cancelButtonText: "No, annulla",
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $("#adminForm")[0].submit();
                }
            });
    })

});