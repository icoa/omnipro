$(document).ready(function () {
    var $table = $('#table');

    var columns = [
        {data: 'id', name: 'services.id', class: 'center', yadcf: 'exact', visible: false},
        {data: 'active_date', name: 'active_date', class: 'center', searchable: true, yadcf: 'date'},
        {data: 'transfer_date', name: 'transfer_date', class: 'center', searchable: true, yadcf: 'date'},
        {data: 'department_code', name: 'departments.code', yadcf: 'default', class: 'center'},
        {data: 'department_name', name: 'departments.name', yadcf: 'default', class: 'center', visible:false},
        {data: 'transfer_name', name: 'transfer.name', yadcf: 'default', class: 'center'},
        {data: 'patient_lastname', name: 'patient_lastname', yadcf: 'default', class: 'center'},
        {data: 'patient_name', name: 'patient_name', yadcf: 'default', class: 'center'},
        {data: 'sdo', name: 'sdo', yadcf: 'default', class: 'center'},
        {data: 'product_name', name: 'products.name', yadcf: 'default', class: 'center'},
        {data: 'injuries', name: 'injuries', yadcf: 'default', class: 'center', yadcf: 'boolean'},
        {data: 'stand_by', name: 'stand_by', yadcf: 'default', class: 'center', yadcf: 'boolean'},
        {
            data: 'status', name: 'status', class: 'center', searchable: true, yadcf: {
            filter_type: "select",
            filter_match_mode: "exact",
            data: parseSelect('#statusSelect')
        }
        },
        {data: 'event_at', name: 'event_at', class: 'center', searchable: true, yadcf: 'date'},
        {data: 'updated_at', name: 'updated_at', class: 'center', searchable: true, yadcf: 'date', visible: false},
        {data: 'actions', name: 'actions', class: 'center', searchable: false, sorting: false, yadcf: null}
    ];


    YadcfHelper.init(columns);

    var $dataTable = $table.DataTable({
        //dom: 'lfrBtip',
        dom: '<"top clearfix"iBfr>t<"bottom clearfix"lp><"clear">',
        searching: true,
        processing: true,
        serverSide: true,
        ajax: $table.data('uri'),
        language: datatables_i18n['it'],
        buttons: ['colvis'],
        stateSave: false,
        stateDuration: 60 * 60 * 24,
        order: [YadcfHelper.getIndex('created_at'), 'desc'],
        columns: columns
    });

    window.$dataTable = $dataTable;

    yadcf.init($dataTable, YadcfHelper.getDefinitions());

});