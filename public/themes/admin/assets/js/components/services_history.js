$(document).ready(function () {
    var $table = $('#table');

    var columns = [
        {data: 'id', name: 'services.id', class: 'center', yadcf: 'exact', visible: false},
        {
            data: 'type', name: 'type', class: 'center', searchable: true, yadcf: {
            filter_type: "select",
            filter_match_mode: "exact",
            data: parseSelect('#typeSelect')
        }
        },
        {data: 'parent_id', name: 'services.parent_id', class: 'center', yadcf: 'exact', visible: false},
        {data: 'sent_activation_at', name: 'sent_activation_at', class: 'center', searchable: true, yadcf: 'date'},
        {data: 'active_date', name: 'active_date', class: 'center', searchable: true, yadcf: 'date', visible:false},
        {data: 'department_code', name: 'departments.code', yadcf: 'default', class: 'center'},
        {data: 'department_name', name: 'departments.name', yadcf: 'default', class: 'center', visible:false},
        {data: 'patient_lastname', name: 'patient_lastname', yadcf: 'default', class: 'center'},
        {data: 'patient_name', name: 'patient_name', yadcf: 'default', class: 'center'},
        {data: 'sdo', name: 'sdo', yadcf: 'default', class: 'center'},
        {data: 'product_name', name: 'products.name', yadcf: 'default', class: 'center'},
        {data: 'injuries', name: 'injuries', class: 'center', yadcf: 'boolean'},
        {data: 'stand_by', name: 'stand_by', class: 'center', yadcf: 'boolean'},
        {data: 'activation_notes', name: 'activation_notes', yadcf: 'default', class: 'center'},
        {
            data: 'status', name: 'status', class: 'center', searchable: true, yadcf: {
            filter_type: "select",
            filter_match_mode: "exact",
            data: parseSelect('#statusSelect')
        }
        },
        {data: 'confirmed_at', name: 'confirmed_at', class: 'center', searchable: true, yadcf: 'date'},
        {data: 'delivered_at', name: 'delivered_at', class: 'center', searchable: true, yadcf: 'date'},
        {data: 'mt_requested_at', name: 'mt_requested_at', class: 'center', searchable: true, yadcf: 'date'},
        {data: 'sent_at', name: 'sent_at', class: 'center', searchable: true, yadcf: 'date'},
        {data: 'disable_date', name: 'disable_date', class: 'center', searchable: true, yadcf: 'date'},
        {data: 'transfer_date', name: 'transfer_date', class: 'center', searchable: true, yadcf: 'date'},
        {data: 'days', name: 'days', yadcf: 'default', class: 'center'},
        {data: 'cost_starts_at', name: 'cost_starts_at', yadcf: 'default', class: 'center'},
        {data: 'cost_ends_at', name: 'cost_ends_at', yadcf: 'default', class: 'center'},
        {data: 'discount', name: 'discount', yadcf: 'default', class: 'center'},
        {data: 'cost', name: 'cost', yadcf: 'default', class: 'center'},
        {data: 'event_at', name: 'event_at', class: 'center', searchable: true, yadcf: 'date', visible: false},
        {data: 'updated_at', name: 'updated_at', class: 'center', searchable: true, yadcf: 'date', visible: false},
        {data: 'actions', name: 'actions', class: 'center', searchable: false, sorting: false, yadcf: null}
    ];


    YadcfHelper.init(columns);

    var $dataTable = $table.DataTable({
        //dom: 'lfrBtip',
        pageLength: 25,
        //scrollY: "420px",
        scrollCollapse: true,
        dom: '<"top clearfix"iBfr>t<"bottom clearfix"lp><"clear">',
        searching: true,
        processing: true,
        serverSide: true,
        ajax: $table.data('uri'),
        language: datatables_i18n['it'],
        buttons: ['colvis'],
        stateSave: true,
        stateDuration: 60 * 60 * 24 * 80,
        order: [YadcfHelper.getIndex('active_date'), 'asc'],
        columns: columns,
        autoWidth: false/*
        scrollX:        true,


        fixedColumns:   {
            leftColumns: 3,
            rightColumns: 1
        }*/
    });

    window.$dataTable = $dataTable;

    yadcf.init($dataTable, YadcfHelper.getDefinitions());

});