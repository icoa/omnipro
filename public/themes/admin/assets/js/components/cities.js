$(document).ready(function () {
    var $table = $('#table');

    var columns = [
        {data: 'id', name: 'id', class: 'center', yadcf: 'exact'},
        {data: 'name', name: 'name', yadcf: 'default'},
        {
            data: 'state_id', name: 'state_id', class: 'center', searchable: true, yadcf: {
            filter_type: "select",
            filter_match_mode: "exact",
            data: parseSelect('#statesSelect')
        }
        },
        {data: 'district', name: 'district', class: 'center', searchable: false, sortable: false, yadcf: null},
        {data: 'created_at', name: 'created_at', class: 'center', searchable: true, yadcf: 'date'},
        {data: 'updated_at', name: 'updated_at', class: 'center', searchable: true, yadcf: 'date'},
        {data: 'actions', name: 'actions', class: 'center', searchable: false, sorting: false, yadcf: null}
    ];

    YadcfHelper.init(columns);

    var $dataTable = $table.DataTable({
        //dom: 'lfrBtip',
        dom: '<"top clearfix"iBfr>t<"bottom clearfix"lp><"clear">',
        searching: true,
        processing: true,
        serverSide: true,
        ajax: $table.data('uri'),
        language: datatables_i18n['it'],
        buttons: ['colvis'],
        stateSave: true,
        stateDuration: 60 * 60 * 24,
        order: [YadcfHelper.getIndex('name'), 'asc'],
        columns: columns
    });

    window.$dataTable = $dataTable;

    yadcf.init($dataTable, YadcfHelper.getDefinitions());

});