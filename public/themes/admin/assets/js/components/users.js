$(document).ready(function () {
    var $table = $('#table');
    if ($table.length) {
        var $dataTable = $table.DataTable({
            //dom: 'lfrBtip',
            dom: '<"top clearfix"iBfr>t<"bottom clearfix"lp><"clear">',
            searching: true,
            processing: true,
            serverSide: true,
            ajax: $table.data('uri'),
            language: datatables_i18n['it'],
            buttons: ['colvis'],
            stateSave: false,
            stateDuration: 60 * 60 * 24,
            order: [6, 'desc'],
            columns: [
                {data: 'id', name: 'users.id', class: 'center'},
                {data: 'users.name', name: 'users.name'},
                {data: 'username', name: 'username', visible:true},
                {data: 'email', name: 'email', visible:false},
                {data: 'role.name', name: 'users.role_id', class: 'center'},
                {data: 'departments', name: 'users.departments', class: 'center', sorting: false},
                {data: 'created_at', name: 'created_at', class: 'center', searchable: true},
                {data: 'updated_at', name: 'updated_at', class: 'center', searchable: true, visible:false},
                {data: 'last_login_at', name: 'last_login_at', class: 'center', searchable: true},
                {data: 'actions', name: 'actions', class: 'center', searchable: false, sorting: false}
            ]
        });

        window.$dataTable = $dataTable;

        yadcf.init($dataTable, [{
            column_number: 0,
            filter_match_mode: "exact",
            filter_type: "text"
        }, {
            column_number: 1,
            filter_type: "text"
        }, {
            column_number: 2,
            filter_type: "text"
        }, {
            column_number: 3,
            filter_type: "text"
        }, {
            column_number: 4,
            filter_type: "select",
            filter_match_mode: "exact",
            data: parseSelect('#rolesSelect')
        },{
            column_number: 5,
            filter_type: "select",
            filter_match_mode: "exact",
            data: parseSelect('#departmentsSelect')
        },{
            column_number: 6,
            filter_type: "date",
            filter_match_mode: "exact",
            date_format: 'YYYY-MM-DD',
            datepicker_type: 'bootstrap-datetimepicker'
        },{
            column_number: 7,
            filter_type: "date",
            filter_match_mode: "exact",
            date_format: 'YYYY-MM-DD',
            datepicker_type: 'bootstrap-datetimepicker'
        },{
            column_number: 8,
            filter_type: "date",
            filter_match_mode: "exact",
            date_format: 'YYYY-MM-DD',
            datepicker_type: 'bootstrap-datetimepicker'
        }]);
    }

    var $form = $('#adminForm');
    if ($form.length) {
        var $role_id = $("#role_id"),
            $role_rel = $(".role_rel");

        $role_id.on('change', function () {
            var val = $(this).val();
            switch (val) {
                case '4':
                    $role_rel.removeClass('hidden');
                    break;

                default:
                    $role_rel.addClass('hidden');
                    break;
            }
        }).trigger('change');
    }
});