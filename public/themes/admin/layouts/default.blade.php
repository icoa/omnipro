<!DOCTYPE html>
<html>
<head>
    <title>{!! Theme::get('title') !!}</title>
    {!! Theme::partial('head') !!}
    {!! Theme::asset()->styles() !!}
    {!! Roomix::configJS() !!}
</head>
<body class="with-side-menu theme-side-caesium-dark-caribbean" id="app">
{!! Theme::partial('header') !!}
{!! Theme::partial('nav') !!}
{!! Theme::content() !!}
{!! Theme::partial('footer') !!}
{!! Theme::asset()->scripts() !!}
<script>
    (function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{!! csrf_token() !!}'
            }
        });
        @if(Auth::user()->pasw_changed != 1 and request('noWarning') != '1')
            swal({
            title: "Avviso di sicurezza",
            text: "La tua password non è stata mai modificata; è consigliabile modificare subito la password del tuo Account per ragioni di sicurezza.",
            type: "warning",
            confirmButtonClass: "btn-warning"
        }, function (isConfirm) {
            if (isConfirm) {
                window.location = '/admin/users/profile?noWarning=1';
            }
        });
        @endif
    })();
</script>
</body>
</html>