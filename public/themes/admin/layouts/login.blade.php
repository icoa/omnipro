<!DOCTYPE html>
<html>
    <head>
        <title>{!! Theme::get('title') !!}</title>
        {!! Theme::partial('head') !!}
        {!! Theme::asset()->styles() !!}
        {!! Roomix::configJS() !!}
    </head>
    <body>
        {!! Theme::content() !!}
        {!! Theme::asset()->scripts() !!}
    </body>
</html>