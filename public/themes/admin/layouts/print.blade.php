<!DOCTYPE html>
<html>
    <head>
        <title>{!! Theme::get('title') !!}</title>
        {!! Theme::partial('head') !!}
        {!! Theme::asset()->styles() !!}
        <style>
            body, html{
                background: #fff !important;
            }
            .buttons{
                position: fixed; right: 10px; top:10px;
            }
        </style>
        <style media="print">
            .buttons{
                display: none !important;
            }
        </style>
    </head>
    <body>
    <div class="buttons">
        <button onclick="window.print();">STAMPA</button>
        <button onclick="window.close();">CHIUDI</button>
    </div>

        {!! Theme::content() !!}
    </body>
</html>