<div class="page-content">
    <div class="container-fluid">
        @themePartial('component.header.default')
        @include('flash::message')
        <section class="box-typical">
            <div class="table-responsive bootstrap-table">
                <table id="table" class="table table-striped table-bordered dataTableFull" data-uri="{!! route("admin.$module.datatable") !!}">
                    <thead>
                    <tr>
                        <th width="60" class="center">ID</th>
                        <th>Nome</th>
                        <th width="200">Provincia</th>
                        <th width="200">Regione</th>
                        <th width="100">Creata il</th>
                        <th width="100">Ult. modifica</th>
                        <th width="100">Azioni</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </section><!--.box-typical-->

    </div><!--.container-fluid-->
</div><!--.page-content-->
<div class="hidden">
    {!! Form::select('_state',Helper::statesOptionsIds(),null,['id' => 'statesSelect']) !!}
</div>