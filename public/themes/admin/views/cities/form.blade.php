{!! BootForm::hidden('task')->value('back')->attribute('v-model','task') !!}
{!! BootForm::text('Nome', 'name')->addGroupClass('row') !!}
{!! BootForm::select('Provincia', 'state_id', Helper::statesOptionsIds())->addGroupClass('row') !!}
{!! BootForm::text('Priorità', 'priority')->addGroupClass('row')->addClass('small') !!}