<div class="page-content">
    <div class="container-fluid">
        @themePartial('component.header.service_history')
        @include('flash::message')
        <section class="box-typical">
            <div class="table-responsive bootstrap-table">
                <table style="table-layout: fixed" id="table" class="table table-striped table-bordered dataTableFull" data-uri="{!! route("admin.$module.datatable") !!}">
                    <thead>
                    <tr>
                        <th width="60" class="center">ID</th>
                        <th width="120">@lang('service.type')</th>
                        <th width="60">@lang('service.parent_id')</th>
                        <th width="100">@lang('service.sent_activation_at')</th>
                        <th width="100">@lang('service.active_date')</th>
                        <th width="100">@lang('service.uo')</th>
                        <th width="200">@lang('service.department_id')</th>
                        <th width="80">@lang('service.patient_lastname')</th>
                        <th width="80">@lang('service.patient_name')</th>
                        <th width="100">@lang('service.sdo')</th>
                        <th width="100">@lang('service.product_id')</th>
                        <th width="100">@lang('service.injuries')</th>
                        <th width="100">@lang('service.stand_by')</th>
                        <th width="100">@lang('service.activation_notes')</th>
                        <th width="100">Status</th>
                        <th width="100">@lang('service.alt.confirmed_at')</th>
                        <th width="100">@lang('service.alt.sent_at')</th>
                        <th width="100">@lang('service.alt.maintenance_date')</th>
                        <th width="100">@lang('service.alt.delivered_at')</th>
                        <th width="100">@lang('service.alt.disable_date')</th>
                        <th width="100">@lang('service.alt.transfer_date')</th>
                        <th width="100">@lang('service.days')</th>
                        <th width="100">@lang('service.cost_starts_at')</th>
                        <th width="100">@lang('service.cost_ends_at')</th>
                        <th width="100">@lang('service.discount')</th>
                        <th width="100">@lang('service.cost')</th>
                        <th width="100">@lang('service.event_at')</th>
                        <th width="100">Ult. modifica</th>
                        <th width="100">Azioni</th>
                    </tr>
                    </thead>
                </table>
            </div>
            @themePartial('services.filters')
        </section><!--.box-typical-->
    </div><!--.container-fluid-->
</div><!--.page-content-->
<style>
    .dataTables_scrollBody .sorting:before,
    .dataTables_scrollBody .sorting_asc:before,
    .dataTables_scrollBody .sorting_desc:before,
    .DTFC_LeftBodyLiner .sorting:before,
    .DTFC_LeftBodyLiner .sorting_asc:before,
    .DTFC_LeftBodyLiner .sorting_desc:before
    {
        display: none !important;
    }

    .DTFC_LeftBodyLiner, .DTFC_RightBodyLiner{
        overflow-x: hidden;
    }
</style>