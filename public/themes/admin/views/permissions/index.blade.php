<div class="page-content">
    <div class="container-fluid">
        @themePartial('component.header.none')
        @include('flash::message')
        {!! BootForm::open()->action( route("admin.$module.store") )->post()->attribute('id','adminForm') !!}
        <section class="box-typical">

            <div class="box-typical-body">

                <div class="table-responsive">
                    <table class="table table-hover">
                        <tbody>
                        @foreach($roles as $role)
                            <?php $selected_components = $role->components->lists('id')->toArray(); ?>
                            <tr>
                                <th class="table-check"></th>
                                <th>{!! $role->name !!}</th>
                            </tr>
                            @foreach($rows as $row)
                                <?php $key = $row->id . "-" . $role->id; ?>
                                <tr>
                                    <td class="table-check">
                                        <div class="checkbox checkbox-only">
                                            <input name="component_role[{!! $role->id !!}][]" type="checkbox" value="{!! $row->id !!}" id="label-{!! $key !!}" {!! in_array($row->id,$selected_components) ? 'checked' : null !!}>
                                            <label for="label-{!! $key !!}"></label>
                                        </div>
                                    </td>
                                    <td>
                                        <label for="label-{!! $key !!}">{!! $row->name !!}</label>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--.box-typical-body-->
        </section>
        <button type="submit" class="btn btn-inline pull-right">SALVA</button>
        {!! BootForm::close() !!}

    </div><!--.container-fluid-->
</div><!--.page-content-->