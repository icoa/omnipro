<?php
$last_login = Cookie::get('last_login');
?>
<div class="page-center">
    <div class="page-center-in">
        <div class="container-fluid">
            <form method="POST" action="/admin/auth/login" id="frm-login" class="sign-box">
                {!! csrf_field() !!}
                <div class="sign-avatar">
                    <img src="/assets/local/logo.png" alt="" width="120" style="width: 120px; margin:0 auto;">
                </div>
                <header class="sign-title">{!! config('roomix.login.title') !!}</header>
                <div class="form-group @if($errors->first('username')) error @endif">
                    {!! Form::text('username', $last_login, ['placeholder' => 'Nome utente', 'required' => 'required', 'class' => 'form-control']) !!}
                    <div class="error-list"><ul><li>{{ $errors->first('username') }}</li></ul></div>
                </div>
                <div class="form-group @if($errors->first('password')) error @endif">
                    {!! Form::password('password', ['placeholder' => 'Password', 'required' => 'required', 'class' => 'form-control']) !!}
                    <div class="error-list"><ul><li>{{ $errors->first('password') }}</li></ul></div>
                </div>
                <div class="form-group">
                    <div class="checkbox float-left">
                        <input type="checkbox" id="signed-in" name="remember" checked/>
                        <label for="signed-in">Ricordami</label>
                    </div>
                    <div class="float-right reset">
                        <a href="mailto:{!! config('roomix.helpdesk.email') !!}"><i class="fa fa-fw fa-life-ring"></i> Helpdesk</a>
                    </div>
                </div>
                <button type="submit" class="btn btn-rounded">ENTRA</button>
                <!--<p class="sign-note align-center">Desideri assistenza telefonica?<br><a href="tel:{!! config('roomix.helpdesk.phone') !!}"><i class="fa fa-fw fa-phone"></i> Chiama lo {!! config('roomix.helpdesk.phone') !!}</a></p>-->
            </form>
        </div>
    </div>
</div><!--.page-center-->