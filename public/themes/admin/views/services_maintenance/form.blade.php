{!! BootForm::hidden('task')->value('back')->attribute('v-model','task') !!}
{!! BootForm::select("Richiedi dall'elenco", 'service_id', Helper::serviceMaintenanceableOptions())->addGroupClass('row')->addClass('medium service_aware select2') !!}
{!! BootForm::text(trans('service.active_date'), 'active_date')->addGroupClass('row')->addClass('small')->readOnly() !!}
{!! BootForm::text(trans('service.maintenance_date'), 'maintenance_date')->addGroupClass('row')->addClass('date small')->attribute('data-mindate',date('Y-m-d')) !!}
{!! BootForm::textarea(trans('service.notes'), 'notes')->addGroupClass('row') !!}
@themePartial('services.forward')