<div class="page-content">
    <div class="container-fluid">
        @themePartial('component.header.default')
        @include('flash::message')
        <section class="box-typical">
            <div class="table-responsive bootstrap-table">
                <table id="table" class="table table-striped table-bordered dataTableFull" data-uri="{!! route("admin.$module.datatable") !!}">
                    <thead>
                    <tr>
                        <th width="60" class="center">ID</th>
                        <th width="100">@lang('service.active_date')</th>
                        <th width="100">@lang('service.maintenance_date')</th>
                        <th width="100">@lang('service.uo')</th>
                        <th width="200">@lang('service.department_id')</th>
                        <th width="80">@lang('service.patient_lastname')</th>
                        <th width="80">@lang('service.patient_name')</th>
                        <th width="100">@lang('service.sdo')</th>
                        <th width="100">@lang('service.product_id')</th>
                        <th width="100">@lang('service.injuries')</th>
                        <th width="100">@lang('service.stand_by')</th>
                        <th width="100">Status</th>
                        <th width="100">@lang('service.sent_at')</th>
                        <th width="100">Ult. modifica</th>
                        <th width="100">Azioni</th>
                    </tr>
                    </thead>
                </table>
            </div>
            @themePartial('services.filters')
        </section><!--.box-typical-->
    </div><!--.container-fluid-->
</div><!--.page-content-->