<div class="page-content">
    <div class="container-fluid">
        @themePartial('component.header.service_confirm')

        <div class="box-typical box-typical-padding">
            @include('flash::message')
            <h5 class="with-border">Riepilogo dei dati inseriti</h5>
            {!! BootForm::openHorizontal($columnSizes)->action( route("admin.$module.send", $model) )->patch()->attribute('id','adminForm') !!}
            {!! BootForm::bind($model) !!}
            {!! BootForm::hidden('id') !!}
            <section class="box-typical proj-page">
                <section class="proj-page-section proj-page-header">
                    <div class="title">
                        Richiesta di manutenzione N° {!! $model->id !!}
                    </div>
                    <a href="{!! route("admin.$module.edit", $model) !!}" class="btn pull-right">Modifica</a>
                </section><!--.proj-page-section-->
                <?php
                $data = [
                        'type' => 'MANUTENZIONE',
                        'sdo' => $model->present()->sdo,
                        'patient_lastname' => $model->present()->patient_lastname,
                        'patient_name' => $model->present()->patient_name,
                        'maintenance_date' => $model->present()->maintenanceDate,
                        'product_id' => $model->present()->productName,
                        'engine_serial' => $model->present()->engine_serial,
                        'notes' => $model->present()->notesHtml,
                ]
                ?>
                <section class="proj-page-section">
                    <div class="tbl tbl-top">
                        @foreach($data as $key => $value)
                            <div class="tbl-row">
                                <div class="tbl-cell tbl-th">{!! trans("service.$key") !!}</div>
                                <div class="tbl-cell">{!! $value !!}</div>
                            </div>
                        @endforeach
                    </div>
                </section>
            </section>
            @themePartial('services.confirm')
            {!! BootForm::close() !!}

        </div><!--.box-typical-->
    </div><!--.container-fluid-->
</div>