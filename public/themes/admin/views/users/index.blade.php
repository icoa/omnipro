<div class="page-content">
    <div class="container-fluid">
        @themePartial('component.header.default')
        @include('flash::message')
        <section class="box-typical">
            <div class="table-responsive bootstrap-table">
                <table id="table" class="table table-striped table-bordered" data-uri="{!! route("admin.$module.datatable") !!}">
                    <thead>
                    <tr>
                        <th width="60" class="center">ID</th>
                        <th>Nome</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Ruolo</th>
                        <th>Dipartimento</th>
                        <th width="100">Creato il</th>
                        <th width="100">Ult. modifica</th>
                        <th width="100">Ult. login</th>
                        <th width="100">Azioni</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </section><!--.box-typical-->

    </div><!--.container-fluid-->
</div><!--.page-content-->
<div class="hidden">
    {!! Form::select('_role',Helper::rolesOptions(),null,['id' => 'rolesSelect']) !!}
    {!! Form::select('_departments',Helper::departmentsOptions(),null,['id' => 'departmentsSelect']) !!}
</div>