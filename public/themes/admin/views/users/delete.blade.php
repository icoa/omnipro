<div data-page="users.delete" class="page">
    <div class="navbar">
        <div class="navbar-inner">
            <div class="left"><a href="{{ route('users.index') }}" class="back link icon-only"><i class="icon icon-back"></i></a></div>
            <div class="center">Cancella utente</div>
            <div class="right"><a href="#" class="link open-panel icon-only"><i class="icon icon-bars"></i></a></div>
        </div>
    </div>
    <div class="page-content">
        <div class="content-block-title">Sei sicuro di voler cancellare questo utente?</div>
        <div class="list-block inputs-list">
            @include('flash::message')
            {!! Form::model($user, ['method' => 'DELETE', 'route' => ['users.destroy', $user]]) !!}
                {!! Form::button('Cancella', ['type' => 'submit']) !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>