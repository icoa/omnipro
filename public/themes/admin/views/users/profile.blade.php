<div class="page-content">
    <div class="container-fluid">
        @themePartial('component.header.profile')
        <div class="box-typical box-typical-padding">
            @include('flash::message')
            <h5 class="with-border">Modifica i dati del tuo Profilo</h5>
            {!! BootForm::openHorizontal($columnSizes)->action( route("admin.$module.updateme", $model) )->patch()->attribute('id','adminForm') !!}
            {!! BootForm::bind($model) !!}
            {!! BootForm::hidden('id') !!}
            {!! BootForm::hidden('task')->value('back')->attribute('v-model','task') !!}
            {!! BootForm::text('Nome', 'name')->addGroupClass('row') !!}
            {!! BootForm::email('Email', 'email')->addGroupClass('row') !!}
            {!! BootForm::text('Username', 'username')->addGroupClass('row') !!}
            {!! BootForm::password('Password', 'password')->addGroupClass('row')->helpBlock('Nota: lasciare vuoto per evitare di re-impostare la Password') !!}
            {!! BootForm::password('Conferma Password', 'password_confirmation')->addGroupClass('row') !!}
            {!! BootForm::close() !!}
        </div><!--.box-typical-->
    </div><!--.container-fluid-->
</div>