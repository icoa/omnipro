{!! BootForm::hidden('task')->value('back')->attribute('v-model','task') !!}
{!! BootForm::text('Nome', 'name')->addGroupClass('row') !!}
{!! BootForm::email('Email', 'email')->addGroupClass('row') !!}
{!! BootForm::text('Username', 'username')->addGroupClass('row') !!}
{!! BootForm::password('Password', 'password')->addGroupClass('row')->helpBlock('Nota: lasciare vuoto per evitare di re-impostare la Password') !!}
{!! BootForm::password('Conferma Password', 'password_confirmation')->addGroupClass('row') !!}
{!! BootForm::select('Ruolo', 'role_id', Helper::rolesOptions(true))->addGroupClass('row') !!}
{!! BootForm::select('Reparti', 'department_options', Helper::departmentsOptions(true))->addGroupClass('row hidden role_rel')->multiple()->addClass('select2') !!}