{!! BootForm::hidden('task')->value('back')->attribute('v-model','task') !!}
{!! BootForm::select("Disattiva dall'elenco", 'service_id', Helper::serviceDisactivableOptions())->addGroupClass('row')->addClass('service_aware medium select2') !!}
{!! BootForm::text(trans('service.active_date'), 'active_date')->addGroupClass('row')->addClass('small')->readOnly() !!}
@if($model)
    {!! BootForm::text(trans('service.discharge_date'), 'discharge_date')->addGroupClass('row')->addClass('date small date_target')->attribute('data-mindate',$model->active_date->format('Y-m-d')) !!}
    {!! BootForm::text(trans('service.disable_date'), 'disable_date')->addGroupClass('row')->addClass('date small date_target')->attribute('data-mindate',$model->active_date->format('Y-m-d')) !!}
@else
    {!! BootForm::text(trans('service.discharge_date'), 'discharge_date')->addGroupClass('row')->addClass('date small date_target') !!}
    {!! BootForm::text(trans('service.disable_date'), 'disable_date')->addGroupClass('row')->addClass('date small date_target') !!}
@endif
{!! BootForm::textarea(trans('service.disactivation_notes'), 'disactivation_notes')->addGroupClass('row') !!}
@themePartial('services.forward')