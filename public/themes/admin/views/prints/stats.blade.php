<h3>Periodo di riferimento: {!! $from !!} - {!! $to !!}</h3>
<h5>Statistiche tavoli</h5>
<table>
    <tr>
        <th colspan="2">Tavoli aperti (dai camerieri)</th>
        <td colspan="2">{!! $stats['services_created']  !!}</td>
    </tr>
    <tr>
        <th colspan="2">Richieste conto (alla cassa)</th>
        <td colspan="2">{!! $stats['services_checkout'] !!}</td>
    </tr>
    <tr>
        <th colspan="2">Tavoli chiusi (dalla cassa)</th>
        <td colspan="2">{!! $stats['services_created'] !!}</td>
    </tr>
</table>

<h5>Statistiche comande</h5>
<table>

    <tr>
        <th colspan="2">Invio comande dai camerieri</th>
        <td colspan="2">{!! $stats['orders_created']  !!}</td>
    </tr>
    <tr>
        <th colspan="2">Comande completate dalla cambusa</th>
        <td colspan="2">{!! $stats['orders_completed'] !!}</td>
    </tr>
    <tr>
        <th colspan="2">Comande verificate dalla cassa</th>
        <td colspan="2">{!! $stats['orders_created'] !!}</td>
    </tr>
</table>
<h5>Statistiche per bottiglia</h5>
<table>
    <thead>
    <tr>
        <th>Denominazione (formato)</th>
        <th>Ordinato</th>
        <th>Stornato</th>
        <th>Venduto</th>
    </tr>
    </thead>
    <tbody>
    @foreach($stats_bottles as $row)
        <tr>
            <td>{!! $row->name !!}</td>
            <td align="center">{!! $row->total !!}</td>
            <td align="center">{!! $row->negative !!}</td>
            <td align="center">{!! $row->real !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>

<style>
    body{
        margin:20px;
    }
    td, th {
        padding: 4px 8px;
        border:1px solid #ccc;
    }
    h5{
        margin-top: 1.2rem;
        margin-bottom: 0.8rem;
    }
</style>