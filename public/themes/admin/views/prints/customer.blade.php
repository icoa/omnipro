@if($model->default_locale == 'it')
<p><br></p>
<div class="visible-print text-center">
    <h3>{!! $model->name !!}</h3>
    <h5>Camera {!! $model->present()->room !!}</h5>
    {!! $model->present()->qrcode !!}
    <p>Esegui una scansione con un lettore QR Code</p>
    <pre class="code">
Accedere con lo smarthphone all'indirizzo: {!! config('app.url') !!}
Inserire come username il valore: {!! $model->username !!}
Inserire come password il valore: {!! $model->password_plain !!}
</pre>
</div>
@endif
@if($model->default_locale == 'en')
<p><br></p>
<div class="visible-print text-center">
    <h3>{!! $model->name !!}</h3>
    <h5>Room {!! $model->present()->room !!}</h5>
    {!! $model->present()->qrcode !!}
    <p>Run a scan with a QR Code reader</p>
    <pre class="code">
With your smartphone go to: {!! config('app.url') !!}
Enter this value as username: {!! $model->username !!}
Enter this value as password: {!! $model->password_plain !!}
</pre>
</div>
@endif