@foreach($rows as $row)
    <div style="padding:20px; border:1px solid #ddd; float:left;">
        <figure>
            {!! $row->qrcode !!}
            <figcaption style="text-align: center; font-size:11px; text-transform: uppercase; font-family: Arial, Helvetica">
                {!! $row->zoneName !!} / {!! $row->name !!}
            </figcaption>
        </figure>
    </div>
@endforeach