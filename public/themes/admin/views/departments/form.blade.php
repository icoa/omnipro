{!! BootForm::hidden('task')->value('back')->attribute('v-model','task') !!}
{!! BootForm::text('Nome', 'name')->addGroupClass('row') !!}
{!! BootForm::text('Codice', 'code')->addGroupClass('row')->addClass('small') !!}
{!! BootForm::select('Status', 'active', Helper::activeOptions())->addGroupClass('row')->addClass('small') !!}
{!! BootForm::textarea('Descrizione', 'description')->addGroupClass('row')->attribute('rows',4) !!}
{!! BootForm::select('Modelli', 'product_options', Helper::productOptions(true))->addGroupClass('row')->multiple()->addClass('select2') !!}