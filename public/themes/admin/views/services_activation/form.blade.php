{!! BootForm::hidden('task')->value('back')->attribute('v-model','task') !!}
{!! BootForm::text(trans('service.active_date'), 'active_date')->addGroupClass('row')->addClass('date small') !!}
{!! BootForm::text(trans('service.sdo'), 'sdo')->addGroupClass('row')->addClass('small input-number') !!}
{!! BootForm::select(trans('service.product_id'), 'product_id', Helper::productOptions())->addGroupClass('row')->addClass('medium') !!}
{!! BootForm::select(trans('service.department_id'), 'department_id', Helper::departmentsOptions(false))->addGroupClass('row')->addClass('medium select2') !!}
{!! BootForm::select(trans('service.injuries'), 'injuries', Helper::booleanOptions())->addGroupClass('row')->addClass('small') !!}
{!! BootForm::select(trans('service.stand_by'), 'stand_by', Helper::booleanOptions())->addGroupClass('row')->addClass('small') !!}
{!! BootForm::text(trans('service.engine_serial'), 'engine_serial')->addGroupClass('row engine-row')->addClass('medium') !!}
<h5 class="with-border">Iniziali paziente</h5>
{!! BootForm::text(trans('service.patient_lastname'), 'patient_lastname')->addGroupClass('row')->addClass('small input-letter') !!}
{!! BootForm::text(trans('service.patient_name'), 'patient_name')->addGroupClass('row')->addClass('small input-letter') !!}
<h5 class="with-border">Note</h5>
{!! BootForm::textarea(trans('service.activation_notes'), 'activation_notes')->addGroupClass('row') !!}
<h5 class="with-border">Scala Braden</h5>
@themePartial('services.braden')
@themePartial('services.forward')