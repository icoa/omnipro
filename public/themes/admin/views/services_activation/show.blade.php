<div class="page-content">
    <div class="container-fluid">
        @themePartial('component.header.back')
        @include('flash::message')
        <div class="row">
            <div class="col-xxl-8 col-lg-8 col-xl-8 col-md-8">
                <div class="box-typical box-typical-padding">
                    <h5 class="with-border">Noleggio N° {!! $model->id !!}</h5>
                    @themePartial('services.main')
                </div><!--.box-typical-->
            </div>
            <div class="col-xxl-4 col-lg-4 col-xl-4 col-md-4">
                <div class="box-typical box-typical-padding">
                    <h5 class="with-border">Cronologia eventi</h5>
                    @themePartial('services.events')
                </div><!--.box-typical-->
            </div>
        </div>

    </div><!--.container-fluid-->
</div>