{!! BootForm::hidden('task')->value('back')->attribute('v-model','task') !!}
{!! BootForm::text('Nome', 'name')->addGroupClass('row') !!}
{!! BootForm::select('Status', 'active', Helper::activeOptions())->addGroupClass('row') !!}
{!! BootForm::textarea('Descrizione', 'description')->addGroupClass('row')->attribute('rows',4) !!}