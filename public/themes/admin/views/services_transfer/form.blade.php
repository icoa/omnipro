{!! BootForm::hidden('task')->value('back')->attribute('v-model','task') !!}
{!! BootForm::select("Richiedi dall'elenco", 'service_id', Helper::serviceTransferableOptions())->addGroupClass('row')->addClass('medium service_aware select2') !!}
{!! BootForm::text(trans('service.active_date'), 'active_date')->addGroupClass('row')->addClass('small')->readOnly() !!}
{!! BootForm::text(trans('service.transfer_date'), 'transfer_date')->addGroupClass('row')->addClass('date small')->attribute('data-mindate',date('Y-m-d')) !!}
{!! BootForm::select(trans('service.transfer_id'), 'transfer_id', Helper::allDepartmentsOptions(true))->addGroupClass('row')->addClass('medium select2') !!}
{!! BootForm::textarea(trans('service.transfer_notes'), 'transfer_notes')->addGroupClass('row') !!}
@themePartial('services.forward')