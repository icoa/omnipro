<div class="page-content">
    <div class="container-fluid">
        @themePartial('component.header.service_create')
        <div class="box-typical box-typical-padding">
            @include('flash::message')
            <h5 class="with-border">Compila i campi per <strong>{!! $entity !!}</strong></h5>
            {!! BootForm::openHorizontal($columnSizes)->action( route("admin.$module.store") )->post()->attribute('id','adminForm') !!}
            @themeView('services_transfer.form')
            {!! BootForm::close() !!}
        </div><!--.box-typical-->
    </div><!--.container-fluid-->
</div>