<div class="page-content">
    <div class="container-fluid">
        @themePartial('component.header.service_update')

        <div class="box-typical box-typical-padding">
            @include('flash::message')
            <h5 class="with-border">Rivedi i campi per <strong>{!! $entity !!}</strong></h5>
            {!! BootForm::openHorizontal($columnSizes)->action( route("admin.$module.update", $model) )->patch()->attribute('id','adminForm') !!}
            <?php $model->service_id = $model->id ?>
            {!! BootForm::bind($model) !!}
            {!! BootForm::hidden('id') !!}
            @themeView('services_transfer.form')
            {!! BootForm::close() !!}

        </div><!--.box-typical-->
    </div><!--.container-fluid-->
</div>