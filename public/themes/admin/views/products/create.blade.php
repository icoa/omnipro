<div class="page-content">
    <div class="container-fluid">
        @themePartial('component.header.upsert')
        <div class="box-typical box-typical-padding">
            @include('flash::message')
            <h5 class="with-border">Compila i campi per questo record</h5>
            {!! BootForm::openHorizontal($columnSizes)->action( route("admin.$module.store") )->post()->attribute('id','adminForm') !!}
            @themeView('products.form')
            {!! BootForm::close() !!}
        </div><!--.box-typical-->
    </div><!--.container-fluid-->
</div>