{!! BootForm::hidden('task')->value('back')->attribute('v-model','task') !!}
{!! BootForm::text('Nome', 'name')->addGroupClass('row') !!}
{!! BootForm::text('Codice', 'code')->addGroupClass('row')->addClass('small') !!}
{!! BootForm::select('Status', 'active', Helper::activeOptions())->addGroupClass('row')->addClass('small') !!}
{!! BootForm::text('Costo giornaliero (&euro;)', 'day_cost')->addGroupClass('row')->addClass('small')->attribute('type','number')->attribute('min',0) !!}
{!! BootForm::select('Reparti', 'department_options', Helper::departmentsOptions(true))->addGroupClass('row')->multiple()->addClass('select2') !!}