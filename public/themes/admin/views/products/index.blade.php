<div class="page-content">
    <div class="container-fluid">
        @themePartial('component.header.default')
        @include('flash::message')
        <section class="box-typical">
            <div class="table-responsive bootstrap-table">
                <table id="table" class="table table-striped table-bordered dataTableFull" data-uri="{!! route("admin.$module.datatable") !!}">
                    <thead>
                    <tr>
                        <th width="60" class="center">ID</th>
                        <th width="100">Codice</th>
                        <th>Nome</th>
                        <th width="120">Costo</th>
                        <th width="100">Status</th>
                        <th width="100">Creato il</th>
                        <th width="100">Ult. modifica</th>
                        <th width="100">Azioni</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </section><!--.box-typical-->

    </div><!--.container-fluid-->
</div><!--.page-content-->