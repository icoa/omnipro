<section class="box-typical proj-page">
    <section class="proj-page-section proj-page-header">
        <div class="title">
            Richiesta servizio N° {!! $model->id !!}
        </div>
    </section><!--.proj-page-section-->
    <section class="proj-page-section">
        @themePartial('services.main')

        @if($model->type == App\Service::TYPE_MAINTENANCE)
            @themePartial('services.maintenance')
        @endif


        @if($model->type == App\Service::TYPE_DISACTIVATION)
            @themePartial('services.disactivation')
        @endif

        @if($model->type == App\Service::TYPE_TRANSFER)
            @themePartial('services.transfer')
        @endif

    </section>
</section>