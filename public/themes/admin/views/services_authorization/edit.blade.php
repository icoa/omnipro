<div class="page-content">
    <div class="container-fluid">
        @themePartial('component.header.service_auth')

        <div class="box-typical box-typical-padding">
            @include('flash::message')
            <h5 class="with-border">Rivedi i campi per <strong>{!! $entity !!}</strong></h5>
            {!! BootForm::openHorizontal($columnSizes)->action( route("admin.$module.update", $model) )->patch()->attribute('id','adminForm') !!}
            {!! BootForm::bind($model) !!}
            {!! BootForm::hidden('id') !!}
            @if($action == 'approve' or $action == 'reject')
                <div class="alert alert-warning">
                    <strong>Attenzione:</strong> stai per
                    @if($action == 'approve') approvare @endif
                    @if($action == 'reject') rifiutare @endif
                    questa richiesta di servizio; per favore per confermare la tua decisione spunta la casella di controllo che trovi di seguito e premi sul pulsante <strong>CONFERMA SCELTA</strong> che trovi in alto.
                </div>
                <input type="hidden" name="action" value="{!! $action !!}">
                <div class="checkbox-toggle -extra-large">
                    <input type="checkbox" name="confirm" value="1" id="confirm">
                    <label for="confirm">Confermo
                        <strong>
                            @if($action == 'approve') l'approvazione @endif
                            @if($action == 'reject') il rifiuto @endif
                        </strong>
                        di questo servizio</label>
                </div>
                @if($errors->has('confirm'))
                <div class="alert alert-danger">
                    <strong>Conferma non riuscita:</strong> Devi confermare la tua azione spuntando la casella di controllo!
                </div>
                @endif
            @endif

            <p><br></p>
            @themeView('services_authorization.form')
            {!! BootForm::close() !!}

        </div><!--.box-typical-->
    </div><!--.container-fluid-->
</div>