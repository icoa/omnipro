<div class="page-content">
    <div class="container-fluid">
        @include('flash::message')
        <h5 class="with-border">Risultati di ricerca per <strong>"{!! $query !!}"</strong></h5>
        <section class="box-typical">

            <div class="table-responsive bootstrap-table">
                <table id="table" class="table table-striped table-bordered dataTable dataTableFull" style="margin: 0 !important;">
                    <thead>
                    <tr>
                        <th width="60" class="align-center">#</th>
                        <th width="200" class="align-center">Tipo</th>
                        <th>Denominazione</th>
                        <th width="150" class="align-center">Azioni</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $counter = 0 ?>
                    @forelse($results as $row)
                        <?php $counter++ ?>
                        <tr>
                            <th class="align-center">{!! $counter !!}</th>
                            <td class="align-center">{!! $row->type !!}</td>
                            <td><strong>{!! $row->name !!}</strong></td>
                            <td class="align-center">
                                <a href="{!! $row->url !!}" class="btn btn-secondary">Mostra</a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3" class="align-center">Nessun risultato di ricerca</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </section><!--.box-typical-->

    </div><!--.container-fluid-->
</div><!--.page-content-->