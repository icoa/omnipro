<div class="panel panel-left panel-cover" id="leftPanel">
    @if(isset($user))
    <div class="content-block">
        <div class="row user-box">
            <div class="col-33">
                <i class="icon m-icon user-icon">account_circle</i>
            </div>
            <div class="col-66">
                <h3>{!! $user->name !!}</h3>
                <h5>{!! $user->rolename !!}</h5>
            </div>
        </div>
    </div>
    @endif
    <div class="content-block">
        <p>
            <button type="button" class="button button-fill app-action logout button-raised" data-action="logout">ESCI
            </button>
        </p>
    </div>
</div>