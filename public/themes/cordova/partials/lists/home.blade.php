@forelse($rows as $row)
    <div class="card with-border" id="order_{!! $row->id !!}" style="border-color: rgba({!! $row->service->present()->rgb !!},1)">
        <div class="card-header">
            <strong class="ellipsis">{!! $row->service->present()->tableListColors !!} <span class="rc">{!! $row->service->name !!}</span></strong>
            <small>{!! $row->createdBy->present()->name !!}</small>
        </div>
        <div class="card-content">
            <div class="list-block media-list bottles-list">
                <ul>
                    @foreach($row->bottles as $bottle)
                        <li class="swipeout" data-id="{!! $bottle->pivot->id !!}" data-order="{!! $row->id !!}" data-done="0">
                            <div class="swipeout-content">
                                <div class="item-content">
                                    <div class="item-inner">
                                        <div class="item-title-row">
                                            <div class="item-title rc">
                                                <strong>{!! $bottle->name !!}</strong>
                                            </div>
                                            <div class="item-after">
                                                    <span class="format rc">{!! $bottle->present()->formatName !!}</span>
                                                    <span class="button button-raised button-quantity">{!! $bottle->pivot->quantity !!}</span>
                                            </div>
                                        </div>
                                        <!--<div class="item-text"><span class="">{!! $bottle->present()->formatName !!}</span></div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="swipeout-actions-right">
                                <a class="swipeout-overswipe bg-green app-action" data-action="toggleBottle"><i class="icon m-icon" style="margin-right: 5px">done</i>FATTO</a>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="card-footer">
            {!! $row->present()->createdFull !!}
            <div class="pull-right right">
                <button class="button button-fill button-raised bg-green app-action" data-post="{!! route('cordova.home.done',$row->id) !!}" data-order="{!! $row->id !!}" data-action="sendOrder" disabled><i class="icon m-icon">done_all</i> CONFERMA</button>
            </div>
        </div>
    </div>
@empty
<div class="alert">Al momento non ci sono comande</div>
@endforelse