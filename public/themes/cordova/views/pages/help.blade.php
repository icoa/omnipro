<div data-page="help" class="page">
    <div class="navbar">
        <div class="navbar-inner">
            <div class="left"><a href="{{ route('cordova.home') }}" class="back link icon-only"><i class="icon icon-back"></i></a></div>
            <div class="center">Aiuto</div>
            <div class="right">{!! Theme::partial('nabvar-notifications') !!}</div>
        </div>
    </div>
    <div class="page-content">
        <div class="content-block-title">Guida ufficiale di Ristomix</div>
        <div class="content-block">
            <p>Ristomix è un'applicazione "web-based", ovvero richiede il browser Chrome Mobile o simile, per essere eseguito.</p>
            <p>Ristomix permette di gestire il tuo flusso di lavoro all'interno del proprio albergo, a seconda del ruolo che ricopri.</p>
        </div>
        <div class="content-block-title">Interfaccia Utente</div>
        <div class="content-block">
            <p>L'interfaccia di Ristomix è molto semplice: nella pagina principale dell'applicazione troverai un elenco di strumenti a cui puoi accedere, così come nel menù a comparsa.</p>
            <p>Ogni strumento presenta una serie di pulsanti o azioni che in quel momento puoi eseguire.</p>
        </div>
    </div>
</div>