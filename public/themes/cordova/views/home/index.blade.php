<div data-page="index" class="page" data-module="galley">

    <div class="navbar">
        <div class="navbar-inner">
            <div class="left">
                <a href="#" class="link icon-only app-action logout" data-action="logout"><i class="icon m-icon">exit_to_app</i></a>
            </div>
            <div class="center sliding">
                Cambusa
            </div>
        </div>
    </div>

    <div class="page-content material">
        <div id="shouldUpdate" data-route="{!! route('cordova.home.order', null) !!}" data-reload="{!! route('cordova.home.orders') !!}">
            {!! Theme::partial('lists.home',compact('rows','module','user')) !!}
        </div>
    </div>
</div>
