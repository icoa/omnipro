/**
 * Created by f.politi on 13/04/2016.
 */
(function () {
    var isCordovaApp = navigator.userAgent.indexOf('Crosswalk') > 0;
    var app = {
        initialize: function () {
            alert('CordovaApp: '+isCordovaApp)
            jQuery("#gcm_id").val('test');
            this.bindEvents();
        },
        bindEvents: function () {
            // Here we register our callbacks for the lifecycle events we care about
            document.addEventListener('deviceready', this.onDeviceReady, false);
            document.addEventListener('pause', this.onPause, false);
            document.addEventListener('resume', this.onResume, false);
        },
        onDeviceReady: function () {
            //alert("onDeviceReady");

            navigator.notification.alert(
                "You are on page '" + document.title + "'",                 // message
                function () {
                    console.log('Alert callback called!');
                }, // callback
                'Messaggio',         // title
                'Fatto'             // buttonName
            );


            var push = PushNotification.init({
                "android": {"senderID": "546250348399"},
                "ios": {"alert": "true", "badge": "true", "sound": "true"}, "windows": {}
            });

            push.on('registration', function (data) {
                alert('REGID: '+data.registrationId);
                jQuery("#gcm_id").val(data.registrationId);
            });

            push.on('notification', function (data) {
                console.log(data.message);
                alert(data.title + " Message: " + data.message);
                // data.title,
                // data.count,
                // data.sound,
                // data.image,
                // data.additionalData
            });

            push.on('error', function (e) {
                alert('ERROR: '+e.message);
            });
        },
        onPause: function () {
            // Here, we check to see if we are in the middle of taking a picture. If
            // so, we want to save our state so that we can properly retrieve the
            // plugin result in onResume(). We also save if we have already fetched
            // an image URI
            alert("onPause");
        },
        onResume: function (event) {
            // Here we check for stored state and restore it if necessary. In your
            // application, it's up to you to keep track of where any pending plugin
            // results are coming from (i.e. what part of your code made the call)
            // and what arguments you provided to the plugin if relevant
            alert("onResume");
        }
    };


    app.initialize();

})();