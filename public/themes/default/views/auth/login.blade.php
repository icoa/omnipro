<?php
$last_login = Cookie::get('last_login');
?>
<div class="page" id="login" data-page="login">
    <!-- page-content has additional login-screen content -->
    <div class="page-content login-screen-content">
        <div class="row">
            <div class="col-100 tablet-50">
                <div class="card">
                    <div class="card-header">Accesso Autisti</div>
                    <div class="card-content">
                        <div class="card-content-inner">
                            <!-- Login form -->
                            <img src="/assets/local/logo.png" width="120" height="120" style="margin:15px auto; display: block">

                            @include('flash::message')
                            @if ($errors->has('error'))
                                <div class="alert alert-danger">{{ $errors->first('error') }}</div>
                            @endif
                            @if(App::environment() == 'demo')
                                <div class="alert alert-warning" style="font-size: 80%">Attenzione: la modalità <strong>DEMO</strong> è attiva; puoi utilizzare <strong>"test"</strong> come password di accesso per tutte le Utenze; ti ricordiamo inoltre che tutti i dati e le attività verranno resattati ogni giorno alle 00:00.</div>
                            @endif
                            <form method="POST" action="/auth/login" id="frm-login">
                                {!! csrf_field() !!}
                                <div class="list-block">
                                    <ul>
                                        <li class="item-content">
                                            <div class="item-inner">
                                                <div class="item-title label">@lang('app.username.label')</div>
                                                <div class="item-input">
                                                    {!! Form::select('username', Helper::usernamesByRole(3), $last_login, ['placeholder' => 'Inserisci il nome utente', 'required' => 'required']) !!}
                                                </div>
                                                <div class="item-error">{{ $errors->first('username') }}</div>
                                            </div>
                                        </li>
                                        <li class="item-content">
                                            <div class="item-inner">
                                                <div class="item-title label">@lang('app.password.label')</div>
                                                <div class="item-input">
                                                    {!! Form::password('password', ['placeholder' => trans('app.password.placeholder'), 'required' => 'required']) !!}
                                                </div>
                                                <div class="item-error">{{ $errors->first('password') }}</div>
                                            </div>
                                        </li>
                                        <li>
                                            <label class="label-checkbox item-content">
                                                <input type="checkbox" name="remember" checked>
                                                <div class="item-media"><i class="icon icon-form-checkbox"></i></div>
                                                <div class="item-inner">
                                                    <div class="item-title">@lang('app.rememberme')</div>
                                                </div>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                                <div class="content-block">
                                    <button type="submit" class="button button-big button-fill">@lang('app.enter')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>