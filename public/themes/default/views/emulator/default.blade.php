<div id="device-resizer">
<div id="device-container" class="style-scope device-resizer">
    <div class="device-list style-scope device-resizer" view="handset">
        <device-view id="handset" device="{!! $size !!}" aspect="16x9" class="style-scope device-resizer">
            <div class="frame-container style-scope device-view">
                <iframe id="frame-content" class="style-scope device-view" src="{!! $url !!}?emulator=1"></iframe>
            </div>
        </device-view>
    </div>
</div>
</div>