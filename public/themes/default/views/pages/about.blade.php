<div data-page="about" class="page">
    <div class="navbar">
        <div class="navbar-inner">
            <div class="left"><a href="/" class="back link icon-only ajax"><i class="icon icon-back"></i></a></div>
            <div class="center">About OmniPro</div>
        </div>
    </div>
    <div class="page-content">
        <div class="content-block-title">Benvenuto in OmniPro</div>
        <div class="content-block">
            <p>OmniPro è un'applicazione "web-based", ovvero richiede il browser Chrome Mobile o simile, per essere eseguito.</p>

            <p>OmniPro permette di gestire il tuo flusso di lavoro all'interno del proprio ospedale, a seconda del ruolo che ricopri.</p>

            <p>Questa applicazione richiede Javascript abilitato per essere eseguita senza problemi;
                in caso di errori o malfunzionamenti ti preghiamo di utilizzare
                la guida fornita all'interno dell'applicazione stessa (sezione "Aiuto" del menù principale)
                o di segnalare il problema tramite la pagina del servizio (vedi "Segnala un problema" dal menù principale)</p>

            <p>Ti auguriamo un'esperienza utente senza particolari problemi!</p>
        </div>
    </div>
</div>