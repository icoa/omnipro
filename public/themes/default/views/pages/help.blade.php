<div data-page="help" class="page">
    <div class="navbar">
        <div class="navbar-inner">
            <div class="left"><a href="/" class="back link icon-only ajax"><i class="icon icon-back"></i></a></div>
            <div class="center">Aiuto</div>
        </div>
    </div>
    <div class="page-content">
        <div class="content-block-title">Guida ufficiale di OmniPro</div>
        <div class="content-block">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris et laoreet sapien. Quisque ut sem ac lacus aliquam placerat. Nullam interdum nulla ex, in condimentum est accumsan vitae. Ut non dui non odio suscipit blandit. In dignissim faucibus maximus. Curabitur tristique dui dapibus felis vestibulum posuere. Vestibulum eu ullamcorper ante. Fusce non consectetur nibh, id rhoncus diam. Etiam quis velit nec quam sagittis sagittis. Praesent eu orci a elit feugiat feugiat eget eget risus. Suspendisse ut metus aliquet, viverra ipsum ac, blandit augue.</p>

            <p>Integer ex orci, eleifend vulputate consequat at, fringilla viverra quam. Mauris quis consectetur nunc. Sed sed pellentesque leo, feugiat facilisis leo. Donec efficitur turpis justo, at fermentum mi fermentum eget. Cras posuere lorem id imperdiet ornare. Nulla mattis ipsum a ex luctus auctor. Sed lobortis lacinia mauris ac rutrum. Fusce pellentesque laoreet sollicitudin. Etiam et libero massa. Nam nisl arcu, sollicitudin ac feugiat ut, convallis nec metus. Nam fermentum ac justo a tempor. Ut sed dictum nibh. Donec nec urna porttitor, sodales libero non, venenatis purus. Vivamus molestie vulputate tellus, ac tempor diam facilisis at. Maecenas et augue nulla.</p>

            <p>Maecenas sit amet leo sem. Curabitur efficitur dignissim mi in elementum. Curabitur luctus massa eu diam varius, ut vestibulum metus sodales. Vestibulum eget lacus mattis, pulvinar dui vitae, scelerisque massa. Aliquam et tellus lacinia, semper dui eget, tristique mauris. Nam convallis, felis nec pellentesque dapibus, mi risus molestie turpis, at egestas enim lectus sit amet neque. Donec pharetra massa elit, sed rhoncus odio suscipit vel. Curabitur a risus felis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed in gravida ante, vitae lobortis felis. Etiam lorem neque, iaculis vel pretium quis, finibus vel nulla. Fusce cursus lorem quam, imperdiet volutpat augue placerat vitae. Mauris sollicitudin nisl quis malesuada varius.</p>
        </div>

    </div>
</div>