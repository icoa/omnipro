<div data-page="{{ $module }}.edit" data-module="{{ $module }}" class="page" id="serviceComponent">
    <div class="navbar">
        <div class="navbar-inner">
            <div class="left"><a href="{{ route("$module.index") }}" class="back ajax link icon-only"><i class="icon icon-back"></i></a></div>
            <div class="center">Servizio N° {!! $model->id !!}</div>
            <div class="right">
                <a class="link" v-on:click='submit'>
                    <i class="icon m-icon">done</i>
                </a>
            </div>
        </div>
    </div>
    <div class="page-content">
        {!! Form::model($model, ['id' => 'mainForm', 'method' => 'PATCH', 'route' => ["$module.update", $model], 'v-on:submit' => 'submitForm']) !!}
        {!! Form::hidden('id') !!}
        @include("theme.default::views.$module.form")
        {!! Form::close() !!}
    </div>
</div>