<div data-page="{{ $module }}.barcode" data-module="{{ $module }}" class="page" id="barcodeServiceComponent">
    <div class="navbar">
        <div class="navbar-inner">
            <div class="left">
                <a href="{{ route("$module.index") }}" class="back ajax link icon-only">
                    <i class="icon icon-back"></i>
                </a>
            </div>
            <div class="center">Ricerca servizio</div>
        </div>
    </div>
    <div class="page-content">
        {!! Form::model(null, ['id' => 'barcodeForm','route' => "$module.search", 'v-on:submit' => 'submitForm']) !!}
        <div class="list-block inputs-list">
            <ul>
                <li>
                    <div class="item-content">
                        <div class="item-inner">
                            <div class="item-title label"><label for="code">Inserisci qui il Barcode</label></div>
                            <div class="item-input item-input-field">
                                <input type="search" name="code" id="code" placeholder="Codice a barre da ricercare" v-el="code" v-model="code" @keydown='keyDown'>
                            </div>
                            <div class="item-error">{{ $errors->first('code') }}</div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="content-block">
            <div class="row">
                <div class="col-50">
                    <button type="button" class="button button-fill button-raised" v-el:reset @click="reset"
                            style="width: 100%">RESET
                    </button>
                </div>
                <div class="col-50">
                    <button type="submit" class="button button-fill button-raised bg-green"
                            v-el:submit style="width: 100%">RICERCA
                    </button>
                </div>
            </div>
        </div>
        <div class="content-block">
            <div class="list-block media-list" id="results"></div>
        </div>
        {!! Form::close() !!}
    </div>
</div>