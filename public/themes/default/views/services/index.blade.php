<?php $random = time(); ?>
<div data-page="{{ $module }}.index" data-module="{{ $module }}" class="page page-rooms-search">
    <div class="navbar">
        <div class="navbar-inner">
            <div class="left">
                @if(method_exists($rows, 'previousPageUrl') and $rows->previousPageUrl())
                    <a href="{!! $rows->previousPageUrl() !!}&r={!! $random !!}" class="link icon-only ajax" data-ignore-cache="true"><i class="icon icon-back"></i></a>
                @else
                    <a href="{{ route('home') }}" class="link icon-only ajax"><i class="icon icon-back"></i></a>
                @endif
            </div>
            <div class="center">
                Elenco servizi
                @if($mode and $mode == 'latest')
                    - Ult. 30 giorni
                @endif
            </div>
            @if(method_exists($rows, 'nextPageUrl') and $rows->nextPageUrl())
                <div class="right">
                    <a href="{!! $rows->nextPageUrl() !!}&r={!! $random !!}" class="link icon-only ajax" data-ignore-cache="true"><i class="icon icon-next"></i></a>
                </div>
            @endif
        </div>
    </div>

    <form data-search-list=".search-here" data-search-in=".item-title" class="searchbar searchbar-init">
        <div class="searchbar-input">
            <input type="search" placeholder="{!! trans('app.search') !!} (in questa pagina)"/><a class="searchbar-clear"></a>
        </div>
    </form>

    <div class="searchbar-overlay"></div>

    <div class="page-content">
        {!! Theme::partial('search-not-found') !!}
        @include('flash::message')
        @if(method_exists($rows, 'currentPage'))
            <div class="content-block-title">
                <strong>{!! $rows->total() !!}</strong> servizi
                <span class="pull-right">Pagina <strong>{!! $rows->currentPage() !!}</strong> di <strong>{!! $rows->lastPage() !!}</strong></span>
            </div>
        @endif
        <div class="list-block media-list search-here searchbar-found" id="shouldUpdate" data-type="partial" data-name="services">
            @themePartial('lists.services')
        </div>
        <div class="content-block">
            <div class="row">
                <div class="col s6">
                    @if(method_exists($rows, 'previousPageUrl') and $rows->previousPageUrl())
                        <a href="{!! $rows->previousPageUrl() !!}&r={!! $random !!}" class="button button-raised ajax" data-ignore-cache="true">PRECEDENTE</a>
                    @endif
                </div>
                <div class="col s6">
                    @if(method_exists($rows, 'nextPageUrl') and $rows->nextPageUrl())
                        <a href="{!! $rows->nextPageUrl() !!}&r={!! $random !!}" class="button button-raised ajax" data-ignore-cache="true">SUCCESSIVO</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>