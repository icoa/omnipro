<div id="step1">
    @themePartial('services.main')
    @if($model->type == App\Service::TYPE_MAINTENANCE)
        @themePartial('services.maintenance')
    @endif
    @if($model->type == App\Service::TYPE_DISACTIVATION)
        @themePartial('services.disactivation')
    @endif
    @if($model->type == App\Service::TYPE_TRANSFER)
        @themePartial('services.transfer')
    @endif
    <input type="hidden" name="action" value="{!! $action !!}">
    <div class="content-block">
        <input class="button button-fill button-raised button-big {!! $button_color !!}" type="submit"
               value="{!! $actionLabel !!}" v-el:submit>
    </div>
</div>
<div id="step2" class="hidden">
    @if($model->type == App\Service::TYPE_MAINTENANCE)
    <div class="content-block-title">Azioni eseguite</div>
    <div class="content-block list-block inputs-list">
        <div class="item-input item-input-field">
            <textarea name="mt_actions_performed" placeholder="Scrivi qui il dettaglio delle azioni eseguite per la manutenzione di questo servizio..."></textarea>
        </div>
    </div>
    @endif
    <div class="content-block-title">Firma grafometrica</div>
    <div class="content-block">
        <div id="signature_pad" style="border:2px solid #333">
            <canvas height="320"></canvas>
        </div>
    </div>
    <textarea id="signature" name="signature" class="hidden"></textarea>

    <div class="content-block">
        <button type="button" class="button button-fill button-raised" v-el:reset @click="reset" style="width: 100%">RESET FIRMA</button>
    </div>
    <div class="content-block">
        <button type="submit" class="button button-fill button-raised button-big bg-green" v-el:confirm @click="confirm" style="width: 100%">CONFERMA ED INVIA</button>
    </div>
</div>
