<div data-page="index" class="page">

    <div class="navbar">
        <div class="navbar-inner">
            <div class="left">
                <a href="#" class="link icon-only app-action logout" data-action="logout"><i class="icon m-icon">exit_to_app</i></a>
            </div>
            <div class="center sliding">{!! $user->name !!}</div>
            <div class="right">
                @foreach($helper->routes()['toolbar'] as $route)
                    <a
                            href="{!! $route['url'] !!}"
                            class="link icon-only {!! isset($route['class']) ? $route['class'] : '' !!}"
                            {!! isset($route['data']) ? implode(' ',$route['data']) : '' !!}
                    >
                        <span class="icon m-icon">{!! $route['icon'] !!}</span>
                    </a>
                @endforeach
            </div>
        </div>
    </div>

    <div class="page-content material">

        <div class="content-block button-grid" style="margin-top:0.5rem; margin-bottom: 0">
            <div class="row">
                @foreach($helper->routes()['main'] as $route)
                    <div class="col-100">
                        <a
                                href="{!! $route['url'] !!}"
                                class="button button-raised button-square {!! isset($route['class']) ? $route['class'] : '' !!}"
                                {!! isset($route['data']) ? implode(' ',$route['data']) : '' !!}
                        >
                            <span class="icon m-icon">{!! $route['icon'] !!}</span>
                            <span class="text">{!! $route['title'] !!}</span>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>

    </div>
</div>
