<!DOCTYPE html>
<html>
    <head>
        <title>{!! Theme::get('title') !!}</title>
        {!! Theme::partial('head') !!}
        {!! Theme::asset()->styles() !!}
        {!! Roomix::configJS() !!}
        <style>
            .page > .searchbar-container {
                z-index: 200;
            }
        </style>
    </head>
    <body id="app">
        @themePartial('header')

        {!! Theme::content() !!}

        {!! Theme::partial('footer') !!}

        {!! Theme::asset()->scripts() !!}
    </body>
</html>