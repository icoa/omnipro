<!DOCTYPE html>
<html>
    <head>
        <title>{!! Theme::get('title') !!}</title>
        {!! Theme::partial('head') !!}
        {!! Theme::asset()->styles() !!}
    </head>
    <body class="">
        {!! Theme::content() !!}

        {!! Theme::asset()->scripts() !!}
    </body>
</html>