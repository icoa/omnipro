<div class="panel panel-left panel-cover" id="leftPanel">
    @if(false)
    @if (isset($user))
        <div class="content-block">
            <div class="row user-box">
                <div class="col-33">
                    <i class="icon m-icon user-icon">account_circle</i>
                </div>
                <div class="col-66">
                    <h3>{!! $user->name !!}</h3>
                    <h5>{!! $user->username !!}</h5>
                </div>
            </div>
        </div>

        <div class="list-block">
            <ul>
                @foreach($helper->routes() as $block)

                    @foreach($block['routes'] as $route)
                        <li>
                            <a
                                    href="{!! $route['url'] !!}"
                                    class="item-link {!! isset($route['class']) ? $route['class'] : '' !!}"
                                    {!! isset($route['data']) ? implode(' ',$route['data']) : '' !!}
                            >
                                <div class="item-content">
                                    <div class="item-media"><i class="icon m-icon">{!! $route['icon'] !!}</i></div>
                                    <div class="item-inner">
                                        <div class="item-title">{!! $route['title'] !!}</div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    @endforeach

                @endforeach
            </ul>
        </div>
    @endif
    @endif
    <div class="content-block">
        <p>
            <button type="button" class="button button-fill app-action logout button-raised" data-action="logout">ESCI
            </button>
        </p>
    </div>
</div>