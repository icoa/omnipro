<div class="content-block-title">@lang('app.components.tables.title')</div>
<div class="list-block">
    <ul>
        <li>
            <a href="#" class="item-link">
                <div class="item-content">
                    <div class="item-media"><i class="icon m-icon">add_alert</i></div>
                    <div class="item-inner">
                        <div class="item-title">{!! trans('app.components.tables.index') !!}</div>
                    </div>
                </div>
            </a>
        </li>
        <li>
            <a href="#" class="item-link">
                <div class="item-content">
                    <div class="item-media"><i class="icon m-icon">add_alert</i></div>
                    <div class="item-inner">
                        <div class="item-title">{!! trans('app.components.tables.create') !!}</div>
                    </div>
                </div>
            </a>
        </li>
    </ul>
</div>


<div class="content-block-title">@lang('app.components.orders.title')</div>
<div class="list-block">
    <ul>
        <li>
            <a href="#" class="item-link">
                <div class="item-content">
                    <div class="item-media"><i class="icon m-icon">speaker_notes</i></div>
                    <div class="item-inner">
                        <div class="item-title">{!! trans('app.components.orders.index') !!}</div>
                    </div>
                </div>
            </a>
        </li>
        <li>
            <a href="#" class="item-link">
                <div class="item-content">
                    <div class="item-media"><i class="icon m-icon">speaker_notes</i></div>
                    <div class="item-inner">
                        <div class="item-title">{!! trans('app.components.orders.create') !!}</div>
                    </div>
                </div>
            </a>
        </li>
    </ul>
</div>


<div class="content-block-title">@lang('app.other')</div>
<div class="list-block">
    <ul>
        <li>
            <a href="{!! route('pages.help') !!}" class="item-link">
                <div class="item-content">
                    <div class="item-media"><i class="icon m-icon">help</i></div>
                    <div class="item-inner">
                        <div class="item-title">{!! trans('app.components.pages.help') !!}</div>
                    </div>
                </div>
            </a>
        </li>
        <li>
            <a href="{!! route('pages.about') !!}" class="item-link">
                <div class="item-content">
                    <div class="item-media"><i class="icon m-icon">explore</i></div>
                    <div class="item-inner">
                        <div class="item-title">{!! trans('app.components.pages.about') !!}</div>
                    </div>
                </div>
            </a>
        </li>
    </ul>
</div>