<?php $qty =0 ?>
<a id="btn-notifications" href="#" class="link icon-only">
    <i class="icon m-icon"><?= $qty == 0 ? 'notifications_none' : 'notifications_active'?></i>
    <span id="countNotification" class="badge bg-red"
          @if($qty == 0)
          style="display: none;"
            @endunless
    >{{ $qty }}</span>
</a>
