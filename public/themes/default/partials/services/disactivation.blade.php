<?php
$data = [
        'discharge_date' => $model->present()->dischargeDate,
        'disable_date' => $model->present()->disableDate,
        'disactivation_notes' => $model->present()->disactivationNotesHtml,
]
?>
<div class="list-block">
    <div class="content-block-title no-bottom">Dettagli disattivazione</div>
</div>
<div class="list-block inputs-list">
    <ul>
        @foreach($data as $key => $value)
            <li>
                <div class="item-content">
                    <div class="item-inner">
                        <div class="item-title label">{!! trans("service.$key") !!}</div>
                        <div class="item-input-field">
                            <span>{!! $value !!}</span>
                        </div>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
</div>