
<?php
$data = [
        'type' => $model->present()->typeText,
        'active_date' => $model->present()->activeDate,
        'sdo' => $model->present()->sdo,
        'patient_initials' => $model->present()->patient_initials,
        'product_id' => $model->present()->productName,
        'uo' => $model->present()->uo,
        'department_id' => $model->present()->departmentName,
        'engine_serial' => $model->present()->engine_serial,
        'injuries' => $model->present()->injuriesText,
        'stand_by' => $model->present()->standByText,
        'activation_notes' => $model->present()->activationNotesHtml,
]
?>
<div class="list-block inputs-list no-top">
    <div class="content-block-title">Dettagli servizio <span class="pull-right">{!! $model->present()->statusBadge !!}</span></div>
    <ul>
        @foreach($data as $key => $value)
            <li>
                <div class="item-content">
                    <div class="item-inner">
                        <div class="item-title label">{!! trans("service.$key") !!}</div>
                        <div class="item-input-field">
                            <span>{!! $value !!}</span>
                        </div>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
</div>