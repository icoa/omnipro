<?php
$data = [
        'transfer_date' => $model->present()->transferDate,
        'transfer_id' => $model->present()->transferDepartment,
        'transfer_notes' => $model->present()->transferNotesHtml,
]
?>
<div class="list-block">
    <div class="content-block-title no-bottom">Dettagli trasferimento</div>
</div>
<div class="list-block inputs-list">
    <ul>
        @foreach($data as $key => $value)
            <li>
                <div class="item-content">
                    <div class="item-inner">
                        <div class="item-title label">{!! trans("service.$key") !!}</div>
                        <div class="item-input-field">
                            <span>{!! $value !!}</span>
                        </div>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
</div>