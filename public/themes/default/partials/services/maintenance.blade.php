<?php
$data = [
        'maintenance_date' => $model->present()->maintenanceDate,
        'notes' => $model->present()->notesHtml,
]
?>
<div class="list-block inputs-list">
    <div class="content-block-title">Dettagli manutenzione</div>
    <ul>
        @foreach($data as $key => $value)
            <li>
                <div class="item-content">
                    <div class="item-inner">
                        <div class="item-title label">{!! trans("service.$key") !!}</div>
                        <div class="item-input-field">
                            <span>{!! $value !!}</span>
                        </div>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
</div>