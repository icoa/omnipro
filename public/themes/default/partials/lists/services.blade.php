<ul>
    @forelse($rows as $row)
        <li>
            <a href="{!! route("services.edit", $row) !!}?reload=1" class="item-link item-content ajax" data-ignore-cache="true">
                <div class="item-media">
                    {!! $row->present()->typeBadge !!}
                </div>
                <div class="item-inner">
                    <div class="item-title-row">
                        <div class="item-title"><strong>{!! $row->sdo !!}</strong><span class="hidden">{!! $row->patient_initials !!}</span></div>
                        <div class="item-after">
                            {{ $row->present()->whenConfirmed }}
                        </div>
                    </div>
                    <div class="item-subtitle">
                        <span style="font-size: 11px">Assegnato a:</span> {!! $row->present()->shippedDriverName !!}
                        <span class="pull-right">{!! $row->present()->statusBadge !!}</span>
                    </div>
                </div>
            </a>
        </li>
    @empty
        <li>
            <div class="alert">Al momento non sono presenti consegne da gestire.</div>
        </li>
    @endforelse
</ul>