<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Roomix config
    |--------------------------------------------------------------------------
    |
    | As follow main version
    |
    */

    'name' => env('APP_NAME','Sample App'),
    'instance' => env('APP_NAME','Sample App'),
    'version' => env('APP_VERSION','0.1'),
    'copyright' => 'Copyright © 2017',
    'welcome' => [
        'title' => 'Benvenuto in OmniPro',
        'message' => 'Seleziona una delle funzionalità attualmente disponibili in questa versione.',
    ],
    'login' => [
        'title' => 'Login Utente',
        'copyright' => '&copy; 2017 All rights reserved - Developed by <strong>ICOA S.r.l.</strong>',
        'message' => 'Questa applicazione permette di gestire il tuo flusso di lavoro all\'interno del locale.',
    ],
    'helpdesk' => [
        'email' => env('HELPDESK_EMAIL',false),
        'phone' => env('HELPDESK_PHONE'),
    ],
    'writelogs' => env('LOG_ENABLED',false),
    'asset_versioning' => env('ASSET_VERSION','0001'),
    'ws' => [
        'host' => env('SOCKET_SERVER','192.168.10.114'),
        'client' => env('SOCKET_CLIENT',env('SOCKET_SERVER','192.168.10.114')),
        'port' => env('SOCKET_PORT',5555),
    ],
    'scope' => env('APP_SCOPE','http://*.collaudo.biz'),
    'report' => [
        'enabled' => env('MAIL_REPORT_ENABLED',false),
        'recipient' => env('MAIL_REPORT_TO'),
        'ccn' => env('MAIL_REPORT_CCN'),
    ],
];
