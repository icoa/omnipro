var elixir = require('laravel-elixir');
var fs = require('fs');

function getArg(key) {
    var index = process.argv.indexOf(key);
    var next = process.argv[index + 1];
    return (index < 0) ? null : (!next || next[0] === "-") ? true : next;
}
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */


var theme = getArg('--theme');
console.log("Theme is: %s", theme);

switch (theme) {
    case 'admin':
        __admin();
        break;
    case 'cash':
        __cash();
        break;
    case 'app':
    case 'cordova':
        __app();
        break;
    case 'default':
    case null:
    default:
        __default();
        break;
}

console.log(elixir.config);


function __admin() {
    var folder = 'admin';
    elixir.config.sourcemaps = false;
    elixir.config.css.folder = folder + "/css";
    elixir.config.css.less.folder = folder + "/less";
    elixir.config.css.outputFolder = folder + "/css";
    //elixir.config.css.cssnano.pluginOptions.
    elixir.config.css.cssnano.pluginOptions = {
        discardComments: {
            removeAll: true
        }
    };
    elixir.config.js.folder = folder + "/js";
    elixir.config.js.outputFolder = folder + "/js";


    elixir.config.cssOutput = 'public/themes/' + folder + '/assets/css';
    elixir.config.jsOutput = 'public/themes/' + folder + '/assets/js';
    elixir.config.publicPath = 'public/themes/' + folder + '/assets';

    elixir(function (mix) {
        mix.less('app.less', 'resources/assets/' + folder + '/css/app.css');
        mix.styles([
            'font-awesome/font-awesome.min.css',
            //'bootstrap-table.min.css',
            'datatables.min.css',
            'dataTables.bootstrap4.min.css',
            'jquery.dataTables.yadcf.css',
            'timedropper.min.css',
            'sweetalert.css',
            'jquery.fileupload.css',
            'main.css',
            'app.css'
        ], 'public/themes/' + folder + '/assets/css');


        mix.scripts([
            'jquery-1.12.0.min.js',
            'vue.min.js',
            'tether.min.js',
            'bootstrap.min.js',
            'plugins.js',
            'datatables.min.js',
            'dataTables.bootstrap4.min.js',
            'jquery.dataTables.yadcf.js',
            'dataTables.fixedColumns.min.js',
            'jquery.mask.min.js',
            'timedropper.min.js',
            'pnotify.js',
            'select2.full.min.js',
            'sweetalert.min.js',
            'jquery.fileupload.js',
            'jquery.iframe-transport.js',
            'shared.js',
            //'bootstrap-table/bootstrap-table.js',
            //'bootstrap-table/bootstrap-table-filter-control.min.js',
            'app.js'
        ], 'public/themes/' + folder + '/assets/js');



        if (elixir.config.production) {
            mix.version([
                'css/all.css',
                'js/all.js'
            ]);

            var json = JSON.parse(fs.readFileSync('public/themes/' + folder + '/assets/build/rev-manifest.json'));
            var cssFile = 'public/themes/' + folder + '/assets/build/'+json['css/all.css'];
            console.log("Reading %s",cssFile);

            fs.readFileSync(cssFile, 'utf8', function (err,data) {
                console.log("I have read: %s",cssFile);
                if (err) {
                    return console.log(err);
                }
                var result = data.replace(/..\/fonts/g, '../../fonts');


                fs.writeFileSync(cssFile, result, 'utf8', function (err) {
                    console.log("I have wrote: %s",cssFile);
                    if (err) return console.log(err);
                });
            });


        }
    });
}

function __default() {
    var folder = 'default';
    elixir.config.sourcemaps = false;
    elixir.config.css.folder = folder + "/css";
    elixir.config.css.less.folder = folder + "/less";
    elixir.config.css.outputFolder = folder + "/css";
    //elixir.config.css.cssnano.pluginOptions.
    elixir.config.css.cssnano.pluginOptions = {
        discardComments: {
            removeAll: true
        }
    };
    elixir.config.js.folder = folder + "/js";
    elixir.config.js.outputFolder = folder + "/js";


    elixir.config.cssOutput = 'public/themes/' + folder + '/assets/css';
    elixir.config.jsOutput = 'public/themes/' + folder + '/assets/js';
    elixir.config.publicPath = 'public/themes/' + folder + '/assets';

    elixir(function (mix) {
        mix.less('app.less', 'resources/assets/' + folder + '/css/app.css');
        mix.styles([
            'framework7.material.css',
            'framework7.material.colors.css',
            'bootstrap.css',
            //'framework7.fix.css',
            'app.css'
        ], 'public/themes/' + folder + '/assets/css');

        mix.scripts([
            'framework7.js',
            'jquery-2.1.4.min.js',
            'vue.min.js',
            'vue-resource.min.js',
            'console.js',
            //'underscore-min.js',
            'ws.js',
            'plugins/signature_pad.js',
            'components/service.create.js',
            'components/service.list.js',
            'components/service.barcode.js',
            'libs/app.audio.js',
            'libs/app.main.js',
            'app.js'
        ], 'public/themes/' + folder + '/assets/js');



        if (elixir.config.production) {
            mix.version([
                'css/all.css',
                'js/all.js'
            ]);
        }
    });
}

function __app() {
    var folder = 'cordova';
    elixir.config.sourcemaps = false;
    elixir.config.css.folder = folder + "/css";
    elixir.config.css.less.folder = folder + "/less";
    elixir.config.css.outputFolder = folder + "/css";
    //elixir.config.css.cssnano.pluginOptions.
    elixir.config.css.cssnano.pluginOptions = {
        discardComments: {
            removeAll: true
        }
    };
    elixir.config.js.folder = folder + "/js";
    elixir.config.js.outputFolder = folder + "/js";


    elixir.config.cssOutput = 'public/themes/' + folder + '/assets/css';
    elixir.config.jsOutput = 'public/themes/' + folder + '/assets/js';
    elixir.config.publicPath = 'public/themes/' + folder + '/assets';

    elixir(function (mix) {
        mix.less('app.less', 'resources/assets/' + folder + '/css/app.css');
        mix.styles([
            'framework7.material.css',
            'framework7.material.colors.css',
            'bootstrap.css',
            'framework7.fix.css',
            'app.css'
        ], 'public/themes/' + folder + '/assets/css');

        mix.scripts([
            'framework7.js',
            'jquery-2.1.4.min.js',
            'console.js',
            'plugins/lscache.js',
            'ws.js',
            'app.js'
        ], 'public/themes/' + folder + '/assets/js');



        if (elixir.config.production) {
            mix.version([
                'css/all.css',
                'js/all.js'
            ]);
        }
    });
}



function __cash() {
    var folder = 'cash';
    elixir.config.sourcemaps = false;
    elixir.config.css.folder = folder + "/css";
    elixir.config.css.less.folder = folder + "/less";
    elixir.config.css.outputFolder = folder + "/css";
    //elixir.config.css.cssnano.pluginOptions.
    elixir.config.css.cssnano.pluginOptions = {
        discardComments: {
            removeAll: true
        }
    };
    elixir.config.js.folder = folder + "/js";
    elixir.config.js.outputFolder = folder + "/js";


    elixir.config.cssOutput = 'public/themes/' + folder + '/assets/css';
    elixir.config.jsOutput = 'public/themes/' + folder + '/assets/js';
    elixir.config.publicPath = 'public/themes/' + folder + '/assets';

    elixir(function (mix) {
        mix.less('app.less', 'resources/assets/' + folder + '/css/app.css');
        mix.styles([
            'framework7.material.css',
            'framework7.material.colors.css',
            'bootstrap.css',
            'framework7.fix.css',
            'app.css'
        ], 'public/themes/' + folder + '/assets/css');

        mix.scripts([
            'framework7.js',
            'jquery-2.1.4.min.js',
            'console.js',
            'plugins/lscache.js',
            'ws.js',
            'app.js'
        ], 'public/themes/' + folder + '/assets/js');



        if (elixir.config.production) {
            mix.version([
                'css/all.css',
                'js/all.js'
            ]);
        }
    });
}