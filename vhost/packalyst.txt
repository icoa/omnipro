[ACL]
http://packalyst.com/packages/package/sarmad/acl
http://packalyst.com/packages/package/larapacks/authorization

[IMPORTANT]
http://packalyst.com/packages/package/sebastiaanluca/laravel-router
https://github.com/spatie/laravel-translatable
https://github.com/adamwathan/bootforms
https://github.com/TypiCMS/Laravel-Translatable-Bootforms
https://laravelcollective.com/docs/5.2/html
https://github.com/caffeinated/modules/wiki
http://packalyst.com/packages/package/sineld/bladeset

[HELPER]
http://packalyst.com/packages/package/overburn/laravel-http-request
http://packalyst.com/packages/package/vi-kon/laravel-menu
http://packalyst.com/packages/package/jor3l/bootstrap-form
http://packalyst.com/packages/package/dericcain/notify
http://packalyst.com/packages/package/digivo/laravel-string-blade-compiler

[OTHER]
https://github.com/Krato/token
http://packalyst.com/packages/package/korchasa/laravel-telegram-bot
https://github.com/akalongman/laravel-multilang
http://packalyst.com/packages/package/bpocallaghan/titan
http://packalyst.com/packages/package/mcstutterfish/fbia-rss
http://packalyst.com/packages/package/infusionweb/laravel-middleware-response-cache
http://packalyst.com/packages/package/profio/laravel-plus

[APPS]
http://packalyst.com/packages/package/nuummite/laravel-followable
http://packalyst.com/packages/package/niklasravnsborg/laravel-pdf
https://github.com/irazasyed/telegram-bot-sdk
http://packalyst.com/packages/package/sugar-agency/laravel-favorites
http://packalyst.com/packages/package/zenapply/laravel-shortener
http://packalyst.com/packages/package/trexology/taxonomy
http://packalyst.com/packages/package/souhailmerroun/schedulable
http://packalyst.com/packages/package/spatie/laravel-google-calendar

[TRAITS]
http://packalyst.com/packages/package/gurzhii/laravel-sluggable-trait
http://packalyst.com/packages/package/sands/uploadable

[PUSH]
http://packalyst.com/packages/package/developerdynamo/push-notification
http://packalyst.com/packages/package/asachanfbd/laravel-push-notification
http://packalyst.com/packages/package/bnbwebexpertise/laravel-push-notifications



[CLIENT]
https://github.com/danielm/uploader
http://packalyst.com/packages/package/rockkeeper/laravel-elfinder