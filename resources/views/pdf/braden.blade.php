<?php
$vars = App\BradenVar::with('choices')->orderBy('position')->get();
$counter = 1;
?>
<html>
<head>
    <title>Scala Braden Noleggio N. {!! $model->id !!}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <style>
        body {
            font-size: 11px;
            font-family: Helvetica;
        }

        .table {
            width:100%;
            border-collapse: collapse;
            border:1px solid #ccc;
            border-width: 1px 0 0 1px;
        }

        .table td, .table th {
            border-collapse: collapse;
            padding: 4px;
            border:1px solid #ccc;
            border-width: 0 1px 1px 0;
        }

        input, label{
            vertical-align: middle;
        }

        .table th {
            background-color: #eee;
        }

        h5{
            font-size: 13px;
        }
    </style>
</head>
<body>
<p>Paziente: <strong>{!! $model->patient_initials !!}</strong></p>
<table class="table table-striped table-bordered" id="bradenTable">
    <thead>
    <tr>
        <th>Indicatori e variabili</th>
        <th>4</th>
        <th>3</th>
        <th>2</th>
        <th>1</th>
    </tr>
    </thead>
    <tbody>
    @foreach($vars as $var)
        @set $name = "braden_point".$counter;
        <tr>
            <th width="20%" style="vertical-align: top">
                <h5 style="margin-bottom: 0">{!! $var->name !!}</h5>
                <p>
                    <small style="font-weight: bold">{!! $var->description !!}</small>
                </p>
                @if($errors->has($name))
                    <span class="label label-danger">{!! $errors->first($name) !!}</span>
                @endif
            </th>
            @foreach($var->choices as $choice)
                @set $checked = false;
                @if(old($name) > 0 and old($name) == $choice->points)
                    @set $checked = true;
                @endif
                @if(isset($model) and $model->$name == $choice->points)
                    @set $checked = true;
                @endif
                <td style="vertical-align: top" width="20%" class="@if($checked) checked @endif">
                    @if($choice->name != null)
                        <label>
                            <input @if($checked) checked="checked" @endif type="radio" name="{!! $name !!}"
                                   value="{!! $choice->points !!}">
                            <strong>{!! $choice->name !!}</strong>
                            <br>
                            <small>{!! $choice->description !!}</small>
                        </label>
                    @endif
                </td>
            @endforeach
        </tr>
        @set $counter++;
    @endforeach
    </tbody>
</table>
<br>
<p>Totale scala Braden: <strong>{!! $model->braden_total !!}</strong></p>
</body>
</html>