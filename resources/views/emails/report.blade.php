<!DOCTYPE html>
<html>
<head>
    <style>
        body, html{
            background: #fff !important;
        }
        table td, table th{
            border:1px solid #aaa;
            padding:3px 6px;
        }
    </style>
</head>
<body>
<div style="font-family: 'Segoe UI','Helvetica Neue',Verdana;">
{!! $html !!}
</div>
</body>
</html>