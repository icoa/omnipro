{
"name": "GeCo | {!! config('roomix.instance') !!}",
"short_name": "{!! config('roomix.instance') !!}",
"icons": [
{
"src": "/static/android-icon-36x36.png",
"sizes": "36x36",
"type": "image/png"
},
{
"src": "/static/android-icon-48x48.png",
"sizes": "48x48",
"type": "image/png"
},
{
"src": "/static/android-icon-72x72.png",
"sizes": "72x72",
"type": "image/png"
},
{
"src": "/static/android-icon-96x96.png",
"sizes": "96x96",
"type": "image/png"
},
{
"src": "/static/android-icon-144x144.png",
"sizes": "144x144",
"type": "image/png"
},
{
"src": "/static/android-icon-192x192.png",
"sizes": "192x192",
"type": "image/png"
}
],
"scope":"{!! config('roomix.scope') !!}",
"start_url": "{!! $url !!}",
"display": "fullscreen",
"orientation": "portrait",
"theme_color": "#14468C",
"background_color": "#f5f5f5",
"mjs_cordova": {
"plugin_mode": "client"
},
"mjs_api_access": [
{ "match": "*" }
]
}