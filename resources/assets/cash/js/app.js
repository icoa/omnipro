// Init App
var myApp = new Framework7({
    modalTitle: 'Raspoutine',
    modalButtonCancel: 'Annulla',
    // Enable Material theme
    material: true,
    cache: false,
    ajaxLinks: '.ajax',

    pushState: true,
    //swipePanel: 'left',
    animatePages: false,
    uniqueHistory: true,
    preloadPreviousPage: false,
    materialRipple: false
});
//swipeOut do not close
var swipeoutStay = true;

// Expose Internal DOM library
var $$ = Dom7;

// Add main view
var mainView = myApp.addView('.view-main', {});

var $$f7dom = $$(document);
// Show/hide preloader for remote ajax loaded pages
// Probably should be removed on a production/local app
$$f7dom.on('ajaxStart', function (e) {
    myApp.showIndicator();
}).on('ajaxComplete', function () {
    myApp.hideIndicator();
}).on('pageInit', function (e) {
    // Page Data contains all required information about loaded and initialized page
    var page = e.detail.page;

    console.log("pageInit: %o", page);
    switch(page.name){
        case 'index':
            Cash.init(page);
            Checkout.init(page);
            break;

        case 'tables':
            Services.init(page);
            break;
    }

});

$(document).ready(function () {
    App.init();
    App.plugins();
});

var AppAudio = {
    path: Ristomix.config.url + "/sound/",
    sounds: ['bell_ring', 'button_tiny', 'tap'],
    soundsDom: [],
    init: function () {
        var that = this;
        $(this.sounds).each(function (i, name) {
            var $audio = $('<audio></audio>');
            $audio.append('<source src="' + that.path + name + '.mp3"></source>');
            $audio.append('<source src="' + that.path + name + '.aac"></source>');
            that.soundsDom[name] = $audio;
        });
    },
    play: function (name) {
        var that = this;
        that.soundsDom[name][0].play();
    }
};


var App = {
    plugins: function () {
        //AppAudio.init();
    },
    init: function () {

        function deferMethod($el) {
            var action = $el.data('action');
            if (action == '' || action == undefined || action == null) {
                action = $el.attr('rel');
            }
            var method = camelCase(action);
            if (typeof App[method] == 'function') {
                App[method]($el);
            } else {
                alert("Method: " + method + " not found!");
            }
        }

        $("body").delegate(".app-action", "click", function (e) {
            e.preventDefault();
            var $el = $(this);
            deferMethod($el);
        }).delegate(".app-option", "change", function (e) {
            var $el = $(this);
            deferMethod($el);
        }).delegate(".swipeout", "open", function (e) {
            var $target = $(this);
            console.log('opened');
            $target.data('verified', 1);
            Cash.checkCard($target.data('order'));
        }).delegate(".swipeout", "close", function (e) {
            var $target = $(this);
            console.log('closed');
            $target.data('verified', 0);
            Cash.checkCard($target.data('order'));
        }).delegate(".swipeout", "deleted", function (e) {
            var $target = $(this);
            $.post($target.data('post'), {}, function (response) {
                console.log(response);
                App.notify(response.message);
            }, 'json');
        });

        var $page = $(".page");
        if ($page.data('page') != 'login') {
            console.log("Current page is: %s", $page.data('page'));
            switch ($page.data('module')) {
                case 'cash':
                    Cash.init($page);
                    Checkout.init($page);
                    break;
            }
        }

        //check
        if (Ristomix.user) {
            connect();
            //check connection status
            window.setInterval(function () {
                if (window.ws) {
                    var status = window.ws.getStatus();
                    if (status == 'closed') {
                        window.connect();
                    }
                }
            }, 5000);
        }

    },
    logout: function (e) {
        myApp.confirm('Sei sicuro di eseguire la disconnessione?', function () {
            window.location = '/cash/auth/logout';
        });
    },
    notify: function (message, callback) {
        myApp.addNotification({
            closeOnClick: true,
            hold: 3000,
            message: message,
            button: {
                text: Ristomix.lexicon.close,
                color: 'yellow'
            },
            onClick: function () {
                if(typeof callback == 'function'){
                    callback.call();
                }
            }
        });
    },
    notifyCallback: function (message, callback) {
        myApp.addNotification({
            //hold: 30000,
            closeOnClick: true,
            message: message,
            button: {
                text: Ristomix.lexicon.close,
                color: 'yellow'
            },
            onClick: function () {
                callback.call();
            }
        });
    },
    doCheckout: function ($el) {
        Cash.submit($el);
    },
    toggleBottle: function($el){

    },
    sendOrder: function($el){
        Cash.sendOrder($el);
    },
    removeBottle: function($el){
        BottleOrder.remove($el);
    }
};


window.connect = function () {
    var connection = Ristomix.config.ws;
    console.log('Connecting to ' + connection);
    window.ws = $.websocket(connection, {
        open: function () {
            console.log('connection is open');
            ws.send("register", Ristomix.user);
        },
        close: function () {
            console.log('close');
        },
        error: function (event) {
            console.log('error');
            console.log(event);
        },
        events: {
            ServiceWasClosed: function (e) {
                var elem = e.data;
                console.log('events.ServiceWasClosed');
                console.log(elem);
                Services.serviceClosed(elem);
            },
            ServiceWasCreated: function (e) {
                var elem = e.data;
                console.log('events.ServiceWasCreated');
                console.log(elem);
                Services.serviceCreated(elem);
            },
            ServiceWasUpdated: function (e) {
                var elem = e.data;
                console.log('events.ServiceWasUpdated');
                console.log(elem);
                Services.serviceUpdated(elem);
                Cash.serviceUpdated(elem);
                Checkout.serviceUpdated(elem);
            },
            ServiceWasCheckedOut: function (e) {
                var elem = e.data;
                console.log('events.ServiceWasCheckedOut');
                console.log(elem);
                Checkout.checkout(elem);
            },
            BottleWasDone: function (e) {
                var elem = e.data;
                console.log('events.BottleWasDone');
                console.log(elem);
                Cash.bottle(elem);
            },
            NegativeOrderWasCreated: function (e) {
                var elem = e.data;
                console.log('events.NegativeOrderWasCreated');
                console.log(elem);
                Cash.orderCreated(elem);
            },
            OrderWasCompleted: function (e) {
                var elem = e.data;
                console.log('events.OrderWasCompleted');
                console.log(elem);
                Cash.orderCompleted(elem);
            }
        }
    });
};


var Cash = {

    $container: null,
    $holder: null,
    $filter: null,

    init: function ($page) {
        var that = this;
        that.$container = $("#shouldUpdateBottles");
        that.$holder = that.$container;
        that.$filter = $("#filter");
        console.log('Cash.init');
        that.initFilter();
        that.setupFilter();
        $('.cancel').click(function (e) {
            e.preventDefault();
            that.clearFilter();
        });


    },

    checkCard: function (order_id) {
        var $el = $("#order_" + order_id);
        if ($el.length) {
            var $li = $el.find('.swipeout');
            var total = $li.length;
            var done = 0;
            if (total > 0) {
                $li.each(function (i, el) {
                    if (parseInt($li.eq(i).data('verified'))) {
                        done++;
                    }
                });
            }
            if (done == total) {
                $el.find('button').prop('disabled', false);
            } else {
                $el.find('button').prop('disabled', true);
            }
        }
    },

    bottle: function (data) {
        console.log('bottle ' + data.id);
        var that = this;
        $.get(that.$container.data('route') + '/' + data.id, {}, function (response) {
            that.$holder.prepend(response.html);
            App.notify(response.message, function () {
                if($$("#showOrdersAction").length){
                    $$("#showOrdersAction").trigger('click');
                }
            });
            that.setupFilter();
        }, 'json');
    },

    orderCreated: function (data) {
        console.log('order ' + data.id);
        var that = this;
        $.get(that.$container.data('order') + '/' + data.id, {}, function (response) {
            that.$holder.append(response.html);
            App.notify(response.message, function () {
                if($$("#showOrdersAction").length){
                    $$("#showOrdersAction").trigger('click');
                }
            });
            that.setupFilter();
        }, 'json');
    },

    orderCompleted: function (data) {
        console.log('order ' + data.id);
        var that = this;
        $.get(that.$container.data('order') + '/' + data.id, {}, function (response) {
            that.$holder.append(response.html);
            App.notify(response.message, function () {
                if($$("#showOrdersAction").length){
                    $$("#showOrdersAction").trigger('click');
                }
            });
            that.setupFilter();
        }, 'json');
    },

    initFilter: function () {
        var that = this;
        that.$filter.change(function (e) {
            var service = parseInt(that.$filter.val());
            that.$holder.find('.filterable').show();
            if (service > 0) {
                that.$holder.find('.filterable').not('.service_' + service).hide();
                that.$filter.addClass('selected');
            } else {
                that.$filter.removeClass('selected');
            }
        });
    },

    setupFilter: function () {
        console.log('setupFilter');
        var that = this;
        var $bottles = that.$holder.find('.filterable');
        var services = [], cache = {};
        $bottles.each(function (i, el) {
            var $bottle = $bottles.eq(i);
            var value = parseInt($bottle.data('service')),
            option = $bottle.data('preview');
            if(cache[value] == undefined){
                services.push({option: option, value: value});
                cache[value] = option;
            }

        });
        that.$filter.html('<option value="">Mostra solo...</option>');
        //console.log(services)
        var byName = services.slice(0);
        byName.sort(function(a,b) {
            var x = a.option.toLowerCase();
            var y = b.option.toLowerCase();
            return x < y ? -1 : x > y ? 1 : 0;
        });
        //console.log('by byName:');
        //console.log(byName);
        $.each(byName, function (id, obj) {
            that.$filter.append('<option value="' + obj.value + '">' + obj.option + '</option>');
        });
    },

    setFilter: function (value) {
        var that = this;
        that.$filter.val(value).trigger('change');
    },

    clearFilter: function () {
        var that = this;
        that.setFilter('');
    },

    countBottlesByService: function (id) {
        var that = this;
        return that.$holder.find('.service_' + id).length;
    },

    submit: function ($el) {
        var that = this;

        var $li = $el.closest('.swipeout');

        var service_id = $li.data('id');
        if (that.countBottlesByService(service_id) > 0) {
            myApp.alert("Attenzione: devi prima confermare i prodotti per questo tavolo");
            that.setFilter(service_id);
            return;
        }
        myApp.swipeoutDelete($li);
    },

    serviceUpdated: function () {
        var that = this;
        $.get(that.$container.data('reload'), {}, function (response) {
            that.$holder.html(response.html);
            that.setupFilter();
        }, 'json');
    },

    sendOrder: function($el){
        var that = this;
        $.post($el.data('post'), {}, function (response) {
            console.log(response);
            App.notify('Ordine confermato con successo');
            var $card = $("#order_" + $el.data('order'));
            $card.remove();
            that.setupFilter();
        }, 'json');
    }
};


var Checkout = {

    $container: null,
    $holder: null,

    init: function ($page) {
        var that = this;
        that.$container = $("#shouldUpdateCheckout");
        that.$holder = that.$container.find('ul');
        console.log('Checkout.init');
    },

    checkout: function (data) {
        console.log('checkout ' + data.id);
        var that = this;
        $.get(that.$container.data('route') + '/' + data.id, {}, function (response) {
            that.$holder.append(response.html);
            App.notifyCallback(response.message, function () {
                if($$("#showOrdersAction").length){
                    $$("#showOrdersAction").trigger('click');
                }
            });
        }, 'json');
    },

    serviceUpdated: function () {
        var that = this;
        $.get(that.$container.data('reload'), {}, function (response) {
            that.$holder.html(response.html);
        }, 'json');
    }
};


var Services = {

    $container: null,

    init: function ($page) {
        var that = this;
        that.$container = $("#shouldUpdateServices");
        console.log('Services.init');
    },

    reloadServices: function () {
        var that = this;
        that.$container = $("#shouldUpdateServices");
        if(that.$container.length <= 0)return;
        $.get(that.$container.data('route'), {}, function (response) {
            that.$container.html(response.html);
        }, 'json');
    },

    serviceCreated: function (data) {
        console.log('serviceCreated ' + data.id);
        App.notify('Tavolo aperto: ' + data.fullname, function () {
            $$("#showTablesAction").trigger('click');
        });
        this.reloadServices();
    },

    serviceUpdated: function (data) {
        console.log('serviceUpdated ' + data.id);
        App.notify('Tavolo modificato: ' + data.fullname, function () {
            $$("#showTablesAction").trigger('click');
        });
        this.reloadServices();
    },

    serviceClosed: function (data) {
        console.log('serviceUpdated ' + data.id);
        App.notify('Tavolo chiuso: ' + data.fullname, function () {
            $$("#showTablesAction").trigger('click');
        });
        this.reloadServices();
    }
};


var BottleOrder = {
    remove: function($el){
        var id = $el.data('id');
        var $target = $("#bottleOrder_"+id);
        if($target.length <= 0)
            return;

        var name = $target.data('fullname');
        var route = $target.data('route');

        myApp.confirm('Rimuovere una bottiglia di <strong>'+name+'</strong>?', function () {
            var $quantity = $target.find('.button-quantity');
            var q = parseInt($quantity.text());
            q = q - 1;

            $.post(route,{},function(response){
                App.notify('Ho rimosso una bottiglia di '+name);
                if(q <= 0){
                    $target.remove();
                }else{
                    $quantity.text( q );
                }
                BottleOrder.checkIfEmpty();
            },'json');


        });
    },

    checkIfEmpty: function(){
        var $items = $("#bottleOrderHolder > li");
        if($items.length <= 0){
            var params = {};
            params.url = mainView.history[mainView.history.length - 2];
            params.ignoreCache = true;
            params.force = true;
            mainView.router.back(params);
        }

    }
};