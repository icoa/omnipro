var AppAudio = {
    path: Ristomix.config.url + "/sound/",
    sounds: ['bell_ring', 'button_tiny', 'tap'],
    soundsDom: [],
    init: function () {
        var that = this;
        $(this.sounds).each(function (i, name) {
            var $audio = $('<audio></audio>');
            $audio.append('<source src="' + that.path + name + '.mp3"></source>');
            $audio.append('<source src="' + that.path + name + '.aac"></source>');
            that.soundsDom[name] = $audio;
        });
    },
    play: function (name) {
        var that = this;
        try{
            that.soundsDom[name][0].play();
        }catch(e){

        }
    }
};


var App = {
    plugins: function () {
        AppAudio.init();
    },
    init: function () {
        App.plugins();
        function deferMethod($el) {
            var action = $el.data('action');
            if (action == '' || action == undefined || action == null) {
                action = $el.attr('rel');
            }
            var method = camelCase(action);
            if (typeof App[method] == 'function') {
                App[method]($el);
            } else {
                alert("Method: " + method + " not found!");
            }
        }

        $("body").delegate(".app-action", "click", function (e) {
            e.preventDefault();
            var $el = $(this);
            deferMethod($el);
        }).delegate(".app-option", "change", function (e) {
            var $el = $(this);
            deferMethod($el);
        });
    },

    onDeleteRecord: function ($el) {
        swal({
                title: "Cancellare questo record?",
                text: "Attenzione: non sarai in grado di recuperare i dati cancellati",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Sì, elimina",
                cancelButtonText: "No, annulla",
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax(
                        {
                            url: $el.attr('href'),
                            type: 'delete',
                            dataType: "json",
                            success: function (response) {
                                if(response.success){
                                    swal({
                                        title: "Record cancellato!",
                                        text: "Il record è stato rimosso con successo",
                                        type: "success",
                                        confirmButtonClass: "btn-success"
                                    });
                                    App.reloadDatatableByEl($el);
                                }else{
                                    swal({
                                        title: "Azione annullata",
                                        text: "Il record non è stato rimosso perchè è riferito da altre entità del sistema",
                                        type: "warning",
                                        confirmButtonClass: "btn-warning"
                                    });
                                }
                            },
                            error: function (response) {
                                swal({
                                    title: "Errore",
                                    text: "Il record non è stato rimosso per un problema interno",
                                    type: "error",
                                    confirmButtonClass: "btn-danger"
                                });
                            }
                        });


                }
            });
    },

    reloadDatatable: function(){
        if(window.$dataTable){
            window.$dataTable.ajax.reload();
        }
    },

    reloadDatatableByEl: function($el){
        var $dataTable = $el.closest('.dataTable');
        if($dataTable.length){
            $dataTable.DataTable().ajax.reload();
        }
    },

    toggleVis: function($el){
        var $target = $('#' + $el.data('target'));
        $el.toggleClass('btn-warning');
        $target.toggleClass('hidden');
    }
};


function parseSelect(selector) {
    var $el = $(selector), data = [];
    if ($el.length) {
        var $options = $el.find('option');
        $.each($options, function (i, option) {
            data.push({
                'value': $options.eq(i).attr('value'),
                'label': $options.eq(i).text()
            })
        });
    }
    return data;
}

$(document).ready(function () {
    /* ==========================================================================
     Scroll
     ========================================================================== */

    if (!("ontouchstart" in document.documentElement)) {

        document.documentElement.className += " no-touch";

        var jScrollOptions = {
            autoReinitialise: true,
            autoReinitialiseDelay: 100
        };

        $('.box-typical-body').jScrollPane(jScrollOptions);
        $('.side-menu').jScrollPane(jScrollOptions);
        //$('.side-menu-addl').jScrollPane(jScrollOptions);
        $('.scrollable-block').jScrollPane(jScrollOptions);
    }

    /* ==========================================================================
     Header search
     ========================================================================== */

    $('.site-header .site-header-search').each(function () {
        var parent = $(this),
            overlay = parent.find('.overlay');

        overlay.click(function () {
            parent.removeClass('closed');
        });

        parent.clickoutside(function () {
            if (!parent.hasClass('closed')) {
                parent.addClass('closed');
            }
        });
    });

    /* ==========================================================================
     Header mobile menu
     ========================================================================== */

    // Dropdowns
    $('.site-header-collapsed .dropdown').each(function () {
        var parent = $(this),
            btn = parent.find('.dropdown-toggle');

        btn.click(function () {
            if (parent.hasClass('mobile-opened')) {
                parent.removeClass('mobile-opened');
            } else {
                parent.addClass('mobile-opened');
            }
        });
    });

    $('.dropdown-more').each(function () {
        var parent = $(this),
            more = parent.find('.dropdown-more-caption'),
            classOpen = 'opened';

        more.click(function () {
            if (parent.hasClass(classOpen)) {
                parent.removeClass(classOpen);
            } else {
                parent.addClass(classOpen);
            }
        });
    });

    // Left mobile menu
    $('.hamburger').click(function () {
        if ($('body').hasClass('menu-left-opened')) {
            $(this).removeClass('is-active');
            $('body').removeClass('menu-left-opened');
            $('html').css('overflow', 'auto');
        } else {
            $(this).addClass('is-active');
            $('body').addClass('menu-left-opened');
            $('html').css('overflow', 'hidden');
        }
    });

    $('.mobile-menu-left-overlay').click(function () {
        $('.hamburger').removeClass('is-active');
        $('body').removeClass('menu-left-opened');
        $('html').css('overflow', 'auto');
    });

    // Right mobile menu
    $('.site-header .burger-right').click(function () {
        if ($('body').hasClass('menu-right-opened')) {
            $('body').removeClass('menu-right-opened');
            $('html').css('overflow', 'auto');
        } else {
            $('.hamburger').removeClass('is-active');
            $('body').removeClass('menu-left-opened');
            $('body').addClass('menu-right-opened');
            $('html').css('overflow', 'hidden');
        }
    });

    $('.mobile-menu-right-overlay').click(function () {
        $('body').removeClass('menu-right-opened');
        $('html').css('overflow', 'auto');
    });

    /* ==========================================================================
     Header help
     ========================================================================== */

    $('.help-dropdown').each(function () {
        var parent = $(this),
            btn = parent.find('>button'),
            popup = parent.find('.help-dropdown-popup'),
            jscroll;

        btn.click(function () {
            if (parent.hasClass('opened')) {
                parent.removeClass('opened');
                jscroll.destroy();
            } else {
                parent.addClass('opened');

                $('.help-dropdown-popup-cont, .help-dropdown-popup-side').matchHeight();

                if (!("ontouchstart" in document.documentElement)) {
                    setTimeout(function () {
                        jscroll = parent.find('.jscroll').jScrollPane(jScrollOptions).data().jsp;
                        //jscroll.reinitialise();
                    }, 0);
                }
            }
        });

        $('html').click(function (event) {
            if (
                !$(event.target).closest('.help-dropdown-popup').length
                && !$(event.target).closest('.help-dropdown>button').length
                && !$(event.target).is('.help-dropdown-popup')
                && !$(event.target).is('.help-dropdown>button')
            ) {
                if (parent.hasClass('opened')) {
                    parent.removeClass('opened');
                    jscroll.destroy();
                }
            }
        });

    });

    /* ==========================================================================
     Side menu list
     ========================================================================== */

    $('.side-menu-list li.with-sub').each(function () {
        var parent = $(this),
            clickLink = parent.find('>span'),
            subMenu = parent.find('ul');

        clickLink.click(function () {
            if (parent.hasClass('opened')) {
                parent.removeClass('opened');
                subMenu.slideUp();
            } else {
                $('.side-menu-list li.with-sub').not(this).removeClass('opened').find('ul').slideUp();
                parent.addClass('opened');
                subMenu.slideDown();
            }
        });
    });


    /* ==========================================================================
     Dashboard
     ========================================================================== */

    // Calculate height
    function dashboardBoxHeight() {
        $('.box-typical-dashboard').each(function () {
            var parent = $(this),
                header = parent.find('.box-typical-header'),
                body = parent.find('.box-typical-body');
            body.height(parent.outerHeight() - header.outerHeight());
        });
    }

    dashboardBoxHeight();

    $(window).resize(function () {
        dashboardBoxHeight();
    });

    // Collapse box
    $('.box-typical-dashboard').each(function () {
        var parent = $(this),
            btnCollapse = parent.find('.action-btn-collapse');

        btnCollapse.click(function () {
            if (parent.hasClass('box-typical-collapsed')) {
                parent.removeClass('box-typical-collapsed');
            } else {
                parent.addClass('box-typical-collapsed');
            }
        });
    });

    // Full screen box


    function setupDashboard() {
        $('.box-typical-dashboard').each(function () {
            var parent = $(this),
                btnExpand = parent.find('.action-btn-expand'),
                classExpand = 'box-typical-full-screen';

            btnExpand.click(function () {
                if (parent.hasClass(classExpand)) {
                    parent.removeClass(classExpand);
                    $('html').css('overflow', 'auto');
                } else {
                    parent.addClass(classExpand);
                    $('html').css('overflow', 'hidden');
                }
                dashboardBoxHeight();
            });
        });
    }

    setupDashboard();

    /* ==========================================================================
     Circle progress bar
     ========================================================================== */

    /*$(".circle-progress-bar").asPieProgress({
     namespace: 'asPieProgress',
     speed: 500
     });

     $(".circle-progress-bar").asPieProgress("start");


     $(".circle-progress-bar-typical").asPieProgress({
     namespace: 'asPieProgress',
     speed: 25
     });

     $(".circle-progress-bar-typical").asPieProgress("start");*/

    /* ==========================================================================
     Select
     ========================================================================== */

    if ($('.bootstrap-select').size()) {
        // Bootstrap-select
        $('.bootstrap-select').selectpicker({
            style: '',
            width: '100%',
            size: 8
        });
    }

    if ($('.select2').size()) {
        // Select2
        //$.fn.select2.defaults.set("minimumResultsForSearch", "Infinity");

        $('.select2').select2();

        $(".select2-icon").select2({
            templateSelection: select2Icons,
            templateResult: select2Icons
        });

        $(".select2-arrow").select2({
            theme: "arrow"
        });

        $(".select2-white").select2({
            theme: "white"
        });

        $(".select2-photo").select2({
            templateSelection: select2Photos,
            templateResult: select2Photos
        });

        /*$(".select2-ajax").select2({
            ajax: {
                url: "https://api.github.com/search/repositories",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: false
            },
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });*/
    }

    function select2Icons(state) {
        if (!state.id) {
            return state.text;
        }
        var $state = $(
            '<span class="font-icon ' + state.element.getAttribute('data-icon') + '"></span><span>' + state.text + '</span>'
        );
        return $state;
    }

    function select2Photos(state) {
        if (!state.id) {
            return state.text;
        }
        var $state = $(
            '<span class="user-item"><img src="' + state.element.getAttribute('data-photo') + '"/>' + state.text + '</span>'
        );
        return $state;
    }

    /* ==========================================================================
     Datepicker
     ========================================================================== */

    var $dates = $('input.date');
    $dates.each(function(i,el){
        var $el = $(el);
       var params =  {
           format: 'DD-MM-YYYY',
           widgetPositioning: {
               horizontal: 'left'
           },
           locale: 'it',
           debug: false
       };
       if($el.data('mindate') != '' && Ristomix.user.role == 'user'){
           params.minDate = $el.data('mindate');
       }
       $el.datetimepicker(params);
    });
    /*$('input.date').datetimepicker({
        format: 'DD-MM-YYYY',
        widgetPositioning: {
            horizontal: 'left'
        },
        minDate:'2017-05-25',
        locale: 'it',
        debug: false
    });*/

    $('input.datepicker').datetimepicker({
        format: 'DD-MM-YYYY HH:mm',
        widgetPositioning: {
            horizontal: 'left'
        },
        locale: 'it',
        debug: false
    });

    $('input.datetimepicker-2').datetimepicker({
        widgetPositioning: {
            horizontal: 'right'
        },
        locale: 'it',
        format: 'LT',
        debug: false
    });

    /* ==========================================================================
     Tooltips
     ========================================================================== */

    // Tooltip
    $('[data-toggle="tooltip"]').tooltip({
        html: true
    });

    // Popovers
    $('[data-toggle="popover"]').popover({
        trigger: 'focus'
    });

    /* ==========================================================================
     Validation
     ========================================================================== */

    /*$('#form-signin_v1').validate({
     submit: {
     settings: {
     inputContainer: '.form-group'
     }
     }
     });

     $('#form-signin_v2').validate({
     submit: {
     settings: {
     inputContainer: '.form-group',
     errorListClass: 'form-error-text-block',
     display: 'block',
     insertion: 'prepend'
     }
     }
     });

     $('#form-signup_v1').validate({
     submit: {
     settings: {
     inputContainer: '.form-group',
     errorListClass: 'form-tooltip-error'
     }
     }
     });

     $('#form-signup_v2').validate({
     submit: {
     settings: {
     inputContainer: '.form-group',
     errorListClass: 'form-tooltip-error'
     }
     }
     });*/

    /* ==========================================================================
     Bar chart
     ========================================================================== */

    /*$(".bar-chart").peity("bar",{
     delimiter: ",",
     fill: ["#919fa9"],
     height: 16,
     max: null,
     min: 0,
     padding: 0.1,
     width: 384
     });*/

    /* ==========================================================================
     Full height box
     ========================================================================== */

    function boxFullHeight() {
        var sectionHeader = $('.section-header');
        var sectionHeaderHeight = 0;

        if (sectionHeader.size()) {
            sectionHeaderHeight = parseInt(sectionHeader.height()) + parseInt(sectionHeader.css('padding-bottom'));
        }

        $('.box-typical-full-height').css('min-height',
            $(window).height() -
            parseInt($('.page-content').css('padding-top')) -
            parseInt($('.page-content').css('padding-bottom')) -
            sectionHeaderHeight -
            parseInt($('.box-typical-full-height').css('margin-bottom')) - 2
        );
        $('.box-typical-full-height>.tbl, .box-typical-full-height>.box-typical-center').height(parseInt($('.box-typical-full-height').css('min-height')));
    }

    boxFullHeight();

    $(window).resize(function () {
        boxFullHeight();
    });

    /* ==========================================================================
     Chat
     ========================================================================== */

    /*function chatHeights() {
     $('.chat-dialog-area').height(
     $(window).height() -
     parseInt($('.page-content').css('padding-top')) -
     parseInt($('.page-content').css('padding-bottom')) -
     parseInt($('.chat-container').css('margin-bottom')) - 2 -
     $('.chat-area-header').outerHeight() -
     $('.chat-area-bottom').outerHeight()
     );
     $('.chat-list-in')
     .height(
     $(window).height() -
     parseInt($('.page-content').css('padding-top')) -
     parseInt($('.page-content').css('padding-bottom')) -
     parseInt($('.chat-container').css('margin-bottom')) - 2 -
     $('.chat-area-header').outerHeight()
     )
     .css('min-height', parseInt($('.chat-dialog-area').css('min-height')) + $('.chat-area-bottom').outerHeight());
     }

     chatHeights();

     $(window).resize(function(){
     chatHeights();
     });*/

    /* ==========================================================================
     Auto size for textarea
     ========================================================================== */

    autosize($('textarea[data-autosize]'));

    /* ==========================================================================
     Pages center
     ========================================================================== */

    $('.page-center').matchHeight({
        target: $('html')
    });

    $(window).resize(function () {
        setTimeout(function () {
            $('.page-center').matchHeight({remove: true});
            $('.page-center').matchHeight({
                target: $('html')
            });
        }, 100);
    });

    /* ==========================================================================
     Cards user
     ========================================================================== */

    $('.card-user').matchHeight();

    /* ==========================================================================
     Fancybox
     ========================================================================== */

    $(".fancybox").fancybox({
        padding: 0,
        openEffect: 'none',
        closeEffect: 'none'
    });

    /* ==========================================================================
     Profile slider
     ========================================================================== */

    /*$(".profile-card-slider").slick({
     slidesToShow: 1,
     adaptiveHeight: true,
     prevArrow: '<i class="slick-arrow font-icon-arrow-left"></i>',
     nextArrow: '<i class="slick-arrow font-icon-arrow-right"></i>'
     });*/

    /* ==========================================================================
     Posts slider
     ========================================================================== */

    /*var postsSlider = $(".posts-slider");

     postsSlider.slick({
     slidesToShow: 4,
     adaptiveHeight: true,
     arrows: false,
     responsive: [
     {
     breakpoint: 1700,
     settings: {
     slidesToShow: 3
     }
     },
     {
     breakpoint: 1350,
     settings: {
     slidesToShow: 2
     }
     },
     {
     breakpoint: 992,
     settings: {
     slidesToShow: 3
     }
     },
     {
     breakpoint: 768,
     settings: {
     slidesToShow: 2
     }
     },
     {
     breakpoint: 500,
     settings: {
     slidesToShow: 1
     }
     }
     ]
     });

     $('.posts-slider-prev').click(function(){
     postsSlider.slick('slickPrev');
     });

     $('.posts-slider-next').click(function(){
     postsSlider.slick('slickNext');
     });*/

    /* ==========================================================================
     Recomendations slider
     ========================================================================== */

    /*var recomendationsSlider = $(".recomendations-slider");

     recomendationsSlider.slick({
     slidesToShow: 4,
     adaptiveHeight: true,
     arrows: false,
     responsive: [
     {
     breakpoint: 1700,
     settings: {
     slidesToShow: 3
     }
     },
     {
     breakpoint: 1350,
     settings: {
     slidesToShow: 2
     }
     },
     {
     breakpoint: 992,
     settings: {
     slidesToShow: 3
     }
     },
     {
     breakpoint: 768,
     settings: {
     slidesToShow: 2
     }
     },
     {
     breakpoint: 500,
     settings: {
     slidesToShow: 1
     }
     }
     ]
     });

     $('.recomendations-slider-prev').click(function(){
     recomendationsSlider.slick('slickPrev');
     });

     $('.recomendations-slider-next').click(function(){
     recomendationsSlider.slick('slickNext');
     });*/

    /* ==========================================================================
     Box typical full height with header
     ========================================================================== */

    /*function boxWithHeaderFullHeight() {
     $('.box-typical-full-height-with-header').each(function(){
     var box = $(this),
     boxHeader = box.find('.box-typical-header'),
     boxBody = box.find('.box-typical-body');

     boxBody.height(
     $(window).height() -
     parseInt($('.page-content').css('padding-top')) -
     parseInt($('.page-content').css('padding-bottom')) -
     parseInt(box.css('margin-bottom')) - 2 -
     boxHeader.outerHeight()
     );
     });
     }

     boxWithHeaderFullHeight();

     $(window).resize(function(){
     boxWithHeaderFullHeight();
     });*/

    /* ==========================================================================
     Gallery
     ========================================================================== */

    /*$('.gallery-item').matchHeight({
     target: $('.gallery-item .gallery-picture')
     });*/

    /* ==========================================================================
     File manager
     ========================================================================== */

    /*function fileManagerHeight() {
     $('.files-manager').each(function(){
     var box = $(this),
     boxColLeft = box.find('.files-manager-side'),
     boxSubHeader = box.find('.files-manager-header'),
     boxCont = box.find('.files-manager-content-in'),
     boxColRight = box.find('.files-manager-aside');

     var paddings = parseInt($('.page-content').css('padding-top')) +
     parseInt($('.page-content').css('padding-bottom')) +
     parseInt(box.css('margin-bottom')) + 2;

     boxColLeft.height('auto');
     boxCont.height('auto');
     boxColRight.height('auto');

     if ( boxColLeft.height() <= ($(window).height() - paddings) ) {
     boxColLeft.height(
     $(window).height() - paddings
     );
     }

     if ( boxColRight.height() <= ($(window).height() - paddings - boxSubHeader.outerHeight()) ) {
     boxColRight.height(
     $(window).height() -
     paddings -
     boxSubHeader.outerHeight()
     );
     }

     boxCont.height(
     boxColRight.height()
     );
     });
     }

     fileManagerHeight();

     $(window).resize(function(){
     fileManagerHeight();
     });*/

    /* ==========================================================================
     Mail
     ========================================================================== */

    /*function mailBoxHeight() {
     $('.mail-box').each(function(){
     var box = $(this),
     boxHeader = box.find('.mail-box-header'),
     boxColLeft = box.find('.mail-box-list'),
     boxSubHeader = box.find('.mail-box-work-area-header'),
     boxColRight = box.find('.mail-box-work-area-cont');

     boxColLeft.height(
     $(window).height() -
     parseInt($('.page-content').css('padding-top')) -
     parseInt($('.page-content').css('padding-bottom')) -
     parseInt(box.css('margin-bottom')) - 2 -
     boxHeader.outerHeight()
     );

     boxColRight.height(
     $(window).height() -
     parseInt($('.page-content').css('padding-top')) -
     parseInt($('.page-content').css('padding-bottom')) -
     parseInt(box.css('margin-bottom')) - 2 -
     boxHeader.outerHeight() -
     boxSubHeader.outerHeight()
     );
     });
     }

     mailBoxHeight();

     $(window).resize(function(){
     mailBoxHeight();
     });*/

    /* ==========================================================================
     Nestable
     ========================================================================== */

    /*$('.dd-handle').hover(function(){
     $(this).prev('button').addClass('hover');
     $(this).prev('button').prev('button').addClass('hover');
     }, function(){
     $(this).prev('button').removeClass('hover');
     $(this).prev('button').prev('button').removeClass('hover');
     });*/

    /* ==========================================================================
     Widget weather slider
     ========================================================================== */

    /*$('.widget-weather-slider').slick({
     arrows: false,
     dots: true,
     infinite: false,
     slidesToShow: 4,
     slidesToScroll: 4
     });*/

    /* ==========================================================================
     Addl side menu
     ========================================================================== */

    setTimeout(function () {
        if (!("ontouchstart" in document.documentElement)) {
            $('.side-menu-addl').jScrollPane(jScrollOptions);
        }
    }, 1000);

    /* ==========================================================================
     Widget chart combo
     ========================================================================== */

    $('.widget-chart-combo-content-in, .widget-chart-combo-side').matchHeight();


    /* ==========================================================================
     Header notifications
     ========================================================================== */

    // Tabs hack
    $('.dropdown-menu-messages a[data-toggle="tab"]').click(function (e) {
        e.stopPropagation();
        e.preventDefault();
        $(this).tab('show');

        // Scroll
        if (!("ontouchstart" in document.documentElement)) {
            jspMessNotif = $('.dropdown-notification.messages .tab-pane.active').jScrollPane(jScrollOptions).data().jsp;
        }
    });

    // Scroll
    var jspMessNotif,
        jspNotif;

    $('.dropdown-notification.messages').on('show.bs.dropdown', function () {
        if (!("ontouchstart" in document.documentElement)) {
            jspMessNotif = $('.dropdown-notification.messages .tab-pane.active').jScrollPane(jScrollOptions).data().jsp;
        }
    });

    $('.dropdown-notification.messages').on('hide.bs.dropdown', function () {
        if (!("ontouchstart" in document.documentElement)) {
            jspMessNotif.destroy();
        }
    });

    $('.dropdown-notification.notif').on('show.bs.dropdown', function () {
        if (!("ontouchstart" in document.documentElement)) {
            jspNotif = $('.dropdown-notification.notif .dropdown-menu-notif-list').jScrollPane(jScrollOptions).data().jsp;
        }
    });

    $('.dropdown-notification.notif').on('hide.bs.dropdown', function () {
        if (!("ontouchstart" in document.documentElement)) {
            jspNotif.destroy();
        }
    });

    /* ==========================================================================
     Steps progress
     ========================================================================== */

    function stepsProgresMarkup() {
        $('.steps-icon-progress').each(function () {
            var parent = $(this),
                cont = parent.find('ul'),
                padding = 0,
                padLeft = (parent.find('li:first-child').width() - parent.find('li:first-child .caption').width()) / 2,
                padRight = (parent.find('li:last-child').width() - parent.find('li:last-child .caption').width()) / 2;

            padding = padLeft;

            if (padLeft > padRight) padding = padRight;

            cont.css({
                marginLeft: -padding,
                marginRight: -padding
            });
        });
    }

    stepsProgresMarkup();

    $(window).resize(function () {
        stepsProgresMarkup();
    });

    /* ========================================================================== */

    $('.control-panel-toggle').on('click', function () {
        var self = $(this);

        if (self.hasClass('open')) {
            self.removeClass('open');
            $('.control-panel').removeClass('open');
        } else {
            self.addClass('open');
            $('.control-panel').addClass('open');
        }
    });

    $('.control-item-header .icon-toggle, .control-item-header .text').on('click', function () {
        var content = $(this).closest('li').find('.control-item-content');

        if (content.hasClass('open')) {
            content.removeClass('open');
        } else {
            $('.control-item-content.open').removeClass('open');
            content.addClass('open');
        }
    });

    $('.date-mask-input').mask("00/00/0000", {placeholder: "__/__/____"});
    $('.time-mask-input').mask('00:00', {placeholder: "__:__"});
    $('.date-and-time-mask-input').mask('00/00/0000 00:00:00', {placeholder: "__/__/____ __:__:__"});
    $(".timedropper").timeDropper({
        format: 'HH:mm',
        mousewheel: true,
        setCurrentTime: false
    });
    //$('#zip-code-mask-input').mask('00000-000', {placeholder: "_____-___"});
    $('.input-money').mask('000000000000000.00', {reverse: true}, {placeholder: "0.00"});
    $('.input-float').mask("000000000000000.00", {reverse: true});
    $('.input-number').mask('000000', {
        translation: {
            'Z': {
                pattern: /[0-9]/, optional: true
            }
        }
    });
    $('.input-percent').mask('##0,00%', {reverse: true}, {placeholder: "0,00%"});
    $('.input-letter').mask('ZZZZ', {placeholder:'____', translation:  {'Z': {pattern: /[A-Za-z]/, optional: true}}});
    //$('#phone-mask-input').mask('0000-0000', {placeholder: "____-____"});
    //$('#phone-with-code-area-mask-input').mask('(00) 0000-0000', {placeholder: "(__) ____-____"});
    //$('#us-phone-mask-input').mask('(000) 000-0000', {placeholder: "(___) ___-____"});
    //$('#ip-address-mask-input').mask('099.099.099.099');
    //$('#mixed-mask-input').mask('AAA 000-S0S');

    var $service_id = $(".service_aware");
    $service_id.change(function (e) {
        var $active_date = $("#active_date");
        var id = parseInt($service_id.val());
        if(id > 0){
            $.get('/admin/services_activation/fetch/' + id, function(response){
                console.log(response);
                if(response && response.success){
                    $active_date.val(response.obj.date);
                    if(Ristomix.user.role == 'user'){
                        var $targets = $('.date_target');
                        $targets.each(function(i,el){
                            $targets.eq(i).data("DateTimePicker").minDate(response.obj.date);
                        });
                    }
                }
            },'json');
        }else{
            $active_date.val('');
        }
    });

    App.init();

    //notification
    PNotify.prototype.options.styling = "bootstrap3";
    var $countNotification = $("#countNotification");
    var $shouldUpdate = $("#shouldUpdate");
    var checkUrl = '/admin/ajax/check', loadUrl = '/admin/ajax/load';
    if ($countNotification.length) {
        return;
        window.setInterval(function () {
            $.get(checkUrl, {count: parseInt($countNotification.text())}, function (response) {
                //console.log(response)
                if (response.data.notify) {
                    $countNotification.text(response.data.count);
                    AppAudio.play('bell_ring');
                    $.each(response.data.notifications, function (i, el) {
                        new PNotify({
                            title: el.title,
                            text: el.message,
                            width: "400px",
                            buttons: {
                                closer: true
                            },
                            addclass: 'info-with-icon'
                        });
                    });
                    try {
                        if ($shouldUpdate.length) {
                            $.get(loadUrl, {type: $shouldUpdate.data('type'), name: $shouldUpdate.data('name')}, function (response) {
                                if (response.success) {
                                    $shouldUpdate.html(response.html);
                                    setupDashboard();
                                }
                            }, 'json');
                        }
                        window.$dataTable.ajax.reload();
                    } catch (e) {

                    }
                }
            }, 'json');
        }, 3000);
    }

});