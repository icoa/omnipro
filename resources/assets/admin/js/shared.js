/**
 * Created by f.politi on 16/05/2016.
 */
var datatables_i18n = [];
datatables_i18n['it'] = {
    "sEmptyTable": "Nessun dato presente nella tabella",
    "sInfo": "Vista da _START_ a _END_ di _TOTAL_ elementi",
    "sInfoEmpty": "Vista da 0 a 0 di 0 elementi",
    "sInfoFiltered": "(filtrati da _MAX_ elementi totali)",
    "sInfoPostFix": "",
    "sInfoThousands": ".",
    "sLengthMenu": "Visualizza _MENU_ elementi",
    "sLoadingRecords": "Caricamento...",
    "sProcessing": "Elaborazione...",
    "sSearch": "Cerca:",
    "sZeroRecords": "La ricerca non ha portato alcun risultato.",
    "oPaginate": {
        "sFirst": "Inizio",
        "sPrevious": "Precedente",
        "sNext": "Successivo",
        "sLast": "Fine"
    },
    "buttons": {
        "colvis": "Visibilità colonne"
    },
    "oAria": {
        "sSortAscending": ": attiva per ordinare la colonna in ordine crescente",
        "sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
    }
};
function open_window(url, width, height) {
    var my_window;

    // screen.width means Desktop Width
    // screen.height means Desktop Height

    var center_left = (screen.width / 2) - (width / 2);
    var center_top = (screen.height / 2) - (height / 2);

    my_window = window.open(url, "Title", "scrollbars=1, width=" + width + ", height=" + height + ", left=" + center_left + ", top=" + center_top);
    my_window.focus();
}
Vue.component('component-toolbar', {
    props: ['back'],
    data: {
        task: 'back'
    },
    methods: {
        test: function () {
            alert("Hi!");
        },
        submit: function (action) {
            if (action == 'back') {
                window.location.href = this.back;
                return;
            }
            this.task = action;
            $("input[name=task]").val(action);
            $("#adminForm")[0].submit();
        },
        popup: function (url) {
            open_window(url, 1024, 768);
        },
        route: function(url){
            window.location.href = url;
        }
    }
});
new Vue({
    el: '#app'
});
var camelCase = (function () {
    var DEFAULT_REGEX = /[-_]+(.)?/g;

    function toUpper(match, group1) {
        return group1 ? group1.toUpperCase() : '';
    }

    return function (str, delimiters) {
        return str.replace(delimiters ? new RegExp('[' + delimiters + ']+(.)?', 'g') : DEFAULT_REGEX, toUpper);
    };
})();

var YadcfHelper = {
    columns: [],
    init: function (columns) {
        this.columns = columns;
    },
    getIndex: function (key) {
        var that = this;
        var cursor = 0;
        $(that.columns).each(function (index, obj) {
            if (obj.data == key) {
                cursor = index;
            }
        });
        return cursor;
    },
    getDefinitions: function () {
        var that = this;
        var yadcfColumns = [], _yadcfModel;
        $(that.columns).each(function (index, data) {

            if (data.yadcf != null) {
                _yadcfModel = {};

                if (data.yadcf == 'default') {
                    _yadcfModel = {
                        filter_type: "text"
                    };
                }

                if (data.yadcf == 'exact') {
                    _yadcfModel = {
                        filter_match_mode: "exact",
                        filter_type: "text"
                    };
                }

                if (data.yadcf == 'date') {
                    _yadcfModel = {
                        filter_type: "date",
                        filter_match_mode: "exact",
                        date_format: 'YYYY-MM-DD',
                        datepicker_type: 'bootstrap-datetimepicker'
                    };
                }

                if (data.yadcf == 'boolean') {
                    _yadcfModel = {
                        filter_type: "select",
                        filter_match_mode: "exact",
                        data: [{value: 1, label: 'Sì'}, {value: 0, label: 'No'}]
                    };
                }

                if (typeof data.yadcf == 'object') {
                    _yadcfModel = data.yadcf;
                }

                _yadcfModel.column_number = index;

                yadcfColumns.push(_yadcfModel);
            }

        });

        return yadcfColumns;
    }
};