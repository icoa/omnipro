var myApp = new Framework7({
    modalTitle: 'OmniPro',
    modalButtonCancel: Ristomix.lexicon.cancel,
    // Enable Material theme
    material: true,
    cache: true,
    ajaxLinks: '.ajax',
    swipeout: false,
    pushState: true,
    //swipePanel: 'left',
    animatePages: false,
    uniqueHistory: true,
    preloadPreviousPage: false,
    materialRipple: false
});

// Expose Internal DOM library
var $$ = Dom7;


// Add main view
var mainView = myApp.addView('.view-main', {});

var $$f7dom = $$(document);

// Show/hide preloader for remote ajax loaded pages
// Probably should be removed on a production/local app
/*$$f7dom.on('ajaxStart', function (e) {
 myApp.showIndicator();
 }).on('ajaxComplete', function () {
 myApp.hideIndicator();
 })*/
$$f7dom.on('pageInit', function (e) {
    // Page Data contains all required information about loaded and initialized page
    var page = e.detail.page;
    App.setPage(page);
    console.log("pageInit: %o", page);
    console.log("currentPage: ", App.getPage().name);
    switch (page.name) {

        case 'services.create':
        case 'services.edit':
            Service.init(page);
            break;

        case 'services.index':
            ServiceList.init(page);
            break;

        case 'services.barcode':
            ServiceBarcode.init(page);
            break;
    }
}).on('pageBack', function (e) {
    // Page Data contains all required information about loaded and initialized page
    var page = e.detail.page;
    console.log("pageBack: %o", page);
    console.log("currentPage: ", App.getPage().name);
    App.setPage(page);
}).on('pageAfterBack', function (e) {
    // Page Data contains all required information about loaded and initialized page
    var page = e.detail.page;
    console.log("onPageAfterBack: %o", page);
});

$(document).ready(function () {
    App.init();
    App.plugins();
});