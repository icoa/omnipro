(function ($) {
    $.extend({
        websocketSettings: {
            open: function () {
                alert();
            }, close: function () {
            }, message: function () {
            }, error: function () {
            }, options: {}, events: {}
        }, websocket: function (url, s) {
            var ws = WebSocket ? new WebSocket(url) : {
                send: function (m) {
                    return false
                }, close: function () {
                }
            };
            $.websocketSettings = $.extend($.websocketSettings, s);
            $(ws).bind('open', $.websocketSettings.open).bind('close', $.websocketSettings.close).bind('message', $.websocketSettings.message).bind('message', function (e) {
                var m = $.parseJSON(e.originalEvent.data);
                var h = $.websocketSettings.events[m.type];
                if (h)h.call(this, m);
            });
            ws._send = ws.send;
            ws.send = function (type, data) {
                var m = {type: type};
                m = $.extend(true, m, $.extend(true, {}, $.websocketSettings.options, m));
                if (data)m['data'] = data;
                return this._send(JSON.stringify(m));
            }
            ws.getStatus = function(){
                switch (ws.readyState) {
                    case WebSocket.CONNECTING:
                        return 'connecting';
                        break;
                    case WebSocket.OPEN:
                        return 'open';
                        break;
                    case WebSocket.CLOSING:
                        return 'closing';
                        break;
                    case WebSocket.CLOSED:
                        return 'closed';
                        break;
                    default:
                        // this never happens
                        break;
                }
                return 'unknown';
            }
            ws.addEventListener("error", function(event) {
                // handle error event
                $.websocketSettings.error(event);
            });
            $(window).unload(function () {
                ws.close();
                ws = null
            });
            return ws;
        }
    });
})(jQuery);