/**
 * Created by Nemesys on 19/09/2015.
 */
var camelCase = (function () {
    var DEFAULT_REGEX = /[-_]+(.)?/g;

    function toUpper(match, group1) {
        return group1 ? group1.toUpperCase() : '';
    }
    return function (str, delimiters) {
        return str.replace(delimiters ? new RegExp('[' + delimiters + ']+(.)?', 'g') : DEFAULT_REGEX, toUpper);
    };
})();

$('body').delegate('input, textarea', 'focusin', function(event) {
    if(navigator.userAgent.indexOf('Android') > -1){
        var scroll = $(this).offset().top;
        window.scrollTo(500, scroll);
    }
});