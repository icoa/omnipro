var Service = {

    $page: null,
    signaturePad:null,

    init: function ($page) {
        this.$page = $page;
        this.vue();

        var wrapper = document.getElementById("signature_pad"),
            canvas = wrapper.querySelector("canvas"),
            signaturePad;

        signaturePad = new SignaturePad(canvas);
        canvas.width = window.innerWidth - 40;
        this.signaturePad = signaturePad;
        return;

        clearButton.addEventListener("click", function (event) {
            signaturePad.clear();
        });

        saveButton.addEventListener("click", function (event) {
            if (signaturePad.isEmpty()) {
                alert("Please provide signature first.");
            } else {
                window.open(signaturePad.toDataURL());
            }
        });
    },

    trigger: function (event, data) {
        var that = this;
        var $page = App.getPage();
        var method = 'on' + event;
        if ($page.name == 'services.create' || $page.name == 'services.edit') {
            try {
                that[method](data);
            } catch (e) {
            }
        }
    },

    onServiceWasCreated: function (obj) {
        var that = this;
        console.log('catched event: onServiceWasCreated');

        App.notify("Creato nuovo tavolo: " + obj.fullname);
        that.reload();
    },

    onServiceWasUpdated: function (obj) {
        var that = this;
        console.log('catched event: onServiceWasUpdated');

        App.notify("Modificato tavolo: " + obj.fullname);
        that.reload();
    },

    onServiceWasClosed: function (obj) {
        var that = this;
        console.log('catched event: onServiceWasClosed');
        //App.notify("Chiuso tavolo: " + obj.fullname);
        var $page = App.getPage();
        if ($page.name == 'services.edit') {
            App.goBack($page);
            return;
        }
        if ($page.name == 'services.create') {
            that.reload();
        }
    },

    vue: function () {

        new Vue({
            el: '#serviceComponent',
            data: {
                tables: []
            },
            ready: function () {
                console.log('Service.ready');
                Vue.http.headers.common['X-CSRF-TOKEN'] = $("input[name=_token]").val();
            },
            methods: {
                submit: function (event) {
                    event.preventDefault();
                    this.$els.submit.click();
                },
                reset: function (event) {
                    event.preventDefault();
                    if (!Service.signaturePad.isEmpty()) {
                        myApp.confirm("Vuoi resettare la firma?", 'Conferma azione', function () {
                            Service.signaturePad.clear();
                        });
                    }
                },
                confirm: function (event) {
                    event.preventDefault();
                    if (Service.signaturePad.isEmpty()) {
                        myApp.alert('La firma è obbligatoria!');
                    } else {
                        $("#signature").val(Service.signaturePad.toDataURL());
                        this.$els.submit.click();
                    }
                },
                submitForm: function (event) {
                    event.preventDefault();
                    var that = this;
                    that.$els.submit.disabled = true;
                    window.setTimeout(function () {
                        that.$els.submit.disabled = false;
                    }, 500);
                    var $action = $('input[name=action]');
                    var $form = $('#mainForm');
                    var $step1 = $('#step1'), $step2 = $("#step2");

                    function submit() {
                        that.$http.post($form.attr('action'), new FormData($form[0])).then(function (response) {
                            if (response.status == 200) {
                                App.notify(response.body.message);
                                App.goBack(Service.$page);
                            }
                        }, function (response) {
                        });
                    }

                    if ($action.val() == 'take') {
                        myApp.confirm("Confermi di voler prendere in carico questa consegna?", 'Conferma azione', function () {
                            submit();
                        });
                    }

                    if ($action.val() == 'deliver') {
                        if($step2.hasClass('hidden')){
                            $step1.addClass('hidden');
                            $step2.removeClass('hidden');
                        }else{
                            myApp.confirm("Confermi di segnare come COMPLETA questa consegna?", 'Conferma azione', function () {
                                submit();
                            });
                        }
                    }

                    if ($action.val() == 'revert') {
                        myApp.confirm("Attenzione: stai per rimuovere lo status di CONSEGNA COMPLETATA da questo servizio. Sei sicuro di voler procedere?", 'Conferma azione', function () {
                            submit();
                        });
                    }

                    if ($action.val() == 'reown') {
                        myApp.confirm("Attenzione: stai per rimuovere l'autista corrente da questo servizio, che verrà assegnato a te. Sei sicuro di voler procedere?", 'Conferma azione', function () {
                            submit();
                        });
                    }


                }
            }
        });
    },

    reload: function () {
        mainView.router.refreshPage();
    }
};