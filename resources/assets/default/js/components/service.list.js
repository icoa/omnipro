var ServiceList = {

    $page: null,

    init: function ($page) {
        this.$page = $page;
        console.log('ServiceList.init');
    },

    trigger: function (event, data) {
        var that = this;
        var $page = App.getPage();
        var method = 'on' + event;
        if ($page.name == 'services.index') {
            try {
                that[method](data);
            } catch (e) {
            }
        }
    },

    onServiceWasCreated: function (obj) {
        var that = this;
        console.log('catched event: onServiceWasCreated');

        App.notify("Creato nuovo tavolo: " + obj.fullname);
        that.reload();
    },

    onServiceWasClosed: function (obj) {
        var that = this;
        console.log('catched event: onServiceWasClosed');

        //App.notify("Tavolo chiuso: " + obj.fullname);
        that.reload();
    },

    onServiceWasUpdated: function (obj) {
        var that = this;
        console.log('catched event: onServiceWasCreated');

        App.notify("Modificato tavolo: " + obj.fullname);
        that.reload();
    },

    onServiceWasCompleted: function (obj) {
        var that = this;
        console.log('catched event: onServiceWasCompleted');

        App.notify("Completato tavolo: " + obj.fullname);
        that.reload();
    },

    onServiceWasAccepted: function (obj) {
        var that = this;
        console.log('catched event: onServiceWasAccepted');

        //App.notify("Completato tavolo: " + obj.name);
        that.reload();
    },

    onOrderWasCreated: function (obj) {
        var that = this;
        console.log('catched event: onOrderWasCreated');

        App.notify("Nuova comanda per "+obj.service.fullname);
        that.reload();
    },

    reload: function () {
        mainView.router.refreshPage();
    }
};