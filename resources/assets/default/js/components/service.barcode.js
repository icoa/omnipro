var ServiceBarcode = {

    $page: null,

    init: function ($page) {
        this.$page = $page;
        this.vue();
    },

    trigger: function (event, data) {
        var that = this;
        var $page = App.getPage();
        var method = 'on' + event;
        if ($page.name == 'services.checkout') {
            try {
                that[method](data);
            } catch (e) {
            }
        }
    },

    vue: function () {
        new Vue({
            el: '#barcodeServiceComponent',
            data: {
                code: null
            },
            ready: function () {
                console.log('ServiceBarcode.ready');
                $("#code")[0].focus();
            },
            methods: {
                keyDown: function (event) {
                    //console.log('keyDown');
                },
                submit: function (event) {
                    event.preventDefault();
                    this.submitForm();
                },
                reset: function (event) {
                    event.preventDefault();
                    this.code = null;
                    $("#code")[0].focus();
                },
                submitForm: function (event) {
                    event.preventDefault();
                    if(this.code == ''){
                        myApp.alert("Nessun codice da ricercare");
                        return;
                    }
                    var that = this;
                    that.$els.submit.disabled = true;
                    window.setTimeout(function () {
                        that.$els.submit.disabled = false;
                    }, 500);

                    var $form = $('#barcodeForm'), $results = $("#results");

                    this.$http.post($form.attr('action'), new FormData($form[0])).then(function (response) {
                        if (response.status == 200) {
                            console.log(response);
                            if(response.body.success){
                                App.notify('Sono stati trovati '+response.body.count+' risultati');
                                $results.html(response.body.html);
                            }else{
                                App.notify('Non sono stati trovati risultati');
                            }
                        }
                    }, function (response) {
                    });

                }
            }
        });
    },

    reload: function () {
        mainView.router.refreshPage();
    }
};