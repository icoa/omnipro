var AppAudio = {
    path: Ristomix.config.url + "/sound/",
    sounds: ['bell_ring', 'button_tiny', 'tap'],
    soundsDom: [],
    init: function () {
        var that = this;
        $(this.sounds).each(function (i, name) {
            var $audio = $('<audio></audio>');
            $audio.append('<source src="' + that.path + name + '.mp3"></source>');
            $audio.append('<source src="' + that.path + name + '.aac"></source>');
            that.soundsDom[name] = $audio;
        });
    },
    play: function (name) {
        var that = this;
        that.soundsDom[name][0].play();
    }
};