var App = {
    $page: null,
    plugins: function () {
        //AppAudio.init();
    },
    setPage: function($page){
      App.$page = $page;
    },
    getPage: function(){
        return mainView.activePage;
        console.log(mainView)
        var $page = $$('.view-main');
        return {name: mainView.data('page')};
        //return App.$page;
    },
    init: function () {

        function deferMethod($el) {
            var action = $el.data('action');
            if (action == '' || action == undefined || action == null) {
                action = $el.attr('rel');
            }
            var method = camelCase(action);
            if (typeof App[method] == 'function') {
                App[method]($el);
            } else {
                alert("Method: " + method + " not found!");
            }
        }

        $("body").delegate(".app-action", "click", function (e) {
            e.preventDefault();
            e.stopPropagation();
            var $el = $(this);
            deferMethod($el);
        }).delegate(".app-option", "change", function (e) {
            var $el = $(this);
            deferMethod($el);
        });

        /*if(Ristomix.user){
            connect();
            //check connection status
            window.setInterval(function () {
                if (window.ws) {
                    var status = window.ws.getStatus();
                    if (status == 'closed') {
                        window.connect();
                    }
                }
            }, 5000);
        }*/

    },
    logout: function (e) {
        myApp.confirm(Ristomix.lexicon.confirm_logout, function () {
            window.location = '/auth/logout';
        });
    },

    notify: function (message) {
        myApp.addNotification({
            hold: 3000,
            message: message,
            button: {
                text: Ristomix.lexicon.close,
                color: 'yellow'
            }
        });
    },

    notifyPersistent: function (message) {
        myApp.addNotification({
            message: message,
            button: {
                text: Ristomix.lexicon.close,
                color: 'yellow'
            }
        });
    },

    goBack: function ($page) {
        var params = {};
        if ($page.query && $page.query.reload) {
            params.url = mainView.history[mainView.history.length - 2];
            params.ignoreCache = true;
            params.force = true;
        }
        console.log('App.goBack');
        console.log(params);
        mainView.router.back(params);
    },

    _csrf: function () {
        Vue.http.headers.common['X-CSRF-TOKEN'] = $("input[name=_token]").val();
    },

    currentPage: function($el){
        alert(App.getPage().name);
    }
};



window.connect = function () {
    var connection = Ristomix.config.ws;
    console.log('Connecting to ' + connection);
    window.ws = $.websocket(connection, {
        open: function () {
            console.log('connection is open');
            ws.send("register", Ristomix.user);
        },
        close: function () {
            console.log('close');
        },
        error: function (event) {
            console.log('error');
            console.log(event);
        },
        events: {
            ServiceWasClosed: function (e) {
                var elem = e.data;
                console.log('events.ServiceWasClosed');
                console.log(elem);
                if(elem.emitter_id == Ristomix.user.id){
                    console.log('Event skipped for same origin');
                    return;
                }
                App.notifyPersistent("Tavolo chiuso: "+elem.fullname);
                //if the page is service.list, refresh the list
                ServiceList.trigger('ServiceWasClosed',elem);
                //if the page is order.choose, refresh the list
                OrderChoose.trigger('ServiceWasClosed',elem);
                //if the page is service.edit, and the service closed is the current service, return back to previous page with message
                //if the page is service.create, add the tables
                //if the page is service.edit, and the service closes is NOT the current service, add the tables
                Service.trigger('ServiceWasClosed',elem);
                //if the page is order.create, reload the services
                Order.trigger('ServiceWasClosed',elem);
                //if the page is service.close, refresh the list
                ServiceClose.trigger('ServiceWasClosed',elem);
                //if the page is service.checkout, refresh the list
                ServiceCheckout.trigger('ServiceWasClosed',elem);
            },
            ServiceWasCreated: function (e) {
                var elem = e.data;
                console.log('events.ServiceWasCreated');
                console.log(elem);
                if(elem.emitter_id == Ristomix.user.id){
                    console.log('Event skipped for same origin');
                    return;
                }
                //if the page is service.list, refresh the list
                ServiceList.trigger('ServiceWasCreated',elem);
                //if the page is order.choose, refresh the list
                OrderChoose.trigger('ServiceWasCreated',elem);
                //if the page is service.create, remove the tables
                Service.trigger('ServiceWasCreated',elem);
                //if the page is service.edit, remove the tables
                Service.trigger('ServiceWasCreated',elem);
            },
            ServiceWasUpdated: function (e) {
                var elem = e.data;
                console.log('events.ServiceWasUpdated');
                console.log(elem);
                if(elem.emitter_id == Ristomix.user.id){
                    console.log('Event skipped for same origin');
                    return;
                }
                //if the page is service.list, refresh the list
                ServiceList.trigger('ServiceWasUpdated',elem);
                //if the page is order.choose, refresh the list
                OrderChoose.trigger('ServiceWasUpdated',elem);
                //if the page is service.create, remove the tables
                Service.trigger('ServiceWasUpdated',elem);
                //if the page is service.edit, remove the tables
                Service.trigger('ServiceWasUpdated',elem);
            },
            ServiceWasCheckedOut: function (e) {
                var elem = e.data;
                console.log('events.ServiceWasCheckedOut');
                console.log(elem);
                if(elem.emitter_id == Ristomix.user.id){
                    console.log('Event skipped for same origin');
                    return;
                }
                //if the page is service.list, refresh the list
                ServiceList.trigger('ServiceWasCheckedOut',elem);
                //if the page is order.choose, refresh the list
                OrderChoose.trigger('ServiceWasCheckedOut',elem);
                //if the page is service.checkout, refresh the list
                ServiceCheckout.trigger('ServiceWasCheckedOut',elem);
            },
            OrderWasCreated: function (e) {
                var elem = e.data;
                console.log('events.OrderWasCreated');
                console.log(elem);
                if(elem.emitter_id == Ristomix.user.id){
                    console.log('Event skipped for same origin');
                    return;
                }
                //if the page is service.list, refresh the list
                ServiceList.trigger('OrderWasCreated',elem);
                //if the page is order.choose, refresh the list
                OrderChoose.trigger('OrderWasCreated',elem);
            },
            ServiceWasCompleted: function (e) {
                var elem = e.data;
                console.log('events.ServiceWasCompleted');
                console.log(elem);
                if(elem.emitter_id == Ristomix.user.id){
                    console.log('Event skipped for same origin');
                    return;
                }

                //if the page is service.list, refresh the list
                ServiceList.trigger('ServiceWasCompleted',elem);
                //if the page is order.choose, refresh the list
                OrderChoose.trigger('ServiceWasCompleted',elem);
                //if the page is service.close, refresh the list
                ServiceClose.trigger('ServiceWasCompleted',elem);
                //if the page is service.checkout, refresh the list
                ServiceCheckout.trigger('ServiceWasCompleted',elem);
            },
            ServiceWasAccepted: function (e) {
                var elem = e.data;
                console.log('events.ServiceWasAccepted');
                console.log(elem);
                if(elem.emitter_id == Ristomix.user.id){
                    console.log('Event skipped for same origin');
                    return;
                }

                //if the page is service.list, refresh the list
                ServiceList.trigger('ServiceWasAccepted',elem);
                //if the page is order.choose, refresh the list
                OrderChoose.trigger('ServiceWasAccepted',elem);
                //if the page is service.close, refresh the list
                ServiceClose.trigger('ServiceWasAccepted',elem);
                //if the page is service.checkout, refresh the list
                ServiceCheckout.trigger('ServiceWasAccepted',elem);
            }
        }
    });
};