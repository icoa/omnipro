/**
 * Created by Nemesys on 19/09/2015.
 */
;(function ( $, window, document, undefined ) {

    // undefined is used here as the undefined global
    // variable in ECMAScript 3 and is mutable (i.e. it can
    // be changed by someone else). undefined isn't really
    // being passed in so we can ensure that its value is
    // truly undefined. In ES5, undefined can no longer be
    // modified.

    // window and document are passed through as local
    // variables rather than as globals, because this (slightly)
    // quickens the resolution process and can be more
    // efficiently minified (especially when both are
    // regularly referenced in your plugin).

    // Create the defaults once
    var pluginName = "jMinPlus",
        defaults = {
            propertyName: "value"
        };

    // The actual plugin constructor
    function jMinPlus( element, options ) {
        this.element = element;
        this.$el = $(element);

        // jQuery has an extend method that merges the
        // contents of two or more objects, storing the
        // result in the first object. The first object
        // is generally empty because we don't want to alter
        // the default options for future instances of the plugin
        this.options = $.extend( {}, defaults, options) ;

        this._defaults = defaults;
        this._name = pluginName;
        this.$input = this.$el.find('input');
        this.$minBtn = this.$el.find('.minus');
        this.$addBtn = this.$el.find('.plus');
        this.val = parseInt(this.$input.val());

        this.init();
    }

    jMinPlus.prototype = {

        init: function() {
            // Place initialization logic here
            // You already have access to the DOM element and
            // the options via the instance, e.g. this.element
            // and this.options
            // you can add more functions like the one below and
            // call them like so: this.yourOtherFunction(this.element, this.options).
//            console.log('jMinPlus');
            this.$minBtn.click($.proxy(this.take, this));
            this.$addBtn.click($.proxy(this.add, this));
        },

        getVal: function() {
            return parseInt(this.$input.val());
        },

        setVal: function(v) {
            return this.$input.val(v);
        },

        add: function(){
            this.val++;
            this.setVal( this.val );
        },

        take: function(){
            this.val--;
            if(this.val <= 0){
                this.val = 0;
            }
            this.setVal( this.val );
        }
    };

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName,
                    new jMinPlus( this, options ));
            }
        });
    };

})( jQuery, window, document );