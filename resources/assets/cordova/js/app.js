// Init App
var myApp = new Framework7({
    modalTitle: 'Raspoutin',
    modalButtonCancel: 'Annulla',
    // Enable Material theme
    material: true,
    cache: false,
    ajaxLinks: '.ajax'
});
//swipeOut do not close
var swipeoutStay = true;

// Expose Internal DOM library
var $$ = Dom7;


// Add main view
var mainView = myApp.addView('.view-main', {});

// Show/hide preloader for remote ajax loaded pages
// Probably should be removed on a production/local app
/*$$(document).on('ajaxStart', function (e) {
    myApp.showIndicator();
});
$$(document).on('ajaxComplete', function () {
    myApp.hideIndicator();
});*/
$(document).ready(function () {
    App.init();
    App.plugins();
});


var AppAudio = {
    path: Ristomix.config.url + "/sound/",
    sounds: ['bell_ring', 'button_tiny', 'tap'],
    soundsDom: [],
    init: function () {
        var that = this;
        $(this.sounds).each(function (i, name) {
            var $audio = $('<audio></audio>');
            $audio.append('<source src="' + that.path + name + '.mp3"></source>');
            $audio.append('<source src="' + that.path + name + '.aac"></source>');
            that.soundsDom[name] = $audio;
        });
    },
    play: function (name) {
        var that = this;
        that.soundsDom[name][0].play();
    }
};


var App = {
    plugins: function () {
        //AppAudio.init();
    },
    init: function () {

        function deferMethod($el) {
            var action = $el.data('action');
            if (action == '' || action == undefined || action == null) {
                action = $el.attr('rel');
            }
            var method = camelCase(action);
            if (typeof App[method] == 'function') {
                App[method]($el);
            } else {
                alert("Method: " + method + " not found!");
            }
        }

        $("body").delegate(".app-action", "click", function (e) {
            e.preventDefault();
            var $el = $(this);
            deferMethod($el);
        }).delegate(".app-option", "change", function (e) {
            var $el = $(this);
            deferMethod($el);
        });


        var $page = $(".page");
        if ($page.data('page') != 'login') {
            console.log("Current page is: %s", $page.data('page'));
            switch ($page.data('module')) {
                case 'galley':
                    Galley.init($page);
                    break;
            }
        }

        if(Ristomix.user){
            connect();
            //check connection status
            window.setInterval(function () {
                if (window.ws) {
                    var status = window.ws.getStatus();
                    if (status == 'closed') {
                        window.connect();
                    }
                }
            }, 5000);
        }
    },
    logout: function (e) {
        myApp.confirm('Sei sicuro di eseguire la disconnessione?', function () {
            window.location = '/cordova/auth/logout';
        });
    },
    notify: function (message) {
        myApp.addNotification({
            hold: 3000,
            message: message,
            button: {
                text: Ristomix.lexicon.close,
                color: 'yellow'
            }
        });
    },
    toggleBottle: function(){

    },
    sendOrder: function($el){
        Galley.sendOrder($el);
    }
};


window.connect = function () {
    var connection = Ristomix.config.ws;
    console.log('Connecting to ' + connection);
    window.ws = $.websocket(connection, {
        open: function () {
            console.log('connection is open');
            ws.send("register", Ristomix.user);
        },
        close: function () {
            console.log('close');
        },
        error: function (event) {
            console.log('error');
            console.log(event);
        },
        events: {
            OrderWasCreated: function (e) {
                var elem = e.data;
                console.log('events.newOrder');
                console.log(elem);
                Galley.newOrder(elem.id);
            },
            ServiceWasUpdated: function (e) {
                var elem = e.data;
                console.log('events.ServiceWasUpdated');
                console.log(elem);
                Galley.reload();
            },
            single: function (e) {
                var elem = e.data;
                console.log('events.single');
                console.log(elem);
            }
        }
    });
};


var Galley = {

    $container: null,

    init: function ($page) {
        var that = this;
        that.$container = $("#shouldUpdate");
        console.log('Galley.init');

        $("body").delegate(".swipeout", "open", function (e) {
            var $target = $(this);
            console.log('opened');
            $target.data('done',1);
            Galley.checkCard($target.data('order'));
        }).delegate(".swipeout", "close", function (e) {
            var $target = $(this);
            console.log('closed');
            $target.data('done',0);
            Galley.checkCard($target.data('order'));
        });

        /*$("body").delegate(".swipeout", "deleted", function (e) {
            var $target = $(this);
            $.post($target.data('post'), {}, function (response) {
                console.log(response);
                App.notify('Conferma inviata alla cassa');
                that.checkCard($target.data('order'));
            }, 'json');
        });*/
    },

    checkCard: function (order_id) {
        var $el = $("#order_" + order_id);
        if ($el.length) {
            var $li = $el.find('.swipeout');
            var total = $li.length;
            var done = 0;
            if(total > 0){
                $li.each(function(i,el){
                   if( parseInt($li.eq(i).data('done'))){
                       done++;
                   }
                });
            }
            if(done == total){
                $el.find('button').prop('disabled',false);
            }else{
                $el.find('button').prop('disabled',true);
            }
        }
    },

    newOrder: function(order_id){
        console.log('newOrder '+order_id);
        var that = this;
        that.$container.find('.alert').remove();
        $.get(that.$container.data('route')+'/'+order_id,{},function(response){
            that.$container.append(response.html);
            App.notify(response.message);
        },'json');
    },

    reload: function(){
        var that = this;
        $.get(that.$container.data('reload'),{},function(response){
            that.$container.html(response.html);
            App.notify(response.message);
        },'json');
    },

    sendOrder: function($el){
        var that = this;
        $.post($el.data('post'), {}, function (response) {
            console.log(response);
            App.notify('Conferma inviata alla cassa');
            var $card = $("#order_" + $el.data('order'));
            $card.hide();
        }, 'json');
    }
};