<?php

use App\Service;

return [
    'patient_name' => 'Iniziale nome paziente',
    'patient_lastname' => 'Iniziale cognome paziente',
    'active_date' => 'Data attivazione',
    'product_id' => 'Modello',
    'sdo' => 'SDO',
    'department_id' => 'Reparto',
    'engine_serial' => 'Matricola motore',
    'injuries' => 'Lesioni cutanee',
    'stand_by' => 'Posizionamento stand-by',

    'name' => '',
    'status' => 'Status',
    'type' => 'Servizio',

    'patient_initials' => 'Iniziali paziente',

    'braden' => 'Scala braden',
    'braden_total' => 'Tot. braden',
    'confirmed_by' => 'Confermato da',
    'delivered_by' => 'Completato da',
    'notes' => 'Note aggiuntive',
    'sent_at' => 'Richiesta il',
    'confirmed_at' => 'Confermata il',
    'delivered_at' => 'Completata il',

    'discharge_date' => 'Data dimissione Paziente',
    'disable_date' => 'Data disattivazione',
    'maintenance_date' => 'Data manutenzione',
    'transfer_date' => 'Data trasferimento',
    'service_id' => 'Servizio richiesto',
    'transfer_id' => 'Reparto destinazione',
    'uo' => 'Unità operativa',

    'created_at' => 'Data creazione',
    'created_by' => 'Creato da',
    'event_at' => 'Ultimo evento il',
    'event_by' => 'Ultimo evento da',

    'activation_notes' => 'Note (attivazione)',
    'disactivation_notes' => 'Note (disattivazione)',
    'transfer_notes' => 'Note (trasferimento)',

    'cost_starts_at' => 'Dal',
    'cost_ends_at' => 'Al',
    'days' => 'GG di noleggio',
    'cost' => 'Importo',
    'discount' => 'Scontistica',
    'signature' => 'Firma grafometrica',
    'sent_activation_at' => 'Data e ora richiesta materasso o antidecubito',
    'mt_actions_performed' => 'Azioni eseguite (manutenzione)',
    'parent_id' => 'ID Genitore',

    'statuses' => [
      Service::STATUS_REQUESTED => 'Inviata',
      Service::STATUS_CONFIRMED => 'Approvata',
      Service::STATUS_CREATED => 'Creata',
      Service::STATUS_REJECTED => 'Rifiutata',
      Service::STATUS_SHIPPING => 'In esecuzione',
      Service::STATUS_DELIVERED => 'Completata',
      Service::STATUS_REVERTED => 'Non consegnata',
    ],

    'statuses_actions' => [
        Service::STATUS_REQUESTED => 'richiesto',
        Service::STATUS_CONFIRMED => 'approvato',
        Service::STATUS_CREATED => 'creato',
        Service::STATUS_REJECTED => 'rifiutato',
        Service::STATUS_SHIPPING => 'preso in carico',
        Service::STATUS_DELIVERED => 'completato l\'attività per',
        Service::STATUS_REVERTED => 'rimosso lo status di CONSEGNA COMPLETATA per',
    ],

    'types' => [
        Service::TYPE_ACTIVATION => 'Attivazione',
        Service::TYPE_DISACTIVATION => 'Disattivazione',
        Service::TYPE_TRANSFER => 'Trasferimento',
        Service::TYPE_MAINTENANCE => 'Manutenzione',
    ],

    'alt' => [
        'transfer_date' => 'Data richiesta trasferimento del materasso',
        'disable_date' => 'Data dismissione da parte dell\'U.O.',
        'delivered_at' => 'Data e ora esecuzione attività',
        'maintenance_date' => 'Data e ora richiesta manutenzione',
        'sent_at' => 'Data e ora consegna materasso a seguito di validazione',
        'confirmed_at' => 'Data e ora validazione dec e/o suoi assistenti',
        'active_date' => 'Data richiesta materasso o antidecubito',
    ]
];