<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Roomix general messages
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'services'   => 'servizio|servizi',
    'hello'   => 'Ciao <strong>:name</strong>, cosa vuoi fare oggi?',
    'user_inactive'   => 'Questo utente non è abilitato',
    'user_expired'   => 'Questo utente ha una password scaduta',

];
