<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 25/07/2016
 * Time: 13:45
 */
return [
    'total' => 'N° Contatti',
    'made_by' => 'Effettuato da',
    'source' => 'attraverso',
    'deal_at' => 'Data',
    'differenze' => 'Differenze rispetto ai dati comunicati',
    'sopralluogo' => 'Necessità di effettuare sopralluogo',
    'dettaglio_proposta' => 'Dettaglio Proposta',
    'status_ar' => 'Accettazione/rifiuto',
    'proposte_particolari' => 'Proposte particolari da comunicare a Poste',
    'eventuale_approvazione' => 'Eventuale approvazione da parte di Poste',
    'controproposta' => 'Controproposta/e',
    'eventuale_approvazione_cp' => 'Eventuale approvazione da parte di Poste',
    'supporto_legale' => 'Necessario supporto legale?',
    'note_legale' => 'Note del legale',
    'supporto_tecnico' => 'Necessario supporto tecnico?',
    'note_tecniche' => 'Note tecniche',
    'esito_trattativa' => 'Esito trattativa',
    'note_generali' => 'Note generali',
    'prossima_azione' => 'Prossima azione da effettuare',
    'data_prossima_azione' => 'Data Prossima azione da effettuare',
    'deadline_trattativa' => 'Deadline trattativa',
    'main' => 'Contatto responsabile',
];