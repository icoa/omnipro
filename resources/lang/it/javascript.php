<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Roomix general messages
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'cancel'   => 'Annulla',
    'close'   => 'Chiudi',
    'confirm_logout'   => 'Sei sicuro di eseguire la disconnessione?',
    'fill_zone'   => "Devi specificare la zona in cui ti trovi",
    'fill_area'   => "Devi specificare l'area in cui ti trovi",
    'confirm_request'   => "Per favore conferma la tua richiesta di assistenza",

];
