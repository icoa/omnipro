<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 25/07/2016
 * Time: 13:45
 */
return [
    'total' => 'N° Proprietari',
    'name' => 'Nome completo',
    'firstname' => 'Nome',
    'lastname' => 'Cognome',
    'company' => 'Nome società',
    'rf_cell' => 'Cell. referente',
    'type' => 'Tipo',
    'birthdate' => 'Data di nascita',
    'cf' => 'Codice fiscale',
    'tel' => 'Telefono',
    'cell' => 'Cellulare',
    'email' => 'Email',
    'vat' => 'Partita I.V.A.',
    'rf_name' => 'Referente',
    'rf_tel' => 'Tel. referente',
    'rf_email' => 'Email referente',
    'address' => 'Indirizzo locatore',
    'cap' => 'C.A.P. locatore',
    'state_id' => 'Provincia locatore',
    'city_id' => 'Città locatore',
    'main' => 'Proprietario principale',
    'notes' => 'Note e/o riferimenti',
];