<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Roomix notifications messages
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'demand' => [
        'accepted' => [
            'title' => 'Richiesta accettata',
            'msg' => "Gentile Cliente, la informiamo che la sua richiesta è stata accettata e :name sarà a disposizione da Lei fra pochi minuti",
        ],
        'routed' => [
            'title' => 'Ordine in consegna',
            'msg' => "Gentile Cliente, la informiamo che il suo Ordine è in consegna e :name sarà a disposizione da Lei fra pochi minuti",
        ],
    ],

];
