<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 25/07/2016
 * Time: 13:45
 */
return [
    'name' => 'Nome documento',
    'mime' => 'Tipo',
    'size' => 'Taglia',
    'filename' => 'File',
    'fileinput' => 'File da Uploadare',
];