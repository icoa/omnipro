<?php

return [

    'title'   => 'Welcome to Ristomix',
    'search'   => 'Search',
    'enter' => 'ENTER',
    'rememberme' => 'Remember me',
    'area' => 'Area / Table',
    'zone' => 'Zone / Location',
    'username' => [
        'label' => 'Username',
        'placeholder' => 'Enter your Username',
    ],
    'password' => [
        'label' => 'Password',
        'placeholder' => 'Enter your Password',
    ],
    'room' => 'Room',
    'functions' => 'Main features',
    'hello' => 'Hi <strong>:name</strong>, what can I do for You?',
    'welcome' => 'Please select one of the following features.',
    'languages' => 'Change language',
    'components' => [
        'demands' => [
            'create' => 'Call a Waiter',
            'index' => 'My calls',
            'desc' => "With this function you can request the assistance of a waiter providing only basic information on where you are; as soon as possible you will be notified of the arrival of a waiter, who will take care of You.",
            'button' => 'Get support',
            'message' => "Call succesfully sent",
            'name' => "Request",
            'show' => "Request details",
            'zonearea' => "Zone / Area",
            'date' => "Request date",
            'requestedBy' => "Requested by",
            'acceptedBy' => "Accepted by",
            'acceptedWhen' => "Accepted on",
        ],
        'pages' => [
            'about' => 'Informations',
            'help' => 'Help',
        ],
        'notifications' => [
            'index' => 'My notifications',
            'none' => 'Actually there are no notifications for You',
            'show' => 'Notification details',
            'title' => 'Title',
            'message' => 'Message',
            'date' => 'Date',
            'details' => 'MORE INFORMATIONS',
        ],
    ],
    'placeholders' => [
        'area' => "Select your area...",
        'zone' => "Select your zone...",
    ],
    'status' => [
        'requested' => "Requested",
        'accepted' => "Accepted",
        'working' => "Working",
        'ready' => "Ready",
        'delivering' => "Delivering",
        'closed' => "Closed",
        'assigned' => "Assigned",
    ],
];