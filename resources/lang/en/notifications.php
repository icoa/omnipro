<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Roomix notifications messages
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'demand' => [
        'accepted' => [
            'title' => 'Request accepted',
            'msg' => "Dear Customer, we inform You that your request has been accepted and :name will be at your location in a few minutes",
        ],
        'routed' => [
            'title' => 'Order shipped',
            'msg' => "Dear Customer, we inform You that your Order is going to be delivered and :name will be at your location in a few minutes",
        ],
    ],

];
