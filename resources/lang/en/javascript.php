<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Roomix general messages
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'cancel'   => 'Cancel',
    'close'   => 'Close',
    'confirm_logout'   => 'Are You sure to logout?',
    'fill_zone'   => "You have to provide your current zone",
    'fill_area'   => "You have to provide your current area",
    'confirm_request'   => "Please confirm your request",

];
