<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Roomix general messages
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'services'   => 'service|services',
    'hello'   => 'Hello <strong>:name</strong>, what You want to do today?',
    'user_inactive'   => 'This user is not enabled',
    'user_expired'   => 'This user has an expired password',

];
